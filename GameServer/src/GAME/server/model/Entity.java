package GAME.server.model;

/**
 * Created with IntelliJ IDEA.
 * User: Kevin
 * Date: 11/9/12
 * Time: 1:32 PM
 *
 * Good for iteration purposes
 *
 */
public class Entity {
    //private Game game;

    protected int id;

    protected int mapRegion = 0;
    protected int x;
    protected int y;
    protected int z; // Map region is the building and z is floor? or Consider each floor a different region?

    public int getID() {
        return id;
    }
    public void setID(int newID) {
        id = newID;
    }

    public int[] getLocation() {
        int[] location = {mapRegion, x, y, z};
        return location;
    }
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
    public int getZ() {
        return z;
    }

    /*public Game getGame() {
        return game;
    }*/
}
