package GAME.server;

import GAME.server.netty.ServerPipelineFactory;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;

import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

/**
 * User: Kevin
 * Date: 1:13 PM, 11/9/12
 * Purpose: This is where you start the server.
 */
public class GameServer {
    private final int port;

    public GameServer(int port) {
        this.port = port;
    }

    public void run() {
        ServerBootstrap bootstrap = new ServerBootstrap(new NioServerSocketChannelFactory(
                Executors.newCachedThreadPool(), Executors.newCachedThreadPool()));

        bootstrap.setPipelineFactory(new ServerPipelineFactory());

        bootstrap.bind(new InetSocketAddress(port));

        System.out.println("Web socket server started at port " + port + ".");
    }

    public static void main(String[] args) {
        // TODO code application logic here
        GameServer gs = new GameServer(8080);
        gs.run();
    }
}
