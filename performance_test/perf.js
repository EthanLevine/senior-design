

var instanceTest = function(_instanceid){
	console.log("insance created: "+ _instanceid);

	var instanceid = _instanceid;
	var user = "user" + instanceid;
	
	var that = this;
	var generalCallBack;
	

	generalCallBack = function(msg) {

		console.log("Instance received message: " + instanceid);
	}
	
	var mq = messageQueue.create(server_address, generalCallBack);

	this.signUp = function(){

		var message = ["auth.create-account", user, "password"];
		mq.send(message)
		.onconfirm(function(){
			console.log("account created:  " + user);


		})
		.onerror(function (code, msg) {
			var message = ["auth.authenticate", user, "password"];
			mq.send(message)
			.onconfirm(function(){
				console.log("account logged in:  " + user);
				that.charCreate();

			})

    	});

	};

	this.charCreate = function(){
		var message = ["char.create", user, 1];
		mq.send(message)
		.onconfirm(function(){
			console.log("char created:  " + user);
			that.handleGames();

		})
		.onerror(function(code, msg) {
			if(code===111)
			{
				//character exists.
				var message = ["char.list"];
				mq.send(message)
				.onconfirm(function(msg){
					var message = ["char.select", msg[0][0]];
					mq.send(message)
					.onconfirm(function(){
						console.log("char selected");
						that.handleGames();
					});

				});

			}

		});

	};

	this.handleGames = function(){
		//first need to get a list and see if any games are open.  if so join, else create a game.
		// instance.create <instance name> <instance password> <task id>
		var message = ["instance.create", user,"", 1];
		mq.send(message)
				.onconfirm(function(msg){
					console.log("game created");
				});

		
		setTimeout(function(){
		message = ["player.set-angle",160];
		 mq.send(message);

		var message = ["weapon",2];
	        mq.send(message);

		var message = ["player.start-attack", (new Date()).getTime()];
		mq.send(message)
				.onconfirm(function(msg){
					console.log("start attack");
				});
		}, 1000);


		
		

	};

};


