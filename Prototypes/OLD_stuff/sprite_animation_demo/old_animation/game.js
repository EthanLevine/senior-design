/*
Crafty can splice sprites based on width, height, x & y locations within sprite map. 

	- .sprite(x,y,w,h);  // NOT USED IN THIS DEMO
	- .crop(x,y,w,h);   // FOR EXPLICITLY DEFINING ENTITIES THAT ARE SMALLER AND NEED CROPPING

This method (below) produces COMPONENTS applicable to entities, based on its location in the sprite map.
Entities that add these created sprite components are also given a component 'sprite'

	- Crafty.sprite(Size,URL,sprites{COLUMN_LOCATION,ROW_LOCATION}, padding for x, padding for y)

Based on this demo, animations are binded to keyboard input (if ARROW_KEY is down, play animation), 
by binding .animate() to the appropriate key

Crafy supports sprite animations by (1) defining the animation to be played and (2) starting/stopping the animation

	1.) .animate(NAME OF REEL, start, ROW #, end) //define animation to be from 'start' to 'end'; where they are the location on the map
	2.) .animate(NAME OF REEL, FRAME duration) //play the animation
		.stop() //stop the animation
*/


window.onload = function() {
	//start crafty
	Crafty.init(1000, 400, 320);//(FRAME RATE,WIDTH,HEIGHT OF CANVAS)
	Crafty.canvas();//need for canvas drawing
	
	//THIS METHOD FOR SPLICING SPRITES IS DEPRECATED AND IS NOT SUPPORTED IN THE NEWER VERSION OF CRAFTY
	//turn the sprite map into usable components
	Crafty.sprite(16, "sprite.png", {
		//<NAME>: [COLUMN,ROW]
		player: [0,3]
	});
		
	//the loading screen that will display while our assets load
	Crafty.scene("loading", function() {
		//load takes an array of assets and a callback when complete
		Crafty.load(["sprite.png"], function() {
			Crafty.scene("main"); //when everything is loaded, run the main scene
		});
		
		//black background with some loading text
		Crafty.background("#c0c0c0");
		Crafty.e("2D, DOM, Text").attr({w: 100, h: 20, x: 150, y: 120})
			.text("Loading")
			.css({"text-align": "center"});
	});
	
	//automatically play the loading scene
	Crafty.scene("loading");
	
	Crafty.scene("main", function() {
		Crafty.c('CustomControls', {
			__move: {left: false, right: false, up: false, down: false},	
			_speed: 3,
			
			CustomControls: function(speed) {
				if(speed) this._speed = speed;
				var move = this.__move;
				
				this.bind('enterframe', function() {
					//move the player in a direction depending on the booleans
					//only move the player in one direction at a time (up/down/left/right)
					if(this.isDown("RIGHT_ARROW")) this.x += this._speed; //if right arrow is being down, increment x 
					else if(this.isDown("LEFT_ARROW")) this.x -= this._speed; // GOING LEFT, MEANS x is decreasing
					else if(this.isDown("UP_ARROW")) this.y -= this._speed;
					else if(this.isDown("DOWN_ARROW")) this.y += this._speed;
				});
				return this;
			}
		});
		
		//create our player entity with some premade components
		player = Crafty.e("2D, Canvas, player, Controls, CustomControls, Animate, Collision")
			.attr({x: 0, y: 0, z: 1})
			.CustomControls(1)
			.animate("walk_left", 6, 3, 8)//.animate(NAME OF REEL, FROM COLUMN #, ROW #, UNTIL COLUMN #)
			.animate("walk_right", 9, 3, 11)
			.animate("walk_up", 3,3,5)
			.animate("walk_down", 0, 3, 2)

			.bind("enterframe", function(e) {
				if(this.isDown("LEFT_ARROW")) {				//if pressed
					if(!this.isPlaying("walk_left")){		//if proper animation is not playing
						this.stop()							//stop animation
						.animate("walk_left", 10);			//play appropriate reel based on button pressed
					}
				} else if(this.isDown("RIGHT_ARROW")) {
					if(!this.isPlaying("walk_right")){
						this.stop()
						.animate("walk_right", 5);						
					}
				} else if(this.isDown("UP_ARROW")) {
					if(!this.isPlaying("walk_up")){
						this.stop()
						.animate("walk_up", 10);
					}
				} else if(this.isDown("DOWN_ARROW")) {
					if(!this.isPlaying("walk_down")){
						this.stop()
						.animate("walk_down", 10);
					}
				}
			})
			//WHEN NO BUTTONS ARE PRESSED, STOP ANIMATIONS
			.bind("keyup", function(e) {
				this.stop();
		})
	});
};