window.onload = function () {
    //start crafty
    Crafty.init(1000,700);
    //Crafty.canvas.init();

    //the loading screen that will display while our assets load
    Crafty.scene("loading", function () {
        //load takes an array of assets and a callback when complete
        Crafty.load(["sprite3.png"], function () {
            Crafty.scene("main"); //when everything is loaded, run the main scene
    
    //black background with some loading text
    Crafty.background("#ccc");
    Crafty.e("2D, DOM, Text").attr({ w: 100, h: 20, x: 150, y: 120 })
        .text("Loading")
        .css({ "text-align": "center" });        
        });
    });

    //splice the part that'll be used from the sprite map
    Crafty.sprite(48, "sprite5.png", {
    //<NAME>: [COLUMN,ROW]
    player_stand: [0,0]
    });

    //automatically play the loading scene
    Crafty.scene("loading");

    Crafty.scene("main", function() {
        var degrees = 360; //SET A DEFAULT ANGLE, to fix bug of playing a default animation, keep track of the angle player is in to determine animation of controls

        var player2 = Crafty.e("2D, DOM, Multiway, SpriteAnimation, PlayerSprite, Text, player_stand")
            .attr({x:200,y:200,z:2})
            .animate("walking",0,0,12)  
            .animate('walking',20,-1)



        //create player on map with components
        var player = Crafty.e("2D, DOM, Multiway, SpriteAnimation, PlayerSprite, Text, player_stand")   
//            .text("'\n' Im part of a sprite map")
            .textColor("000000")
            .attr({x:150,y:50,z:2})
            .origin("center")
            .multiway(2, {W: -90, S: 90, D: 0, A: 180})// to use directional controls up,down,left,right

            //set up reels to be animated later
            .animate("walking",0,0,12)
            .animate("walking_left",0,1,2)
            .animate("walking_right",3,1,5)

            //.animate("walking",50,-1) //run the animation
            
            .requires("Keyboard")
            .bind("KeyDown",function(e){
                //if player is facing left of screen within 10 degrees of 90 - (80-100 degrees inclusive)
                //adjust animations and controls accordingly
                if(80<=degrees && degrees<=100 ){
                    if(this.isDown("A")||this.isDown("D")) {             
                        if(!this.isPlaying("walking")){ 
                            this.stop()
                            .animate('walking',20,-1) 
                        }
                    }
                    else if(this.isDown("W")) {             
                        if(!this.isPlaying("walking_left")){
                            this.stop()
                            .animate('walking_left',20,-1) 
                        }
                    }
                    else if(this.isDown("S")) {             
                        if(!this.isPlaying("walking_right")){     
                            this.stop()
                            .animate('walking_right',20,-1)
                        }
                    }
                }
                //if player is facing towards top of screen
                else if(170<=degrees && degrees<=190){
                    if(this.isDown("W")||this.isDown("S")) {             
                        if(!this.isPlaying("walking")){ 
                            this.stop()
                            .animate('walking',20,-1) 
                        }
                    }
                    else if(this.isDown("A")) {             
                        if(!this.isPlaying("walking_right")){
                            this.stop()
                            .animate('walking_right',20,-1) 
                        }
                    }
                    else if(this.isDown("D")) {             
                        if(!this.isPlaying("walking_left")){     
                            this.stop()
                            .animate('walking_left',20,-1)
                        }
                    }
                }
                //if player is facing towards right of screen
                else if(260<=degrees && degrees<=280){
                    if(this.isDown("D")||this.isDown("A")) {             
                        if(!this.isPlaying("walking")){ 
                            this.stop()
                            .animate('walking',20,-1) 
                        }
                    }
                    else if(this.isDown("W")) {             
                        if(!this.isPlaying("walking_right")){
                            this.stop()
                            .animate('walking_right',20,-1) 
                        }
                    }
                    else if(this.isDown("S")) {             
                        if(!this.isPlaying("walking_left")){     
                            this.stop()
                            .animate('walking_left',20,-1)
                        }
                    }
                }
                //if player is facing towards bottom of screen
                else if((350<=degrees && degrees<=360)||(degrees >= 0 && degrees <= 10)){
                    if(this.isDown("W")||this.isDown("S")) {             
                        if(!this.isPlaying("walking")){ 
                            this.stop()
                            .animate('walking',20,-1) 
                        }
                    }
                    else if(this.isDown("D")) {             
                        if(!this.isPlaying("walking_right")){
                            this.stop()
                            .animate('walking_right',20,-1) 
                        }
                    }
                    else if(this.isDown("A")) {             
                        if(!this.isPlaying("walking_left")){     
                            this.stop()
                            .animate('walking_left',20,-1)
                        }
                    }
                }
                //DEFAULT animation for angles not pointed NORTH, SOUTH, EAST, WEST
                else{
                    if(this.isDown("W")||this.isDown("A")||this.isDown("S")||this.isDown("D")) {             
                        if(!this.isPlaying("walking")){ 
                            this.stop()
                            .animate('walking',10,-1) 
                        }
                    }
                }

            })//stop animation when no key is being pressed
           .bind("KeyUp", function(e) {
                this.stop()
            }) 
    
        //incorporate rotation into animation
        Crafty.addEvent(this, "mousemove", function(e) {
            var pos = Crafty.DOM.translate(e.clientX, e.clientY);
                     
            player.centery=player._y+(player._h/2);
            player.centerx=player._x+(player._w/2);
            
            //finds the players center and does rotation based on center point.
            var theta = Math.atan2(pos.y - player.centery, pos.x - player.centerx);
            if (theta < 0){
               theta += 2 * Math.PI;
            }
            
            //keeps the angle under 360 and positive.
            var angle = (theta * 180 / Math.PI) + 90
            if (angle>360)
            {
                angle=angle-360;
            }
            player.rotation=angle;
            degrees = angle;
            //console.log(degrees);
        ;
        });

    });
};


