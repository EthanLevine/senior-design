/*
Keys 1,2,3,4 for the phases.


*/

Crafty.init(800	, 500);
Crafty.background("gray");
bullet_w=40;
bullet_h=40;

var phase1on=false;
var phase2on=false;
var phase3on=false;
var phase4on=false;
var bullet;

//player
var player = Crafty.e("2D, DOM, Image")
.attr({x:300,y:150,w:150,h:150,z:1})
.image("sprite.png", "no-repeat")
.origin("center")
;



 //mouse based rotation
Crafty.addEvent(this, "mousemove", function(e) {
		
		player._centerx=player._x+(player._w/2);
		player._centery=player._y+(player._h/2);
		
		
		var pos = Crafty.DOM.translate(e.clientX, e.clientY);
        var theta = Math.atan2(pos.y - ( player._centery), pos.x - (player._centerx));
        if (theta < 0){
           theta += 2 * Math.PI;
        }
        player.angle2=(theta * 180 / Math.PI)
        var angle = (theta * 180 / Math.PI) + 90
        if (angle>360)
        {
                angle=angle-360;
        }
       
		player.rotation=angle;
	

});

Crafty.addEvent(this, "keydown", function(e){
		1
		//turns sprite image on/off
        if(e.key === Crafty.keys['1']) {
			phase1(player);
		}
		else if(e.key === Crafty.keys['2']) {
			phase2(player);
		}
		else if(e.key === Crafty.keys['3']) {
			phase3(player);
		}
		else if(e.key === Crafty.keys['4']) {
			phase4(player);
		}

		
	});
	
function phase1(entity)
{
	if(!phase1on)
	{	
		phase1on=true;
		bullet = Crafty.e("2D, DOM, Color")
		.attr({x:player._x ,y:player._y , w:bullet_w, h:bullet_h, z:2})
		.color("red");
		
		
	
	}
	else
	{	
		phase1on=false;
		bullet.destroy();
	
	}
	


}



function phase2(entity)
{
	if(!phase2on)
	{	
		phase2on=true;
		bullet = Crafty.e("2D, DOM, Color")
		.attr({x:player._centerx ,y:player._centery , w:bullet_w, h:bullet_h, z:2})
		
		.color("red");
		
		bullet.rotation=player._rotation;
	
	}
	else
	{	
		phase2on=false;
		bullet.destroy();
	
	}
	


}




function phase3(entity)
{
	if(!phase3on)
	{	
		bulletx=entity._centerx+Math.round(100*Math.cos((entity.rotation-90)*Math.PI/180));
        bullety=entity._centery+Math.round(100*Math.sin((entity.rotation-90)*Math.PI/180));
        
        
      
		
		phase3on=true;
		bullet = Crafty.e("2D, DOM, Color")
		.attr({x:bulletx ,y:bullety , w:bullet_w, h:bullet_h, z:2})
		
		.color("red");
		
		bullet.rotation=player._rotation;

	
	}
	else
	{	
		phase3on=false;
		bullet.destroy();
	
	}
	


}


function phase4(entity)
{
	if(!phase4on)
	{	
		bulletx=entity._centerx+Math.round(100*Math.cos((entity.rotation-90)*Math.PI/180));
        bullety=entity._centery+Math.round(100*Math.sin((entity.rotation-90)*Math.PI/180));
        
        offsetx=Math.round((bullet_w/2)*Math.sin((90-entity.rotation)*Math.PI/180));
        offsety=Math.round((bullet_w/2)*Math.cos((90-entity.rotation)*Math.PI/180));
        
        bulletx=bulletx-offsetx;
        bullety=bullety-offsety;
      
		
		phase4on=true;
		bullet = Crafty.e("2D, DOM, Color")
		.attr({x:bulletx ,y:bullety , w:bullet_w, h:bullet_h, z:2})
		
		.color("red");
		
		bullet.rotation=player._rotation;

	
	}
	else
	{	
		phase4on=false;
		bullet.destroy();
	
	}
	


}

