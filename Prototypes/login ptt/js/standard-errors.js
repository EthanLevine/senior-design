/*Standard Errors file*/

    
    var UNKNOWN_FAILURE             =    1;
    var INVALID_ACTION              =    2;
    
    
    var JSON_BAD_FORMAT             =   11;
    var JSON_BAD_TYPE               =   12;
    var JSON_TOO_FEW_ARGS           =   13;
    
    var AUTH_FAILED_AUTHENTICATE    =  101;
    var AUTH_DUPLICATE_USERNAME     =  102;
 
    
    var CHAR_DUPLICATE_NAME         =  111;
    var CHAR_INVALID_TYPE           =  112;
    var CHAR_ID_NOT_FOUND           =  113;
    
    var INSTANCE_ID_NOT_FOUND       =  121;
    var INSTANCE_WRONG_PASSWORD     =  122;
    var INSTANCE_FULL               =  123;