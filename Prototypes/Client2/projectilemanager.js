//--------------------------new projectile and weapon system-------------------------------------------
var projectileManager = function()
{
	"use strict";


	var projcount = 0;
	var maxProj = 50;
	var mprojcount = 0;
	var maxMobProj = 15;
	var pbulletspeed = 10;
	var difficulty = .01;
	
	this.changeDifficulty = function(dnum)
	{
		difficulty = dnum;
	}

	this.standProjPlayer = function(entity)
	{
		if (projcount<maxProj)
		{
			if(projcount<0)
			{
				alert("projectile count error");
			}
			projcount++;
			var bullet_w=32;
			var bullet_h=32;
			var sideproj=entity._rotation-90;
			
			entity.centery=entity._y+(entity._h/2);
			entity.centerx=entity._x+(entity._w/2);
			 
			var bulletx=entity.centerx+Math.round(30*Math.cos((entity.rotation-90)*Math.PI/180));
			var bullety=entity.centery+Math.round(30*Math.sin((entity.rotation-90)*Math.PI/180));
			
			var offsetx=Math.round((bullet_w/2)*Math.sin((90-entity.rotation)*Math.PI/180));
			var offsety=Math.round((bullet_w/2)*Math.cos((90-entity.rotation)*Math.PI/180));
			
			bulletx=bulletx-offsetx;
			bullety=bullety-offsety;


		var damager = Crafty.e("2D, DOM, Color, Image, AI, Collision, pbullet")
			.attr({w:bullet_w, h:bullet_h, z:2, x: bulletx, y: bullety, rotation: entity._rotation, AId: sideproj, btype:0, bfromx:Math.round(Math.cos(sideproj * (Math.PI / 180)) * 1000 * entity.firespeed) / 1000, bfromy: Math.round(Math.sin(sideproj * (Math.PI / 180)) * 1000 * entity.firespeed) / 1000, bounce: Math.random(), damage: entity.damage})
			.image("game_images_sprites/orange1.png","no-repeat")
			.bind('EnterFrame', function()
	    	{
	        	//player bullet
        		if(this.hit('solid') || this.hit('boundary') || this.hit('monsters') || this.hit('monsterportals'))
		        {
            		projcount--;
            		this.destroy();
		        }
		        else
		        {
		            this.x = this.x + this.bfromx;
		            this.y = this.y + this.bfromy;
		        }
		    });
		}
	};

	this.stickProjPlayer = function(entity)
	{
		if (projcount<maxProj)
		{
			if(projcount<0)
			{
				alert("projectile count error");
			}
			projcount++;
			var bullet_w=32;
			var bullet_h=32;
			var sideproj=entity._rotation-90;
			
			entity.centery=entity._y+(entity._h/2);
			entity.centerx=entity._x+(entity._w/2);
			 
			var bulletx=entity.centerx+Math.round(30*Math.cos((entity.rotation-90)*Math.PI/180));
			var bullety=entity.centery+Math.round(30*Math.sin((entity.rotation-90)*Math.PI/180));
			
			var offsetx=Math.round((bullet_w/2)*Math.sin((90-entity.rotation)*Math.PI/180));
			var offsety=Math.round((bullet_w/2)*Math.cos((90-entity.rotation)*Math.PI/180));
			
			bulletx=bulletx-offsetx;
			bullety=bullety-offsety;


		var damager = Crafty.e("2D, DOM, Color, Image, AI, Collision, pbullet")
		.attr({w:bullet_w, h:bullet_h, z:2, x: bulletx, y: bullety, rotation: entity._rotation, AId: sideproj, btype:0, bfromx:Math.round(Math.cos(sideproj * (Math.PI / 180)) * 1000 * entity.firespeed) / 1000, bfromy: Math.round(Math.sin(sideproj * (Math.PI / 180)) * 1000 * entity.firespeed) / 1000, bounce: Math.random(), damage: entity.damage})
		.image("game_images_sprites/orange1.png","no-repeat")
			.bind('EnterFrame', function()
	    	{
	        	//player bullet
        		if(this.hit('solid') || this.hit('boundary') || this.hit('monsters') || this.hit('monsterportals'))
		        {
	        		if (this.bounce<.5)
	        		{
		            	this.rotation = this.rotation + 50;
	        		}
	        		else
	        		{
	        			this.rotation = this.rotation - 50;
	        		}
	        		if(Math.random()>.8)
	        		{
	            		projcount--;
	            		this.destroy();
	        		}
		        }
		        else
		        {
		            this.x = this.x + this.bfromx;
		            this.y = this.y + this.bfromy;
		        }
		    });
		}
	};

	this.VortexProjMob = function(entity)
	{
		if (projcount<maxProj)
		{
			if(projcount<0)
			{
				alert("projectile count error");
			}
			projcount++;
			var bullet_w=32;
			var bullet_h=32;
			var sideproj=entity._rotation-90;
			
			entity.centery=entity._y+(entity._h/2);
			entity.centerx=entity._x+(entity._w/2);
			 
			var bulletx=entity.centerx+Math.round(30*Math.cos((entity.rotation-90)*Math.PI/180));
			var bullety=entity.centery+Math.round(30*Math.sin((entity.rotation-90)*Math.PI/180));
			
			var offsetx=Math.round((bullet_w/2)*Math.sin((90-entity.rotation)*Math.PI/180));
			var offsety=Math.round((bullet_w/2)*Math.cos((90-entity.rotation)*Math.PI/180));
			
			bulletx=bulletx-offsetx;
			bullety=bullety-offsety;

		var damager = Crafty.e("2D, DOM, Color, Image, AI, Collision, pbullet")
			.attr({w:bullet_w, h:bullet_h, z:2, x: bulletx, death:0, y: bullety, rotation: entity._rotation, AId: sideproj, btype:0, bfromx:bulletx, bfromy: bullety, bounce: Math.random(), damage: 1})
			.image("game_images_sprites/poison.png","repeat")
			.bind('EnterFrame', function()
	    	{
				if(this.btype == 0)
	    		{
		        		this.h=64;
		        		this.w=64;
		        		this.origin("center");
		        		this.removeComponent('pbullet');
		        		this.addComponent('monsters, solid');
		        		for(var i=0; i<9; i++)
		        		{
			        		if(this.hit('solid') || this.hit('safe') || this.hit('boundary'))
					        {
					           	this.death=1;
					        }
			        		else
			        		{
			        			this.rotation=this.rotation-i*10;
			        		}
		        		}
		        		if(this.death==1)
		        		{
			            	mprojcount--;
			            	this.destroy();
		        		}
		        		this.btype=1;
						this.bfromx=this.x;
		        		this.bfromy=this.y;
		        		if(this.death == 0)
			            {
				            this.timeout(function()
			                {
				            	this.btype=2;
				        		loadwave3(this.x,this.y);
				        		loadwave3(this.x,this.y);
				            	this.timeout(function()
						        {
				            		if(this.death == 0)
						            {
						            	this.death=1;
						            	mprojcount--;
					            		this.destroy();
						            }
						        }, 100);
			                }, 5000);
			            }
	    		}
	    		else if (this.btype ==1)
	    		{
	    			this.rotation=this.rotation-50;
	    			this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * 4) / 1000;
		            this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * 4) / 1000;
					if(this.hit('solid') || this.hit('safe') || this.hit('boundary'))
			        {
						this.x=this.bfromx;
		        		this.y=this.bfromy;
						this.AId= Math.floor(Math.random()*45)*8;
			        }
					else
					{
						this.bfromx=this.x;
		        		this.bfromy=this.y;
		        		ghostspawnx=this.x;
		        		ghostspawny=this.y;
		        		monsterspawnx=this.x;
		        		monsterspawny=this.y;
					}
			        if(this.hit('player'))
			        {	
			        	this.rotation=this.rotation-50;
			        }
	        	}
	    		else
	    		{
	    			this.rotation=this.rotation-50;
	    		}
		    });
		}
	};

	this.stunProjMob = function(entity)
	{
		if (mprojcount<maxMobProj)
		{
			if(mprojcount<0)
			{
				alert("projectile count error");
			}
			mprojcount++;

			var entityHit;

			var bullet_w=64;
			var bullet_h=32;
			var sideproj=entity._rotation-90;
			
			entity.centery=entity._y+(entity._h/2);
			entity.centerx=entity._x+(entity._w/2);
			 
			var bulletx=entity.centerx+Math.round(30*Math.cos((entity.rotation-90)*Math.PI/180));
			var bullety=entity.centery+Math.round(30*Math.sin((entity.rotation-90)*Math.PI/180));
			
			var offsetx=Math.round((bullet_w/2)*Math.sin((90-entity.rotation)*Math.PI/180));
			var offsety=Math.round((bullet_w/2)*Math.cos((90-entity.rotation)*Math.PI/180));
			
			bulletx=bulletx-offsetx;
			bullety=bullety-offsety;

		var damager = Crafty.e("2D, DOM, Color, Image, AI, Collision, monsters")
		.attr({w:bullet_w, h:bullet_h, z:2, x: bulletx, death:0, y: bullety, rotation: entity._rotation, AId: sideproj, btype:0, bfromx:bulletx, bfromy: bullety, bounce: Math.random(), damage: 1})
		.image("game_images_sprites/ice1.png","repeat")
		.origin("center")
		.bind('EnterFrame', function()
	    	{
	        	//ice stun bullet
				if(this.btype == 0)
	        	{
						this.bfromx=this.x;
		        		this.bfromy=this.y;
		        		this.btype = 1;
		        		this.timeout(function()
				        {
			        		if(this.death==0)
				            {
				            	this.timeout(function()
						        {
				            		mprojcount--;
					            	this.destroy();
						        }, 100);
			            	}
			        		else
			        		{
			        			this.timeout(function()
						        {
			        				mprojcount--;
					            	this.destroy();
						        }, difficulty * 1500);
			        		}
				        }, difficulty * 500);
	        		}
	        		else if (this.btype ==1 && this.death!=1)
	        		{
	        			if(this.hit('player'))
	        			{
	        				this.death=1;
	        				this.addComponent('solid');
	        			}
	        			else
	        			{
        					this.rotation=this.rotation+50;
				            this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * pbulletspeed) / 1000;
		        			if(this.hit('safe') || this.hit('boundary') || this.hit('pbullet') || this.hit('solid'))
					        {
			        			this.x=this.bfromx;
					        }
					        else
					        {
					        	this.bfromx=this.x;
					        }
		        			this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * pbulletspeed) / 1000;
		        			if(this.hit('safe') || this.hit('boundary') || this.hit('pbullet'))
					        {
			        			this.y=this.bfromy;
					        }
					        else
					        {
					        	this.bfromy=this.y;
					        }
	        			}
		        	}
		        	else if(this.death==1)
		        	{
		        		this.rotation=this.rotation+50;
		        	}
		    })
			.bind("MessureFPS", function()
	 		{
				for(var i = 0; i<5; i++)
				{
					this.timeout(function()
					{
						if(this.hit('player'))
				        {
				        	entityHit = this.hit("player");
							for(var i = 0; i<entityHit.length; i++)
							{
								entityHit[i].obj.health = entityHit[i].obj.health - this.damage;
							}
				        }
		        	}, i*200);
				}
	 		});
		}
	};
	this.VortexPlayer = function(entity)
	{
		if (projcount<maxProj)
		{
			if(projcount<0)
			{
				alert("projectile count error");
			}
			projcount++;
			var bullet_w=32;
			var bullet_h=32;
			var sideproj=entity._rotation-90;
			
			entity.centery=entity._y+(entity._h/2);
			entity.centerx=entity._x+(entity._w/2);
			 
			var bulletx=entity.centerx+Math.round(30*Math.cos((entity.rotation-90)*Math.PI/180));
			var bullety=entity.centery+Math.round(30*Math.sin((entity.rotation-90)*Math.PI/180));
			
			var offsetx=Math.round((bullet_w/2)*Math.sin((90-entity.rotation)*Math.PI/180));
			var offsety=Math.round((bullet_w/2)*Math.cos((90-entity.rotation)*Math.PI/180));
			
			bulletx=bulletx-offsetx;
			bullety=bullety-offsety;

		damager.attr({w:bullet_w, h:bullet_h, z:2, x: bulletx, death:0, y: bullety, rotation: entity._rotation, AId: sideproj, btype:0, bfromx:bulletx, bfromy: bullety, bounce: Math.random()})
		.image("game_images_sprites/orange1.png","no-repeat")
		.bind('EnterFrame', function()
	    	{
				if(this.btype == 0)
        		{
        			if(projcount<maxmobnum/2)
        			{
		        		for(var i=0; i<7; i++)
		        		{
			        		if(this.hit('safe') || this.hit('boundary'))
					        {
			        			if(this.death == 0)
					            {
					            	this.death=1;
					            }
					        }
			        		else
			        		{
			        			this.rotation=this.rotation-i*50;
			        		}
		        		}
		        		if(this.death==1)
		        		{
			            	projcount--;
			            	this.destroy();
		        		}
		        		this.btype=1;
						this.bfromx=this.x;
		        		this.bfromy=this.y;
        			}
        			else
        			{
        				if(this.death == 0)
			            {
			            	this.death=1;
	        				projcount--;
	        				this.destroy();
			            }
        			}
        		}
        		else if (this.btype==1)
        		{
        			this.btype=2;
        			this.image("game_images_sprites/orange1.png","repeat");
        			this.w=64;
        			this.origin("center");
        			this.timeout(function()
		                {
			            	this.btype=3;
			            	this.timeout(function()
					        {
			            		if(this.death == 0)
					            {
					            	this.death=1;
    			            	projcount--;
			            		this.destroy();
					            }
					        }, 100);
		                }, 1000);
        		}
        		else if(this.btype==2)
        		{
        			this.rotation=this.rotation-40;
        			this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * pbulletspeed) / 1000;
		            this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * pbulletspeed) / 1000;
					if(this.hit('safe') || this.hit('boundary'))
			        {
						this.x=this.bfromx;
		        		this.y=this.bfromy;
						this.AId= Math.floor(Math.random()*45)*8;
			        }
					else if(this.hit('solid'))
			        {
						if(Math.random()<difficulty)
						{
							this.AId= Math.floor(Math.random()*45)*8;
		        			this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * pbulletspeed) / 1000;
				            this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * pbulletspeed) / 1000;
				            if(this.hit('safe') || this.hit('boundary'))
					        {
								this.x=this.bfromx;
				        		this.y=this.bfromy;
								this.AId= Math.floor(Math.random()*45)*8;
					        }
						}
			        }
					else if(this.hit('monsters'))
					{
						if(Math.random()>difficulty)
						{
							createpulse(this.x, this.y);
							this.AId = Math.floor(Math.random()*45)*8;
						}
					}
					else
					{
						this.bfromx=this.x;
		        		this.bfromy=this.y;
					}
	        	}
        		else
        		{
        			this.rotation=this.rotation-40;
        		}
	        });
		}
	};
}

//--------------------------end new projectile and weapon system---------------------------------------
