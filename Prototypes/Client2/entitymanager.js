// entitymanager.js
// an entity manager object for the UConnocalypse client
//
// 
// This library provides a one place for managing the entities in the game.
// 
//
// For documentation on using this library, see the project's wiki at:
//   https://bitbucket.org/EthanLevine/senior-design/wiki/entitymanager.js


//TODO: need to set local player id.  Need to bind mouse movements here.  Need to bind Keys here.

	
var entityManagerCreator = function(){

	"use strict";
	//vars are private
	var entityMap = {};

	var itemsMap = {};
	var itemsImagesMap = {};
	var localPlayerId;
	var gameStarted = false;
	var localPlayerBound = false;
	var that = this;
	var chatting;
	
	var entitySpeciesTable=[];
	//[entityspeciesid]= w, h, imageurl, name
	entitySpeciesTable[1] = [6, 6, "sprites/projectiles/bullet.png", "bullets"];
	entitySpeciesTable[2] = [48, 48, "sprites/enemies/reaper.png", "reaper"];
	entitySpeciesTable[3] = [48, 48, "sprites/enemies/fire_monster.png", "fire_monster"];

	var SPRITE_DIR = "sprites/";
	var itemTemplateSpriteTable = [];
	itemTemplateSpriteTable[1] = SPRITE_DIR + "weapons/pistol.png";
	itemTemplateSpriteTable[2] = SPRITE_DIR + "weapons/sword.png";
	itemTemplateSpriteTable[3] = SPRITE_DIR + "weapons/shotgun.png";
	itemTemplateSpriteTable[4] = SPRITE_DIR + "items/shield_2.png";
	itemTemplateSpriteTable[6] = SPRITE_DIR + "items/banana.gif";
	itemTemplateSpriteTable[7] = SPRITE_DIR + "items/star.png";
	itemTemplateSpriteTable[8] = SPRITE_DIR + "items/apple.gif";



	this.getEntity = function(id) {
		return entityMap[id];
	};

	this.chatfocusin = function() {
		chatting=true;
	};

	this.chatfocusout = function() {
		chatting=false;
	};

	this.getChatting = function(){
		return chatting;
	};


	this.getLocalPlayerBound = function() {
		return localPlayerBound;
	};


	this.bindLocalPlayer = function () {

		if(localPlayerId!=undefined){


			localPlayerBound = true;
			bindKeystoMessages();
			addMouseRotation();

			//need a Crafty entity to bind the update loop into its enterframe binding.  lets use the localplayer.
			that.getLocalPlayer().bind('EnterFrame', that.updateLoop);
		}

	};


	this.updateLoop = function(){
		//this function is going to get called each tick to update all the entities positions.
      
		var ticktime = myTimer.getTickTime()/1000;  //convert to seconds.
		var newx;
		var newy;

			for(var entity in entityMap){
				newx = entityMap[entity]._x  + entityMap[entity].xvelocity*ticktime;
				newy = entityMap[entity]._y  + entityMap[entity].yvelocity*ticktime;
				//make sure we are assigning numbers and not NaN's
				if(!isNaN(newx) && !isNaN(newy)){
					entityMap[entity].attr({x: newx, y: newy});
				}
				
			}

	};

	this.setGameStarted = function(value){
		gameStarted = value;
	};

	this.getGameStarted = function() {
		return gameStarted;
	};





	this.setLocalPlayerId = function(id){
		
		localPlayerId=id;
		//this is temporary until server sends me my local speed.
		that.setLocalPlayerVelocity(100);
	};

	this.setLocalPlayerVelocity = function(velocity){
		
		entityMap[localPlayerId].velocity=velocity;

	};

	this.getLocalPlayer = function() {

		return entityMap[localPlayerId];
	};


	this.createEntity = function(message) {//priviledged function
		//engine.add-entity <entity id> <entity species id> <initial x> <initial y>
		//first check to see if the entity exists in array, this should not happen, but if it does something is wrong.
		var temp = false;
		var entityid = message[1];
		var entitySpecies = message[2];
		var initialx = message[3];
		var initialy = message[4];
		var initialangle = message[5];
		var initialxvel = message[6];
		var initialyvel = message[7];

		if(entityMap[entityid]===undefined){//entity id doesn't exist, create.

			if(entitySpecies===-2){//these are player entities.
				initialx = initialx - 24;
				initialy = initialy - 24;
				/*
				redirected to the sprites folder instead of ALL_sprites
				
				ENGINEER - player_engineer.png
				BIOLOGIST - player_biologist.png
				CHEMIST - player_chemist.png
				PHILOSOPHER - player_philospher.png
				*/

				Crafty.sprite(48, "sprites/player/player_engineer.png", {
			    //<NAME>: [COLUMN,ROW]
			    	localPlayerSprite: [0,0]
			    });

				entityMap[entityid] = Crafty.e("2D, DOM,SpriteAnimation, localPlayerSprite");
				entityMap[entityid].attr({x: initialx, y: initialy, z: 10, w: 48, h: 48, xvelocity: 0, yvelocity: 0})
				.animate("walking",0,3,13)
				.animate("standing",0,3,0)
				.animate("punch",0,1,11)
				.animate("standing",20,-1)
			//	.image("game_images_sprites/ALL_sprites/sprite.gif","no-repeat") // this will change once we have species info
	       	    .origin("center");
				
				entityMap[entityid].attr({health:50,maxhealth:100,exp:30,maxexp:60,ability:60,maxability:120,gold:10});

				temp = entityMap[entityid];
			}
			else{
				//[entityspeciesid]= w, h, imageurl, name
				initialx = initialx - (entitySpeciesTable[entitySpecies][0])/2;//offsets
				initialy = initialy - (entitySpeciesTable[entitySpecies][1])/2;

				entityMap[entityid] = Crafty.e("2D, DOM,Image")
				.attr({x:initialx, y:initialy, z:10, w:entitySpeciesTable[entitySpecies][0], h:entitySpeciesTable[entitySpecies][1], xvelocity:initialxvel, yvelocity:initialyvel })
				.origin("center")
				.image(entitySpeciesTable[entitySpecies][2]);

				//bugfix for non appearing sprites:
				entityMap[entityid].w=entitySpeciesTable[entitySpecies][0];
				entityMap[entityid].h=entitySpeciesTable[entitySpecies][1];

				temp = entityMap[entityid];

			}




		}
		else{
			console.log(entitySpecies);
		}
		
		return temp;

	};

	this.updateEntity = function(message){
		 // engine.update-entity <entity id> <new x> <new y> <new angle> <new x velocity> <new y velocity>
		var temp = false;
		var entityid = message[1];
		var newx = message[2]-24;//x&y offset
		var newy = message[3]-24;
		var newangle = message[4];
		var newxvelocity = message[5];
		var newyvelocity = message[6];
		var limit = 10; //10 pixels limit for rubberbanding

		if(entityid===localPlayerId){//local player
			temp = entityMap[entityid];
			//check to see if we are within limit pixels if so don't update because of rubberbanding
			entityMap[entityid].attr({ xvelocity : newxvelocity, yvelocity : newyvelocity});

			if(Math.abs(entityMap[entityid]._x-newx)>limit || Math.abs(entityMap[entityid]._y-newy)>limit){
				entityMap[entityid].attr({x:newx, y:newy});

			}




		}

		else if(entityMap[entityid]!==undefined){//non local player
			//entity exists
			
			entityMap[entityid].attr({x:newx, y:newy, rotation: newangle, xvelocity : newxvelocity, yvelocity : newyvelocity});
			temp = entityMap[entityid];
		}

		return temp;

	};

	this.placeItem = function(message) {
		var temp = false;
		//placeItem should take the following parameters (item_id, item_template_id, item_name, item_quality, x, y)
		//need to create a craftyjs entity, bind a sprite according to item_template_id, bind a hover mouse to show item name in color depending to quality 

		var item_id = message[1];
		var item_template_id = message[2];
		var item_name = message[3];
		var item_quality = message[4];
		var x = message[5] - 24;//x&y offset
		var y = message[6] - 24;
		//var imageurl = getItemImage(item_template_id);
		var imageurl = itemTemplateSpriteTable[item_template_id]; 

		

		if(itemsMap[item_id]===undefined){//item id doesn't exist, create.
	
			itemsMap[item_id] = Crafty.e("2D, DOM, Image, Mouse")
			.attr({x: x, y: y, z: 1, w: 48, h: 48, xvelocity: 0, yvelocity: 0})
			.image(imageurl,"no-repeat") // this will change once we have species info
       	    .origin("center");

			

			//binding hover over to display item name  still need to set color
			itemsMap[item_id].textbox =  Crafty.e("DOM, Text");
			itemsMap[item_id].textbox.attr({x:itemsMap[item_id]._x, y: itemsMap[item_id]._y-15});
			itemsMap[item_id].attach(itemsMap[item_id].textbox);

			itemsMap[item_id].bind("MouseOver", function(){ 
				itemsMap[item_id].textbox.text(item_name);
				switch(item_quality){
					//set color here to text depending on item_quality
					case 0:
						itemsMap[item_id].textbox.textColor("CCCCCC"); //grey
					break;

					case 1:
						itemsMap[item_id].textbox.textColor("FFFFFF"); //white
					break;

					case 2:
						itemsMap[item_id].textbox.textColor("228b22"); //green
					break;

					case 3:
						itemsMap[item_id].textbox.textColor("0000ff");  //blue
					break;

					case 4:
						itemsMap[item_id].textbox.textColor("a020f0");  //purple
					break;

					case 5:
						itemsMap[item_id].textbox.textColor("ff4500");  //orange
					break;

				}
  
		    });

			itemsMap[item_id].bind("MouseOut", function(){ 
				itemsMap[item_id].textbox.text("");
			});

			itemsMap[item_id].bind("Click",function(e){
				//first check to see if player is within certain distance
				var distance_threshhold = 50;
				
				var localPlayerx = entityManager.getLocalPlayer()._x;
				var localPlayery = entityManager.getLocalPlayer()._y;
				var distance = Math.sqrt((localPlayerx-x)*(localPlayerx-x) + (localPlayery-y)*(localPlayery-y));


				if(distance<distance_threshhold){
					//apply pickup actions
					var message = ["inventory.pickup", item_id];
					mq.send(message)
					.onconfirm(function (msg) {
						console.log("Confirm: ");
						console.log(msg);

					})
					.onerror(function (code, msg) {
						console.log("Error: " + msg);

					});
				}




			});


		}



	};

	var getItemImage = function(item_template_id){
		//private method that is a helper for retreiving images for items
		var imageurl=false;

		if(itemsImagesMap[item_template_id]===undefined){
			//we don't know about this item's image, get it from the server
			var message = ["item.template-info", item_template_id];

			mq.send(message)
			.onconfirm(function (msg) {
				console.log("Confirm getItemImage: ");
				console.log(msg);
				imageurl = msg[0];
			  
			})
			.onerror(function (code, msg) {
				console.log("Error getItemImage: " + msg);

			});

		}
		else{

			imageurl = itemsImagesMap[item_template_id];

		}

		return imageurl;


	};

	this.removeEntity = function(message){
		//engine.remove-entity <entity id>
		var temp = false;
		var entityid = message[1];
		

		if(entityMap[entityid]!==undefined){

			entityMap[entityid].destroy(); // destroys the Crafy Entity, including removing all attributes and removing from the stage.
			delete entityMap[entityid]; //removes the entity from the array and sets that index value to undefined.

			var msg = ["engine.remove-entity-confirm", entityid];
			mq.send(msg);

			temp = true;
		}
		return temp;


	};

	this.removeItem = function(message){
		//item.remove <item id>
		var temp = false;
		var itemid = message[1];

		if(itemsMap[itemid]!==undefined){

			itemsMap[itemid].destroy(); // destroys the Crafy Entity, including removing all attributes and removing from the stage.
			delete itemsMap[itemid]; //removes the entity from the array and sets that index value to undefined.

			temp = true;
		}
		return temp;


	};

};