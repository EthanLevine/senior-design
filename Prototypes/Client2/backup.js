
//-------------------------------------hud start here------------------------------------
var setupHUD =function()
{
	"use strict";

	var myHUD;
	var myHUD2;
	var HUDbox;
	var objective = "unknown";
	var difficulty = .01;
	this.changeDifficulty = function(newd)
	{
		difficulty = newd;
	}
	this.difficulty= function()
	{
		return difficulty;
	}
	this.setupBasic = function(entity)
	{
		myHUD = Crafty.e("2D, Text, DOM, Persist")
			.attr({x:16,y:16, z:5,w:1000,h:16})
			.textColor("#FFFFFF")
			.bind('EnterFrame', function() 
			{
				this.text("Health: " + entity.health + " | Gold: " + entity.gold + " | Current Score: " + entity.score + " | Difficulty: " + Math.round(difficulty*100) +" | Player Level: " + entity.level);
				this.x=(Crafty.viewport._x*-1);
				this.y=(Crafty.viewport._y*-1);
			});
		myHUD2 = Crafty.e("2D, Text, DOM, Persist")
			.attr ({x: myHUD._x, y: myHUD._y+16, z:5, w:500, h:32})
			.textColor("#FFFFFF")
			.bind('EnterFrame', function() 
			{
				this.text("Current Objective:" + objective);
			});
		myHUD.attach(myHUD2);

		HUDbox = Crafty.e("2D, Color, DOM, Persist")
			.attr({x: myHUD._x, y: myHUD._y, z:4, w:1000, h:32, alpha:0.8})
			.color("black");
		myHUD.attach(HUDbox);
	};
	this.changeObjective = function(newobj)
	{
		objective = newobj;
	};

	this.loadplayer = function(entity)
	{
		var healthshields = new Array();
		for(var i = 0; i<5; i++)
		{
			healthshields[i] = Crafty.e('2D, DOM, Image, Persist, Tween')
				.image("game_images_sprites/shieldpowerup.png","no-repeat")
				.attr({x: (entity._x + entity._w/2 -16) + i*20 - 40, y:entity._y -50, rotation:0, z:4, h:32, w:32, active:0, alpha: 0.1, snum: i})
				.origin("center")
				.bind("EnterFrame", function()
				{
					if(this.active==0)
				   	{
				   		this.active=1;
				   		entity.attach(this);
				   	}
			        if(entity.health >= (this.snum * 20) + 20 && this.alpha != .7)
					{
						this.alpha = .7;
					}
					else if(entity.health < (this.snum * 20) + 20 && this.alpha != .05)
					{
						this.alpha = .05;
					}
				});
		}
	};

	this.loadplayer2 = function(entity)
	{
		var healthnumber = Crafty.e('2D, DOM, Text, Collision, Persist')
			.text("loading")
			.attr({x: entity.x -10, y: entity.y +50, z:3})
			.textColor('#FF0000')
			.bind("EnterFrame", function()
			{
			    this.text("Health:" + entity.health + " Gold:" + entity.gold);                
			});
			entity.attach(healthnumber);
	};

};
//-----------------------------------------hud end here--------------------------------------





//----------------------------------------start ai manager------------------------------------
var AImanager = function()
{
	"use strict";

	var entityHit;
	var deathnum = 0;
	var difficulty = .01;
	var maxMobNum = 15;
	var mnumber = 0;
	//holds all ghosts
	var ghosts = [];
	var gnumber = 0;
	var maxGhosts = 5;
	//holds all portals
	var portals = {};
	var portalnum = 0;
	var maxPortalNum = 10;
	//holds all targets for monsters
	var AItargets = [];
	//holds all multiplying AI
	var multiMob=[]
	var maxMulti = 5;
	var munumber = 0;


	/*
	var solidCheckl = Crafty.e('2D, DOM, Collision')
	.attr({h:16, w:16, x:0, y:0});
	var solidCheckr = Crafty.e('2D, DOM, Collision')
	.attr({h:16, w:16, x:0, y:0});
	var solidChecku = Crafty.e('2D, DOM, Collision')
	.attr({h:16, w:16, x:0, y:0});
	var solidCheckd = Crafty.e('2D, DOM, Collision')
	.attr({h:16, w:16, x:0, y:0});
	*/

	this.changeDifficulty = function(dnum)
	{
		difficulty = dnum;
	}

	this.addTargets = function(entity, enumber)
	{
		if(AItargets[enumber]===undefined)
		{		
			AItargets[enumber] = entity;
		}
	}
	this.deleteTargets = function(enumber)
	{
		if(AItargets[enumber]!==undefined)
		{
			delete AItargets[enumber]; //removes the entity from the array and sets that index value to undefined.
		}
	}
	var killGhost = function (entity)
	{
		if (ghosts[entity.id] !== undefined)
		{
			mnumber--;
			gnumber--;
			ghosts[entity.id].destroy();
			delete ghosts[entity.id];
		}
	}
	this.addGhost = function(hp, speed)
	{
		if(mnumber<maxMobNum && gnumber < maxGhosts)
		{
			mnumber++;
			gnumber++;
			var curTarget;
			var curTargetPoint;
			var spawnsite;
			var ghost = Crafty.e('2D, DOM, Image, Collision, SmartAI, MessureFPS, monsters, Tween')
				.attr({w:32, h:32, z:2, x: 10 ,y: 10, alpha:0.8, rotation:0, stuck:0, health: hp, mspeed: speed, spawnx: 10, spawny: 10, damage: 1, id: -1})
				.image("game_images_sprites/ghost.png","no-repeat")
				.origin("center")
				.bind("EnterFrame", function() 
				{
					if(this.hit('pbullet'))
					{
						entityHit = this.hit("pbullet");
						for(var i = 0; i<entityHit.length; i++)
						{
							this.health = this.health - entityHit[i].obj.damage;
						}
						if (this.hit('shield'))
						{
							entityHit = this.hit("shield");
							for(var i = 0; i<entityHit.length; i++)
							{
								this.health = this.health - entityHit[i].obj.damage;
							}
						}
						if(this.health<0)
						{
							if (portalnum !=0)
							{
								while(portalnum !=0)
								{
									spawnsite = portals[Math.floor(Math.random() * maxPortalNum + 1)];
						        	if(spawnsite!==undefined)
						        	{
										this.x = spawnsite._x;
										this.y = spawnsite._y;
										this.health = hp;
										deathnum++;
										break;
									}
								}
							}
							else
							{
								deathnum++;
								killGhost[this];
							}
						}
					}
					if(this.hit('player'))
					{
						entityHit = this.hit("player");
						for(var i = 0; i<entityHit.length; i++)
						{
							entityHit[i].obj.health = entityHit[i].obj.health - this.damage;
						}
						if (portalnum !=0)
						{
							while(portalnum !=0)
							{
								spawnsite = portals[Math.floor(Math.random() * maxPortalNum + 1)];
					        	if(spawnsite!==undefined)
					        	{
									this.x = spawnsite._x;
									this.y = spawnsite._y;
									this.health = hp;
									break;
								}
							}
						}
					}
					if(this.hit('safe') || this.hit('boundary'))
					{
						if (portalnum !=0)
						{
							while(portalnum !=0)
							{
								spawnsite = portals[Math.floor(Math.random() * maxPortalNum + 1)];
					        	if(spawnsite!==undefined)
					        	{
									this.x = spawnsite._x;
									this.y = spawnsite._y;
									this.health = hp;
									break;
								}
							}
						}
					}
				})
				.bind("MessureFPS", function() 
				{
					for(var i = 0; i<=5; i++)
					{
						this.timeout(function()
						{
							this.rotation = this.rotation + 20;
						}, i*200);
					}
					curTargetPoint = Math.floor(Math.random()* AItargets.length);
					if(AItargets[curTargetPoint] !== undefined)
					{
						curTarget = AItargets[curTargetPoint];
						this.tween({alpha: 0.3, x: curTarget.x , y: curTarget.y, rotation:this.rotation + 360}, this.mspeed);
					}
					else
					{
						this.health++;
					}

					this.alpha = 1;
				});
			if(ghosts.length == gnumber)
			{
				ghosts[ghosts.length]= ghost;
			}
			else
			{
				for(var i = 0; i<=maxGhosts; i++)
				{
					if (ghosts[i]===undefined)
					{
						ghost.id = i;
						ghosts[i]=ghost;
						break;
					}
				}
			}
		}
	};

	var killMultiplier = function(entity)
	{
		if (multiMob[entity.id] !== undefined)
		{
			mnumber--;
			munumber--;
			multiMob[entity.id].destroy();
			delete multiMob[entity.id];
		}
	}

	this.addMultiplier = function(hp, speed)
	{
		if(maxMobNum>mnumber)
		{
			if (portalnum !=0)
			{
				var spawnsite = portals[Math.floor(Math.random() * maxPortalNum + 1)];
				var entityHit;
				mnumber++;
				munumber++;
	        	if(spawnsite!==undefined)
	        	{
					var monster = Crafty.e("2D, DOM, Image, Collision, AIadaptive, monsters, Tween, MessureFPS")
					    .attr({w:32, h:50, x: spawnsite._x, y: spawnsite._y, currrandom: 1, AId:1, oldfromx: spawnsite._x, oldfromy: spawnsite._y, health: hp, mspeed: speed, id: 1, stuck: .8, damage:1, cfromx:0, cfromy:0})
					    .image("game_images_sprites/sprite2.png","no-repeat")
				 		.bind("EnterFrame", function()
				 		{
				 			//change x
							if(this.AId == 45 || this.AId == 315)
							{
								this.x = this.x + this.mspeed;
				           		if(this.hit('solid'))
					        	{
									this.x = this.oldfromx;
					        	}
				           		else
				           		{
									this.oldfromx=this.x;
									this.oldfromy=this.y;
				           		}
				           	}
				           	else if(this.AId == 135 || this.AId== 225)
				           	{
					           	this.x = this.x - this.mspeed;
				           		if(this.hit('solid'))
					        	{
									this.x = this.oldfromx;
					        	}
				           		else
				           		{
									this.oldfromx=this.x;
									this.oldfromy=this.y;
				           		}
				           	}
				           	else if(this.AId == 0)
				           	{
				           		this.x = this.x + this.mspeed;
				           		if(this.hit('solid'))
					        	{
									this.x = this.oldfromx;
					        	}
				           		else
				           		{
									this.oldfromx=this.x;
									this.oldfromy=this.y;
				           		}
				           	}
				           	else if(this.AId == 180)
				           	{
				           		this.x = this.x - this.mspeed;
				           		if(this.hit('solid'))
					        	{
									this.x = this.oldfromx;
					        	}
				           		else
				           		{
									this.oldfromx=this.x;
									this.oldfromy=this.y;
				           		}
				           	}
				           	//change y
				           	if(this.AId == 45 || this.AId == 135)
							{
								this.y = this.y - this.mspeed;
				           		if(this.hit('solid'))
					        	{
									this.y = this.oldfromy;
					        	}
				           		else
				           		{
									this.oldfromx=this.x;
									this.oldfromy=this.y;
				           		}
				           	}
				           	else if(this.AId == 315 || this.AId== 225)
				           	{
					           	this.y = this.y + this.mspeed;
				           		if(this.hit('solid'))
					        	{
									this.y = this.oldfromy;
					        	}
				           		else
				           		{
									this.oldfromx=this.x;
									this.oldfromy=this.y;
				           		}
				           	}
				           	else if(this.AId == 90)
				           	{
				           		this.y = this.y - this.mspeed;
				           		if(this.hit('solid'))
					        	{
									this.y = this.oldfromy;
					        	}
				           		else
				           		{
									this.oldfromx=this.x;
									this.oldfromy=this.y;
				           		}
				           	}
				           	else if(this.AId == 270)
				           	{
				           		this.y = this.y + this.mspeed;
				           		if(this.hit('solid'))
					        	{
									this.y = this.oldfromy;
					        	}
				           		else
				           		{
									this.oldfromx=this.x;
									this.oldfromy=this.y;
				           		}
				           	}
				           	//check if stuck
				           	if(this.oldfromx==this.cfromx && this.oldfromy==this.cfromy)
				           	{
								this.AId = Math.round(Math.random()*8)*45;
				           	}
				           	else
				           	{
				           		this.cfromx = this.x;
				           		this.cfromy = this.y;
				           	}
				           	//check if shot
							if(this.hit('pbullet'))
					        {
					        	entityHit = this.hit("pbullet");
								for(var i = 0; i<entityHit.length; i++)
								{
									this.health = this.health - entityHit[i].obj.damage;
								}
								if(this.health<0)
								{
									deathnum++;
									killMultiplier(this);
								}
					        }
				 		})
				 		.bind("MessureFPS", function()
				 		{
							this.AId = Math.round(Math.random()*8)*45;
							for(var i = 0; i<10; i++)
							{
								this.timeout(function()
								{
									if(this.hit('player'))
							        {
							        	entityHit = this.hit("player");
										for(var i = 0; i<entityHit.length; i++)
										{
											entityHit[i].obj.health = entityHit[i].obj.health - this.damage;
										}
							        }
							        if(this.hit('safe'))
									{
										if (portalnum !=0)
										{
											while(portalnum !=0)
											{
												spawnsite = portals[Math.floor(Math.random() * maxPortalNum + 1)];
									        	if(spawnsite!==undefined)
									        	{
													this.x = spawnsite._x;
													this.y = spawnsite._y;
													this.health = hp;
													break;
												}
											}
										}
									}
					        	}, i*100);
							}
				 		});
					if(multiMob.length == munumber)
					{
						multiMob[multiMob.length]= monster;
					}
					else
					{
						for(var i = 0; i<=maxMulti; i++)
						{
							if (multiMob[i]===undefined)
							{
								multiMob.id = i;
								multiMob[i]=monster;
								break;
							}
						}
					}
				}
			}
		}
	};

	var killPortal = function(pnum)
	{
		if(portals[pnum]!==undefined)
    	{
			portals[pnum].destroy(); // destroys the Crafy Entity, including removing all attributes and removing from the stage.
			delete portals[pnum]; //removes the entity from the array and sets that index value to undefined.
		}
	}
	this.getPortals = function()
	{
		return portalnum;
	}
	this.addPortals = function(portalx, portaly, portalhp)
	{
		if(portalnum<maxPortalNum)
		{
        	portalnum++;
        	var curPortal;
        	for(var i =0; i<= maxPortalNum; i++)
        	{
	        	if(portals[i]===undefined)
	        	{	
					portals[i] = Crafty.e('2D, DOM, Image, Collision, monsterportals, Persist')
						.attr({w:58, h:50, z:10, x: portalx, y: portaly, rotation: 0, pnum: i, health: portalhp})
						.image("game_images_sprites/portal.png","repeat")
						.origin("center")
		                .bind("EnterFrame", function()
		                {
							this.rotation = this.rotation+36;
	                		if(this.hit('pbullet'))
		                    {
			                    entityHit = this.hit("pbullet");
								for(var i = 0; i<entityHit.length; i++)
								{
									this.health = this.health - entityHit[i].obj.damage;
								}
			                    if(this.health<0)
		                        {
		                            portalnum--;
		                            killPortal(this.pnum);
		                        }
							}
	                	});
					curPortal = i;
					break;
				}
			}
		}
	};

};

//-----------------------------------------AI manager end here-------------------------------


//--------------------------new projectile and weapon system-------------------------------------------
var projectileManager = function()
{
	"use strict";


	var projcount = 0;
	var maxProj = 50;
	var mprojcount = 0;
	var maxMobProj = 15;
	var pbulletspeed = 10;
	var difficulty = .01;
	
	this.changeDifficulty = function(dnum)
	{
		difficulty = dnum;
	}

	this.standProjPlayer = function(entity)
	{
		if (projcount<maxProj)
		{
			if(projcount<0)
			{
				alert("projectile count error");
			}
			projcount++;
			var bullet_w=32;
			var bullet_h=32;
			var sideproj=entity._rotation-90;
			
			entity.centery=entity._y+(entity._h/2);
			entity.centerx=entity._x+(entity._w/2);
			 
			var bulletx=entity.centerx+Math.round(30*Math.cos((entity.rotation-90)*Math.PI/180));
			var bullety=entity.centery+Math.round(30*Math.sin((entity.rotation-90)*Math.PI/180));
			
			var offsetx=Math.round((bullet_w/2)*Math.sin((90-entity.rotation)*Math.PI/180));
			var offsety=Math.round((bullet_w/2)*Math.cos((90-entity.rotation)*Math.PI/180));
			
			bulletx=bulletx-offsetx;
			bullety=bullety-offsety;


		var damager = Crafty.e("2D, DOM, Color, Image, AI, Collision, pbullet")
			.attr({w:bullet_w, h:bullet_h, z:2, x: bulletx, y: bullety, rotation: entity._rotation, AId: sideproj, btype:0, bfromx:Math.round(Math.cos(sideproj * (Math.PI / 180)) * 1000 * entity.firespeed) / 1000, bfromy: Math.round(Math.sin(sideproj * (Math.PI / 180)) * 1000 * entity.firespeed) / 1000, bounce: Math.random(), damage: entity.damage})
			.image("game_images_sprites/orange1.png","no-repeat")
			.bind('EnterFrame', function()
	    	{
	        	//player bullet
        		if(this.hit('solid') || this.hit('boundary') || this.hit('monsters') || this.hit('monsterportals'))
		        {
            		projcount--;
            		this.destroy();
		        }
		        else
		        {
		            this.x = this.x + this.bfromx;
		            this.y = this.y + this.bfromy;
		        }
		    });
		}
	};

	this.stickProjPlayer = function(entity)
	{
		if (projcount<maxProj)
		{
			if(projcount<0)
			{
				alert("projectile count error");
			}
			projcount++;
			var bullet_w=32;
			var bullet_h=32;
			var sideproj=entity._rotation-90;
			
			entity.centery=entity._y+(entity._h/2);
			entity.centerx=entity._x+(entity._w/2);
			 
			var bulletx=entity.centerx+Math.round(30*Math.cos((entity.rotation-90)*Math.PI/180));
			var bullety=entity.centery+Math.round(30*Math.sin((entity.rotation-90)*Math.PI/180));
			
			var offsetx=Math.round((bullet_w/2)*Math.sin((90-entity.rotation)*Math.PI/180));
			var offsety=Math.round((bullet_w/2)*Math.cos((90-entity.rotation)*Math.PI/180));
			
			bulletx=bulletx-offsetx;
			bullety=bullety-offsety;


		var damager = Crafty.e("2D, DOM, Color, Image, AI, Collision, pbullet")
		.attr({w:bullet_w, h:bullet_h, z:2, x: bulletx, y: bullety, rotation: entity._rotation, AId: sideproj, btype:0, bfromx:Math.round(Math.cos(sideproj * (Math.PI / 180)) * 1000 * entity.firespeed) / 1000, bfromy: Math.round(Math.sin(sideproj * (Math.PI / 180)) * 1000 * entity.firespeed) / 1000, bounce: Math.random(), damage: entity.damage})
		.image("game_images_sprites/orange1.png","no-repeat")
			.bind('EnterFrame', function()
	    	{
	        	//player bullet
        		if(this.hit('solid') || this.hit('boundary') || this.hit('monsters') || this.hit('monsterportals'))
		        {
	        		if (this.bounce<.5)
	        		{
		            	this.rotation = this.rotation + 50;
	        		}
	        		else
	        		{
	        			this.rotation = this.rotation - 50;
	        		}
	        		if(Math.random()>.8)
	        		{
	            		projcount--;
	            		this.destroy();
	        		}
		        }
		        else
		        {
		            this.x = this.x + this.bfromx;
		            this.y = this.y + this.bfromy;
		        }
		    });
		}
	};

	this.VortexProjMob = function(entity)
	{
		if (projcount<maxProj)
		{
			if(projcount<0)
			{
				alert("projectile count error");
			}
			projcount++;
			var bullet_w=32;
			var bullet_h=32;
			var sideproj=entity._rotation-90;
			
			entity.centery=entity._y+(entity._h/2);
			entity.centerx=entity._x+(entity._w/2);
			 
			var bulletx=entity.centerx+Math.round(30*Math.cos((entity.rotation-90)*Math.PI/180));
			var bullety=entity.centery+Math.round(30*Math.sin((entity.rotation-90)*Math.PI/180));
			
			var offsetx=Math.round((bullet_w/2)*Math.sin((90-entity.rotation)*Math.PI/180));
			var offsety=Math.round((bullet_w/2)*Math.cos((90-entity.rotation)*Math.PI/180));
			
			bulletx=bulletx-offsetx;
			bullety=bullety-offsety;

		var damager = Crafty.e("2D, DOM, Color, Image, AI, Collision, pbullet")
			.attr({w:bullet_w, h:bullet_h, z:2, x: bulletx, death:0, y: bullety, rotation: entity._rotation, AId: sideproj, btype:0, bfromx:bulletx, bfromy: bullety, bounce: Math.random(), damage: 1})
			.image("game_images_sprites/poison.png","repeat")
			.bind('EnterFrame', function()
	    	{
				if(this.btype == 0)
	    		{
		        		this.h=64;
		        		this.w=64;
		        		this.origin("center");
		        		this.removeComponent('pbullet');
		        		this.addComponent('monsters, solid');
		        		for(var i=0; i<9; i++)
		        		{
			        		if(this.hit('solid') || this.hit('safe') || this.hit('boundary'))
					        {
					           	this.death=1;
					        }
			        		else
			        		{
			        			this.rotation=this.rotation-i*10;
			        		}
		        		}
		        		if(this.death==1)
		        		{
			            	mprojcount--;
			            	this.destroy();
		        		}
		        		this.btype=1;
						this.bfromx=this.x;
		        		this.bfromy=this.y;
		        		if(this.death == 0)
			            {
				            this.timeout(function()
			                {
				            	this.btype=2;
				        		loadwave3(this.x,this.y);
				        		loadwave3(this.x,this.y);
				            	this.timeout(function()
						        {
				            		if(this.death == 0)
						            {
						            	this.death=1;
						            	mprojcount--;
					            		this.destroy();
						            }
						        }, 100);
			                }, 5000);
			            }
	    		}
	    		else if (this.btype ==1)
	    		{
	    			this.rotation=this.rotation-50;
	    			this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * 4) / 1000;
		            this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * 4) / 1000;
					if(this.hit('solid') || this.hit('safe') || this.hit('boundary'))
			        {
						this.x=this.bfromx;
		        		this.y=this.bfromy;
						this.AId= Math.floor(Math.random()*45)*8;
			        }
					else
					{
						this.bfromx=this.x;
		        		this.bfromy=this.y;
		        		ghostspawnx=this.x;
		        		ghostspawny=this.y;
		        		monsterspawnx=this.x;
		        		monsterspawny=this.y;
					}
			        if(this.hit('player'))
			        {	
			        	this.rotation=this.rotation-50;
			        }
	        	}
	    		else
	    		{
	    			this.rotation=this.rotation-50;
	    		}
		    });
		}
	};

	this.stunProjMob = function(entity)
	{
		if (mprojcount<maxMobProj)
		{
			if(mprojcount<0)
			{
				alert("projectile count error");
			}
			mprojcount++;

			var entityHit;

			var bullet_w=64;
			var bullet_h=32;
			var sideproj=entity._rotation-90;
			
			entity.centery=entity._y+(entity._h/2);
			entity.centerx=entity._x+(entity._w/2);
			 
			var bulletx=entity.centerx+Math.round(30*Math.cos((entity.rotation-90)*Math.PI/180));
			var bullety=entity.centery+Math.round(30*Math.sin((entity.rotation-90)*Math.PI/180));
			
			var offsetx=Math.round((bullet_w/2)*Math.sin((90-entity.rotation)*Math.PI/180));
			var offsety=Math.round((bullet_w/2)*Math.cos((90-entity.rotation)*Math.PI/180));
			
			bulletx=bulletx-offsetx;
			bullety=bullety-offsety;

		var damager = Crafty.e("2D, DOM, Color, Image, AI, Collision, monsters")
		.attr({w:bullet_w, h:bullet_h, z:2, x: bulletx, death:0, y: bullety, rotation: entity._rotation, AId: sideproj, btype:0, bfromx:bulletx, bfromy: bullety, bounce: Math.random(), damage: 1})
		.image("game_images_sprites/ice1.png","repeat")
		.origin("center")
		.bind('EnterFrame', function()
	    	{
	        	//ice stun bullet
				if(this.btype == 0)
	        	{
						this.bfromx=this.x;
		        		this.bfromy=this.y;
		        		this.btype = 1;
		        		this.timeout(function()
				        {
			        		if(this.death==0)
				            {
				            	this.timeout(function()
						        {
				            		mprojcount--;
					            	this.destroy();
						        }, 100);
			            	}
			        		else
			        		{
			        			this.timeout(function()
						        {
			        				mprojcount--;
					            	this.destroy();
						        }, difficulty * 1500);
			        		}
				        }, difficulty * 500);
	        		}
	        		else if (this.btype ==1 && this.death!=1)
	        		{
	        			if(this.hit('player'))
	        			{
	        				this.death=1;
	        				this.addComponent('solid');
	        			}
	        			else
	        			{
        					this.rotation=this.rotation+50;
				            this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * pbulletspeed) / 1000;
		        			if(this.hit('safe') || this.hit('boundary') || this.hit('pbullet') || this.hit('solid'))
					        {
			        			this.x=this.bfromx;
					        }
					        else
					        {
					        	this.bfromx=this.x;
					        }
		        			this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * pbulletspeed) / 1000;
		        			if(this.hit('safe') || this.hit('boundary') || this.hit('pbullet'))
					        {
			        			this.y=this.bfromy;
					        }
					        else
					        {
					        	this.bfromy=this.y;
					        }
	        			}
		        	}
		        	else if(this.death==1)
		        	{
		        		this.rotation=this.rotation+50;
		        	}
		    })
			.bind("MessureFPS", function()
	 		{
				for(var i = 0; i<5; i++)
				{
					this.timeout(function()
					{
						if(this.hit('player'))
				        {
				        	entityHit = this.hit("player");
							for(var i = 0; i<entityHit.length; i++)
							{
								entityHit[i].obj.health = entityHit[i].obj.health - this.damage;
							}
				        }
		        	}, i*200);
				}
	 		});
		}
	};
	this.VortexPlayer = function(entity)
	{
		if (projcount<maxProj)
		{
			if(projcount<0)
			{
				alert("projectile count error");
			}
			projcount++;
			var bullet_w=32;
			var bullet_h=32;
			var sideproj=entity._rotation-90;
			
			entity.centery=entity._y+(entity._h/2);
			entity.centerx=entity._x+(entity._w/2);
			 
			var bulletx=entity.centerx+Math.round(30*Math.cos((entity.rotation-90)*Math.PI/180));
			var bullety=entity.centery+Math.round(30*Math.sin((entity.rotation-90)*Math.PI/180));
			
			var offsetx=Math.round((bullet_w/2)*Math.sin((90-entity.rotation)*Math.PI/180));
			var offsety=Math.round((bullet_w/2)*Math.cos((90-entity.rotation)*Math.PI/180));
			
			bulletx=bulletx-offsetx;
			bullety=bullety-offsety;

		damager.attr({w:bullet_w, h:bullet_h, z:2, x: bulletx, death:0, y: bullety, rotation: entity._rotation, AId: sideproj, btype:0, bfromx:bulletx, bfromy: bullety, bounce: Math.random()})
		.image("game_images_sprites/orange1.png","no-repeat")
		.bind('EnterFrame', function()
	    	{
				if(this.btype == 0)
        		{
        			if(projcount<maxmobnum/2)
        			{
		        		for(var i=0; i<7; i++)
		        		{
			        		if(this.hit('safe') || this.hit('boundary'))
					        {
			        			if(this.death == 0)
					            {
					            	this.death=1;
					            }
					        }
			        		else
			        		{
			        			this.rotation=this.rotation-i*50;
			        		}
		        		}
		        		if(this.death==1)
		        		{
			            	projcount--;
			            	this.destroy();
		        		}
		        		this.btype=1;
						this.bfromx=this.x;
		        		this.bfromy=this.y;
        			}
        			else
        			{
        				if(this.death == 0)
			            {
			            	this.death=1;
	        				projcount--;
	        				this.destroy();
			            }
        			}
        		}
        		else if (this.btype==1)
        		{
        			this.btype=2;
        			this.image("game_images_sprites/orange1.png","repeat");
        			this.w=64;
        			this.origin("center");
        			this.timeout(function()
		                {
			            	this.btype=3;
			            	this.timeout(function()
					        {
			            		if(this.death == 0)
					            {
					            	this.death=1;
    			            	projcount--;
			            		this.destroy();
					            }
					        }, 100);
		                }, 1000);
        		}
        		else if(this.btype==2)
        		{
        			this.rotation=this.rotation-40;
        			this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * pbulletspeed) / 1000;
		            this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * pbulletspeed) / 1000;
					if(this.hit('safe') || this.hit('boundary'))
			        {
						this.x=this.bfromx;
		        		this.y=this.bfromy;
						this.AId= Math.floor(Math.random()*45)*8;
			        }
					else if(this.hit('solid'))
			        {
						if(Math.random()<difficulty)
						{
							this.AId= Math.floor(Math.random()*45)*8;
		        			this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * pbulletspeed) / 1000;
				            this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * pbulletspeed) / 1000;
				            if(this.hit('safe') || this.hit('boundary'))
					        {
								this.x=this.bfromx;
				        		this.y=this.bfromy;
								this.AId= Math.floor(Math.random()*45)*8;
					        }
						}
			        }
					else if(this.hit('monsters'))
					{
						if(Math.random()>difficulty)
						{
							createpulse(this.x, this.y);
							this.AId = Math.floor(Math.random()*45)*8;
						}
					}
					else
					{
						this.bfromx=this.x;
		        		this.bfromy=this.y;
					}
	        	}
        		else
        		{
        			this.rotation=this.rotation-40;
        		}
	        });
		}
	};
}

//--------------------------end new projectile and weapon system---------------------------------------







// - - - - past code here - - -

/*
var difficultyn = prompt("Please enter a difficulty between 100 and 1", "");
if (difficultyn!=null && 0<(difficultyn * 1) && (difficultyn * 1)<=100)
{
	if(isNaN(difficultyn))
	{
		difficultynum=2;
		alert("For ignoring instructions . . . you are at 200% difficulty")
	}
	else
	{
		difficultynum = Math.round(difficultyn *1)/100;
	}
}
*/









//things that have a chance of happening on impact with monsters
/*
function monsterbadeffect(entity)
{

	var selectbad = Math.ceil(Math.random()*8)
    if (selectbad == 1)
    {
    	if(Math.random()>mainHUD.difficulty())
    	{
    		fastheal(player[1]);
    	}
	}
	else if(selectbad == 2)
	{
		createproj(entity,1);
	}
    else if(selectbad == 3)
    {
    	if(Math.random()<mainHUD.difficulty()*.75)
    	{
    		//createportal(Math.floor(Math.random()*81)*16+300, Math.floor(Math.random()*100)*16+300, 0);
    	}
    }
    else if(selectbad == 4)
    {
    	if(Math.random()>difficultynum)
    	{
			createpulse(entity.x,entity.y);
    	}
    }
    else if(selectbad == 5)
    {
    	if(Math.random()>difficultynum)
    	{
			createproj(entity,0);
    	}
    }
    else if(selectbad == 6)
    {
    	if(Math.random()>mainHUD.difficulty())
    	{
			createripsym(entity.x,entity.y,1);
    	}
    }
    else if(selectbad == 7)
    {
    	if(Math.random()<mainHUD.difficulty())
    	{
			//wave1target(1);
			//wave1target(2);
    	}
    }
    else if(selectbad == 8)
    {
    	if(Math.random()>difficultynum)
    	{
			createproj(entity,2);
    	}
    }
}
*/
/*
//monster portal called every second by player
function createportal(posix, posiy, type)
{
	if(portalnum<0)
	{
		alert("portal error");
	}
        if(portalnum<portalmax)
        {
        	portalnum++;
            var monsterportal= Crafty.e('2D, DOM, Color, Image, monsterportals, Collision')
                .attr({w:58, h:50, z:1, x: posix, y: posiy, AId: 0, active:0, health:portalhp, death:0})
				.image("game_images_sprites/portal.png","repeat")
				.origin("center")
                .bind("EnterFrame", function()
                {
                	if(this.active ==0)
            		{
                		for(var i=0; i<11; i++)
		        		{
			        		if(this.hit('solid') || this.hit('safe') || this.hit('boundary'))
					        {
			        			this.death=1;
					        }
			        		else
			        		{
			        			this.rotation=this.rotation-i*36;
			        		}
		        		}
                		if(this.death==1)
                		{
                			portalnum--;
	                    	this.destroy();
                		}
                        this.active=1;
            		}
                	else
                	{
						this.rotation= this.rotation+36;
                		if(this.hit('pbullet'))
	                    {
		                    this.health = this.health - pbulletdamage;
		                    if (this.hit('shield'))
		                    {
		                        this.health= this.health - pbulletdamage;
		                    }
		                    if(this.health<0)
	                        {
		                    	if(Math.random() <= difficultynum)
				                {
		        					monsterbadeffect(this);
				                }
	                            createripsym(this.x,this.y, 1);
	                            if(maxmobnum>mnumber)
	                            {
									reapercreate(1);
									reapercreate(2);
	                            }
	                            portalnum--;
	                            this.destroy();
	                        }
						}
						else
						{
							ghostspawnx = this.x;
							ghostspawny = this.y;
							monsterspawnx = this.x;
							monsterspawny = this.y;
							loadwave1();
						}
                	}
                });
	        }
}
*/


















/*
var noproj = 0;
var mproj = 0;
function createproj(entity,type)
{
	if (projcount<maxmobnum)
	{
		if(type==1)
		{
			noproj++;
		}
		if(projcount<0)
		{
			alert("projectile count error");
		}
		projcount++;
		bullet_w=32;
		bullet_h=32;
		sideproj=entity._rotation-90;
		
		entity.centery=entity._y+(entity._h/2);
		entity.centerx=entity._x+(entity._w/2);
		 
		bulletx=entity.centerx+Math.round(30*Math.cos((entity.rotation-90)*Math.PI/180));
		bullety=entity.centery+Math.round(30*Math.sin((entity.rotation-90)*Math.PI/180));
		
		offsetx=Math.round((bullet_w/2)*Math.sin((90-entity.rotation)*Math.PI/180));
		offsety=Math.round((bullet_w/2)*Math.cos((90-entity.rotation)*Math.PI/180));
		
		bulletx=bulletx-offsetx;
		bullety=bullety-offsety;
		var proj = Crafty.e("2D, DOM, Color, Image, AI, Collision, pbullet") 
		    .image("game_images_sprites/orange1.png","no-repeat")
		    .attr({w:bullet_w, h:bullet_h, z:2, x: bulletx, death:0, y: bullety, rotation: entity._rotation, AId: sideproj, btype:0, bfromx:bulletx, bfromy: bullety, bounce: Math.random()})
	        .bind('EnterFrame', function()
	    	{
	        	//player bullet
	        	if(type==0)
	        	{
	        		if(this.death==1)
	        		{
	        			if(this.bounce<.5)
	        			{
	        				this.rotation=this.rotation+40;
	        			}
	        			else
	        			{
	        				this.rotation=this.rotation-40;
	        			}
	        		}
	        		else if(this.hit('solid') || this.hit('boundary') || this.hit('monsters') || this.hit('monsterportals'))
			        {
		        		if (this.bounce<.5)
		        		{
			            	this.rotation = this.rotation + 50;
		        		}
		        		else
		        		{
		        			this.rotation = this.rotation - 50;
		        		}
		        		if(this.death==0)
		        		{
		        			this.death=1;
		        			this.timeout(function()
		   					{
			            		projcount--;
			            		this.destroy();
		   					}, 200);
		        		}
			        }
			        else
			        {
			        	this.bfromx=this.x;
		        		this.bfromy=this.y;
			            this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * pbulletspeed) / 1000;
			            this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * pbulletspeed) / 1000;
			        }
	        	}
	        	//robot ice stun bullet
	        	else if(type==3)
		       	{
	        		if(this.btype == 0)
	        		{
	        			projcount--;
	        			mproj++;
	        			if(mproj>10)
	        			{
	        				mproj--;
	        				this.destroy();
	        			}
        				this.image("game_images_sprites/ice1.png","repeat");
        				this.w=64;
        				this.origin("center");
        				this.removeComponent('pbullet');
		        		this.addComponent('monsters');
		        		this.btype=1;
						this.bfromx=this.x;
		        		this.bfromy=this.y;
		        		this.timeout(function()
				        {
			        		if(this.death==0)
				            {
				            	this.timeout(function()
						        {
				            		mproj--;
					            	this.destroy();
						        }, 100);
			            	}
			        		else
			        		{
			        			this.timeout(function()
						        {
			        				mproj--;
					            	this.destroy();
						        }, mainHUD.difficulty() * 1500);
			        		}
				        }, mainHUD.difficulty() * 500);
	        		}
	        		else if (this.btype ==1)
	        		{
	        			if(this.hit('player'))
	        			{
	        				this.death=1;
	        				this.rotation=this.rotation+50;
	        				if (this.bounce<.5)
	        				{
	        					this.x=player[1].x;
	        					this.y=player[1].y;
	        				}
	        				else
	        				{
	        					this.x=robomind.x;
	        					this.x=robomind.y;
	        				}
	        				this.addComponent('solid');
	        			}
	        			else
	        			{
        					this.rotation=this.rotation+50;
				            this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * pbulletspeed) / 1000;
		        			if(this.hit('safe') || this.hit('boundary') || this.hit('pbullet') || this.hit('solid'))
					        {
			        			this.x=this.bfromx;
					        }
					        else
					        {
					        	this.bfromx=this.x;
					        }
		        			this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * pbulletspeed) / 1000;
		        			if(this.hit('safe') || this.hit('boundary') || this.hit('pbullet'))
					        {
			        			this.y=this.bfromy;
					        }
					        else
					        {
					        	this.bfromy=this.y;
					        }
	        			}
		        	}
		       	}
	        	//monster spawning vortex that causes player stun on hit
	        	else if(type==1)
	        	{
	        		if(this.btype == 0)
	        		{
	        			if(noproj<5)
	        			{
	        				this.image("game_images_sprites/poison.png","repeat");
			        		this.h=64;
			        		this.w=64;
			        		this.origin("center");
			        		this.removeComponent('pbullet');
			        		this.addComponent('monsters, solid');
			        		for(var i=0; i<9; i++)
			        		{
				        		if(this.hit('solid') || this.hit('safe') || this.hit('boundary'))
						        {
						           	this.death=1;
						        }
				        		else
				        		{
				        			this.rotation=this.rotation-i*10;
				        		}
			        		}
			        		if(this.death==1)
			        		{
			        			noproj--;
				            	projcount--;
				            	this.destroy();
			        		}
			        		this.btype=1;
							this.bfromx=this.x;
			        		this.bfromy=this.y;
			        		if(this.death == 0)
				            {
					            this.timeout(function()
				                {
					            	this.btype=2;
					        		loadwave3(this.x,this.y);
					        		loadwave3(this.x,this.y);
					            	this.timeout(function()
							        {
					            		if(this.death == 0)
							            {
							            	this.death=1;
							            	projcount--;
							            	noproj--;
						            		this.destroy();
							            }
							        }, 100);
				                }, 5000);
				            }
	        			}
	        			else
	        			{
	        				if(this.death == 0)
				            {
		        				projcount--;
		        				this.destroy();
				            }
	        			}
	        		}
	        		else if (this.btype ==1)
	        		{
	        			this.rotation=this.rotation-50;
	        			this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * 4) / 1000;
			            this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * 4) / 1000;
						if(this.hit('solid') || this.hit('safe') || this.hit('boundary'))
				        {
							this.x=this.bfromx;
			        		this.y=this.bfromy;
							this.AId= Math.floor(Math.random()*45)*8;
				        }
						else
						{
							this.bfromx=this.x;
			        		this.bfromy=this.y;
			        		ghostspawnx=this.x;
			        		ghostspawny=this.y;
			        		monsterspawnx=this.x;
			        		monsterspawny=this.y;
						}
				        if(this.hit('player'))
				        {	
				        	player[1].gold = player[1].gold - Math.ceil(player[1].gold/100);
				        	this.rotation=this.rotation-50;
				        }
		        	}
	        		else
	        		{
	        			this.rotation=this.rotation-50;
	        		}
	        	}
	        	//player bullet vortex
	        	else if(type==2)
	        	{
	        		if(this.btype == 0)
	        		{
	        			if(projcount<maxmobnum/2)
	        			{
			        		for(var i=0; i<7; i++)
			        		{
				        		if(this.hit('safe') || this.hit('boundary'))
						        {
				        			if(this.death == 0)
						            {
						            	this.death=1;
						            }
						        }
				        		else
				        		{
				        			this.rotation=this.rotation-i*50;
				        		}
			        		}
			        		if(this.death==1)
			        		{
				            	projcount--;
				            	this.destroy();
			        		}
			        		this.btype=1;
							this.bfromx=this.x;
			        		this.bfromy=this.y;
	        			}
	        			else
	        			{
	        				if(this.death == 0)
				            {
				            	this.death=1;
		        				projcount--;
		        				this.destroy();
				            }
	        			}
	        		}
	        		else if (this.btype==1)
	        		{
	        			this.btype=2;
	        			this.image("game_images_sprites/orange1.png","repeat");
	        			this.w=64;
	        			this.origin("center");
	        			this.timeout(function()
   		                {
   			            	this.btype=3;
   			            	this.timeout(function()
   					        {
   			            		if(this.death == 0)
   					            {
   					            	this.death=1;
        			            	projcount--;
    			            		this.destroy();
   					            }
   					        }, 100);
   		                }, 1000);
	        		}
	        		else if(this.btype==2)
	        		{
	        			this.rotation=this.rotation-40;
	        			this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * pbulletspeed) / 1000;
			            this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * pbulletspeed) / 1000;
						if(this.hit('safe') || this.hit('boundary'))
				        {
							this.x=this.bfromx;
			        		this.y=this.bfromy;
							this.AId= Math.floor(Math.random()*45)*8;
				        }
						else if(this.hit('solid'))
				        {
							if(Math.random()<mainHUD.difficulty())
							{
								this.AId= Math.floor(Math.random()*45)*8;
			        			this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * pbulletspeed) / 1000;
					            this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * pbulletspeed) / 1000;
					            if(this.hit('safe') || this.hit('boundary'))
						        {
									this.x=this.bfromx;
					        		this.y=this.bfromy;
									this.AId= Math.floor(Math.random()*45)*8;
						        }
							}
				        }
						else if(this.hit('monsters'))
						{
							if(Math.random()>mainHUD.difficulty())
							{
								createpulse(this.x, this.y);
								this.AId = Math.floor(Math.random()*45)*8;
							}
						}
						else
						{
							this.bfromx=this.x;
			        		this.bfromy=this.y;
						}
		        	}
	        		else
	        		{
	        			this.rotation=this.rotation-40;
	        		}
	        	}
		    });
	}
}
//end projectiles for entity
*/


                                         
/*                                               
//create a AI controlled shield power-up that has a random chance of respawning
function createshield(posix,posiy)
{
    if(shieldcount<shieldmax)
    {
    	shieldcount++;
        shieldpowerup[shieldcount] = Crafty.e("2D, Color, DOM, Image, AI, Collision, shield, MessureFPS, pbullet")
            .attr({w:500,z:0, h:500, x:posix, y:posiy, AId: 1, stuck:0, equip:0, px:0, py:0})
            .image("game_images_sprites/shieldpowerup.png","no-repeat")
            .timeout(function()
			{
				shieldcount--;
				playerpowershield=0;
				this.destroy();
			}, 5000)
            .bind('EnterFrame', function()
            {
				if(playerpowershield == 0)
				{
					if(this.hit('player'))
		            {
		                playerpowershield = shieldcount;
		            }
		            else if(this.hit('solid'))
		            {
	            		this.x = this.px;
	            		this.y = this.py;
						this.AId = this.AId -180;
						this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * 10) / 1000;
						this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * 10) / 1000;
						this.AId= Math.floor(Math.random()*45)*8;
					}
		            else
		            {
	            		this.px = this.x;
	            		this.py = this.y;
	            		this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * 4) / 1000;
						this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * 4) / 1000;
		            }
				}
				else
				{
	                playerpowershield = shieldcount;
	                this.w = 55;
	                this.h = 55;
					if(this.stuck == 0)
	                {
						this.stuck = 1;
						this.x = player[1].x + 24;
						this.y = player[1].y + 24;
					}
					else
					{
						this.stuck = 0;
						this.x = robomind.x + 16;
						this.y = robomind.y + 16;
					}
                    this.rotation = this.rotation + 70;
   				}
			})
			.bind('MessureFPS', function()
            {
				this.AId= Math.floor(Math.random()*45)*8;
			});
	}
}
//used to make blood that times out randomly
function createblood(posix, posiy)
{
	if (bloodcount<maxmobnum/2)
	{
		bloodcount++;
		mnumber++;
		blood[bloodcount] = Crafty.e("2D, DOM, Image, Collision, blood")
	    	.attr({w:50, h:50,z:0, x: posix, y: posiy, AId: 1, stuck:0})
	    	.image("game_images_sprites/blood.png","no-repeat")
			.timeout(function()
			{
				mnumber--;
           		bloodcount--;
           		this.destroy();
			}, 500);
	}
}
//create gold on death
function createripsym(posix,posiy,tempnum)
{
    deathcount++;
    if(lootcount<maxmobnum/2)
    {
        lootcreate(posix,posiy);
      	lootcount++;
	    ripsym[deathnum] = Crafty.e("2D, DOM, Image, Collision, deathsym, MessureFPS, Tween")
	        .image("game_images_sprites/gold.png","no-repeat")
	        .attr({w:32, h:32, x: posix, y: posiy})
	        .bind('MessureFPS', function() 
	        {
	        	this.tween({x: player[tempnum].x, y: player[tempnum].y}, 15)
	            if(this.hit('player'))
	            {
	            	player[tempnum].health++;
	                player[tempnum].gold = player[tempnum].gold + 20;
	                if(pbulletdamage<100)
	                {
	                	pbulletdamage++;
	                }
                	lootcount--;
                	this.destroy();
	          	}
	        });
    }
}
    

//determine what loot is created
function lootcreate(posix,posiy)
{
	createblood(posix,posiy);
	var select = Math.ceil(Math.random()*4)
    if (select == 1)
    {
		createshield(posix,posiy);
    }
    else if (select == 2)
    {
    	if(Math.random() <= mainHUD.difficulty())
    	{
			loadwave1();
    	}
    }
    else if (select == 3)
    {
    	if(Math.random() >= mainHUD.difficulty())
    	{
        	createpulse(posix,posiy);
    	}
    }
    else if (select == 4)
    {
    	if(Math.random() <= mainHUD.difficulty())
    	{
        	reapercreate(1);
       		reapercreate(2);
    	}
    }
}
//spawn death boss
function reapercreate(target)
{
	if(bossnum<bossmax && mnumber<maxmobnum)
	{
		mnumber++;
		bossnum++;
		monster[mnumber] = Crafty.e("2D, DOM, Image, death, Collision, AI, monsters, MessureFPS, Tween")
		    .attr({w:330, h:290, x: monsterspawnx -160,y: monsterspawny -150, AId:1, health: monster4hp, mspeed:100, alhpa:0.8})
		    .image("game_images_sprites/death.png","no-repeat")
		    .origin("center")
		    .bind('EnterFrame', function() 
		    {
		       	if(this.hit('pbullet'))
		        {
		            this.health= this.health - pbulletdamage;
		            if (this.hit('shield'))
		            {
						this.health= this.health - pbulletdamage;
		            }
		            if(this.health <0)
		            {
						mnumber--;
						bossnum--;
						createripsym(this.x,this.y,target);
						this.destroy();
					}
		        }
		        else if(this.hit('player'))
		        {
		        	if(this.mspeed>10)
		        	{
		        		this.mspeed=this.mspeed-5;
		        	}
		        }
		        else if(this.hit('safe') || this.hit('boundary'))
				{
					this.x = monsterspawnx;
					this.y = monsterspawny;
				} 
			})
			.bind('MessureFPS', function()
			{
				this.tween({alpha: 0.2, x:player[target].x, y:player[target].y}, this.mspeed);
           });
	}
}
var ad1num = 0;
function adaptivesolid(posix, posiy, target)
{
	if(maxmobnum>mnumber)
	{
	mnumber++;
	ad1num++;
	monster[ad1num] = Crafty.e("2D, DOM, Image, Collision, AIadaptive, monsters, Tween, MessureFPS")
	    .attr({w:32, h:50, x: posix, y: posiy, currrandom: Math.random(), AId:1, oldfromx: monsterspawnx, oldfromy: monsterspawny, stuck:0, health: monster2hp, mspeed:6})
	    .image("game_images_sprites/sprite2.png","no-repeat")
 		.bind("EnterFrame", function()
 		{
 			this.currrandom = Math.random();
			this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * this.mspeed) / 1000;
           	if(this.hit('boundary'))
           	{
				mnumber--;
				ad1num--;
				this.destroy();
           	}
           	else if(this.hit('solid') || this.hit('safe'))
	        {
				this.x = this.oldfromx;
	        }
           	else
           	{
				this.oldfromx=this.x;
           	}
           	this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * this.mspeed) / 1000;
           	if(this.hit('boundary'))
           	{
				mnumber--;
				ad1num--;
				this.destroy();
           	}
           	else if(this.hit('solid') || this.hit('safe'))
	        {
       			this.y = this.oldfromy;
	        }
           	else
           	{
				this.oldfromy=this.y;
           	}
           	if(this.hit('monsters'))
			{
				if(this.currrandom <= difficultynum)
				{
					if(this.mspeed<8)
		        	{
		        		this.mspeed=10;
		        		this.timeout(function()
	   	    		    {
		        			this.mspeed=6;
	   	    	        }, 750);
		        	}
					if(Math.random() <= difficultynum/2)
					{
						if(Math.random() <.5)
						{
		       				this.AId = (Math.atan2(this.y - player[target].y, this.x - player[target].x) * (180 / Math.PI) + 180);
						}
						else
						{
							this.AId = Math.floor(Math.random()*45)*8;
						}
					}
				}
			}
			else if(this.hit('pbullet'))
	        {
	            this.health= this.health - pbulletdamage;
	            if (this.hit('shield'))
	            {
					this.health= this.health - pbulletdamage;
	            }
	            if(this.health <0)
	            {
					mnumber--;
					ad1num--;
					if(this.currrandom <= difficultynum)
                    {
						monsterbadeffect(this);
                    }
					createripsym(this.x,this.y,target);
					this.destroy();
				}
	        }
			else if(this.hit('player'))
	        {	
	        	player[target].health = player[target].health - Math.ceil(player[target].health/100);
	        	adaptivesolid(this.x, this.y, 1);
	        	adaptivesolid(this.x, this.y, 2);
	        	if(this.mspeed<11)
	        	{
	        		this.mspeed=11;
	        	}
	        }
 		})
 		.bind("MessureFPS", function()
 		{
 			if(Math.random() <.25)
			{
   				this.AId = (Math.atan2(this.y - player[target].y, this.x - player[target].x) * (180 / Math.PI) + 180);
			}
			else
			{
				this.AId = Math.floor(Math.random()*45)*8;
			}
 		});
	}
}

function monster2create()
{
mnumber++;
monster[mnumber] = Crafty.e("2D, DOM, Image, monsters, Collision, AI")
    .attr({w:50, h:50, x: monsterspawnx ,y: monsterspawny, AId:1, AIid:1, stuck:0, health: monster2hp})
    .image("game_images_sprites/sprite3.png","no-repeat")
    .bind('EnterFrame', function() 
    {
        if(this.hit('solid'))
        {
            this.stuck = this.stuck + 1;
            if(this.stuck>50)
            {  
                this.x = monsterspawnx;
                this.y = monsterspawny;
            }
            this.AId = this.AId -180;
            this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * 10) / 1000;
            this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * 10) / 1000;
        }
        else if(this.hit('pbullet'))
        {
                this.health= this.health - pbulletdamage;
                if (this.hit('shield'))
                {
                        this.health= this.health - pbulletdamage;
                }
                if(this.health >0)
                {
                        if(Math.random() <= mainHUD.difficulty())
                        {
                                monsterbadeffect(this);
                        }
                }
                else
                {
                        mnumber--;
                        createripsym(this.x,this.y, Math.floor(Math.random()*3)+1);
                        this.destroy();
                }
        }
        else if(this.hit('weak'))
        {
                if(mnumber+3<maxmobnum)
            {
                monster2create();
                monster2create();
                monster2create();
                if (Math.random() <= mainHUD.difficulty())
                {
                    if(monsterspeed<10)
                    {
                        monsterspeed++;
                    }
                }
                if(Math.random() <=.5)
                {
					this.AId = this.AId -90;
                }
                else
                {
					this.AId = this.AId +90;        
                }
                this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * monsterspeed) / 1000;
                this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * monsterspeed) / 1000;
                this.AId = Math.floor(Math.random()*360);
            }
            this.x = monsterspawnx;
            this.y = monsterspawny;
        }
        else if(this.hit('blood'))
        {
            mnumber--;
            this.destroy();
        }
        else if(this.hit('monsters'))
        {
                if(Math.random() <=.5)
                {
                        this.AId = this.AId -90;
                }
                else
                {
                        this.AId = this.AId +90;        
                }
            this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * monsterspeed) / 1000;
            this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * monsterspeed) / 1000;
        }
        else if(this.hit('death'))
        {
            loadwave1();
            mnumber--;
            this.destroy();
        }
        else
        {
            if (Math.random() <= .1)
            {
                this.AId = Math.floor(Math.random()*360);
                this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * monsterspeed) / 1000;
                this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * monsterspeed) / 1000;
            }
                else
                {
                        this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * 1) / 1000;
                        this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * 1) / 1000;
                }
        }               
    });
}
function monster1create()
{
mnumber++;
monster[mnumber] = Crafty.e("2D, DOM, Image, monsters, Collision, AI")
    .attr({w:50, h:50, z:1, x: monsterspawnx ,y: monsterspawny, AId:1, stuck:0, health:monster1hp})
    .image("game_images_sprites/sprite2.png","no-repeat")
    .bind('EnterFrame', function(from) 
    {
        if(this.hit('solid'))
        {
            this.stuck = this.stuck+1;
            if(this.stuck>50)
            {
                this.x = monsterspawnx;
                this.y = monsterspawny;
            }
            this.AId = this.AId-180;
            this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * 10) / 1000;
            this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * 10) / 1000;
        }
        else if(this.hit('pbullet'))
        {
			this.health= this.health - pbulletdamage;
			if (this.hit('shield'))
			{
				this.health= this.health - pbulletdamage;
			}
			if(this.health >0)
			{
		        if(Math.random() <= mainHUD.difficulty())
		        {
					monsterbadeffect(this);
		        }
			}
			else
			{
		        mnumber--;
		        createripsym(this.x,this.y, Math.floor(Math.random()*3)+1);
		        this.destroy();
			}
        }
        else if(this.hit('weak'))
        {
            if(mnumber<maxmobnum)
            {
                if (Math.random() <= mainHUD.difficulty())
                {
					if(mnumber +5<maxmobnum)
					{
	                    monster1create();
	                    monster1create();
	                    monster1create();
	                    monster1create();
	                    monster1create();
					}
                    if(monsterspeed<10)
                    {
                        monsterspeed++;
                    }
                }
            }
            if(Math.random() <= 0.5)
			{
                this.AId = this.AId -90;
			}
			else
			{
				this.AId = this.AId +90;        
			}
            this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * monsterspeed) / 1000;
            this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * monsterspeed) / 1000;
        }
        else if(this.hit('monsters'))
        {
			if(Math.random() <= 0.5)
			{
				this.AId = this.AId -90;
			}
			else
			{
				this.AId = this.AId +90;        
			}
            this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * monsterspeed) / 1000;
            this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * monsterspeed) / 1000;
            this.AId = Math.floor(Math.random()*360);     
        }
        else if(this.hit('death'))
        {
            loadwave1(); 
            this.destroy();      
        }
        else
        {
            if (Math.random() <= .1)
            {
                this.AId = Math.floor(Math.random()*360);
                this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * monsterspeed) / 1000;
                this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * monsterspeed) / 1000;
            }
            else
            {
                this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * monsterspeed) / 1000;
                this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * monsterspeed) / 1000;
            }
        }               
    });
}
*/            













// Previous code section
/*
	    .bind('EnterFrame', function() 
	    {
	        if(this.AItype==0)
	        {
	                this.AId = (Math.atan2(this.y - player[1]._y, this.x - player[1]._x) * (180 / Math.PI) + 180);
	                this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * this.mspeed) / 1000;
	            this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * this.mspeed) / 1000;
	        }
	        else if(this.AItype==1)
	        {
	                this.AId = (Math.atan2(this.y - player[1]._y, this.x - player[1]._x) * (180 / Math.PI) + 135);
	                this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * this.mspeed) / 1000;
	            this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * this.mspeed) / 1000;
	        }
	        else if(this.AItype==2)
	        {
	                this.AId = (Math.atan2(this.y - player[1]._y, this.x - player[1]._x) * (180 / Math.PI) + -135);
	                this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * this.mspeed) / 1000;
	            this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * this.mspeed) / 1000;
	        }
	        else if(this.AItype==3)
	        {
	                this.AId = (Math.atan2(this.y - player[1]._y, this.x - player[1]._x) * (180 / Math.PI) -90);
	                this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * this.mspeed) / 1000;
	            this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * this.mspeed) / 1000;
	        }
	        else if(this.AItype==4)
	        {
	                this.AId = (Math.atan2(this.y - player[1]._y, this.x - player[1]._x) * (180 / Math.PI) + 180);
	                this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * this.mspeed) / 1000;
	            this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * this.mspeed) / 1000;
	        }
	        this.AItype = Math.round(Math.random*4);
	        //this.AItype=3;
	        this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * 1000 * this.mspeed*1.5) / 1000;
	        this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * 1000 * this.mspeed*1.5) / 1000;     
	        if(this.hit('boundary'))
	        {
	            this.stuck = this.stuck+1;
	            if(this.stuck>10)
	            {
	                this.x = ghostspawnx;
	                this.y = ghostspawny;
	            }
	        }
	        else if(this.hit('pbullet'))
	        {
				this.health= this.health - pbulletdamage;
				if (this.hit('shield'))
				{
					this.health= this.health - pbulletdamage;
				}
				if(this.health >0)
				{
					if(Math.random() >= difficultynum)
					{
						monsterbadeffect(this);
					}
				}
				else
				{
					mnumber--;
					createripsym(this.x,this.y);
					this.destroy();
				}
	        }
	        else if(this.hit('player'))
	        {
	                if(mnumber<maxmobnum)
	            {
	                loadwave1();
	                player[1].x = Math.floor(Math.random()*100)+700;
	                player[1].y = Math.floor(Math.random()*100)+1665;
	                player[2].x = Math.floor(Math.random()*100)+700;
	                player[2].y = Math.floor(Math.random()*100)+1665;
	            }
	            this.destroy();
	        }
	        else if(this.hit('ghost'))
	        {
				if(Math.random() <= difficultynum)
				{
					if(this.mspeed<3)
					{
						this.mspeed=3;
					}
				}
	        }
	         else if(this.hit('death'))
	        {
				if (Math.random() <= .001)
	             {
					if(mnumber<=maxmobnum)
					{
						loadwave1();
					}
					this.x = ghostspawnx;
	                this.y = ghostspawny;
	             }
	        }       
	    });
	 */













//pentity1 is the player, pentity 2 is the playerbox
function mainplayer(pentity1, pentity2, playernum)
{
	pentity1.image("game_images_sprites/sprite.gif","no-repeat")
	   .attr({z:1, w:48, h:48, x:10, y:900, level:1, gold:100, score:0, health:100, weaponuse:0, weapontype:0, ability:0 , pfire:0, deathcount: 0, playernum: playernum, firespeed: 10, damage: 1})
	   .origin("center")
	   .bind('MessureFPS', function()
		{
			this.score = Math.floor((mainHUD.difficulty()*10*this.level*(deathcount + this.gold + this.health))/((deathnum + 1)));
			if(this.ability==1)
			{
				equipability(this, 1);
			}
			else
			{
				this.timeout(function()
				{
					fastheal(this);
				}, 200);
				this.timeout(function()
				{
					fastheal(this);
				}, 400);
				this.timeout(function()
				{
					fastheal(this);
				}, 600);
				this.timeout(function()
				{
					fastheal(this);
				}, 800);
			}
		})
		.bind('EnterFrame', function()
	    {
			if(this.pfire==1)
			{
			//player weapon types I have currently made are
				//shotgun(this);
				//createpulse(this.x,this.y) // do not use because it puts a pulse every frame
				//burstfireriffle2(this);
				//burstfireriffle(this);                   
				//createmedic(this); // currently broken
				if(this.weapontype==0)
				{
					burstfireriffle(this);
				}
				else if(this.weapontype==1)
				{
					burstfireriffle2(this);
				}
				else if(this.weapontype==2)
				{
					shotgun(this);
				}
				else if(this.weapontype==3)
				{
					shotgun2(this);
				}
				else if(this.weapontype==4)
				{
					createmedic(this);
				}
				if(this.weapontype==0)
				{
					if(this.weaponuse ==0)
					{
						burstfireriffle(this);
					}
					else if(this.weaponuse == 1)
					{
						this.weaponuse = 2;
						this.timeout(function()
						{
				   			this.weaponuse = 0;
						}, 200);
					}
				}
				else if(this.weapontype==1)
				{
					if(this.weaponuse ==0)
					{
						burstfireriffle2(this);
					}
					else if(this.weaponuse == 1)
					{
						this.weaponuse = 2;
						this.timeout(function()
						{
				   			this.weaponuse = 0;
						}, 500);
					}
				}
				else if(this.weapontype==2)
				{
					if(this.weaponuse ==0)
					{
						shotgun(this);
					}
					else if(this.weaponuse == 1)
					{
						this.weaponuse = 2;
						this.timeout(function()
						{
				   			this.weaponuse = 0;
						}, 400);
					}
				}
				else if(this.weapontype==3)
				{
					if(this.weaponuse ==0)
					{
						shotgun2(this);
					}
					else if(this.weaponuse == 1)
					{
						this.weaponuse = 2;
						this.timeout(function()
						{
				   			this.weaponuse = 0;
						}, 950);
					}
				}
				else if(this.weapontype==4)
				{
					if(this.weaponuse ==0)
					{
						createmedic(this);
					}
					else if(this.weaponuse == 1)
					{
						this.weaponuse = 2;
						this.timeout(function()
						{
				   			this.weaponuse = 0;
						}, 1000);
					}
				}
		    }
    		if (this.health<0)
   			{
   				GameOver(this, 98, 898);
   				this.health=100;
   			}
   			if (this.gold<0)
			{
				GameOver(this, 98, 898);
				this.gold=100;
			}
	    })
		.bind('Moved', function(from) 
		{
			if(pentity2.hit('solid'))
			{
	           this.attr({x: from.x, y:from.y});
	        }
	        if (pentity2.hit('portalup'))
	        {
	            worldBkg.image("game_images_sprites/floor2ite.png","no-repeat");
	            deletefloor1();
	            loadfloor2();
	        }
	        else if (pentity2.hit('portaldown')){
	            worldBkg.image("game_images_sprites/floor1ite.png","no-repeat");
	            deletefloor2();
	            loadfloor1();
	        }
	        if(this.hit('shield'))
	        {
	            playerpowershield=1;
	        }   
	    })
	   .bind('KeyDown', function(e)
		{
			if(e.key == Crafty.keys['C'])
			{
				if(curfloor==1)
				{
					if(wallcolor == 0)
					{
						addcolor1();
						wallcolor = 1;
					}
					else
					{
						deletecolor1();
						wallcolor = 0;
					}
				}
				else if(curfloor==2)
				{
					if(wallcolor == 0)
					{
						addcolor2();
						wallcolor = 1;
					}
					else
					{
						deletecolor2();
						wallcolor = 0;
					}
				}
			}
	        else if(e.key == Crafty.keys['Q'])
	     	{
				if(this.weapontype==0)
				{
					this.weapontype=1;
				}
				else if(this.weapontype==1)
				{
					this.weapontype=2;
				}
				else if(this.weapontype==2)
				{
					this.weapontype=3;
				}
				else if(this.weapontype==3)
				{
					this.weapontype=4;
				}
				else if(this.weapontype==4)
				{
					this.weapontype=5;
				}
				else if(this.weapontype==5)
				{
					this.weapontype=0;
				}
			}
			else if(e.key == Crafty.keys['1'])
			{
				this.weapontype=0;
			}
			else if(e.key == Crafty.keys['2'])
			{
				this.weapontype=1;
			}
			else if(e.key == Crafty.keys['3'])
			{
				this.weapontype=2;
			}
			else if(e.key == Crafty.keys['4'])
			{
				this.weapontype=3;
			}
			else if(e.key == Crafty.keys['5'])
			{
				this.weapontype=4;
			}
			/*
			else if(e.key == Crafty.keys['6'])
			{
				if(this.ability ==0)
		    	{
			    	//display ability activate
			    	var abilitydisplay = Crafty.e("2D, DOM, Text")
			        .attr({x: HUD.x + 400, y: HUD.y+320, w: 500, z:5})
			        .textColor('#FF0000')
			        .text("Ability Activated But It Will Cost 10 Gold Per Second")
			        .timeout(function()
			        {
			        	this.destroy();
			        }, 1000);
			        HUD.attach(abilitydisplay);
			        pentity1.ability = 1;
		    	}
		    	else
		    	{
		    		//display turned off ability
		        	var abilitydisplay = Crafty.e("2D, DOM, Text")
		            .attr({x: HUD.x + 400, y: HUD.y+340, w: 500, z:5})
		            .textColor('#FF0000')
		            .text("Ability Off")
		            .timeout(function()
		            {
		            	this.destroy();
		            }, 1000);
		            HUD.attach(abilitydisplay);
		            pentity1.ability = 0;
		    	}
				this.weapontype=5;
			}
			*/
			else if(e.key == Crafty.keys['SPACE'])
			{
				this.pfire=1;
			}
			else if(e.key == Crafty.keys['P'])
			{
	           	var pposition = Crafty.e("2D, Text, DOM")
	           	.attr ({x: HUD.x, y: HUD.y+32, z:5, w:500, h:32})
	           	.textColor('#FF0000')
	           	.bind('EnterFrame', function()
	           	{
	           		this.text("Player1 Position: x: " + Math.floor(pentity1.x / 16) + " or " + Math.floor(pentity1.x / 16)*16 + " y: " + Math.floor(pentity1.y/16) +" or " + Math.floor(pentity1.y / 16)*16);
	           	});
	           	HUD.attach(pposition);
				var ppositionpixel1 = Crafty.e("2D, Text, DOM, Color")
				.attr({x:16, y:16, h:16, w:16, z:5})
				.color("red")
	           .timeout(function()
				{
					this.destroy();
				}, 10000)
	           	.bind('EnterFrame', function()
	           	{
	           		this.x = Math.floor(pentity1.x/16)*16;
	           		this.y = Math.floor(pentity1.y/16)*16;
	           	});
	           	var ppositionpixel2 = Crafty.e("2D, Text, DOM, Color")
	           	.attr({x: ppositionpixel1.x, y: ppositionpixel1.y+32, h:16, w:16, z:5})
	           	.color("red")
				.timeout(function()
				{
					this.destroy();
				}, 10000);
	           	ppositionpixel1.attach(ppositionpixel2);
			}
		})  
		.bind('KeyUp', function(e)
		 {
		    if(e.key == Crafty.keys['SPACE'])
		    {
		    	this.pfire=0;
		    }
		 });
	pentity2.attr({z:0, w:40, h:40, x: pentity1.x, y: pentity1.y});
	pentity1.attach(pentity2);
}
