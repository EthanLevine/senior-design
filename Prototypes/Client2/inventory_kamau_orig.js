//---------------------------------------inventory------------------------------------------------
var inventoryManager = function()
{
	"use strict";

	var inventoryOn = false;
	var shield = "game_images_sprites/shieldpowerup.png";
	var items = [];
	var equip = [];
	var item;
	var invenMax = 7;
	var screenx = 1000;

	for(var h=0; h<invenMax*10; h++)
	{
		items[h] = "sprites/items/blank.png";
	}
	this.changeItem = function(itemnum,itimage)
	{
		items[itemnum] = itimage;
	}
	this.createInventory =function(entity)
	{
		if(inventoryOn == false)
		{
			inventoryOn = true;
			mq.send(["inventory.open"])
				.onconfirm(function(msg)
				{
					//(item id, item name, item template id, item quality, column, row)
					/*
						
						msg[i][0] is id
						msg[i][1] is name
						msg[i][2] is template id
						msg[i][3] is iten quality
						msg[i][4] is column
						msg[i][5] is row
						
						inventory.equip, itemid
						inventory.unequip, itemid
						weapon x=11, y=1
						armor x=11 y=2
						
					*/
					for (var i = 0; i < msg.length; i++)
					{
						switch(msg[i][2]) //item image finder
						{
							case 0: //gold
								item= "game_images_spites/gold.png";
							break;
							case 1: //bat
								item= "sprites/weapons/bat.png";
							break;
							case 2: //fully auto gun
								item= "sprites/weapons/fully_auto.png";
							break;
							case 3: // pistol
								item= "sprites/weapons/pistol.png";
							break;
							case 4: //rocket launcher
								item= "sprites/weapons/rocket_launcher.png";
							break;
							case 5: //shotgun
								item= "sprites/weapons/shotgun.png";
							break;
							case 6: //sword
								item= "sprites/weapons/sword.png";
							break;
							case 7: //medicine
								item= "sprites/items/medicine.png";
							break;
							case 8: //crate
								item= "sprites/items/crate.png";
							break;
							case 9: //grenade
								item= "sprites/items/grenade.png";
							break;
							default:
								item = "sprites/items/blank.png";
							break;
						}
						if(msg[i][4] < 10 && msg[i][5] <10)
						{
							items[msg[i][4] * 10 + msg[i][5]] = item;
						}
						else if(msg[i][5] == 0)
						{
							equip[0]="game_images_spites/gold.png";
						}
						else if(msg[i][5] == 1)
						{
							equip[1]="game_images_spites/gold.png";
						}
						else if(msg[i][5] == 2)
						{
							equip[2]= "game_images_spites/gold.png";
						}
						else if(msg[i][5] == 3)
						{
							equip[3]= "game_images_spites/gold.png";
						}
						else if(msg[i][5] == 4)
						{
							equip[4]= "game_images_spites/gold.png";
						}
						else if(msg[i][5] == 5)
						{
							equip[5]= "game_images_spites/gold.png";
						}
					}
					
					for(var i = 0; i<invenMax; i++)
					{
						var inventory = Crafty.e("2D, DOM, HTML, Persist")
						.attr({x:175, y: i*65 + 80, w:600, h: 55, z:20, alpha: .75, row: i})
						.bind('EnterFrame', function() 
						{
							this.x=(Crafty.viewport._x*-1) + 200;
							this.y=(Crafty.viewport._y*-1) + this.row*65 + 80;
							if(inventoryOn==false)
							{
								this.destroy();
							}
						})
						.replace("\
							<style>\
							.item{\
								width:50px; height:50px; text-align:center;\
							}\
							</style>\
							<div style=\"background-color:black; color:white;\">\
							<table class=\"table table-bordered\">\
							<tr>\
							<td class=\"item\"><img id=\"0\" >\
							</td>\
							<td class=\"item\"><img id=\"1\" >\
							</td>\
							<td class=\"item\"><img id=\"2\" >\
							</td>\
							<td class=\"item\"><img id=\"3\" >\
							</td>\
							<td class=\"item\"><img id=\"4\" >\
							</td>\
							<td class=\"item\"><img id=\"5\" >\
							</td>\
							<td class=\"item\"><img id=\"6\" >\
							</td>\
							<td class=\"item\"><img id=\"7\" >\
							</td>\
							<td class=\"item\"><img id=\"8\" >\
							</td>\
							<td class=\"item\"><img id=\"9\" >\
							</td>\
							</tr>\
							</table>\
							</div>\
						");
						//return game_images_sprites/shieldpowerup.png;
						for(var j=0; j<10; j++)
						{
							var x = document.getElementById(j);
							x.id = "img" + (i * 10 + j);
							x.src = items[i * 10 + j];
						}
					}
					var equiped = Crafty.e("2D, DOM, HTML, Persist")
						.attr({x:screenx - 175, xoffset:screenx - 190, y: 40, w:175, h: 55, z:20, alpha: .75})
						.bind('EnterFrame', function() 
						{
							this.x=(Crafty.viewport._x*-1) + this.xoffset;
							this.y=(Crafty.viewport._y*-1) + 140;
							if(inventoryOn==false)
							{
								this.destroy();
							}
						})
						.replace("\
							<style>\
							.item{\
								height:50px; text-align:center;\
							}\
							</style>\
							<div style=\"background-color:black; color:white;\">\
							<table class=\"table table-bordered\">\
							<tr>\
							<td\">head\
							</td>\
							<td class=\"equip\"><img id=\"0\" src=\"game_images_sprites/poison.png\"/>\
							</td>\
							</tr>\
							<tr>\
							<td>weapon\
							</td>\
							<td class=\"equip\"><img id=\"1\" src=\"game_images_sprites/poison.png\"/>\
							</td>\
							</tr>\
							<tr>\
							<td>armor\
							</td>\
							<td class=\"item\"><img id=\"2\" src=\"game_images_sprites/poison.png\"/>\
							</td>\
							</tr>\
							<tr>\
							<td>Damage Type\
							</td>\
							<td class=\"item\"><img id=\"3\" src=\"game_images_sprites/poison.png\"/>\
							</td>\
							</tr>\
							<tr>\
							<td>Player Helper\
							</td>\
							<td class=\"item\"><img id=\"4\" src=\"game_images_sprites/poison.png\"/>\
							</td>\
							</tr>\
							<tr>\
							<td>Artifact\
							</td>\
							<td class=\"item\"><img id=\"5\" src=\"game_images_sprites/poison.png\"/>\
							</td>\
							</tr>\
							</table>\
							</div>\
						");
						for(var j=0; j<6; j++)
						{
							var x = document.getElementById(j);
							x.id = "equip" + (j);
							x.src = equip[j];
						}
					var closeMe = Crafty.e("2D, DOM, HTML, Mouse, Image, Persist")
						.attr({x:screenx - 175, xoffset:screenx - 90, y: 40, w:55, h: 55, z:20})
						.bind('EnterFrame', function() 
						{
							this.x=(Crafty.viewport._x*-1) + this.xoffset;
							this.y=(Crafty.viewport._y*-1) + 80;
						})
						.bind("Click", function(e)
						{
							inventoryOn = false;
							mq.send(["inventory.close"]);
							this.destroy();
						})
						.replace("\
							<button type=\"button\" id=\"chatButton\" class=\"btn btn-danger\">EXIT</button>\
						");

				});
			
		}
		else
		{
			
		}
	}
}
//---------------------------------------end inventory------------------------------------------------