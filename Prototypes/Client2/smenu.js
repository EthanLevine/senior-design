
//-----------------------------------------menu start here----------------------------------
var menuCreator = function(makeInventory, mainHUD)
{
	
	"use strict";

	var myHUD = Crafty.e("2D, DOM, Persist")
		.attr({x:16,y:16, z:5})
		.bind('EnterFrame', function() 
		{
			this.x=(Crafty.viewport._x*-1);
			this.y=(Crafty.viewport._y*-1);
		});
	//vars are private
	var Menu = Crafty.e("2D, DOM, HTML, Mouse, Persist")
		.attr({x: myHUD._x + 50, y: myHUD._y + 565, z:5, w: 75, h: 50})
		.replace("\
			<button type=\"button\" id=\"mmenuButton\" class=\"btn btn-inverse\">Menu</button>\
		");
	myHUD.attach(Menu);
	console.log("made menu button");
	var menuOpen = false;
	var menuItem = new Array();
	var menuItems = new Array();
	var menuItemBTN = new Array();
	var menuItemText= new Array();
	var menuItemFunction = new Array();
	var menuItemActive = new Array();
	var menuItemWidth = 140;
	var menuItemHeight = 25;
	var menuItemIndent = 10;
	var menuItemSpacing = 30;
	var menuItemCount = 0;
	var menuArray = [];
	var screenx = 1000;
	var screeny = 600;


	//Create a menu function
	this.makemenuitem = function(number, active, text, mfunction)
	{
		menuItemText[number]= number + ") " + text;
		menuItemActive[number] = active;
		menuItemFunction[number] = mfunction ;
		menuItemCount=menuItemCount+1;
		//console.log(menuItemFunction[number]);
	};
		
	//Create a function to delete all entities in the array
	var deleteMenuEntities = function()
	{
		if(menuOpen==true)
		{
			menuOpen=false;
			while(menuArray.length > 0)
			{
				try{menuArray[0].destroy();}catch(err){console.log("Destroy failed");};
				try{menuArray.splice(0, 1);}catch(err){console.log("Splice failed");};//remove the first menu
			}
			for(var i=0; i< menuItemCount; i++)
			{
				menuItems[i].destroy();
				menuItemBTN[i].destroy();
			} 
		}
	};

	//Create a menu function
	var makePlayerMenu = function()
	{
		if(menuOpen==false)
		{
			menuOpen=true;
		    var boundsCheck = Crafty.e("2D, DOM, Mouse, Persist")
		    	.attr({w:screenx, h:screeny, x:myHUD._x, y: myHUD._y, z:9});
		    Menu.attach(boundsCheck);
		    //Create the background window
		    var menuBkgWin = Crafty.e("2D, DOM, Color, Mouse, Persist")
		    	.color("black")
		    	.attr({w:menuItemWidth + 10, h: menuItemCount*(menuItemHeight + 5)+20, x:Menu._x-16, y:Menu._y - (menuItemCount*(menuItemHeight + 5)+30), z:10, alpha:.8});
		    Menu.attach(menuBkgWin);
		    //Create the triangle at the bottom of the window
		    var menuBkgTri = Crafty.e("2D, DOM, Color, Mouse, Persist")
		    	.color("black")
		    	.attr({w:16, h:16, x:menuBkgWin.x+40, y:menuBkgWin.y+menuBkgWin.h, alpha:.3, z:9, rotation:45.0});
		    Menu.attach(menuBkgTri);
		    //Create a title bar
		    var menuTitle = Crafty.e("2D, DOM, Text, Persist")
		        .attr({x:menuBkgWin.x+4, y:menuBkgWin.y+2, z:10, w:70, h:40})
		        .text("Player Menu")
		        .css({"font": "8pt Arial", "color": "#0F0", "text-align": "left"});
		    Menu.attach(menuTitle);
		    //Create the health text and button
		    menuItems[0] = Crafty.e("2D, DOM, Text, Persist")
				.attr({x:menuBkgWin.x+menuItemIndent, y:menuBkgWin.y+30, z:10, w:menuItemWidth, h:40})
				.text("" + menuItemText[0])
				.css({"font": "8pt Arial", "color": "#FFF", "text-align": "left"});
			Menu.attach(menuItems[0]);
		    menuItemBTN[0] = Crafty.e("2D, DOM, HTML, Mouse, Persist")
		        .attr({w:menuItemWidth, h:menuItemHeight, x:menuBkgWin.x+2, y:menuBkgWin.y+22, z:10, alpha:0.2, place:0 })
		        .replace("\
					<style>\
					.smenu{\
						height: 7px; width: 140px; text-align:center; background-color:white;\
					}\
					</style>\
					<div style=\" color:white;\">\
					<table class=\"table table-bordered\">\
					<tr>\
					<td class=\"smenu\">\
					</td>\
					</tr>\
					</table>\
					</div>\
				");
		    Menu.attach(menuItemBTN[0]);
			if(menuItemActive[0] ==1)
			{
				//console.log(menuItemFunction[0]);

				menuItemBTN[0].bind('Click', function(e)
				{
		    		if(menuItemFunction[this.place] != " ")
		    		{
		    			console.log("menu item 0 clicked")
		    			eval(menuItemFunction[this.place]);
		    			deleteMenuEntities();
		    		}
		    		else
		    		{
		    			deleteMenuEntities();
		    		}
		    	});
		    	menuItemBTN[0].alpha =.5;
		    }
		    for(var i=1; i<menuItemCount; i++)
		    {
		    	//console.log(menuItemFunction[i]);
		    	menuItems[i] = Crafty.e("2D, DOM, Text, Persist")
			       	.attr({x:menuBkgWin.x + menuItemIndent, y:menuItems[i-1].y+menuItemSpacing, z:10, w:menuItemWidth, h:menuItemHeight + 5})
			        .text("" + menuItemText[i])
			        .css({"font": "8pt Arial", "color": "#FFF", "text-align": "left"});
			    Menu.attach(menuItems[i]);
			    menuItemBTN[i] = Crafty.e("2D, DOM, HTML, Mouse, Persist")
		      		.attr({w:menuItemWidth, h: menuItemHeight, x:menuBkgWin.x+2, y:menuItemBTN[i-1].y+menuItemSpacing, z:10, alpha:0.2, place:i})
		      		.replace("\
					<style>\
					.smenu{\
						height: 7px; width: 140px; text-align:center; background-color:white;\
					}\
					</style>\
					<div style=\" color:white;\">\
					<table class=\"table table-bordered\">\
					<tr>\
					<td class=\"smenu\">\
					</td>\
					</tr>\
					</table>\
					</div>\
				");
		    	Menu.attach(menuItemBTN[i]);
		    	if(menuItemActive[i] ==1)
			    {
			    	//console.log(menuItemFunction[i]);

					menuItemBTN[i].bind('Click', function(e)
					{
			    		if(menuItemFunction[this.place] != " ")
			    		{
			    			eval(menuItemFunction[this.place]);
			    			deleteMenuEntities();
			    		}
			    		else
						{
			    			deleteMenuEntities();
			    		}
			    	});
			    	menuItemBTN[i].alpha =.5;
			    }
		    }
		    //Create the Use text and button
		    //Add all of the menu entities to the menuArray
		    menuArray.push(boundsCheck, menuBkgWin, menuBkgTri, menuTitle);

		    //The bounds check event ensures that if the user clicks off of the menu, the menu closes          
		    boundsCheck.bind('Click', function(e)
		    {
		    	deleteMenuEntities();
		    });

		    //Define what each menu item does below 
		}               
	};
	//When the user clicks on the menu, show the game menu
	console.log("binding menu button");
	Menu.bind('Click', function(e)
	{
	    makePlayerMenu();
	});
	
}

//--------------------------------------menu end here-------------------------------------------------
