//--------------------------------------chat function start here------------------------------------------
var chatManager = function()
{
	"use strict";

	var screeny = 600;
	var screenx = 1000;

	var chatStarter;
	var mainEntity;
	var tchat;
	var speechHistory;
		
	
	var chatButton = function()
	{
		if(mainEntity !==undefined && $("#chat_field").val() !="" && $("#chat_field").val() !=" ")
		{
			chatNow(mainEntity, null, 3, 16, 0, 400);
		}
	};
	this.setupChat= function()
	{
		chatStarter = Crafty.e("2D, DOM, HTML, Persist")
			.attr({x:200, y: 35, w:800, h: 50, z:20, alpha: .75})
			.replace("\
					<div>\
					    <div class=\"input-append\">\
					      <input class=\"span9\" type=\"text\" id=\"chat_field\" maxlength=\"200\">\
					      <button type=\"button\" id=\"chatNowButton\" class=\"btn btn-primary\">CHAT</button>\
						</div>\
					</div>\
				")
			.bind('EnterFrame', function() 
			{
				this.x=(Crafty.viewport._x*-1) + 200;
				this.y=(Crafty.viewport._y*-1) + screeny - 35;
			});
		$('#chatNowButton').click(function()
		{
			chatButton();
		});
		$('#chat_field').keypress(function (e)
		{
			switch (e.which)
			{
				case 13:
				chatButton();
				e.preventDefault();
				break;
				case 10:
				chatButton();
				e.preventDefault();
				break;
			}
		});
		speechHistory = Crafty.e("2D, DOM, HTML, Persist")
			.attr({x: chatStarter.x, y: chatStarter.y - 75, z: 19, w: 800, h: 100, alpha: .6})
			.replace("<textarea class=\"input-xlarge\" id=\"textareachat\" rows=\"3\" style=\"width:750px;resize:none;\" disabled></textarea>");
		chatStarter.attach(speechHistory);

		$('#chat_field').focusin(function()
		{	
			entityManager.chatfocusin();
		});
		$('#chat_field').focusout(function()
		{	
			entityManager.chatfocusout();
		});
	};
	this.setupMainTarget = function(mainTarget)
	{
		mainEntity = mainTarget;
	};
	this.displayMessage=function(entity, chat_Text, chatTime, chatOffsetx, chatOffsety, bwidth)
	{
		if(entity != undefined)
		{
			chatNow(entity, chat_Text, chatTime, chatOffsetx, chatOffsety, bwidth);
		}
		else
		{
			console.log("Chat Error");
		}
	};

	this.chatHistory=function(chat_Text)
	{
		
		var psconsole = $('#textareachat');
		psconsole.append(chat_Text);
		psconsole.append('\n');
    	psconsole.scrollTop(
        psconsole[0].scrollHeight - 90);
	};
	
	var chatNow = function(entity, chat_Text, chatTime, chatOffsetx, chatOffsety, bwidth)
	{
		
		var chatText =$("#chat_field").val();
		if(mainEntity !==undefined)
		{
			if(chat_Text == null)
			{
				mq.send(["player.say", $("#chat_field").val()])
				.onconfirm(function(msg)
				{
					console.log("chat message sent");
					$("#chat_field").val('');
				});
				/*
				$('#schat').text('says: ' + chatText);
				tchat = document.getElementById("schat");
				tchat.id = "fschat";
				$("#chat_field").val('');
				  */
			}
			else
			{
				var speechBubble = Crafty.e("2D, DOM, HTML, Persist")
					.attr({x: entity._x + entity._w/2 + chatOffsetx, y: entity._y +entity._h/2 + chatOffsety, z: 19, w: bwidth, h: 100})
					.replace("\
						<table class=\"table table-bordered\">\
						<tr>\
						<td id=\"schat\" style=\"background-color:black; color:white;\"> loading\
						</td>\
						</tr>\
						</table>\
					");
				entity.attach(speechBubble);
				$('#schat').text(chat_Text);
				tchat = document.getElementById("schat");
				tchat.id = "fschat2";
				if(chatTime>0)
				{
					speechBubble.timeout(function() {
						this.destroy();
					}, chatTime * 1000);
				}
				else
				{
					speechBubble.bind("Click", function(e)
					{
						speechBubble.destroy();
					});
				}
			}
		}
		else
		{
			console.log("Please Set Up Main Entity");
		}
	};
}
//------------------------------------------chat function end here----------------------------------
