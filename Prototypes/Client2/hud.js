var setupHUD =function()
{
	"use strict";

	var myHUD;
	var myHUD2;
	var myHUD3;
	var HUDbox;
	var HUDbars1;
	var bar1display;
	var HUDbars2;
	var HUDbars3;
	var bar2display;
	var objective = "unknown";
	var difficulty = .01;

	//updates of hud
	var HUDcontroller;

	//show Stats
	var showStats = false;

	this.changeDifficulty = function(newd)
	{
		difficulty = newd;
	}
	this.difficulty= function()
	{
		return difficulty;
	}
	this.setupBasic = function(entity)
	{
		console.log("hud setup basic called");
		myHUD = Crafty.e("2D, DOM, Persist")
			.attr({x:16,y:300, z:5, w:1000,h:16});
		HUDbox = Crafty.e("2D, HTML, DOM, Persist")
			.attr({x: myHUD._x, y: myHUD._y, z:4, w:1000, h:75, alpha:0.4})
			.replace("\
				<div style=\"background-color:black; color:white;\">\
				<table class=\"table table-bordered\" style = \"height: 75px; width: 999px; text-align:center;\">\
				<tr>\
				<td>\
				</td>\
				</tr>\
				</table>\
				</div>\
			");
		myHUD.attach(HUDbox);
		myHUD3 = Crafty.e("2D, Text, DOM, Persist")
			.attr ({x: myHUD._x + 60, y: myHUD._y+50, z:5, w:500, h:32})
			.textColor("#FFFFFF");
		myHUD.attach(myHUD3);


		HUDbars1 = Crafty.e("2D, HTML, DOM, Persist")
			.attr({x: HUDbox._x +60, y: HUDbox._y + 5, z:4, w:900, h:22, alpha:0.5})
			.replace("\
				<div class=\"progress progress-striped active\">\
					<div class=\"bar bar-danger healthmeter\" style=\"width: 0%;\"></div>\
				</div>\
			");
		myHUD.attach(HUDbars1);
		bar1display =Crafty.e("2D, HTML, DOM, Persist")
			.attr({x: HUDbars1._x-45, y: HUDbars1._y, z:4, w:100, h:25, alpha:0.7})
			.replace("\
				<div class=\"btn-group btn-group-vertical\">\
					<button class=\"btn btn-mini btn-danger\" type=\"button\">Health</button>\
					<button class=\"btn btn-mini\" type=\"button\">Exp</button>\
				</div>\
			");
		myHUD.attach(bar1display);
		HUDbars2 = Crafty.e("2D, HTML, DOM, Persist")
			.attr({x: HUDbars1._x-10, y: HUDbars1._y+ HUDbars1.h, z:4, w:400, h:22, alpha:0.5})
			.replace("\
				<div class=\"progress progress-striped active\">\
				    <div class=\"bar expmeter\" style=\"width: 0%;\"></div>\
				</div>\
			");
		myHUD.attach(HUDbars2);
		HUDbars3 = Crafty.e("2D, HTML, DOM, Persist")
			.attr({x: HUDbars2._x+HUDbars2.w+100, y: HUDbars2._y, z:4, w:400, h:22, alpha:0.5})
			.replace("\
				<div class=\"progress progress-striped active\">\
				    <div class=\"bar bar-warning abilitymeter\" style=\"width: 0%;\"></div>\
				</div>\
			");
		myHUD.attach(HUDbars3);
		bar2display =Crafty.e("2D, HTML, DOM, Persist")
			.attr({x: HUDbars3._x-45, y: HUDbars3._y, z:4, w:100, h:25, alpha:0.7})
			.replace("\
				<div class=\"btn-group btn-group-vertical\">\
					<button class=\"btn btn-mini btn-warning\" type=\"button\">Ability</button>\
				</div>\
			");
		myHUD.attach(bar2display);

		HUDcontroller=Crafty.e("DOM")
			.attr({target: entity})
			.bind('EnterFrame', function() 
			{
				//for myHUD
				myHUD.x=(Crafty.viewport._x * -1);
				myHUD.y=(Crafty.viewport._y * -1);
				//for hud 3
				myHUD3.text("Current Objective:" + objective);
				//for bar1display
				$('.healthmeter').css('width', (entity.health/entity.maxhealth) * 100 + '%');
				$('.healthmeter').text(Math.round((entity.health/entity.maxhealth) * 100 )+ '%');
				$('.expmeter').css('width', (entity.exp/entity.maxexp) * 100 + '%');
				$('.expmeter').text(Math.round((entity.exp/entity.maxexp) * 100) + '%');
				$('.abilitymeter').css('width', (entity.ability/entity.maxability) * 100 + '%');
				$('.abilitymeter').text(Math.round((entity.ability/entity.maxability) * 100) + '%');
				this.target = entity;
			});

	};
	this.viewStats = function()
	{
		if(showStats==false)
		{
			showStats=true;
			myHUD2 = Crafty.e("2D, HTML, DOM, Persist")
				.attr({x: myHUD._x, y: myHUD._y + HUDbox.h, z:5, w: screenx-1, h:10, alpha: .4})
				.replace("\
					<style>\
					.hud2{\
						background-color:black; margin-top: 0px; margin-bottom:0px; text-align:center;\
					}\
					</style>\
					<div style=\"color:white; background-color:black;\">\
					<table class=\"table table-bordered\">\
					<tr>\
					<td class=\"hud2 healthDisplay\"> loading\
					</td>\
					<td class=\"hud2 goldDisplay\"> loading\
					</td>\
					<td class=\"hud2 scoreDisplay\"> loading\
					</td>\
					<td class=\"hud2 difficultyDisplay\"> loading\
					</td>\
					<td class=\"hud2 levelDisplay\"> loading\
					</td>\
					<td class=\"hud2 expDisplay\"> loading\
					</td>\
					<td style=\"width:50px;\"><button type=\"button\" id=\"closeStats\" class=\"btn btn-mini btn-danger\">Close</button>\
					</td>\
					</tr>\
					</table>\
					</div>\
				")
				.bind('EnterFrame', function() 
				{
					//for hud 2
					if(HUDcontroller.target !==undefined)
					{
						$('.healthDisplay').text('Health: ' + HUDcontroller.target.health);
						$('.goldDisplay').text('Gold: ' + HUDcontroller.target.gold);
						$('.scoreDisplay').text('Score: ' + HUDcontroller.target.score);
						$('.difficultyDisplay').text('Difficulty: ' + Math.round(difficulty*100));
						$('.levelDisplay').text('Player Level: ' + HUDcontroller.target.level);
						$('.expDisplay').text('EXP: ' + HUDcontroller.target.exp);
					}
					else
					{
						showStats=false;
						myHUD2.destroy();
					}
				});
				$('#closeStats').click(function()
				{
					showStats=false;
					myHUD2.destroy();
				});
			myHUD.attach(myHUD2);
		}
		else
		{
			consol.log("view stats error or already open");
		}
	};
	
	this.changeObjective = function(newobj)
	{
		objective = newobj;
	};

	this.loadplayer = function(entity)
	{
		var healthshields = new Array();
		for(var i = 0; i<5; i++)
		{
			healthshields[i] = Crafty.e('2D, DOM, Image, Persist, Tween')
				.image("game_images_sprites/shieldpowerup.png","no-repeat")
				.attr({x: (entity._x + entity._w/2 -16) + i*20 - 40, y:entity._y -50, rotation:0, z:4, h:32, w:32, active:0, alpha: 0.1, snum: i})
				.origin("center")
				.bind("EnterFrame", function()
				{
					if(this.active==0)
				   	{
				   		this.active=1;
				   		entity.attach(this);
				   	}
			        if(entity.health >= (this.snum * 20) + 20 && this.alpha != .7)
					{
						this.alpha = .7;
					}
					else if(entity.health < (this.snum * 20) + 20 && this.alpha != .05)
					{
						this.alpha = .05;
					}
				});
		}
	};

	this.loadplayer2 = function(entity)
	{
		var healthnumber = Crafty.e('2D, DOM, Text, Collision, Persist')
			.text("loading")
			.attr({x: entity.x -10, y: entity.y +50, z:3})
			.textColor('#FF0000')
			.bind("EnterFrame", function()
			{
			    this.text("Health:" + entity.health + " Gold:" + entity.gold);                
			});
			entity.attach(healthnumber);
	};

};