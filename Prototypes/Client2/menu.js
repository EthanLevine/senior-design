// menu.js
// a menu object for the UConnocalypse client
//
// 
// This library provides a simple menu system to be used by the client.  
// The menu system requires Crafty js, and relies on the viewport to locate itself.
//
// For documentation on using this library, see the project's wiki at:
//   https://bitbucket.org/EthanLevine/senior-design/wiki/menu.js



var menuCreator = function(){
	
	"use strict";
	//vars are private

	var menuList = []; //contains a list of items to be added to the menuItems
	var menuItems = []; //this contains our actual Crafty entities for the menu items.
	var menuItemH = 20;
	var menuItemW = 80;
	var menu; //actual menu entity
	var menuUp = 0;

	var displayMenu = function(){ //creates the menu, when clicked on the word menu.
		//displayMenu is called when the menu word is clicked on and it displays menu items.
		
		var menuItemCount = menuList.length;
		
		if(!menuUp){
			menuUp=true;
			for(var i=0; i<menuItemCount; i++){
				
				menuItems[i] = Crafty.e("2D, DOM, Color, Text, Persist")
				.attr({x:menu._x,y:(menu._y-10-(i+1)*menuItemH)-(i*1),h:menuItemH,w:menuItemW,z:10})//here each item's y is determined by its index.
				.css({"font": "8pt Arial", "color": "orange", "text-align": "left", "margin-left":"10px"})
				.text(menuList[i][0]);


				menu.attach(menuItems[i]);

				menuItems[i].button = Crafty.e("2D, DOM, Color, Mouse, Persist")
				.attr({x:menu._x,y:(menu._y-20-(i+1)*menuItemH)-(i*1),h:menuItemH,w:menuItemW,alpha:0.2,z:10})
				.color("white");
				
				menu.attach(menuItems[i].button);

				if(menuList[i][2]){ //if item is enabled, bind the click function.
					menuItems[i].button.bind("Click",menuList[i][1]);	
				}
				else{//else gray out the text.

					menuItems[i].attr({alpha:0.3});
				}


			}

			menu.clickAway = Crafty.e("2D, DOM, Color, Mouse, Persist")
			.attr({x: menu._x, y:menu._y-800, z:1, w: 1000, h: 800,alpha:0.2})
			.bind("Click",function(e){closeMenu();});

			menu.attach(menu.clickAway);

			

		}

	};

	var closeMenu = function () { //private function called when clicked outside of an active menu.
		if(menuUp){
			var menuItemCount = menuList.length;
			for(var i=0; i<menuItemCount; i++){
				menuItems[i].button.destroy();
				menuItems[i].destroy();

			}
			menu.clickAway.destroy();
			menuUp = false;

		}
	};


	this.addMenuItem = function(menuText, callFunction){  //priviledged function, adds items to the menu list.
		//priviledged method to add menu items
		menuList.push([menuText,callFunction, true]); //Menu Text, Call function when Clicked, and true for active.
	

	};

	
	this.disableItem = function(menuText){  //if item is found it's disabled, true is returned, else false is returned.
		var disabled = false;

		var menuItemCount = menuList.length;
			for(var i=0; i<menuItemCount; i++){
				
				if(menuList[i][0]===menuText)
				{
					menuList[i][2] = false;
					disabled = true;
				}
			
			}
		return disabled;

	};

	this.removeItem = function(menuText){  //if item is found it's removed, true is returned, else false is returned.
		var removed = false;
		var menuItemCount = menuList.length;
		if(!menuUp){ // make sure the menu isn't up.  otherwise all bets are off!


			for(var i=0; i<menuItemCount; i++){
				
				if(menuList[i][0]===menuText)
				{
					menuList.splice(i,1);
					removed = true;
					break;
				}
			
			}
		}
		return removed;

	};

	menu = Crafty.e("2D, DOM, Color, Text, Persist")
	.attr({x: 50, y:500, z:10, w: 70, h: 32})
	.text("Menu")
	.textColor('#FF0000');

	menu.menuButton = Crafty.e("2D, DOM, Color,Text, Mouse, Persist")
	.attr({x: menu._x, y:menu._y, z:menu._z, w: menu._w, h: menu._h});
	menu.attach(menu.menuButton);
	menu.menuButton.bind("Click", function(e){displayMenu();});

	menu.bind('EnterFrame', function() 
	{
		this.x=(Crafty.viewport.x*-1+100);
		this.y=(Crafty.viewport.y*-1+580);
	});
	
};