//---------------------------------------inventory------------------------------------------------
var inventoryManager = function() {
	"use strict";

	var item;
	var screenx = 1000;

	var invenOpen = false;
	var INVEN_HEIGHT = 7;
	var INVEN_WIDTH = 10;

	var items = new Array(INVEN_WIDTH * INVEN_HEIGHT);
	var equip = new Array(2);

	var SPRITE_DIR = "sprites/";
	var itemTemplateSpriteTable = [];
	itemTemplateSpriteTable[1] = SPRITE_DIR + "weapons/pistol.png";
	itemTemplateSpriteTable[2] = SPRITE_DIR + "weapons/sword.png";
	itemTemplateSpriteTable[3] = SPRITE_DIR + "weapons/shotgun.png";
	itemTemplateSpriteTable[4] = SPRITE_DIR + "items/shield_2.png";

	this.createInventory = function(entity)
	{
		cInventory(entity);
	}
	var cInventory = function(entity)
	{
		if(invenOpen === false)
		{
			invenOpen = true;
			mq.send(["inventory.open"])
			.onconfirm(function(msg)
			{
				//(item id, item name, item template id, item quality, column, row)
				/*
					
					msg[i][0] is id
					msg[i][1] is name
					msg[i][2] is template id
					msg[i][3] is iten quality
					msg[i][4] is column
					msg[i][5] is row
					
					inventory.equip, itemid
					inventory.unequip, itemid
					weapon x=INVEN_WIDTH, y=1
					armor x=INVEN_WIDTH, y=2
					
				*/
				if(msg != undefined)
				{
					for (var i = 0; i < msg.length; i++)
					{
						// Populate our inventory and equipment variables
						if (msg[i][4] !== INVEN_WIDTH)
						{
							items[msg[i][4] * INVEN_WIDTH + msg[i][5]] = msg[i];
						}
						else 
						{
							equip[msg[i][5]] = msg[i]
						}

					}
					for (var i = 0; i < INVEN_HEIGHT; i++) {
						var inventory = Crafty.e("2D, DOM, HTML, Persist")
						.attr({x:175, y: i*65 + 80, w:600, h: 55, z:20, alpha: .75, row: i})
						.bind('EnterFrame', function() {
							this.x=(Crafty.viewport._x*-1) + 200;
							this.y=(Crafty.viewport._y*-1) + this.row*65 + 80;
							if (invenOpen == false)
							{
								this.destroy();
							}
						})
						.replace("\
							<style>\
							.item{\
								width:50px; height:50px; text-align:center;\
							}\
							</style>\
							<div style=\"background-color:black; color:white;\">\
							<table class=\"table table-bordered\">\
							<tr>\
							<td class=\"item\"><img id=\"0\" onclick=\"\">\
							</td>\
							<td class=\"item\"><img id=\"1\" onclick=\"\">\
							</td>\
							<td class=\"item\"><img id=\"2\" onclick=\"\">\
							</td>\
							<td class=\"item\"><img id=\"3\" onclick=\"\">\
							</td>\
							<td class=\"item\"><img id=\"4\" onclick=\"\">\
							</td>\
							<td class=\"item\"><img id=\"5\" onclick=\"\">\
							</td>\
							<td class=\"item\"><img id=\"6\" onclick=\"\">\
							</td>\
							<td class=\"item\"><img id=\"7\" onclick=\"\">\
							</td>\
							<td class=\"item\"><img id=\"8\" onclick=\"\">\
							</td>\
							<td class=\"item\"><img id=\"9\" onclick=\"\">\
							</td>\
							</tr>\
							</table>\
							</div>\
						");
						for(var j=0; j < INVEN_WIDTH; j++) {
							var x = document.getElementById(j);
							x.id = "img" + (i * INVEN_HEIGHT + j);
							if (items[i * INVEN_HEIGHT + j] != null)
							{
								x.src = itemTemplateSpriteTable[items[i * INVEN_HEIGHT + j][2]];
								$('#img' + (i * INVEN_HEIGHT + j)).click(function()
								{
									mq.send(["inventory.equip", items[i * INVEN_HEIGHT + j][0]])
									.onconfirm(function(msg)
									{
										console.log("equip");
										invenOpen = false;
										cInventory(entity);
									})
									.onerror(function(code, msg)
									{
										console.log(msg);
									});
								});
							}
							else
							{
								x.src = "sprites/items/blank.png";
							}
						}
					}
				}
				else
				{
					console.log("inventory.open problem. msg undefined " + msg);
				}
				var equiped = Crafty.e("2D, DOM, HTML, Persist")
				.attr({x:screenx - 175, xoffset:screenx - 190, y: 40, w:175, h: 55, z:20, alpha: .75})
				.bind('EnterFrame', function()
				{
					this.x=(Crafty.viewport._x*-1) + this.xoffset;
					this.y=(Crafty.viewport._y*-1) + 140;
					if(invenOpen === false)
					{
						this.destroy();
					}
				})
				.replace("\
					<style>\
					.item{\
						height:50px; text-align:center;\
					}\
					</style>\
					<div style=\"background-color:black; color:white;\">\
					<table class=\"table table-bordered\">\
					<tr>\
					<td>head\
					</td>\
					<td class=\"equip\"><img id=\"0\" src=\"game_images_sprites/poison.png\"/>\
					</td>\
					</tr>\
					<tr>\
					<td>Weapon\
					</td>\
					<td class=\"equip\"><img id=\"1\" src=\"game_images_sprites/poison.png\"/>\
					</td>\
					</tr>\
					<tr>\
					<td>Armor\
					</td>\
					<td class=\"item\"><img id=\"2\" src=\"game_images_sprites/poison.png\"/>\
					</td>\
					</tr>\
					<tr>\
					<td>Strength\
					</td>\
					<td class=\"item\"><img id=\"3\" src=\"game_images_sprites/poison.png\"/>\
					</td>\
					</tr>\
					<tr>\
					<td>Dexterity\
					</td>\
					<td class=\"item\"><img id=\"4\" src=\"game_images_sprites/poison.png\"/>\
					</td>\
					</tr>\
					<tr>\
					<td>Fortitude\
					</td>\
					<td class=\"item\"><img id=\"5\" src=\"game_images_sprites/poison.png\"/>\
					</td>\
					</tr>\
					</table>\
					</div>\
				");
				for(var j=0; j<6; j++)
				{
					var x = document.getElementById(j);
					x.id = "equip" + (j);
					x.src = "sprites/items/blank.png";
					if (equip[j] != null)
					{
						x.src = itemTemplateSpriteTable[equip[j][2]];
						mq.send(["inventory.unequip , equip[j][0]"])
						.onconfirm(function(msg)
						{
							console.log("unequip");
							invenOpen = false;
							cInventory(entity);
						});
					}
				}
				var closeMe = Crafty.e("2D, DOM, HTML, Mouse, Image, Persist")
				.attr({x:screenx - 175, xoffset:screenx - 90, y: 40, w:55, h: 55, z:20})
				.bind('EnterFrame', function()
				{
					this.x=(Crafty.viewport._x*-1) + this.xoffset;
					this.y=(Crafty.viewport._y*-1) + 80;
				})
				.bind("Click", function(e)
				{
					invenOpen = false;
					//mq.send(["inventory.close"]);
					this.destroy();
				})
				.replace("<button type=\"button\" id=\"chatButton\" class=\"btn btn-danger\">EXIT</button>");
			});
		} else {
		}
	}
}
//---------------------------------------end inventory------------------------------------------------