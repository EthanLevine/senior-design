// menu.js
// a timer object for the UConnocalypse client
//
// 
// This library provides a time synchronization, latency calculation, tick time calculation.
//



var Timer = function() {
"use strict";

var that = this;
var lastTime;
var t0, t1, t2, t3;
var currentLatency = 0;
var offset = 0;
var smoothLatency; 
var smoothOffset;

this.getTickTime = function() {
	if(lastTime===0)
	{
		lastTime= (new Date()).getTime();
		return 0;
	}
	else
	{
		var temp = lastTime;
		lastTime = (new Date()).getTime();
		return lastTime-temp;
	}
};


this.epoch = function() {
	
	return (new Date()).getTime();
};

this.getLatency = function() {

    return{
        smooth: smoothLatency,
        current: currentLatency
    };
};

this.getOffset = function (){
    return {
    	smooth: smoothOffset,
    	current: offset

    };
};

this.syncTime = function() {
	var message;
	t0 = (new Date()).getTime();


	message = (["network.time-request",t0,smoothLatency])
	mq.send(message)
	.onconfirm(function (msg) {
		t3 = (new Date()).getTime();
		var alpha = 0.1;
		
		t1 = msg[0];
		t2 = msg[1];

		//next two lines are taken from the NTP protocol
		offset = ((t1-t0)+(t2-t3))/2;
		currentLatency = (t3-t0)-(t2-t1);

        if(smoothLatency===undefined){
            smoothLatency = currentLatency;
        }
        else{
            smoothLatency = currentLatency*alpha + smoothLatency*(1-alpha); //exponential weighted sum
        }

        if(smoothOffset===undefined){
            smoothOffset = offset;
        }
        else{
            smoothOffset = offset*alpha + smoothOffset*(1-alpha); //exponential weighted sum
        }

		

	})
	.onerror(function (code, msg) {
		console.log("Network Time Error: " + msg);

	});



	

};





};