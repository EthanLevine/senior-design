var direction="";
var myId; //my entity Id, will be sent to me from the server.


var sendMessage = function(message){

   mq.send(message)
    .onconfirm(function (msg) {
      console.log("Confirm: ");
      console.log(msg);
      
    })
    .onerror(function (code, msg){
      console.log("Error: " + msg);

    });
}

Crafty.scene("startGame", function () 
{
  entityManager.setGameStarted(true);

  
 
    //this ensures that we bind local player before proceeding and that we have local player id before binding.



  //need to wait for playerid to come in.
	var worldBkg = Crafty.e("2D, DOM, Image, Persist");
    worldBkg.attr({w:1616, h:2256, x: 0, y: 0, z:-1});
    worldBkg.image("game_images_sprites/floor1ite.png", "no-repeat"); // actual floor1ite image made by Kamau


 


    


 });//end of crafty scene start






var bindKeystoMessages = function(){

var localPlayer = entityManager.getLocalPlayer();
  
localPlayer.bind('KeyDown', function(e) {//binding key movements to my id in the entity table.  Although they could be bound to any entityid.
  if(!(entityManager.getChatting()===true)){


    if(e.key===Crafty.keys['SPACE']){

        var message = ["player.start-attack", myTimer.epoch()];
        mq.send(message)
         .onconfirm(function () {


        })
        .onerror(function (code, msg) {
          console.log(msg);

        }); 
    }  

    if(e.key===Crafty.keys['1']){
        var message = ["weapon",1];
        mq.send(message);
        console.log("weapon 1");

    }
    if(e.key===Crafty.keys['2']){
        var message = ["weapon",2];
        mq.send(message);
        console.log("weapon 2");


    }
    if(e.key===Crafty.keys['3']){
        var message = ["weapon",3];
        mq.send(message);
        console.log("weapon 3");


    }



    if(e.key == Crafty.keys['W']) {
        if (direction==""){
          direction="N";
          localPlayer.xvelocity=0;
          localPlayer.yvelocity=-localPlayer.velocity;
          
        }
        if (direction=="E"){
          direction="NE";
          localPlayer.xvelocity=localPlayer.velocity*0.707;
          localPlayer.yvelocity=-localPlayer.velocity*0.707;
          
        }
        if (direction=="W"){
          direction="NW";
          localPlayer.xvelocity=-localPlayer.velocity*0.707;
          localPlayer.yvelocity=-localPlayer.velocity*0.707;
          
        }
      }
      if(e.key == Crafty.keys['S']) {
        if (direction==""){
          direction="S";
          localPlayer.xvelocity=0;
          localPlayer.yvelocity=localPlayer.velocity;
          
        }
        if (direction=="E"){
          direction="SE";
          localPlayer.xvelocity=localPlayer.velocity*0.707;
          localPlayer.yvelocity=localPlayer.velocity*0.707;
          
        }
        if (direction=="W"){
          direction="SW";
          localPlayer.xvelocity=-localPlayer.velocity*0.707;
          localPlayer.yvelocity=localPlayer.velocity*0.707;
          
        }
      }
      if(e.key == Crafty.keys['D']) {
        if (direction==""){
          direction="E";
          localPlayer.xvelocity=localPlayer.velocity;
          localPlayer.yvelocity=0;
          
        }
        if (direction=="N"){
          direction="NE";
          localPlayer.xvelocity=localPlayer.velocity*0.707;
          localPlayer.yvelocity=-localPlayer.velocity*0.707;
          
        }
        if (direction=="S"){
          direction="SE";
          localPlayer.xvelocity=localPlayer.velocity*0.707;
          localPlayer.yvelocity=localPlayer.velocity*0.707;
          
        }

      }
      if(e.key == Crafty.keys['A']) {
        if (direction==""){
          direction="W";
          localPlayer.xvelocity=-localPlayer.velocity;
          localPlayer.yvelocity=0;
          
        }
        if (direction=="N"){
          direction="NW";
          localPlayer.xvelocity=-localPlayer.velocity*0.707;
          localPlayer.yvelocity=-localPlayer.velocity*0.707;

        
        }
        if (direction=="S"){
          direction="SW";
          localPlayer.xvelocity=-localPlayer.velocity*0.707;
          localPlayer.yvelocity=localPlayer.velocity*0.707;
          
        }
      }

        message = ["player.set-motion", direction, myTimer.epoch()];


        
      mq.send(message)
      .onconfirm(function () {
      
        
      })
      .onerror(function (code, msg) {
        console.log(msg);

      });

      //update players animation
      if(!localPlayer.isPlaying("walking") && direction !==""){
        localPlayer.stop();
        localPlayer.animate("walking",28,-1);
        
      }
}

    


})
.bind('KeyUp', function(e) {
if(!(entityManager.getChatting()===true)){
    if(e.key===Crafty.keys['SPACE']){

        var message = ["player.stop-attack", myTimer.epoch()];
        mq.send(message)
         .onconfirm(function () {


        })
        .onerror(function (code, msg) {
          console.log(msg);

        });
    }


      if(e.key == Crafty.keys['W']) {
        if (direction=="N"){
          direction="";
          localPlayer.xvelocity=0;
          localPlayer.yvelocity=0;
          
        }
        if (direction=="NE"){
          direction="E";
          localPlayer.xvelocity=localPlayer.velocity;
          localPlayer.yvelocity=0;
          
        }
        if (direction=="NW"){
          direction="W";
          localPlayer.xvelocity=-localPlayer.velocity;
          localPlayer.yvelocity=0;
          
        }
      }
      if(e.key == Crafty.keys['S']) {
        if (direction=="S"){
          direction="";
          localPlayer.xvelocity=0;
          localPlayer.yvelocity=0;
          
        }
        if (direction=="SE"){
          direction="E";
          localPlayer.xvelocity=localPlayer.velocity;
          localPlayer.yvelocity=0;
          
        }
        if (direction=="SW"){
          direction="W";
          localPlayer.xvelocity=-localPlayer.velocity;
          localPlayer.yvelocity=0;
          
        }
      }
      if(e.key == Crafty.keys['D']) {
        if (direction=="E"){
          direction="";
           localPlayer.xvelocity=0;
          localPlayer.yvelocity=0;
          
        }
        if (direction=="NE"){
          direction="N";
          localPlayer.xvelocity=0;
          localPlayer.yvelocity=-localPlayer.velocity;
          
        }
        if (direction=="SE"){
          direction="S";
          localPlayer.xvelocity=0;
          localPlayer.yvelocity=localPlayer.velocity;
          
        }
      }
      if(e.key == Crafty.keys['A']) {
        if (direction=="W"){
          direction="";
           localPlayer.xvelocity=0;
          localPlayer.yvelocity=0;
          
        }
        if (direction=="NW"){
          direction="N";
          localPlayer.xvelocity=0;
          localPlayer.yvelocity=-localPlayer.velocity;
          
        }
        if (direction=="SW"){
          direction="S";
          localPlayer.xvelocity=0;
          localPlayer.yvelocity=localPlayer.velocity;
          
        }
      }
      message = ["player.set-motion", direction, myTimer.epoch()];
      
        mq.send(message)
      .onconfirm(function () {
        // login is successful, move to the next screen.
        
        //go to next scene.
        
      })
      .onerror(function (code, msg) {
        console.log(msg);

      });
       //update players animation
      if(localPlayer.isPlaying("walking") && direction ===""){
          localPlayer.stop();
        

      }
  }

});//end of bind key up

    Crafty.viewport.centerOn(localPlayer,0);
    Crafty.viewport.follow(localPlayer, 0, 0);



}//end of bindKeystoMessages




var generalCallback = function(serverMessage)
{//this here is where we receive server message that we didn't initiate.
  
  switch(serverMessage[0])
  {

    case "item.place":
      if(!entityManager.getGameStarted()){
        Crafty.scene("startGame");
      
      }
      entityManager.placeItem(serverMessage);
    break;

    case "item.remove":
      entityManager.removeItem(serverMessage);
    break;


    case "engine.update-entity":       
        // engine.update-entity <entity id> <new x> <new y> <new angle> <new x velocity> <new y velocity>
        //This is where the server tells us about entity movements
      if(!entityManager.getGameStarted()){
        Crafty.scene("startGame");
      
      }

      if(entityManager.updateEntity(serverMessage)===false)
      {
        console.log("Error on update entity, entityid " + serverMessage[1] + " doesn't exists");
      }

    break;
    case "engine.remove-entity":
        entityManager.removeEntity(serverMessage);
    break;


    case "engine.add-entity": 
      if(!entityManager.getGameStarted()){
          Crafty.scene("startGame");
        
        }
    
      console.log(serverMessage);  
      if(entityManager.createEntity(serverMessage)===false)
      {
        console.log("Error on entity creation, entityid " + serverMessage[1] + " already exists");
      }

      console.log()
    break;

    case "player.set-entity-id":
      if(!entityManager.getGameStarted()){
        Crafty.scene("startGame");

      
      }
      console.log("player.set-entity-id received: " + serverMessage[1]);
      entityManager.setLocalPlayerId(serverMessage[1]);
      entityManager.bindLocalPlayer();
	  console.log("game.js attempting to set up hud");
      myhud = new setupHUD();
	  console.log("game.js attempting to set up inventory");
	  myInventory = new inventoryManager();
	  console.log("game.js attempting to set up menu");
      myMenu = new menuCreator(myInventory,myhud);
	  console.log("game.js attempting to set up setupBasic Hud");
      myhud.setupBasic(entityManager.getLocalPlayer());
	  console.log("game.js attempting to set up chat");
	  myChat=new chatManager();
	  console.log("game.js attempting to set up main player for chat");
	  myChat.setupMainTarget(entityManager.getLocalPlayer());
	  console.log("game.js attempting to set up setupChat");
	  myChat.setupChat();
	  //myInventory.changeItem(0,"game_images_sprites/gold.png");
	  myMenu.makemenuitem(0, 1, "Inventory", "makeInventory.createInventory();");

    break;

    case "player.chat":
      myChat.chatHistory(serverMessage[1]);

    break;

    case "time.ping":

      // Ethan's note:  This is rather fragile, "mq" better be defined by this time
      mq.send(["time.pong", myTimer.epoch()]);
    break;
  }


}

 var addMouseRotation = function(){
    var localPlayer = entityManager.getLocalPlayer();
    //mouse based rotation
    Crafty.addEvent(this, "mousemove", function(e) 
    {
      var pos = Crafty.DOM.translate(e.clientX, e.clientY);
      //24 point offset is to get to the players center instead of top left corner
      var theta = Math.atan2(pos.y - (localPlayer._y+24), pos.x - (localPlayer._x+24));
      if (theta < 0){
         theta += 2 * Math.PI;
      }
      localPlayer.angle2=(theta * 180 / Math.PI)
      var angle = (theta * 180 / Math.PI) + 90
      if (angle>360)
      {
              angle=angle-360;
      }

      angle=Number(angle.toFixed(1));
      localPlayer.rotation=angle;
     

      if(!localPlayer.anglesent)//angle has never been sent.
      {
        message = ["player.set-angle",angle];
        localPlayer.anglesent = angle;
        mq.send(message)
        .onconfirm(function () {
       

        })
        .onerror(function (code, msg) {
        console.log(msg);

        });
      }
      else if(Math.abs(angle-localPlayer.anglesent)>2)
      {//last angle that was sent is more than 5 degrees off from our current angle. Send new angle.
        message = ["player.set-angle",angle];
        localPlayer.anglesent = angle;
        mq.send(message)
        .onconfirm(function () {
       

        })
        .onerror(function (code, msg) {
        console.log(msg);

        });


      }
   
    });



  }