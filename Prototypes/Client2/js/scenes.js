var positive = "#66cc66";
var negative = "#ff6666";
var neutral = "#ffffff"

/*JS FOR THE LOGIN FORM*/
var username_filled = false;
var pw_filled = false;

/*CHECK IF USERNAME INPUT IS VALID, HAS 4 OR MORE CHARACTERS*/
function validateuser(){
	var usr = document.getElementById('username_field');
	var error = document.getElementById('error');
	
	if(usr.value!=""){
		if(usr.value.length >= 4){
			username_filled = true;
			usr.style.backgroundColor = neutral;
			if(usr.value.length==20){
				error.innerHTML = '<div class="alert alert-error scene_error">Max 20 characters reached</div>';
			}
			else{
				error.innerHTML = '';//clear error message
			}
		}
		else{
			username_filled = false;
			usr.style.backgroundColor = negative;
			error.innerHTML = '<div class="alert alert-error scene_error">Username is too short</div>';
		}
	}
	else{
		username_filled = false;
		error.innerHTML = '';
	}

	if(pw_filled==true && username_filled==true){
		loginButton.disabled = false;
	}
	else{
		loginButton.disabled = true;
	}
}

/*CHECK THAT PASSWORD FIELD IS NOT EMPTY*/
function validatepw(){
	var pw = document.getElementById('password_field');
	var error = document.getElementById('error');	

	if(pw.value!=""){
		pw_filled = true;
		if(pw.value.length==20){
			error.innerHTML = '<div class="alert alert-error scene_error">Max 20 characters reached</div>';
		}
		else{
			error.innerHTML = '';
		}
	}
	else{
		pw_filled = false;
	}

	if(pw_filled==true && username_filled==true){
		loginButton.disabled = false;
	}
	else{
		loginButton.disabled = true;
	}
}


/*JS FOR SIGNUP SCENE*/
var validpw = false;
var validuser = false;

/*VALIDATE PW MATCHES PW2 AND THAT IT IS BETWEEN 4-2O CHARACTERS*/
function checkPass1(){
	var pass1 = document.getElementById('password_field');
    var pass2 = document.getElementById('confirm_password_field');
    var signupbutton = document.getElementById('signup_button');
    var error = document.getElementById('error');

    if(pass1.value.length == 20){
    	error.innerHTML = '<div class="alert alert-error scene_error">Max of 20 characters reached</div>';
    }
    else{
    	error.innerHTML = '';
    }

    if(pass1.value == pass2.value){ //cases when pw match
        if(pass1.value == "" && pass2.value ==""){//both empty
        	pass2.style.backgroundColor = neutral;
        	validpw = false;
        	error.innerHTML = '';
        }
        else{
	        pass2.style.backgroundColor = positive;
	        validpw = true;
	        error.innerHTML = '';//clear error message
        }
    }
    else if(pass1.value != pass2.value){ //passwords don't match
    	if(pass2.value==""){ //first form is filled, but not the second
    		pass2.style.backgroundColor = neutral;
	    	validpw = false;
    	}
    	else{ //both are filled and don't match
	   		pass2.style.backgroundColor = negative;	
	   		validpw = false;
	   		error.innerHTML = '<div class="alert alert-error scene_error">Passwords DON\'T match</div>';
	   	}
    }

    if(validpw==true && validuser==true){ //if username is filled and passwords match
    	signupbutton.disabled = false; //enable button
    }
    else{
    	signupbutton.disabled = true; //disable if not valid
    }
}

/*VALIDATE PW2 THAT IT MATCHES PW1*/
function checkPass2()
{
    //Store the password field objects into variables ...
	var pass1 = document.getElementById('password_field');
    var pass2 = document.getElementById('confirm_password_field');
    var error = document.getElementById('error');
    var signupbutton = document.getElementById('signup_button');

    if(pass1.value == pass2.value){ //password match, 
        if(pass1.value == "" && pass2.value ==""){//both empty
        	pass2.style.backgroundColor = neutral;
        	validpw = false;
        	error.innerHTML = '';
        }
        else{
	        pass2.style.backgroundColor = positive;
	        validpw = true;
	        error.innerHTML = '';//clear error message
        }
    }
    else if(pass1.value != pass2.value){
	    if(pass2.value=="") // second form is empty, still need second pw
	    {
	    	pass2.style.backgroundColor = neutral;
	    	validpw = false;
	    	error.innerHTML = '';
	    }
	    else if(pass1.value != pass2.value){ //passwords don't match
	   		pass2.style.backgroundColor = negative;	
	   		validpw = false;
	   		error.innerHTML = '<div class="alert alert-error scene_error" >Passwords DON\'T match</div>';
	    }
    }

    if(validpw==true && validuser==true){ //if username is filled and passwords match
    	signupbutton.disabled = false; //enable button
    }
    else{
    	signupbutton.disabled = true; //disable if not valid
    }
}

/*VALIDATE LENGTH OF USERNAME*/
function checkUserLength(){
	var signupbutton = document.getElementById('signup_button');
	var usr_str = document.getElementById('username_field');
	var error = document.getElementById('error');

	if(usr_str.value!=""){ // if username is filled in
		if(usr_str.value.length < 4){ 
			usr_str.style.backgroundColor = negative;
			validuser = false;
			error.innerHTML = '<div class="alert alert-error scene_error">Username is too short</div>';
		}
		else if(usr_str.value.length >= 4){
			usr_str.style.backgroundColor = neutral;
			validuser = true;
			error.innerHTML = '';//clear error message

			if(usr_str.value.length == 20){ //once user types in 30, alert them
				error.innerHTML = '<div class="alert alert-error scene_error">Max of 20 characters allowed</div>';
			}
		}
	}
	else{ //username input is blank
		validuser = false;
		usr_str.style.backgroundColor = negative;
		error.innerHTML = '<div class="alert alert-error scene_error">Can\'t be blank</div>';
	}

	if(validpw==true && validuser==true){//if forms are valid
    	signupbutton.disabled = false; //enable button
    }
    else{
    	signupbutton.disabled = true; //disable button
    }
}


/*JS FOR GAME CREATION SCENE*/
function checkGameNameLength(){
	var creategame = document.getElementById('createGame');
	var str = document.getElementById('gamename_field');
	var error = document.getElementById('error');

	if(str.value != ""){
		if(str.value.length >= 4){
			str.style.backgroundColor = neutral;
			creategame.disabled = false;
			if(str.value.length == 30){
				error.innerHTML = '<div class="alert-error alert scene_error">Max of 30 characters reached</div>';
			}
			else{
				error.innerHTML = '';
			}
		}
		else{
			str.style.backgroundColor = negative;
			creategame.disabled = true;
			error.innerHTML = '<div class="alert alert-error scene_error">Name of game is too short</div>'
		}
	}
	else{
		str.style.backgroundColor = negative;
		error.innerHTML = '<div class="alert alert-error scene_error">Can\'t be blank</div>';
		creategame.disabled = true;
	}
}

/*JS FOR CHARACTER CREATION*/
var charname_filledin = false;
var chartype_selected = false;

function checkCharNameLength(){
	var createchar = document.getElementById('createchar_button');
	var str = document.getElementById('charactername_field');
	var error = document.getElementById('error');

	if(str.value!=""){ // if username is filled in
		if(str.value.length < 4 && str.value.length != 0){				
			str.style.backgroundColor = negative;
			charname_filledin = false;
			error.innerHTML = '<div class="alert alert-error scene_error">Username is too short</div>';
			createchar.disabled = true;
		}
		else if(str.value.length >= 4){
			str.style.backgroundColor = neutral;
			charname_filledin = true; 
			error.innerHTML = '';//clear the error message
			createchar.disabled = false;

			if(str.value.length == 20){ //once user types in 20 characters, alert them
				error.innerHTML = '<div class="alert alert-error scene_error">Max of 20 characters reached</div>';
			}
		}
	}
	else{ //username input is blank
		charname_filledin = false;
		str.style.backgroundColor = negative;
		error.innerHTML = '<div class="alert alert-error scene_error">Name can\'t be blank</div>';
	}
}




