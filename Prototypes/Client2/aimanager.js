//----------------------------------------start ai manager------------------------------------
var AImanager = function(makeProj, chat)
{
	"use strict";

	var entityHit;
	var deathnum = 0;
	var difficulty = .01;
	var maxMobNum = 15;
	var mnumber = 0;
	//holds all ghosts
	var ghosts = [];
	var gnumber = 0;
	var maxGhosts = 5;
	//holds all portals
	var portals = {};
	var portalnum = 0;
	var maxPortalNum = 10;
	var targetPortal;
	//holds all targets for monsters
	var AItargets = [];
	//holds all multiplying AI
	var multiMob=[]
	var maxMulti = 5;
	var munumber = 0;
	var waveOn = false;

	var addHealthBar = function(entity)
	{
		var HUDbars1 = Crafty.e("2D, HTML, DOM, Persist")
			.attr({x: 60, y: 5, z:4, w:75, h:16, alpha:0.5 , id: entity.id})
			.replace("\
				<div class=\"progress progress-striped active\">\
					<div class=\"bar bar-danger\" id=\"AIhealthmeter\" style=\"width: 0%;\"></div>\
				</div>\
			");
		var x = document.getElementById("AIhealthmeter");
		x.id = "AIhealthmeter" + (entity.id);
		if(x !== undefined)
		{
			var $name = $('#AIhealthmeter' + (entity.id));

			HUDbars1.bind('EnterFrame', function() 
			{
				if(entity !== undefined)
				{
					this.x= entity.x + entity.w/2 - this.w/2;
					this.y= entity.y - this.h;
					$name.css('width', (entity.health/entity.maxhealth) * 100 + '%');
					$name.text(Math.round((entity.health/entity.maxhealth) * 100) + "%");
					//console.log((entity.health/entity.maxhealth) * 100 + '%');
				}
				else
				{
					myHUD2.destroy();
				}
			});
		}
		else
		{
			console.log("health bar error");
		}
		entity.attach(HUDbars1);
	};

	this.changeWave = function()
	{
		if(waveOn ==false)
		{
			waveOn=true;
			createPortals(500,890,100);
			createPortals(1312,1280,100);
			createPortals(1008,1584,100);
			createPortals(1375,560,100);
			createPortals(496,720,100);
			createPortals(864,384,100);
		}
		else
		{
			waveOn=false;
			killAll();
		}
	};
	this.changeDifficulty = function(dnum)
	{
		difficulty = dnum;
	};

	this.difficulty= function()
	{
		return difficulty;
	};

	this.addTargets = function(entity, enumber)
	{
		if(AItargets[enumber]===undefined)
		{		
			AItargets[enumber] = entity;
		}
	};
	this.deleteTargets = function(enumber)
	{
		if(AItargets[enumber]!==undefined)
		{
			delete AItargets[enumber]; //removes the entity from the array and sets that index value to undefined.
		}
	};

	var chatNow=function(entity, chat_Text, chatTime, chatOffsetx, chatOffsety, bwidth)
	{
		chat.displayMessage(entity, chat_Text, chatTime, chatOffsetx, chatOffsety, bwidth);
	};

	var killGhost = function (entity)
	{
		if (ghosts[entity.id] !== undefined)
		{
			mnumber--;
			gnumber--;
			targetPortal = entity.id;
			entity.destroy();
			delete ghosts[targetPortal];
		}
	};
	this.addghost = function(hp, speed)
	{
		createGhost(hp, speed);
	};
	var createGhost = function(hp, speed)
	{
		if(mnumber<maxMobNum && gnumber < maxGhosts)
		{
			if (portalnum >0)
			{
				for(var i = 0; i<=maxGhosts; i++)
				{
					if (ghosts[i]===undefined)
					{
						var spawnsite = [];
						var spawnsitetarget;
						var spawnsitepoint = 0;
						var curTargetPoint;
						var curTarget;
						var entityHit;
						for(var i = 0; i<maxPortalNum; i++)
						{
							if(portals[i]!==undefined)
							{
								spawnsite[spawnsitepoint] = portals[i];
								spawnsitepoint++;
							}
							if(spawnsitepoint == portalnum)
							{
								break;
							}
						}
						if(spawnsitepoint>0)
						{
							spawnsitetarget = spawnsite[Math.floor(Math.random() * spawnsitepoint)];
						}
						mnumber++;
						gnumber++;
						ghosts[i] = Crafty.e('2D, DOM, Image, Collision, SmartAI, MessureFPS, monsters, Tween')
							.attr({w:32, h:32, z:2, x: 10 ,y: 10, alpha:0.8, rotation:0, stuck:0, health: hp, mspeed: speed, spawnx: 10, spawny: 10, damage: 1 + Math.round(difficulty*4), id: i, exp:2 + Math.round(difficulty*10)})
							.image("game_images_sprites/ghost.png","no-repeat")
							.origin("center")
							.bind("EnterFrame", function() 
							{
								if(this.hit('pbullet'))
								{
									entityHit = this.hit("pbullet");
									for(var i = 0; i<entityHit.length; i++)
									{
										this.health = this.health - entityHit[i].obj.damage;
									}
									if (this.hit('shield'))
									{
										entityHit = this.hit("shield");
										for(var i = 0; i<entityHit.length; i++)
										{
											this.health = this.health - entityHit[i].obj.damage;
										}
									}
									if(this.health<0)
									{
										for(var i = 0; i <AItargets.length; i++)
										{
											if(AItargets[i] !== undefined)
											{
												AItargets[i].exp = AItargets[i].exp + this.exp;
											}
										}
										if (portalnum >0)
										{
											while(portalnum <=maxPortalNum)
											{
												spawnsite = portals[Math.floor(Math.random() * maxPortalNum + 1)];
									        	if(spawnsite!==undefined)
									        	{
													this.x = spawnsite._x;
													this.y = spawnsite._y;
													this.health = hp;
													deathnum++;
													break;
												}
											}
										}
										else
										{
											deathnum++;
											killGhost[this];
										}
									}
								}
								if(this.hit('player'))
								{
									entityHit = this.hit("player");
									for(var i = 0; i<entityHit.length; i++)
									{
										entityHit[i].obj.health = entityHit[i].obj.health - this.damage;
									}
									if (portalnum >0)
									{
										while(portalnum >0)
										{
											spawnsite = portals[Math.floor(Math.random() * maxPortalNum + 1)];
								        	if(spawnsite!==undefined)
								        	{
												this.x = spawnsite._x;
												this.y = spawnsite._y;
												this.health = hp;
												break;
											}
										}
									}
								}
								if(this.hit('safe') || this.hit('boundary'))
								{
									if (portalnum > 0)
									{
										while(portalnum >0)
										{
											spawnsite = portals[Math.floor(Math.random() * maxPortalNum + 1)];
								        	if(spawnsite!==undefined)
								        	{
												this.x = spawnsite._x;
												this.y = spawnsite._y;
												this.health = hp;
												break;
											}
										}
									}
									else
									{
										killGhost[this];
									}
								}
							})
							.bind("MessureFPS", function() 
							{
								for(var i = 0; i<=5; i++)
								{
									this.timeout(function()
									{
										this.rotation = this.rotation + 20;
									}, i*200);
								}
								if(AItargets.length >0 && portalnum>0)
								{
									if(AItargets.length>1)
									{
										curTargetPoint = Math.round(Math.random()* (AItargets.length -1));
									}
									else
									{
										curTargetPoint = 0;
									}
									if(AItargets[curTargetPoint] !== undefined)
									{
										curTarget = AItargets[curTargetPoint];
										this.tween({alpha: 0.3, x: curTarget.x , y: curTarget.y, rotation:this.rotation + 360}, this.mspeed);
									}
									else
									{
										console.log("target " + curTargetPoint + " undefined | AItargets : " + AItargets.length);
									}
								}
								else
								{
									killGhost[this];
								}
								this.alpha = 1;
							});
					break;
					}
				}
			}
			else
			{
				console.log("createGhost not created due to portalnum of " + portalnum);
			}
		}
	};

	var killMultiplier = function(entity)
	{
		if (multiMob[entity.id] !== undefined)
		{
			targetPortal = entity.id;
			mnumber--;
			munumber--;
			entity.destroy();
			delete multiMob[targetPortal];
		}
	};
	this.addMultiplier = function(hp, speed)
	{
		createMultiplier(hp, speed);
	};

	var createMultiplier = function(hp, speed)
	{
		if(maxMobNum>mnumber && munumber<maxMulti)
		{
			if (portalnum >0)
			{
				var spawnsite = [];
				var spawnsitetarget;
				var spawnsitepoint =0;
				var entityHit;
				for(var i = 0; i<maxPortalNum; i++)
				{
					if(portals[i]!==undefined)
					{
						spawnsite[spawnsitepoint] = portals[i];
						spawnsitepoint++;
					}
					if(spawnsitepoint == portalnum)
					{
						break;
					}
				}
				if(spawnsitepoint>0)
				{
					spawnsitetarget = spawnsite[Math.floor(Math.random() * spawnsitepoint)];
				}
				mnumber++;
				munumber++;
	        	if(spawnsitetarget!==undefined)
	        	{
	        		if(spawnsitetarget.x!==undefined && spawnsitetarget.y!==undefined)
	        		{
						for(var i = 0; i<=maxMulti; i++)
						{
							if (multiMob[i]===undefined)
							{
								multiMob[i] = Crafty.e("2D, DOM, Image, Collision, AIadaptive, monsters, Tween, MessureFPS")
								    .attr({w:32, h:50, x: spawnsitetarget._x, y: spawnsitetarget._y, oldfromx: spawnsitetarget._x, oldfromy: spawnsitetarget._y, cfromx: spawnsitetarget._x, cfromy: spawnsitetarget._y, z:1, look: 200, curTarget: 0, targetfound:0 , AId:1, health: hp, maxhealth: hp, mspeed: speed, id: i, stuck: 0, hstuck:.5, damage:1 + Math.round(difficulty*5), exp:20 + Math.round(difficulty*10), chatselector:0 , active:1})
								    .image("game_images_sprites/sprite2.png","no-repeat")
							 		.bind("EnterFrame", function()
							 		{
							 			if(this.active==1)
							 			{
								 			if(this.AId == 0 || this.AId == 45 || this.AId == 90 || this.AId == 135 || this.AId == 180 || this.AId == 225 || this.AId == 270 || this.AId == 315)
								 			{
									 			//change x
												if(this.AId == 45 || this.AId == 315)
												{
													this.x = this.x + this.mspeed;
									           		if(this.hit('solid'))
										        	{
														this.x = this.oldfromx;
										        	}
										        	else if (this.hit('AIadaptive'))
										        	{
										        		if(Math.random()> this.hstuck)
										        		{
										        			this.x = this.oldfromx;
										        		}
										        	}
									           		else
									           		{
														this.oldfromx=this.x;
									           		}
									           	}
									           	else if( this.AId == 135 || this.AId== 225)
									           	{
										           	this.x = this.x - this.mspeed;
									           		if(this.hit('solid'))
										        	{
														this.x = this.oldfromx;
										        	}
										        	else if (this.hit('AIadaptive'))
										        	{
										        		if(Math.random()> this.hstuck)
										        		{
										        			this.x = this.oldfromx;
										        		}
										        	}
									           		else
									           		{
														this.oldfromx=this.x;
									           		}
									           	}
									           	else if(this.AId == 0)
									           	{
									           		this.x = this.x + this.mspeed;
									           		if(this.hit('solid'))
										        	{
														this.x = this.oldfromx;
										        	}
										        	else if (this.hit('AIadaptive'))
										        	{
										        		if(Math.random()> this.hstuck)
										        		{
										        			this.x = this.oldfromx;
										        		}
										        	}
									           		else
									           		{
														this.oldfromx=this.x;
									           		}
									           	}
									           	else if(this.AId == 180)
									           	{
									           		this.x = this.x - this.mspeed;
									           		if(this.hit('solid'))
										        	{
														this.x = this.oldfromx;
										        	}
										        	else if (this.hit('AIadaptive'))
										        	{
										        		if(Math.random()> this.hstuck)
										        		{
										        			this.x = this.oldfromx;
										        		}
										        	}
									           		else
									           		{
														this.oldfromx=this.x;
									           		}
									           	}
									           	//change y
									           	if(this.AId == 45 || this.AId == 135)
												{
													this.y = this.y - this.mspeed;
									           		if(this.hit('solid'))
										        	{
														this.y = this.oldfromy;
										        	}
										        	else if (this.hit('AIadaptive'))
										        	{
										        		if(Math.random()> this.hstuck)
										        		{
										        			this.y = this.oldfromy;
										        		}
										        	}
									           		else
									           		{
														this.oldfromy=this.y;
									           		}
									           	}
									           	else if(this.AId == 315 || this.AId== 225)
									           	{
										           	this.y = this.y + this.mspeed;
									           		if(this.hit('solid'))
										        	{
														this.y = this.oldfromy;
										        	}
										        	else if (this.hit('AIadaptive'))
										        	{
										        		if(Math.random()> this.hstuck)
										        		{
										        			this.y = this.oldfromy;
										        		}
										        	}
									           		else
									           		{
														this.oldfromy=this.y;
									           		}
									           	}
									           	else if(this.AId == 90)
									           	{
									           		this.y = this.y - this.mspeed;
									           		if(this.hit('solid'))
										        	{
														this.y = this.oldfromy;
										        	}
										        	else if (this.hit('AIadaptive'))
										        	{
										        		if(Math.random()> this.hstuck)
										        		{
										        			this.y = this.oldfromy;
										        		}
										        	}
									           		else
									           		{
														this.oldfromy=this.y;
									           		}
									           	}
									           	else if(this.AId == 270)
									           	{
									           		this.y = this.y + this.mspeed;
									           		if(this.hit('solid'))
										        	{
														this.y = this.oldfromy;
										        	}
										        	else if (this.hit('AIadaptive'))
										        	{
										        		if(Math.random()> this.hstuck)
										        		{
										        			this.y = this.oldfromy;
										        		}
										        	}
									           		else
									           		{
														this.oldfromy=this.y;
													}
									           	}
											}
											else
											{
												//following player
												this.x = this.x + Math.round(Math.cos(this.AId * (Math.PI / 180)) * this.mspeed);
												if(this.hit('solid'))
												{
													this.x = this.oldfromx;
												}
												else if (this.hit('AIadaptive'))
												{
													if(Math.random()> this.hstuck)
													{
														this.x = this.oldfromx;
													}
												}
												else
												{
													this.oldfromx=this.x;
												}
												this.y = this.y + Math.round(Math.sin(this.AId * (Math.PI / 180)) * this.mspeed);
												if(this.hit('solid'))
												{
													this.y = this.oldfromy;
												}
												else if (this.hit('AIadaptive'))
												{
													if(Math.random()> this.hstuck)
													{
														this.y = this.oldfromy;
													}
												}
												else
												{
													this.oldfromy=this.y;
												}
											}
								           	//check if stuck
								           	if((this.oldfromx==this.cfromx || this.x==this.cfromx) && (this.oldfromy==this.cfromy || this.y==this.cfromy))
								           	{
												this.AId = Math.round(Math.random()*8)*45;
												this.chatselector = Math.random();
												if (this.chatselector<.01)
												{
													chatNow(this, "I Am So Hungry! FEED ME YOUR SOUL!", 2, 16, 0, 200);
												}
												else if(this.chatselector<.02)
												{
													chatNow(this, "Am I Lost?", 1, 16, 0, 100);
												}
												else if (this.chatselector<.03)
												{
													chatNow(this, "I Will Find You.", 1, 16, 0, 100);
												}
												this.stuck++;
												if(this.stuck>50)
												{
													killMultiplier(this);
												}
								           	}
								           	else
								           	{
								           		this.stuck=0;
								           		this.cfromx = this._x;
								           		this.cfromy = this._y;
								           	}
								           	//check if shot
											if(this.hit('pbullet'))
									        {
									        	entityHit = this.hit("pbullet");
												for(var i = 0; i<entityHit.length; i++)
												{
													this.health = this.health - entityHit[i].obj.damage;
												}
												if(this.health<0)
												{
													for(var i = 0; i <AItargets.length; i++)
													{
														if(AItargets[i] !== undefined)
														{
															AItargets[i].exp = AItargets[i].exp + this.exp;
														}
													}
													deathnum++;
													killMultiplier(this);
												}
												else if(Math.random()< .1)
												{
													chatNow(this, "Help me!", 1, 16, 0, 100);
												}
									        }
									    }
							 		})
							 		.bind("MessureFPS", function()
							 		{
							 			//error check
							 			if(this.x==undefined || this.y==undefined)
							 			{
							 				console.log("multiMob " + this.id + " position error");
							 			}
							 			this.image("game_images_sprites/sprite2.png","no-repeat");
							 			//end error check
							 			this.active=0;
							 			if(AItargets.length !=0)
										{
											for(var j = 0; j <AItargets.length; j++)
											{
												this.curTarget = AItargets[j];
												if(this.curTarget != undefined)
												{
													if((this._x - screenx < this.curTarget._x && this._x > this.curTarget._x) || (this._x + screenx > this.curTarget._x && this._x < this.curTarget._x) && (this._y - screeny < this.curTarget._y && this._y > this.curTarget._y) || (this._y + screeny > this.curTarget._y && this._y < this.curTarget._y))
													{
														this.active=1;
														if((this._x - this.look < this.curTarget._x && this._x > this.curTarget._x) || (this._x + this.look > this.curTarget._x && this._x < this.curTarget._x))
											 			{
											 				if((this._y - this.look < this.curTarget._y && this._y > this.curTarget._y) || (this._y + this.look > this.curTarget._y && this._y < this.curTarget._y))
											 				{
											 					this.chatselector = Math.random();
											 					if(this.chatselector <.02)
											 					{
											 						chatNow(this, "Target Aquired", 1, 16, 0, 100);
											 					}
											 					else if(this.chatselector <.04)
											 					{
											 						chatNow(this, "There Is No Escape", 1, 16, 0, 100);
											 					}
											 					else if(this.chatselector <.06)
											 					{
											 						chatNow(this, "I Can Smell Your Fear", 1, 16, 0, 100);
											 					}
											 					else if(this.chatselector <.06)
											 					{
											 						chatNow(this, "I Can Smell Your Fear", 1, 16, 0, 100);
											 					}
											 					else if(this.chatselector <.06)
											 					{
											 						chatNow(this, "I Can Smell Your Fear", 1, 16, 0, 100);
											 					}
																for(var k = 0; k<4; k++)
																{
																	this.timeout(function()
																	{
																		this.targetfound = 1;
																		this.AId = (Math.atan2(this._y - this.curTarget._y, this._x - this.curTarget._x) * (180 / Math.PI) + 180);
																	}, (1000/4)*k);
																}
																break;
															}
														}
													}
												}
											}
										}
							 			if(this.targetfound !=0)
							 			{
							 				this.targetfound = 0;
											this.AId = Math.round(Math.random()*8)*45;
										}
										for(var j = 0; j<10; j++)
										{
											this.timeout(function()
											{
												if(this.hit('player'))
										        {
										        	entityHit = this.hit("player");
													for(var k = 0; k<entityHit.length; k++)
													{
														entityHit[k].obj.health = entityHit[k].obj.health - this.damage;
													}
										        }
										        if(this.hit('safe'))
												{
													if (portalnum !=0)
													{
														while(portalnum !=0)
														{
															spawnsite = portals[Math.floor(Math.random() * maxPortalNum + 1)];
												        	if(spawnsite!==undefined)
												        	{
																this.x = spawnsite._x;
																this.y = spawnsite._y;
																this.health = hp;
																break;
															}
														}
													}
												}
								        	}, j*100);
										}
							 		});
								addHealthBar(multiMob[i]);
								if(Math.random() < difficulty)
								{
								var Boss=Crafty.e("2D, DOM, Image, MessureFPS")
									.image("game_images_sprites/death.png","no-repeat")
									.attr({w:320, h:275, x: multiMob[i].x, y: multiMob[i].y, z: 2, id: i, alpha:.4})
									.bind("EnterFrame", function()
							 		{
							 			if(multiMob[this.id]!==undefined)
							 			{
								 			this.x= multiMob[this.id].x -this.w/2;
								 			this.y= multiMob[this.id].y -this.h/2;
							 			}
							 			else
							 			{
							 				this.destroy();
							 			}
							 		});
							 		multiMob[i].health=multiMob[i].health+multiMob[i].health/2;
							 		multiMob[i].damage=multiMob[i].damage+multiMob[i].damage/2;
							 		multiMob[i].mspeed=multiMob[i].mspeed*2;
									break;
								}
							}
						}
					}
					else
					{
						console.log("spawnsitetarget defined but x and y undefined");
					}
				}
			}
			else
			{
				console.log("Multiplier not created due to portalnum of " + portalnum);
			}
		}
	};

	var killPortal = function(pnum)
	{
		if(portals[pnum.id]!==undefined)
    	{
    		targetPortal = pnum.id;
			portalnum--;
			portals[targetPortal].destroy();
			delete portals[targetPortal];
		}
		else
		{
			console.log("portal " + pnum.id + " deletion error")
		}
	};
	this.getPortals = function()
	{
		return portalnum;
	};
	this.addPortals = function(portalx, portaly, portalhp)
	{
		createPortals(portalx, portaly, portalhp);
	}
	var createPortals = function(portalx, portaly, portalhp)
	{
		if(portalnum<maxPortalNum)
		{
        	for(var i =0; i< maxPortalNum; i++)
        	{
	        	if(portals[i]===undefined)
	        	{	
	        		portalnum++;
					portals[i] = Crafty.e('2D, DOM, Image, Collision')
						.attr({x: portalx, y: portaly, w:28, h:25, z:5, rotation: 0, health: portalhp, exp:50, id: i})
						.image("game_images_sprites/portal.png")
						.origin("center")
						.bind("EnterFrame", function()
						{
							if (this.rotation < 350)
							{
								this.rotation = this.rotation+10;
							}
							else
							{
								this.rotation=0;
								this.image("game_images_sprites/portal.png");
								this.origin("center")
							}
							if(this.hit('pbullet'))
							{
								entityHit = this.hit("pbullet");
								for(var j = 0; j < entityHit.length; j++)
								{
									this.health = this.health - entityHit[j].obj.damage;
								}
								if(this.health<0)
								{
									for(var k = 0; k < AItargets.length; k++)
									{
										AItargets[k].exp = AItargets[k].exp + this.exp;
									}
									killPortal(this);
								}
							}
						});
						break;
				}
			}
		}
	};

	var killAll= function()
	{
		for(var i = 0; i<=Math.round(maxMobNum*1.5); i++)
		{
			if(portals[i] !==undefined)
			{
				portalnum--;
				portals[i].destroy();
				delete portals[i];
			}
			if (multiMob[i] !== undefined)
			{
				mnumber--;
				munumber--;
				multiMob[i].destroy();
				delete multiMob[i];
			}
			if (ghosts[i] !== undefined)
			{
				mnumber--;
				gnumber--;
				ghosts[i].destroy();
				delete ghosts[i];
			}
		}
	};

	var GameTimer = Crafty.e('MessureFPS')
		.bind("MessureFPS", function()
		{
			if(waveOn != false && portalnum>0)
			{
				if(Math.random()<.2 && difficulty <= 2)
	    		{
	    			difficulty= difficulty +.01;
	    		}
				if(difficulty<.25)
				{
					maxMulti=5;
					maxGhosts=2;
					maxMobNum = 15;
					maxPortalNum = 10;
					createMultiplier(10, 4);
				}
				else if(difficulty<.5)
				{
					maxMulti=6;
					maxGhosts=2;
					maxMobNum = 15;
					maxPortalNum = 12;
					createGhost(5, 70);
					createMultiplier(10, 4);
				}
				else if(difficulty<.75)
				{
					maxMulti=7;
					maxGhosts=4;
					maxMobNum = 15;
					maxPortalNum = 14;
					createGhost(5, 70);
					createMultiplier(10, 4);
				}
				else if(difficulty<.1)
				{
					maxMulti=8;
					maxGhosts=5;
					maxMobNum = 15;
					maxPortalNum = 16;
					createGhost(5, 70);
					createMultiplier(10, 4);
				}
				else
				{
					maxMulti = 15;
					maxGhosts = 6;
					maxMobNum = 30;
					maxPortalNum = 20;
					createGhost(5, 70);
					createMultiplier(10, 4);
				}
			}
		});

};

//-----------------------------------------AI manager end here-------------------------------