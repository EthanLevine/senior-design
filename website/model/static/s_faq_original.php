<html>
<head>
<style>
.faq_icons{
	position:relative;top:6px; margin-right:10px;
}

.faq_arrows{
	margin-right:5px;
}

.faq_answers{
margin:10px 0px 10px 23px;
}

.bold_font{
	font-weight:bold;
}
.nav li.active a{
    text-decoration: underline;
    font-weight: bolder;
}
.icon_position{
	position:relative; bottom:27px; right:10px;
}
.right_body{
	width:730px;
}


</style>
<script type="text/javascript">
	function showStuff(id,ptr) {
		if(document.getElementById(id).style.display == 'none'){//show answer
			document.getElementById(id).style.display = 'block';
			document.getElementById(ptr).innerHTML= '<i class="icon-chevron-down faq_arrows"></i>';
		}
		else{//hide answer
			document.getElementById(id).style.display = 'none';
			document.getElementById(ptr).innerHTML= '<i class="icon-chevron-up faq_arrows"></i>';
		}
	}
</script>
</head>
<body data-spy="scroll" data-target="#navbar">
	<div style="width:180px;" class="pull-left" id="navbar">
		<div style="position:fixed;" id="navbar">
			<ul class="nav nav-tabs nav-stacked" style="width:180px;">
				<li><a href="#game_faq">Game</a><i class="icon-chevron-right pull-right icon_position" style=""></i></li>
				<li><a href="#acct_faq">Account</a><i class="icon-chevron-right pull-right icon_position" ></i></li>
				<li><a href="#priv_faq">Privacy</a><i class="icon-chevron-right pull-right icon_position"></i></li>
				<li><a href="#misc_faq">Miscellaneous</a><i class="icon-chevron-right pull-right icon_position"></i></li>
				<li><a href="#contactUs" data-toggle="modal">Contact Us</a><i class="icon-envelope pull-right icon_position"></i></li>
			</ul>	
		</div>
	</div>
	<div class="pull-right right_body">
		<h2>Frequently Asked Questions</h2>


<div class="accordion" id="accordion2">
  <div class="accordion-group">
    <div class="accordion-heading">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
        Collapsible Group Item #1
      </a>
    </div>
    <div id="collapseOne" class="accordion-body collapse in">
      <div class="accordion-inner">
        Anim pariatur cliche...
      </div>
    </div>
  </div>
  <div class="accordion-group">
    <div class="accordion-heading">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
        Collapsible Group Item #2
      </a>
    </div>
    <div id="collapseTwo" class="accordion-body collapse">
      <div class="accordion-inner">
        Anim pariatur cliche...
      </div>
    </div>
  </div>
</div>

		
		<div id="game_faq">
		<h3><i class="icon-off faq_icons"></i>Game FAQ</h3>
		<ul class="unstyled">
			<li>
				<p><a href="#" onclick="showStuff('game1','arrow1'); return false;"><span id="arrow1"><i class="icon-chevron-up faq_arrows"></i></span>
					What are the system requirements?</a><br>
				<div id="game1" class="faq_answers" style="display:none;"><!--KEEP display:none as inline css, requires double clicking to show answers if not inline-->
					<table class="table table-condensed table-bordered" style="width:630px;">
						<tr>
							<td class="bold_font">System</td>
							<td class="bold_font">Minimum Requirements</td>
							<td class="bold_font">Recommended Settings</td>
						</tr>
						<tr>
							<td class="bold_font">OS</td>
							<td>Mac OSX, Windows(XP,Vista,7,8)</td>
							<td>Mac OSX, Windows(Vista,7,8)</td>
						</tr>
						<tr>
							<td class="bold_font">Browser</td>
							<td>IE (8 and up), Chrome, Firefox, Opera??</td>
							<td>IE (8 and up), Chrome, Firefox, Opera??</td>
						</tr>
						<tr>
							<td class="bold_font">Connection Speed</td>
							<td>DSL (128kpbps) or better</td>
							<td>DSL (256kpbps) or better</td>
						</tr>						
					</table>
				</div></p>
			</li>
			<li>
				<p><a href="#" onclick="showStuff('game2','arrow2'); return false;"><span id="arrow2"><i class="icon-chevron-up faq_arrows"></i></span>
					Can I play on a mobile/tablet device?</a><br>
				<span id="game2" class="faq_answers" style="display:none;">
					Unfortunately, UConnocalypse <strong>CANNOT</strong> be played on mobile or tablet devices. It is designed for Desktop PCs and Laptops. 
					Perhaps, in the future, we will support mobile and tablet devices, but for now it isn't possible to do so.
				</span></p>
			</li>
			<li>
				<p><a href="#" onclick="showStuff('game3','arrow3'); return false;"><span id="arrow3"><i class="icon-chevron-up faq_arrows"></i></span>
					How do I save my progress in a game?</a><br>
				<span id="game3" class="faq_answers" style="display:none;">
					To save your progress, just click "SAVE" on the HUD during gameplay OR just make sure to regularly reach checkpoints to let auto-save do it for you.
				</span></p>
			</li>		
			<li>
				<p><a href="#" onclick="showStuff('game4','arrow4'); return false;"><span id="arrow4"><i class="icon-chevron-up faq_arrows"></i></span>
					Help! The game does NOT load.</a><br>
				<span id="game4" class="faq_answers" style="display:none;">
					Sorry to hear that. Just try refreshing the browser and logging back in again. If the problem persists, please send us a message and we'll investigate the issue.
				</span></p>
			</li>
		</ul>
		</div>
		<hr>

		<div id="acct_faq">
		<h3><i class="icon-user faq_icons"></i>Account FAQ</h3>
		<ul class="unstyled">
			<li>
				<p><a href="#" onclick="showStuff('act1','act_arrow1'); return false;"><span id="act_arrow1"><i class="icon-chevron-up faq_arrows"></i></span>
					How do I register?</a><br>
				<span id="act1" class="faq_answers" style="display:none;">
					To register for an account, just click the "Sign Up" button on the login screen and properly complete the form and then SUBMIT. Voila! 
					You should be on your way to saving Jay Hickey and the rest of UConn.
				</span></p>
			</li>
			<li>
				<p><a href="#" onclick="showStuff('act2','act_arrow2'); return false;"><span id="act_arrow2"><i class="icon-chevron-up faq_arrows"></i></span>
					How do I change my password?</a><br>
				<span id="act2" class="faq_answers" style="display:none;">
					This feature is currently not enabled. Should you need to update/modify your password, please send us a message and we'll assist you.
				</span></p>
			</li>
			<li>
				<p><a href="#" onclick="showStuff('act3','act_arrow3'); return false;"><span id="act_arrow3"><i class="icon-chevron-up faq_arrows"></i></span>
					What if I lost my password?</a><br>
				<span id="act3" class="faq_answers" style="display:none;">
					UHHHH.... 
				</span></p>
			</li>		
			<li>
				<p><a href="#" onclick="showStuff('act4','act_arrow4'); return false;"><span id="act_arrow4"><i class="icon-chevron-up faq_arrows"></i></span>
					How do iI change my account information?</a><br>
				<span id="act4" class="faq_answers" style="display:none;">
					This feature is currently not enabled. Should you need to update/modify your password, please send us a message and we'll assist you.
				</span></p>
			</li>
			<li>
				<p><a href="#" onclick="showStuff('act5','act_arrow5'); return false;"><span id="act_arrow5"><i class="icon-chevron-up faq_arrows"></i></span>
					How do I delete my account?</a><br>
				<span id="act5" class="faq_answers" style="display:none;">
					This feature is currently not available to users - only to the administrators. Should you need to delete the account, please send us a message 
					and we'll be glad to assist you in that process.
				</span></p>
			</li>
		</ul>
		</div>
		<hr>

		<div id="priv_faq">
		<h3><i class="icon-lock faq_icons"></i> Privacy Policy FAQ</h3>
		</div>

		<hr>
		<div id="misc_faq">
		<h3><i class="icon-asterisk faq_icons"></i>Miscellaneous FAQ</h3>
		</div>

		<!-- Modal -->
		<div hidden id="contactUs" class="modal hide fade">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3>Send Us A Message</h3>
			</div>
			<form class="form-horizontal" method="post" name="mail" action="http://web.uconn.edu/cgi-bin/formmail.pl">
				<div class="modal-body">	
					<input type="hidden" name="recipient" value="kraig.dandan@uconn.edu">
					<input type="hidden" name="required" value="email,subject,message">
					<div class="pull-left">
						<div class="control-group">
							<div class="input-prepend">
								<span class="add-on"><i class="icon-envelope"></i></span>
								<input type="text" class="span5" name="email" value="" size="64" placeholder="Your Email (Required)" maxLength="30" required>
							</div>
						</div>
						<div class="control-group">
							<div class="input-prepend">
								<span class="add-on"><i class="icon-tag"></i></span>
								<input type="text" class="span5" name="subject" value="" size="64" placeholder="Subject (Optional)" maxLength="30">
							</div>
						</div>
					</div>

					<div class="pull-right">
						<img src="../../view/img/husky_vs_zombie.png" width="100" height="60" style="margin:0px; border:solid 1px #c0c0c0;"/>
					</div>

					<div>
						<textarea name="message" rows="4" style="resize:none; width:515px;" placeholder="Enter your message... (Required - max of 300 characters)" maxLength="300" required></textarea><br>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-large" data-dismiss="modal">Cancel</button>
					<input type="submit" value="Send" class="btn btn-large btn-primary">
				</div>
			</form>
		</div>
		<!--modal -->
	</div>
</body>
</html>