<style>
.hero_mod{
	border:1px solid;
	padding:50px;
}

.minibox_preview{ /*3 boxes below the jumbotron*/
	height:160px;
	margin-bottom:10px;
	overflow:hidden;
}

.icon_position{
	position:relative;top:5px;
}

</style>

<div id='home1'>
	<div class="hero-unit hero_mod">	
		<div style="width:400px;" class="">
			<h2>Welcome to UConnocalypse!</h2>

			<p>
			UConn is in trouble. Save Johnathan and the Storrs campus from the apocalypse
			by battling the monsters wreaking havoc all over 
			campus.
			</p>

			<p>
				<a href="../pages/about"class="btn btn-primary btn-large"><i class="icon-white icon-info-sign"></i>
				Learn more
				</a>
				&nbsp;&nbsp;

				<a href="" class="btn btn-danger btn-large"><i class="icon-white icon-play"></i> 
				Play Now!
				</a>
			</p>
		</div>
		<img src="../../view/img/husky_vs_zombie.png" alt="husky_vs_zombie" class="pull-right" style="margin-top:-300px; height:340px; width:380px;">
	</div>

	<div class="row">
		<div class="span4">
			<h3><a href="../pages/about.php" style="color:#383838"><i class="icon-info-sign icon_position"></i> Learn more</a></h3>
			<div class="minibox_preview">
				<p>UConnocalypse is a 2D, top-down, multiplayer, action-adventure game that is built in HTML5, Java, and JavaScript (Crafty.js).
				 The objective is to save UConn Storrs Campus from the apocalypse coming down upon the school. Play with friends or play solo - fight monsters, zombies, 
				 bosses and complete tasks all over campus...
				</p>
			</div>
			<p><a class="btn" href="../pages/about.php">View details &raquo;</a></p>
		</div>
		<div class="span4">
			<h3><a href="../pages/howto.php" style="color:#383838"><i class="icon-certificate icon_position"></i> How-to-play</a></h3>
			<div class="minibox_preview">
				<p>Uconnocalypse takes a similar approach to most common online/PC games using the mouse, the WASD configuration, and some extra keys. 
					The the mouse/cursor is used for direction of aim, whereas the WASD keys (W - Move UP, A - Move LEFT, S - Move DOWN, and D - Move RIGHT) are used for movements and
					the SPACE bar is used...</p>
			</div>
			<p><a class="btn" href="../pages/howto.php">View details &raquo;</a></p>
		</div>
		<div class="span4">
			<h3><a href="../pages/faq.php" style="color:#383838"><i class="icon-question-sign icon_position"></i> Got questions?</a></h3>
			<div class="minibox_preview">
				<p>We have a compilation of common questions regarding Accounts, Game, Privacy, and more... Hope this helps! If theres
				a Q/A that's not listed, please send us a message here and we'll try to get back to you as soon as we can. We also welcome comments and suggestions - or if you
				just want to say hi, send us a message!</p>
			</div>
			<p><a class="btn" href="../pages/faq.php">View details &raquo;</a></p>
		</div>
	</div>
</div>