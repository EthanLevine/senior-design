<style>
hr{
	height:1px;
	background-color: #ccc;
}
.info_row{
	margin-bottom:15px; margin-top:15px;
}
.title_info{
	text-align:center;
}
.info_img{
	width: 300px; height: 200px;
}
.learn_tags{
	width:923px; 
	margin:15px auto;
}

.nav li.active a{
    text-decoration: underline;
    font-weight: bolder;
}

.icon_position{
	position:relative; bottom:27px; right:10px;
}

.right_body{
	width:730px;
}

/*.page_title{
	text-decoration:underline;
}*/

.tab_content{
	width:420px;
}

.fixed_nav_holder{
	width:180px; 
	height:100%;
}

.fixed_nav{
	width:180px;
	position:fixed;
}

.gallery_button{
	width:160px; margin:0px auto;
}

.left_description{
	width:400px; margin-right:30px;
}

.right_description{
	width:400px;
}
</style>

<body data-spy="scroll" data-target="#navbar">
	<div >
		<div class="pull-left fixed_nav_holder" id="navbar">
			<div class="fixed_nav">
				<ul class="nav nav-tabs nav-stacked">
					<li><a href="#gallery">Gallery</a><i class="icon-chevron-right pull-right icon_position" style=""></i></li>
					<li><a href="#gameplay">Gameplay</a><i class="icon-chevron-right pull-right icon_position"></i></li>
					<li><a href="#rules">Rules</a><i class="icon-chevron-right pull-right icon_position"></i></li>
					<li><a href="#campaign">Campaign</a><i class="icon-chevron-right pull-right icon_position"></i></li>
					<li><a href="#multiplayer">Multiplayer</a><i class="icon-chevron-right pull-right icon_position"></i></li>
					<li><a href="#characters">Characters</a><i class="icon-chevron-right pull-right icon_position"></i></li>
					<li><a href="#weapons">Weapons/Items</a><i class="icon-chevron-right pull-right icon_position"></i></li>
					<li><a href="#maps">Maps</a><i class="icon-chevron-right pull-right icon_position"></i></li>
					<li><a href="#levels">Levels</a><i class="icon-chevron-right pull-right icon_position"></i></li>
				</ul>
			</div>
		</div>
		<div id='about1' class="pull-right right_body">	
			<h1 class="page_title">About</h1>
			<p>
				The game is intended for UConn students who can relate to the happenings and characters at the University.
				The objective of the game is to save UConn Storrs Campus from the apocalypse coming down 
				upon the school. Play with friends or play solo - fight monsters, zombies, bosses and complete tasks 
				all over campus.
				
				Overall, the it is designed around a multiplayer functionality: stressing teamwork and completing objectives 
				as a team. Up to four players must battle monsters together, use their unique abilities together, and trade 
				items together when needed.
			</p>
			<hr style="height:2px; background-color: #ccc;">
			<div class="info_row" id="gallery">
				<div>
					<h3 class="title_info">Gallery</h3>
					<p>
						Here is a compilation of in-game screenshots to show you what UConnocalypse looks like and what its about.
						Click on the button below to view the images. 
					</p>
				</div>

				<div class="gallery_button">
					<a href="#myModal" role="button" class="btn btn-large" data-toggle="modal"><i class="icon-picture"></i> View Gallery</a>
				</div>
				<!-- Modal -->
				<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 id="myModalLabel">UConnocalypse Gallery</h3>
					</div>
					
					<div class="modal-body">
						<div id="myCarousel" class="carousel slide">
							<ol class="carousel-indicators">
								<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
								<li data-target="#myCarousel" data-slide-to="1"></li>
							</ol>
							
							<!-- Carousel items -->
							<div class="carousel-inner">
								<div class="active item"><img src="../../view/img/apocalypse.jpg"/><div class="carousel-caption"><p>FASDFASFAFS</p></div></div>
								<div class="item"><img src="../../view/img/apocalypse2.jpg"/></div>
							</div>
							
							<!-- Carousel nav -->
							<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
							<a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
						</div>
					</div>
					<!--
					<div class="modal-footer">
					<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
					<button class="btn btn-primary">Save changes</button>
					</div>
					-->
				</div>
			</div>

			<div class="info_row" id="gameplay" style="height:330px;">
				<hr>
				<h3 class="title_info">Gameplay</h3>
				<div>
					<img src="http://placehold.it/300x200"/>
					<div class="pull-right right_description">
						<p>
							Progress will be saved by seeing which objectives have been completed. 
							The game will have four to five objectives, each of which marks an event 
							the player must complete in order to get to the next objective of the game. 
							When the player completes all objectives, they will beat the game. There 
							are also sub-objectives within each objective that player must also fulfill; 
							however, the number of sub-objectives will depend on the difficulty the 
							player has set. That is, higher difficulties will have more sub-objectives. 
							Player progress will be saved at the most recently completed objective. The 
							player will continue onto the next objective if they had saved their progress.
						</p>
					</div>
				</div>
			</div>

			<div class="info_row" id="rules" >
				<hr>
				<h3 class="title_info">Rules</h3>
				<div class="pull-left left_description" style="">
					<p>Etiam porta sem malesuada magna mollis euismod. Aenean lacinia bibendum nulla 
			sed consectetur. Vestibulum id ligula porta felis euismod semper. Donec id elit 
			non mi porta gravida at eget metus. Integer posuere erat a ante venenatis 
			dapibus posuere velit aliquet.Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
				</div>
				<img src="http://placehold.it/300x200"/>
			</div>

			<div class="info_row" id="campaign">
				<hr>
				<h3 class="title_info">Campaign</h3>
				<div>
					<img src="http://placehold.it/300x200"/>
					<div class="pull-right right_description">
					<p>Etiam porta sem malesuada magna mollis euismod. Aenean lacinia bibendum nulla 
			sed consectetur. Vestibulum id ligula porta felis euismod semper. Donec id elit 
			non mi porta gravida at eget metus. Integer posuere erat a ante venenatis 
			dapibus posuere velit aliquet. Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
					</div>
				</div>
			</div>

			<div class="info_row" id="multiplayer" style="height:290px;">
				<hr>
				<h3 class="title_info">Multiplayer</h3>
				<div class="pull-left left_description">
					<p>
					Not only can UConnpocalypse be played solo in campaign mode, you can also play with others (upto 3 players)! 
					Need help beating a level or want to help someone? Set up or join a "Private" game with your friends and play 
					along together in a cooperative mode. Or just simply play with anyone by creating or joining
					a "Public" game. The game is designed to be challenging to emphasize teamwork; to encourage player interaction
					by trading items and strategize together.
					</p>
				</div>
				<img src="http://placehold.it/300x200"/>
			</div>

			<div class="info_row" id="characters">
				<hr>
				<div>
					<h3 class="title_info">Characters</h3>
					<p>
					In the game, there will be four player classes; Biologist (Pre-med), Engineering, Chemist, and Philosopher. 
					Each character will possess specific powers and abilities specific to their class as described below. Much of these described 
					skills and abilities are spoofs and stereotypes of the modern day student. These spoofs are employed within 
					the game for comedy to help the student relate to real-life events.
					</p>
				</div>
				
				<div class="tabbable">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#char1" data-toggle="tab">Engineer</a></li>
						<li><a href="#char2" data-toggle="tab">Chemist</a></li>
						<li><a href="#char3" data-toggle="tab">Biologist</a></li>
						<li><a href="#char4" data-toggle="tab">Philosopher</a></li>
					</ul>
					<div class="tab-content" style="padding-left:20px;">
						<div id="pane1" class="tab-pane active">
							<img src="http://placehold.it/260x180"/>		
							<ul class="pull-right unstyled tab_content">
								<h5>Engineer special skills &amp; abilities:</h5>
								<li><i class="icon-ok"></i> Build and use awesome robots to fight enemies and assist</li>
								<li><i class="icon-ok"></i> Build and use awesome machinery</li>
								<li><i class="icon-ok"></i> Build and use awesome machinery</li>
								<li><i class="icon-ok"></i> Build and use awesome machinery</li>
								<li><i class="icon-ok"></i> Build and use awesome machinery</li>
							</ul>
						</div>

						<div id="char2" class="tab-pane">
							<img src="http://placehold.it/260x180"/>		
							<ul class="pull-right unstyled tab_content">
								<h5>Chemist special skills &amp; abilities:</h5>
								<li><i class="icon-ok"></i> Use chemicals to casts spells</li>
								<li><i class="icon-ok"></i> Use chemicals to casts spells</li>
								<li><i class="icon-ok"></i> Use chemicals to casts spells</li>
								<li><i class="icon-ok"></i> Use chemicals to casts spells</li>
								<li><i class="icon-ok"></i> Use chemicals to casts spells</li>
							</ul>
						</div>

						<div id="char3" class="tab-pane">
							<img src="http://placehold.it/260x180"/>	
							<ul class="pull-right unstyled tab_content">
								<h5>Biologist special skills &amp; abilities:</h5>
								<li><i class="icon-ok"></i> Restoring Health and Life</li>
								<li><i class="icon-ok"></i> Restoring Health and Life</li>
								<li><i class="icon-ok"></i> Restoring Health and Life</li>
								<li><i class="icon-ok"></i> Restoring Health and Life</li>
								<li><i class="icon-ok"></i> Restoring Health and Life</li>
							</ul>
						</div>

						<div id="char4" class="tab-pane">
							<img src="http://placehold.it/260x180"/>
							<ul class="pull-right unstyled tab_content">
								<h5>Philosopher special skills &amp; abilities:</h5>
								<li><i class="icon-ok"></i> fasdfadsfasdfasdfadsfad</li>
								<li><i class="icon-ok"></i> fasdfadsfasdfasdfadsfad</li>
								<li><i class="icon-ok"></i> fasdfadsfasdfasdfadsfad</li>
								<li><i class="icon-ok"></i> fasdfadsfasdfasdfadsfad</li>
								<li><i class="icon-ok"></i> fasdfadsfasdfasdfadsfad</li>	
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="info_row" id="weapons">
				<hr>		
				<div>
					<h3 class="title_info">Weapons/Items</h3>
					<p>Etiam porta sem malesuada magna mollis euismod. Aenean lacinia bibendum nulla 
			sed consectetur. Vestibulum id ligula porta felis euismod semper. Donec id elit 
			non mi porta gravida at eget metus. Integer posuere erat a ante venenatis 
			dapibus posuere velit aliquet. Donec id elit non mi porta gravida at eget metus. 
			Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa 
			justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
				</div>
			</div>

			<div class="info_row" id="maps" style="height:280px;">
				<hr>
				<h3 class="title_info">Maps</h3>
				<div class="left_description pull-left">
					<p>Etiam porta sem malesuada magna mollis euismod. Aenean lacinia bibendum nulla 
			sed consectetur. Vestibulum id ligula porta felis euismod semper. Donec id elit 
			non mi porta gravida at eget metus. Integer posuere erat a ante venenatis 
			dapibus posuere velit aliquet. Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
				</div>
				<img src="http://placehold.it/300x200"/>
			</div>

			<div class="info_row" id="levels">
				<hr>
				<div>
					<h3 class="title_info">Levels</h3>
					<p>
					There will be four difficulty levels in the game, ranging from Freshman, Sophomore, Junior, Senior, and Super-Senior. 
					Each level being of increasing difficulty respectively.
					Once the player beats the game on Freshman difficulty, they can move onto 
					Sophomore difficulty, which will feature tougher enemies and more 
					sub-objectives for each objective. Having multiple difficulties enhances the 
					replayability and give the player a challenge, as well as give the player a 
					different experience each time they play through the game.
					</p>
				</div>

				<div class="tabbable">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#lvl1" data-toggle="tab">Freshman</a></li>
						<li><a href="#lvl2" data-toggle="tab">Sophomore</a></li>
						<li><a href="#lvl3" data-toggle="tab">Junior</a></li>
						<li><a href="#lvl4" data-toggle="tab">Senior</a></li>
						<li><a href="#lvl5" data-toggle="tab">Super-Senior</a></li>
					</ul>
					<div class="tab-content" style="padding-left:20px;">
						<div id="lvl1" class="tab-pane active">
							<ul class="unstyled">
								<h5>Requirements and Benefits:</h5>
								<li>Lorem Ipsum</li>
								<li>Lorem Ipsum</li>
								<li>Lorem Ipsum</li>
								<li>Lorem Ipsum</li>

							</ul>
						</div>

						<div id="lvl2" class="tab-pane">
							<ul class="unstyled">
								<h5>Requirements and Benefits:</h5>

							</ul>
						</div>

						<div id="lvl3" class="tab-pane">
							<ul class="unstyled">
								<h5>Requirements and Benefits:</h5>

							</ul>
						</div>

						<div id="lvl4" class="tab-pane">
							<ul class="unstyled">
								<h5>Requirements and Benefits:</h5>

							</ul>
						</div>

						<div id="lvl5" class="tab-pane">
							<ul class="unstyled">
								<h5>Requirements and Benefits:</h5>

							</ul>
						</div>				
					</div><!-- /.tab-content -->
				</div><!-- /.tabbable -->
			</div>
		</div>
	</div>
</body>