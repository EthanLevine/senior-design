<style>
.minibox_preview{ /*3 boxes below the jumbotron*/
	height:160px;
	margin-bottom:10px;
	overflow:hidden;
}

.minibox_headers{
	color:#383838
}

.home_page_container{
position:relative; top:-50px;	
}

.carousel_jumbotron{
height:400px;
overflow: hidden;
}

.jumbotron_bg{
	height:400px;
}

.call_to_action{
	 height:235px; padding:20px; margin-top:50px; background:rgb(192,192,192); background: rgba(192,192,192,.5);
}

.call_to_action_btn{
	margin:0 auto; width:150px;
}

.call_to_action_btns{
	margin:0 auto; width:300px;
}

h3{
	text-align: center;
}


</style>

<div class="home_page_container">
	<div id="myCarousel" class="carousel slide">
		<!--
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
		</ol>
		-->
		<!-- Carousel items -->
		<div class="carousel-inner carousel_jumbotron">

			<!--SLIDE 1-->
			<div class="active item">
				<div class="jumbotron_bg" style="background-image:url('../../view/img/test_blur.jpg'); background-size:100%;">
					<div class="container">
						<div class="hero-unit call_to_action" style="width:780px; margin-left:auto; margin-right:auto;">
							<div class="pull-left" style="width:440px;">
								<h2>Welcome to UConnocalypse!</h2>
								<p>
								This is a 2D, top-down, multiplayer, action-adventure game; where an 
								apococalypse is coming down upon UConn and you have to save everyone by battling monsters and completing tasks. 
								</p>

								<div class="call_to_action_btns">
									<a href="../pages/about"class="btn btn-primary btn-large"><i class="icon-white icon-info-sign"></i>
									Learn more
									</a>
									&nbsp;&nbsp;

									<a href="" class="btn btn-danger btn-large"><i class="icon-white icon-play"></i> 
									Sign Up!
									</a>
								</div>
							</div>
							<div class="pull-right">
								<img src="../../view/img/husky_vs_zombie_clear" height="300" width="275"/>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!--SLIDE 2-->
			<div class="item">
				<div class="jumbotron_bg" style="background-image:url('../../view/img/test_blur.jpg'); background-size:100%;">
					<div class="container">
						<div class="pull-right hero-unit call_to_action">
							<div style="width:200px; height:190px;" class="pull-left">
							<p>
							Check out some in-game footage and screenshots and see what the game is all about.
							</p>
							</div>
							<img src="http://placehold.it/210x170" class="pull-right" style="opacity:.5"/>

							<div class="call_to_action_btn">
								<a href="../pages/about" class="btn btn-danger btn-large"><i class="icon-white icon-picture"></i>
								View Gallery
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!--SLIDE 3-->
			<div class="item">
				<div class="jumbotron_bg" style="background-image:url('../../view/img/test_blur.jpg'); background-size:100%;">
					<div class="container">
						<div class="pull-right hero-unit call_to_action">
							<div style="width:200px;" class="pull-left">
							<p>
							UConn is in trouble. Save Johnathan and the Storrs campus from the apocalypse
							by battling the monsters wreaking havoc all over 
							campus.
							</p>
							</div>
							<img src="../../view/img/husky_vs_zombie" height="240" width="220" class="pull-right" style="opacity:.5"/>

							<div class="call_to_action_btn">
								<a href="../pages/about" class="btn btn-danger btn-large"><i class="icon-white icon-"></i>
								Play Now!
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Carousel nav -->
		<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
		<a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
	</div>

	<div class="container" style="margin-bottom:-50px; margin-top:0px;">
		<div class="row">
			<div class="span4">
				<h3><a href="../pages/howto.php" class="minibox_headers">How-to-play</a></h3>
				<div class="minibox_preview">
					<p>Uconnocalypse takes a similar approach to most common online/PC games using the mouse, the WASD configuration, and some extra keys. 
						The the mouse/cursor is used for direction of aim, whereas the WASD keys (W - Move UP, A - Move LEFT, S - Move DOWN, and D - Move RIGHT) are used for movements and
						the SPACE bar is used...</p>
				</div>
				<p><a class="btn" href="../pages/howto.php">View details &raquo;</a></p>
			</div>
			<div class="span4">
				<h3><a href="../pages/faq.php" class="minibox_headers">Questions?</a></h3>
				<div class="minibox_preview">
					<p>We have a compilation of common questions regarding Accounts, Game, Privacy, and more... Hope this helps! If theres
					a Q/A that's not listed, please send us a message here and we'll try to get back to you as soon as we can. We also welcome comments and suggestions - or if you
					just want to say hi, send us a message!</p>
				</div>
				<p><a class="btn" href="../pages/faq.php">View details &raquo;</a></p>
			</div>
			<div class="span4">
				<h3><a href="../pages/about.php" class="minibox_headers">Connect</a></h3>
				<div class="minibox_preview">
					<ul class="unstyled" style="margin:0px auto;">
						<li ><a href="https://bitbucket.org/EthanLevine/senior-design/src"><img src="../../view/img/bitbucket-icon.png" height="64" width="64" style="margin-right:10px;"/>View the project on BitBucket</a></li>
						<br>
						
						<script src="http://squaresend.com/squaresend.js"></script>					
						<li ><a href="mailto:bkqblqp?sqs_title=Contact+Us&sqs_label_submit=Submit"><img src="../../view/img/mail-icon.png" height="64" width="64" style="margin-right:10px;"/>Send us a message</a></li>
					</ul>
					<br>
					<br>
					<!--
					<div style="margin:0px auto; width:170px;">
					<button class="btn btn-large"><i class="icon-envelope"></i> &nbsp; Message Us</button>
					</div>
					-->
				</div>
				<!--
				<p><a class="btn" href="../pages/about.php">View details &raquo;</a></p>
				-->
			</div>
		</div>
	</div>
</div>