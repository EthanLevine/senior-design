<html>
<head>
<style>
.faq_icons{
	position:relative;top:6px; margin-right:10px;
}

.faq_arrows{
	margin-right:5px;
}

.faq_answers{
margin:10px 0px 10px 23px;
}

.bold_font{
	font-weight:bold;
}
.nav li.active a{
    text-decoration: underline;
    font-weight: bolder;
}
.icon_position{
	position:relative; bottom:27px; right:10px;
}
.right_body{
	width:730px;
}


</style>
<script type="text/javascript">
	function showStuff(id,ptr) {
		if(document.getElementById(id).style.display == 'none'){//show answer
			document.getElementById(id).style.display = 'block';
			document.getElementById(ptr).innerHTML= '<i class="icon-chevron-down faq_arrows"></i>';
		}
		else{//hide answer
			document.getElementById(id).style.display = 'none';
			document.getElementById(ptr).innerHTML= '<i class="icon-chevron-up faq_arrows"></i>';
		}
	}
</script>
</head>
<!--
<body data-spy="scroll" data-target="#navbar">
	<div style="width:180px;" class="pull-left" id="navbar">
		<div style="position:fixed;" id="navbar">
			<ul class="nav nav-tabs nav-stacked" style="width:180px;">
				<li><a href="#game_faq">Game</a><i class="icon-chevron-right pull-right icon_position" style=""></i></li>
				<li><a href="#acct_faq">Account</a><i class="icon-chevron-right pull-right icon_position" ></i></li>
				<li><a href="#priv_faq">Privacy</a><i class="icon-chevron-right pull-right icon_position"></i></li>
				<li><a href="#misc_faq">Miscellaneous</a><i class="icon-chevron-right pull-right icon_position"></i></li>
				<li><a href="#contactUs" data-toggle="modal">Contact Us</a><i class="icon-envelope pull-right icon_position"></i></li>
			</ul>	
		</div>
	</div>
	<div class="pull-right right_body">
-->
<div>
	<h2>Frequently Asked Questions</h2>
	<p>Here are some common and frequently asked questions. Hope these question and answers help! If you have a question that is not posted, please send us a message and we'll try to get back to you as soon as we can. </p>
	<hr>

	<div id="game_faq">
		<h3><i class="icon-off faq_icons"></i>Game FAQ</h3>

		<div class="accordion" id="accordion2">
		  	<div class="accordion-group">
		    	<div class="accordion-heading">
		      		<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#game_1">
		        	What are the system requirements?
		      		</a>
		    	</div>
		    	<div id="game_1" class="accordion-body collapse">
		      		<div class="accordion-inner">
						<table class="table table-condensed table-bordered" style="width:630px;">
							<tr>
								<td class="bold_font">System</td>
								<td class="bold_font">Minimum Requirements</td>
								<td class="bold_font">Recommended Settings</td>
							</tr>
							<tr>
								<td class="bold_font">OS</td>
								<td>Mac OSX, Windows(XP,Vista,7,8)</td>
								<td>Mac OSX, Windows(Vista,7,8)</td>
							</tr>
							<tr>
								<td class="bold_font">Browser</td>
								<td>IE (8 and up), Chrome, Firefox, Opera??</td>
								<td>IE (8 and up), Chrome, Firefox, Opera??</td>
							</tr>
							<tr>
								<td class="bold_font">Connection Speed</td>
								<td>DSL (128kpbps) or better</td>
								<td>DSL (256kpbps) or better</td>
							</tr>						
						</table>
		      		</div>
		    	</div>
		  	</div>
		  
		  	<div class="accordion-group">
		    	<div class="accordion-heading">
			      	<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#game_2">
			        	Can I play on a mobile/tablet device?
			      	</a>
		    	</div>
		    	<div id="game_2" class="accordion-body collapse">
		      		<div class="accordion-inner">
		        		Unfortunately, UConnocalypse <strong>CANNOT</strong> be played on mobile or tablet devices. It is designed for Desktop PCs and Laptops. 
							Perhaps, in the future, we will support mobile and tablet devices, but for now it isn't possible to do so.
		      		</div>
		    	</div>
		  	</div>

		  	<div class="accordion-group">
		    	<div class="accordion-heading">
			      	<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#game_3">
			        	How do I save my progress in a game?
			      	</a>
		    	</div>
		    	<div id="game_3" class="accordion-body collapse">
		      		<div class="accordion-inner">
		        		To save your progress, just click "SAVE" on the HUD during gameplay OR just make sure to regularly reach checkpoints to let auto-save do it for you.
		      		</div>
		    	</div>
		  	</div>

		  	<div class="accordion-group">
		    	<div class="accordion-heading">
			      	<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#game_4">
			        	Help! The game does NOT load.
			      	</a>
		    	</div>
		    	<div id="game_4" class="accordion-body collapse">
		      		<div class="accordion-inner">
		        		Sorry to hear that. Just try refreshing the browser and logging back in again. If the problem persists, please send us a message and we'll investigate the issue.
		      		</div>
		    	</div>
		  	</div>

		  	<div class="accordion-group">
		    	<div class="accordion-heading">
			      	<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#game_5">
			        	Add more questions...
			      	</a>
		    	</div>
		    	<div id="game_5" class="accordion-body collapse">
		      		<div class="accordion-inner">
		        		Add more answers...
		      		</div>
		    	</div>
		  	</div>
		</div>
	</div> <!--game-->

	<div id="acct_faq">
		<h3><i class="icon-user faq_icons"></i>Account FAQ</h3>
		<div class="accordion" id="accordion3">
		  	<div class="accordion-group">
		    	<div class="accordion-heading">
		      		<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#acct_1">
		      			How do I register?
		      		</a>
		    	</div>
		    	<div id="acct_1" class="accordion-body collapse">
		      		<div class="accordion-inner">
		      			To register for an account, just click the "Sign Up" button on the login screen and properly complete the form and then SUBMIT. Voila! You should be on your way to saving Jay Hickey and the rest of UConn.
		      		</div>
		    	</div>
		  	</div>
		  
		  	<div class="accordion-group">
		    	<div class="accordion-heading">
			      	<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#acct_2">
			        	How do I change my password?
			      	</a>
		    	</div>
		    	<div id="acct_2" class="accordion-body collapse">
		      		<div class="accordion-inner">
		        		This feature is currently not enabled. Should you need to update/modify your password, please send us a message and we'll assist you.
		      		</div>
		    	</div>
		  	</div>

		  	<div class="accordion-group">
		    	<div class="accordion-heading">
			      	<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#acct_3">
			        	What if I lost my password?
			      	</a>
		    	</div>
		    	<div id="acct_3" class="accordion-body collapse">
		      		<div class="accordion-inner">
		        		UHHHHH
		      		</div>
		    	</div>
		  	</div>

		  	<div class="accordion-group">
		    	<div class="accordion-heading">
			      	<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#acct_4">
			      		How do I change my account information?
			      	</a>
		    	</div>
		    	<div id="acct_4" class="accordion-body collapse">
		      		<div class="accordion-inner">
		        		This feature is currently not enabled. Should you need to update/modify your password, please send us a message and we'll assist you.
		      		</div>
		    	</div>
		  	</div>

		  	<div class="accordion-group">
		    	<div class="accordion-heading">
			      	<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#acct_5">
			        	How do I delete my account or account information?
			      	</a>
		    	</div>
		    	<div id="acct_5" class="accordion-body collapse">
		      		<div class="accordion-inner">
		        		uhhhhh...
		      		</div>
		    	</div>
		  	</div>

		  	<div class="accordion-group">
		    	<div class="accordion-heading">
			      	<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#acct_6">
			      		ADD MORE...
			      	</a>
		    	</div>
		    	<div id="acct_6" class="accordion-body collapse">
		      		<div class="accordion-inner">
		        		ADD MORE...
		      		</div>
		    	</div>
		  	</div>
		</div>	
	</div>
		

		<div id="misc_faq">
		<h3><i class="icon-asterisk faq_icons"></i>Miscellaneous FAQ</h3>
		</div>
<a href="#contactUs" class="btn" data-toggle="modal">Contact Us</a><i class="icon-envelope pull-right icon_position"></i>
		<!-- Modal -->
		<div hidden id="contactUs" class="modal hide fade">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3>Send Us A Message</h3>
			</div>
			<!--
			<form class="form-horizontal" method="post" name="mail" action="http://web.uconn.edu/cgi-bin/formmail.pl">
				<div class="modal-body">	
					<input type="hidden" name="recipient" value="kraig.dandan@uconn.edu">
					<input type="hidden" name="required" value="email,subject,message">
					<div class="pull-left">
						<div class="control-group">
							<div class="input-prepend">
								<span class="add-on"><i class="icon-envelope"></i></span>
								<input type="text" class="span5" name="email" value="" size="64" placeholder="Your Email (Required)" maxLength="30" required>
							</div>
						</div>
						<div class="control-group">
							<div class="input-prepend">
								<span class="add-on"><i class="icon-tag"></i></span>
								<input type="text" class="span5" name="subject" value="" size="64" placeholder="Subject (Optional)" maxLength="30">
							</div>
						</div>
					</div>

					<div class="pull-right">
						<img src="../../view/img/husky_vs_zombie.png" width="100" height="60" style="margin:0px; border:solid 1px #c0c0c0;"/>
					</div>

					<div>
						<textarea name="message" rows="4" style="resize:none; width:515px;" placeholder="Enter your message... (Required - max of 300 characters)" maxLength="300" required></textarea><br>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-large" data-dismiss="modal">Cancel</button>
					<input type="submit" value="Send" class="btn btn-large btn-primary">
				</div>
			</form>
			-->

<?php
       require_once "Mail.php";

        $from = "<from.gmail.com>";
        $to = "<to.yahoo.com>";
        $subject = "Hi!";
        $body = "Hi,\n\nHow are you?";

        $host = "ssl://smtp.gmail.com";
        $port = "465";
        $username = "myaccount@gmail.com";  //<> give errors
        $password = "password";

        $headers = array ('From' => $from,
          'To' => $to,
          'Subject' => $subject);
        $smtp = Mail::factory('smtp',
          array ('host' => $host,
            'port' => $port,
            'auth' => true,
            'username' => $username,
            'password' => $password));

        $mail = $smtp->send($to, $headers, $body);

        if (PEAR::isError($mail)) {
          echo("<p>" . $mail->getMessage() . "</p>");
         } else {
          echo("<p>Message successfully sent!</p>");
         }

    ?>  <!-- end of php tag-->

		</div>
		<!--modal -->
	</div>
</div>
<!--</body>-->
</html>