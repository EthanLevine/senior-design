This is a description of the UConn-ocalypse Game Server source code:

-------------
uconnocalypse
**Starter - Responsible for setting up components of the server and launching them.
------------------
uconnocalypse.auth
**Authenticator - Responsible for authenticating existing user accounts and creating new ones.
                   * Also responsible for creating GameConnection objects on successful
                   * authentication.
                   *
                   * All cryptographic code (related to hashing passwords) lives in this class, as
                   * well as a database connection to access and modify the user database.
------------------
uconnocalypse.comm
**CommPortal - Handles all client-to-server communication.  Whenever a new action is defined
                * that a client can call, it should be added to this class.
                *
                * To handle the communication properly, instances of this class need references
                * to an Authenticator object and a GameServer object, which are passed into the
                * constructor.
                *
                * Messages in this class are JSON objects.  The "action" entry must always be
                * present, and is used to determine what to do with the packet.

**CommSender - Handles all server-to-client communication.  Whenever a new action is defined
                * that the server sends to the client, it should be implemented in this class.
                *
                * A new instance of this class is created with every client connection (or to
                * be specific, every NettyServerHandler).
                *
                * Instances of this class should generally be found in GameConnection objects.
                *
                * Messages are formatted as JSON objects, for easy consumption in the
                * JavaScript client.

**GameConnection - Represents an authenticated connection with a client.  Instances of this
                    * class contain a CommSender object which is used to send messages back to the
                    * client.  This class also includes a server-unique ID which is used to
                    * identify the remote client, as well as information about the authenticated
                    * user.
-------------------
uconnocalypse.model
**Entity - Base class for all entities in the game
            * Entities are objects that are drawn onto the screen:
            * - Players
            * - NPCs
            * - Floor Items
            * - Objects, if we get there

**GameState - This class will keep track of current game state/progress, i.e., story and quest related

**Item - This is a representation of Items that are on the ground, not for all the items in inventories/chests

**MapRegion - This class represents a section of the map. It is used to keep track of players, enemies, items, and
                * the current status of the region.

**Mob - Mob = Mobile Entity
         * A mobile entity can be an NPC/Monster, Player, or a Projectile

**NPC - This class represents a Non-Player Character

**Player - This class represents a user's character that is in the game

**Projectile - This class will model projectiles such as arrows and bullets, these are mobile entities
----------------------
uconnocalypse.model.ai
**AI - This is the base AI class that will be used to make many diverse artificial intelligences

**SimpleAI - This is a simple AI, the mob just strolls around in it's little spawning area
-------------------
uconnocalypse.netty
**NettyServer - Listens for WebSocket connections and spawns new NettyServerHandler objects
                 * when a connection is accepted.  Details about this process should be modified
                 * in the NettyServerPipelineFactory class.
                 *
                 * Instances of this class need a reference to a CommPortal, which is passed to
                 * NettyServerHandlers via the NettyServerPipelineFactory.

**NettyServerHandler - The low-level connection between a client and the server.  Instances of this
                        * class are created by a NettyServer object (more specifically, a
                        * NettyServerPipelineFactory object).  Although this class is responsible for
                        * the low-level communication (and handshaking) of network connections, all
                        * code for parsing, processing, and formatting messages is located in the
                        * uconnocalypse.comm package.

**NettyServerPipelineFactory - Required for the Netty Server
--------------------
uconnocalypse.server
**GameInstance - This represents an instance of the game that is controlled by the GameServer

**GameServer - The Game Server is in charge of keeping track of the connected clients and mapping them to the
                * active games
--------------------