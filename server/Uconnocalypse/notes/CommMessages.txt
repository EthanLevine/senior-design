Messages we need to implement before demo:

# Client should only be informed of monsters in it's region
# It will receive current monster locations when it enters a new region
# These will be sent in an order of expanding region around the Player's current position (ideally)
# We also should decide if we send a new-monster method or just have the client use the NPC_ID to
# determine if it is new or not.
# We would also need to include the TYPE of the monster so the client would know what sprite to display
# Are we going to be sending rotational updates to the clients too for NPCs?
#   I suppose for a 'STALKER' AI we would want that
update-monster NPC_ID CURRENT_HEALTH HEALTH_TOTAL X Y

---------
Handling the update-player method (i.e., when we send it):
    If we want the other players to be aware of what mapRegion the player is in-
    Or if we want it to know of updates to the other players health (lose/gain health)
        then we will send it whenever the player is changes region, as long as it's not our current one
        or whenever the player takes damage or gains health
    If a player is in our current region we receive all updates of that player
---------

# Remove-player, what does this involve? Permanent removal?
# Client should determine based on an update-player message that they have left our region
# and would then stop displaying the player
remove-player PLAYER_ID

# For remove-monster, we could consider just sending an update-monster message with a health of 0
# The client could then appropriately do kill animation/removal of monster
remove-monster MONSTER_ID



We may also want to include player specific HUD updates:
Health, money, etc.
