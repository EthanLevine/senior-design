package uconnocalypse.data;

import uconnocalypse.engine.Point;
import uconnocalypse.server.GameServer;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.*;

@Entity
@Table(name="stashes")
public class Stash implements Serializable {
    
    @Column(name="id")
    @Id
    @GeneratedValue
    private Long id;
    
    @Column(name="height", nullable=false)
    protected Integer height;
    
    @Column(name="width", nullable=false)
    protected Integer width;
    
    @OneToMany(mappedBy="stash")
    protected Set<StashedItem> stashedItems;
    
    public Long getId() { return id; }
    public Integer getHeight() { return height; }
    public Integer getWidth() { return width; }
    public Set<StashedItem> getStashedItems() { return stashedItems; }

    public Point getNextFreeSlot() {
        //if(isStashFull()) return null;
        boolean[][] invMap = new boolean[width][height];
        for(StashedItem stashedItem : stashedItems) {
            invMap[stashedItem.getSlotX()][stashedItem.getSlotY()] = true;
        }
        for(int i = 0; i < invMap.length; i++) {
            for(int j = 0; j < invMap[i].length; j++) {
                if (!invMap[i][j]) return new Point(i, j);
            }
        }
        return null;
    }

    public boolean isStashFull() {
        int maxSpace = width * height;
        return stashedItems.size() >= maxSpace;
    }

    protected void addItem(Item item) {
        Point p = getNextFreeSlot();
        StashedItem stashedItem = GameServer.databaseManager.createStashedItem(item, (int)p.x, (int)p.y, this);
        stashedItems.add(stashedItem);
        GameServer.databaseManager.saveStash(this);
    }

    protected void removeItem(StashedItem stashedItem) {
        stashedItems.remove(stashedItem);
        GameServer.databaseManager.deleteStashedItem(stashedItem);
        GameServer.databaseManager.saveStash(this);
    }
}
