package uconnocalypse.data;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.*;

@Entity
@Table(name="map_region_templates")
public class MapRegionTemplate implements Serializable {
    @Column(name="id")
    @Id
    @GeneratedValue
    private Long id;

    @Column(name="name", nullable=false)
    protected String name;
    
    @OneToMany(mappedBy="mapRegionTemplate")
    protected Set<FixedSpawn> fixedSpawns;
    
    @OneToMany(mappedBy="mapRegionTemplate")
    protected Set<Barrier> barriers;

    @Column(name="width")
    protected Integer width;

    @Column(name="height")
    protected Integer height;
    
    public Long getId() { return id; }
    public String getName() { return name; }
    public Set<FixedSpawn> getFixedSpawns() { return fixedSpawns; }
    public Set<Barrier> getBarriers() { return barriers; }
    public Integer getWidth() { return width; }
    public Integer getHeight() { return  height; }
}
