package uconnocalypse.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import org.hibernate.Hibernate;
import org.mindrot.bcrypt.BCrypt;

public final class DatabaseManager {
    
    private final EntityManagerFactory emf;
    private final Random random;
    
    public DatabaseManager(EntityManagerFactory emf) {
        this.emf = emf;
        random = new Random();
    }
    
    public Account authenticateUser(String username, String password) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            List<Account> accountMatches =
                    em.createQuery("FROM Account a WHERE a.username = ?1", Account.class)
                    .setParameter(1, username).setMaxResults(1).getResultList();
            if (accountMatches.isEmpty()) {
                return null;
            } else {
                Account account = accountMatches.get(0);
                if (BCrypt.checkpw(password, account.passwordHash)) {
                    EntityTransaction et = em.getTransaction();
                    et.begin();
                    account.lastAccessTime = new GregorianCalendar();
                    et.commit();
                    return account;
                } else {
                    return null;
                }
            }
        } finally {
            if (em != null) em.close();
        }
    }
    
    public boolean isAccountNameTaken(String username) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            return !em.createQuery("SELECT a.username FROM Account a WHERE a.username = ?1", String.class)
                    .setParameter(1, username).setMaxResults(1).getResultList().isEmpty();
        } finally {
            if (em != null) em.close();
        }
    }
    
    public Account createNewUser(String username, String password) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            EntityTransaction et = em.getTransaction();
            Account account = new Account();
            account.username = username;
            account.passwordHash = BCrypt.hashpw(password, BCrypt.gensalt());
            account.creationTime = new GregorianCalendar();
            account.lastAccessTime = account.creationTime;
            account.accountStash = new Stash();
            account.accountStash.height = GameConstants.ACCOUNT_STASH_HEIGHT;
            account.accountStash.width = GameConstants.ACCOUNT_STASH_WIDTH;
            em.persist(account.accountStash);
            account.gold = 0;
            et.begin();
            //em.persist(account.accountStash);
            em.persist(account);
            et.commit();
            return account;
        } finally {
            if (em != null) em.close();
        }
    }
    
    public boolean isCharacterNameTaken(String name) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            return !em.createQuery("SELECT c.name FROM PlayerCharacter c WHERE c.name = ?1", String.class)
                    .setParameter(1, name).setMaxResults(1).getResultList().isEmpty();
        } finally {
            if (em != null) em.close();
        }
    }
    
    public List<PlayerCharacter> getCharacterList(long accountId) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            List<PlayerCharacter> characters = em.createQuery("FROM PlayerCharacter c WHERE c.account.id = ?1", PlayerCharacter.class)
                    .setParameter(1, accountId).getResultList();
            return characters;
        } finally {
            if (em != null) em.close();
        }
    }
    
    public PlayerCharacter createCharacter(long accountId, String characterName, int characterType) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            EntityTransaction et = em.getTransaction();
            PlayerCharacter character = new PlayerCharacter();
            character.account = em.find(Account.class, accountId);
            character.charType = characterType;
            character.name = characterName;
            character.creationTime = new GregorianCalendar();
            character.expTotal = 0L;
            character.level = 1;
            character.stats = new AttributeSet();
            character.stats.strength = 10;
            character.stats.dexterity = 10;
            character.stats.fortitude = 10;
            character.stats = mergeAttributeSet(character.stats);
            character.inventory = new Stash();
            character.inventory.stashedItems = new HashSet<>();
            character.inventory.height = GameConstants.INVENTORY_HEIGHT;
            character.inventory.width = GameConstants.INVENTORY_WIDTH;
            et.begin();
            em.persist(character.inventory);
            em.persist(character);
            Hibernate.initialize(character.inventory.getStashedItems());
            et.commit();
            return character;
        } finally {
            if (em != null) em.close();
        }
    }

    public void deleteCharacter(long characterId) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            EntityTransaction et = em.getTransaction();
            et.begin();
            PlayerCharacter character = em.find(PlayerCharacter.class, characterId);
            em.remove(character);
            et.commit();
        } finally {
            if (em != null) em.close();
        }
    }
    
    public PlayerCharacter selectCharacter(long accountId, long characterId) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            List<PlayerCharacter> matches = em.createQuery("FROM PlayerCharacter c WHERE c.id = ?1 AND c.account.id = ?2")
                    .setParameter(1, characterId)
                    .setParameter(2, accountId)
                    .setMaxResults(1).getResultList();
            if (matches.isEmpty()) {
                return null;
            } else {
                PlayerCharacter player = matches.get(0);
                Hibernate.initialize(player.inventory.getStashedItems());
                return matches.get(0);
            }
        } finally {
            if (em != null) em.close();
        }
    }

    public boolean accountOwnsCharacter(long accountId, long characterId) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            List<PlayerCharacter> matches = em.createQuery("FROM PlayerCharacter c WHERE c.id = ?1 AND c.account.id = ?2")
                    .setParameter(1, characterId)
                    .setParameter(2, accountId)
                    .setMaxResults(1).getResultList();
            return !matches.isEmpty();
        } finally {
            if (em != null) em.close();
        }
    }
    
    public List<Task> getAllTasks() {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            return em.createQuery("FROM Task t ORDER BY t.taskOrder ASC", Task.class).getResultList();
        } finally {
            if (em != null) em.close();
        }
    }
    
    public Set<Long> getAvailableTaskIds(long accountId) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            Set<Long> returnedTasks = new HashSet<>();
            for (Task t : em.find(Account.class, accountId).getCompletedTasks()) {
                returnedTasks.add(t.getId());
                List<Task> nextTask = em.createQuery("FROM Task t WHERE t.taskOrder > ?1 ORDER BY t.taskOrder ASC", Task.class)
                        .setParameter(1, t.taskOrder).setMaxResults(1).getResultList();
                if (!nextTask.isEmpty()) {
                    returnedTasks.add(nextTask.get(0).getId());
                }
            }
            List<Task> firstTask = em.createQuery("FROM Task t ORDER BY t.taskOrder ASC", Task.class)
                    .setMaxResults(1).getResultList();
            if (!firstTask.isEmpty()) {
                returnedTasks.add(firstTask.get(0).getId());
            }
            return returnedTasks;
        } finally {
            if (em != null) em.close();
        }
    }
    
    public Task getTask(long taskId) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            Task task = em.find(Task.class, taskId);
            Hibernate.initialize(task.getObjectives());
            return task;
        } finally {
            if (em != null) em.close();
        }
    }
    
    public MapRegionTemplate getMapRegionTemplate(long mapRegionTemplateId) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            MapRegionTemplate template = em.find(MapRegionTemplate.class, mapRegionTemplateId);
            Hibernate.initialize(template.getBarriers());
            Hibernate.initialize(template.getFixedSpawns());
            return template;
        } finally {
            if (em != null) em.close();
        }
    }
    
    protected AttributeSet mergeAttributeSet(AttributeSet newSet) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            // Search for the exact attribute set
            List<AttributeSet> discoveredSets =
                    em.createQuery("FROM AttributeSet a WHERE a.strength = ?1 AND a.dexterity = ?2 AND a.fortitude = ?3", AttributeSet.class)
                    .setParameter(1, newSet.strength)
                    .setParameter(2, newSet.dexterity)
                    .setParameter(3, newSet.fortitude)
                    .setMaxResults(1).getResultList();
            if (discoveredSets.isEmpty()) {
                EntityTransaction et = em.getTransaction();
                et.begin();
                em.persist(newSet);
                et.commit();
                return newSet;
            } else {
                return discoveredSets.get(0);
            }
        } finally {
            if (em != null) em.close();
        }
    }
    
    public Item instantiateItem(long itemTemplateId) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            EntityTransaction et = em.getTransaction();
            // First, fetch the template.
            ItemTemplate template = em.find(ItemTemplate.class, itemTemplateId);
            // Create a new item.
            Item item = new Item();
            item.itemTemplate = template;
            item.attributes = template.attributeSet;
            // Only fetch mods if they're allowed
            int appliedModCount = 0;
            if (template.maximumMods > 0) {
                // Note:  getResultList() gives a READ-ONLY list back, so we have
                // to copy it since we're going to be removing items.
                List<ItemMod> availableModsReadOnly =
                        em.createQuery("FROM ItemMod im WHERE ?1 < im.maximumLevel AND ?1 > im.minimumLevel AND ?2 = im.equipSlot", ItemMod.class)
                        .setParameter(1, template.itemLevel)
                        .setParameter(2, template.equipSlot)
                        .getResultList();
                List<ItemMod> availableMods = new ArrayList<>(availableModsReadOnly.size());
                Collections.copy(availableMods, availableModsReadOnly);
                StringBuilder newName = new StringBuilder(template.name);
                // always include at least minimumMods,
                // then have a 30% chance to include another mod, until maximumMods
                for (int i = 0; i < template.maximumMods && !availableMods.isEmpty(); i++) {
                    if (i < template.minimumMods || random.nextDouble() < 0.3) {
                        int modIndex = random.nextInt(availableMods.size());
                        ItemMod mod = availableMods.get(modIndex);
                        availableMods.remove(modIndex);
                        item.attributes = item.attributes.add(mod.minimumAttributeSet);
                        item.attributes = item.attributes.add(mod.incrementalAttributeSet.scale(template.itemLevel - mod.minimumLevel));
                        item.attributes = item.attributes.add(mod.randomAttributeSet.scale(random.nextDouble()));
                        newName.insert(0, mod.namePrefix == null ? "" : mod.namePrefix);
                        newName.append(mod.nameSuffix == null ? "" : mod.nameSuffix);
                        appliedModCount++;
                    } else {
                        break;
                    }
                }
                item.name = newName.toString();
            } else {
                item.name = template.name;
            }
            // If an item template is LEGENDARY, it will always be LEGENDARY
            if (template.baseItemQuality.equals(ItemQuality.LEGENDARY)) {
                item.itemQuality = ItemQuality.LEGENDARY;
            } else {
                // If an item template is NOT LEGENDARY, the highest allowed
                // quality is EXOTIC.  Each item mod brings its base quality up.
                // In addition, the item quality cannot be less than TRASH.
                item.itemQuality = template.baseItemQuality + appliedModCount;
                if (item.itemQuality > ItemQuality.EXOTIC) {
                    item.itemQuality = ItemQuality.EXOTIC;
                } else if (item.itemQuality < ItemQuality.TRASH) {
                    item.itemQuality = ItemQuality.TRASH;
                }
            }
            item.attributes = mergeAttributeSet(item.attributes);
            et.begin();
            em.persist(item);
            et.commit();
            return item;
        } finally {
            if (em != null) em.close();
        }
    }

    public Item getItem(long id) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            return em.createQuery("FROM Item c WHERE c.id = ?1", Item.class)
                    .setParameter(1, id).getSingleResult();
        } finally {
            if (em != null) em.close();
        }
    }

    public StashedItem getStashedItem(long id) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            return em.createQuery("FROM StashedItem c WHERE c.id = ?1", StashedItem.class)
                    .setParameter(1, id).getSingleResult();
        } finally {
            if (em != null) em.close();
        }
    }

    public ItemTemplate getItemTemplate(long id) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            return em.createQuery("FROM ItemTemplate c WHERE c.id = ?1", ItemTemplate.class)
                    .setParameter(1, id).getSingleResult();
        } finally {
            if (em != null) em.close();
        }
    }

    /******************************************
     * Save persist methods
     ******************************************/
    public void saveCharacter(PlayerCharacter player) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            EntityTransaction et = em.getTransaction();
            et.begin();
            //em.persist(player.inventory);
            //em.persist(player.equipment);
            em.merge(player);
            et.commit();
        } finally {
            if (em != null) em.close();
        }
    }

    public void saveStash(Stash stash) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            EntityTransaction et = em.getTransaction();
            et.begin();
            em.merge(stash);
            for(StashedItem stashedItem : stash.getStashedItems()) {
                em.merge(stashedItem);
            }
            et.commit();
        } finally {
            if (em != null) em.close();
        }
    }

    public void deleteStashedItem(StashedItem stashedItem) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            EntityTransaction et = em.getTransaction();
            et.begin();
            em.find(StashedItem.class, stashedItem.getId());
            em.remove(stashedItem);
            et.commit();
        } finally {
            if (em != null) em.close();
        }
    }

    public StashedItem createStashedItem(Item item, Integer slotX, Integer slotY, Stash stash) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            EntityTransaction et = em.getTransaction();
            StashedItem stashedItem = new StashedItem();
            stashedItem.itemId = item.getId();
            stashedItem.item = item;
            stashedItem.slotX = slotX;
            stashedItem.slotY = slotY;
            stashedItem.stash = stash;
            et.begin();
            em.persist(stashedItem);
            et.commit();
            return stashedItem;
        } finally {
            if (em != null) em.close();
        }
    }
}
