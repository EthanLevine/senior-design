package uconnocalypse.data;

public class Difficulty {
    private Difficulty() {}
    
    public static final int FRESHMAN = 1;
    public static final int SOPHOMORE = 2;
    public static final int JUNIOR = 3;
    public static final int SENIOR = 4;
}
