package uconnocalypse.data;

import uconnocalypse.server.GameServer;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="attribute_sets")
public class AttributeSet implements Serializable {
    
    @Column(name="id")
    @Id
    @GeneratedValue
    private Long id;
    
    // will be renamed
    @Column(name="strength", nullable=false)
    protected Integer strength;
    
    // will be renamed
    @Column(name="dexterity", nullable=false)
    protected Integer dexterity;
    
    // will be renamed
    @Column(name="fortitude", nullable=false)
    protected Integer fortitude;
    
    protected AttributeSet add(AttributeSet other) {
        AttributeSet newAS = new AttributeSet();
        newAS.strength = strength + other.strength;
        newAS.dexterity = dexterity + other.dexterity;
        newAS.fortitude = fortitude + other.fortitude;
        newAS = GameServer.databaseManager.mergeAttributeSet(newAS);
        return newAS;
    }
    
    protected AttributeSet scale(double attribScale) {
        AttributeSet scaledAS = new AttributeSet();
        scaledAS.strength = (int)(strength.doubleValue() * attribScale);
        scaledAS.dexterity = (int)(strength.doubleValue() * attribScale);
        scaledAS.fortitude = (int)(fortitude.doubleValue() * attribScale);
        scaledAS = GameServer.databaseManager.mergeAttributeSet(scaledAS);
        return scaledAS;
    }

    protected AttributeSet remove(AttributeSet other) {
        AttributeSet newAS = new AttributeSet();
        newAS.strength = strength - other.strength;
        newAS.dexterity = dexterity - other.dexterity;
        newAS.fortitude = fortitude - other.fortitude;
        newAS = GameServer.databaseManager.mergeAttributeSet(newAS);
        return newAS;
    }

    public Integer getStrength() { return strength; }
    public Integer getDexterity() { return dexterity; }
    public Integer getFortitude() { return fortitude; }
}
