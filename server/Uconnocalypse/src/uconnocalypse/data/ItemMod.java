package uconnocalypse.data;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="item_mods")
public class ItemMod implements Serializable {
    
    @Column(name="id")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(name="equip_slot", nullable=false)
    protected Integer equipSlot;
    
    @ManyToOne
    @JoinColumn(name="min_attribute_set_id", nullable=false)
    protected AttributeSet minimumAttributeSet;
    
    @ManyToOne
    @JoinColumn(name="rand_attribute_set_id", nullable=false)
    protected AttributeSet randomAttributeSet;
    
    @ManyToOne
    @JoinColumn(name="inc_attribute_set_id", nullable=false)
    protected AttributeSet incrementalAttributeSet;
    
    @Column(name="min_level", nullable=false)
    protected Integer minimumLevel;
    
    @Column(name="max_level", nullable=false)
    protected Integer maximumLevel;
    
    @Column(name="name_prefix", nullable=false)
    protected String namePrefix;
    
    @Column(name="name_suffix", nullable=false)
    protected String nameSuffix;

    public Long getId() { return id; }
    public Integer getEquipSlot() { return equipSlot; }
    public AttributeSet getMinimumAttributeSet() { return minimumAttributeSet; }
    public AttributeSet getRandomAttributeSet() { return randomAttributeSet; }
    public AttributeSet getIncrementalAttributeSet() { return incrementalAttributeSet; }
    public Integer getMinimumLevel() { return minimumLevel; }
    public Integer getMaximumLevel() { return maximumLevel; }
    public String getNamePrefix() { return namePrefix; }
    public String getNameSuffix() { return nameSuffix; }
}
