package uconnocalypse.data;

import uconnocalypse.engine.Point;
import uconnocalypse.server.GameServer;

import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.*;

@Entity
@Table(name="characters")
public class PlayerCharacter implements Serializable {
    
    @Column(name="id")
    @Id
    @GeneratedValue
    private Long id;
    
    @ManyToOne
    @JoinColumn(name="account_id")
    protected Account account;
    
    @Column(name="name", unique=true, nullable=false)
    protected String name;
    
    @Column(name="character_type", nullable=false)
    protected Integer charType;
    
    @Column(name="creation_time", nullable=false)
    @Temporal(TemporalType.TIMESTAMP)
    protected Calendar creationTime;
    
    @Column(name="exp_level", nullable=false)
    protected Integer level;
    
    @Column(name="exp_total", nullable=false)
    protected Long expTotal;

    @ManyToOne
    @JoinColumn(name="stats_id")
    protected AttributeSet stats;

    @ManyToOne
    @JoinColumn(name="inventory_id")
    protected Stash inventory;
    
    @ManyToOne
    @JoinColumn(name="equipped_weapon_id", nullable=true)
    protected Item equippedWeapon;
    
    @ManyToOne
    @JoinColumn(name="equipped_armor_id", nullable=true)
    protected Item equippedArmor;
    
    public Long getId() { return id; }
    public Account getAccount() { return account; }
    public String getName() { return name; }
    public Integer getCharType() { return charType; }
    public Calendar getCreationTime() { return creationTime; }
    public Integer getLevel() { return level; }
    public Long getExpTotal() { return expTotal; }
    public Stash getInventory() { return inventory; }
    public Item getEquippedWeapon() { return equippedWeapon; }
    public Item getEquippedArmor() { return equippedArmor; }

    // Variables not tracked by the DB
    @Transient
    private boolean inventoryOpen = false;

    /**
     * Checking that we are qualified to use this item should be done beforehand.
     * @param stashedItem - The item in our inventory that we want to equip
     */
    public void equipInventoryItem(StashedItem stashedItem) {
        int slot = stashedItem.getItem().getItemTemplate().getEquipSlot();
        if (slot > 10) {
            // eqiup a weapon
            // start by unequipping
            inventory.removeItem(stashedItem);
            if (equippedWeapon != null) {
                inventory.addItem(equippedWeapon);
                stats = stats.remove(equippedWeapon.getAttributes());
            }
            equippedWeapon = stashedItem.getItem();
        } else if (slot == GameConstants.EQUIPMENT_SLOT_BODY) {
            // equip armor
            inventory.removeItem(stashedItem);
            if (equippedArmor != null) {
                inventory.addItem(equippedArmor);
                stats = stats.remove(equippedArmor.getAttributes());
            }
            equippedArmor = stashedItem.getItem();
        } else {
            // error - cannot equip
            // TODO: error here
        }
        
        stats = stats.add(stashedItem.getItem().getAttributes());
        
        GameServer.databaseManager.saveStash(inventory);
        GameServer.databaseManager.saveCharacter(this);
    }

    /**
     * There should be a check to make sure that there is space in the inventory beforehand.
     */
    public void unequipWeapon() {
        if (equippedWeapon != null) {
            stats = stats.remove(equippedWeapon.getAttributes());
            inventory.addItem(equippedWeapon);
            equippedWeapon = null;
            GameServer.databaseManager.saveCharacter(this);
        }
    }
    
    public void unequipArmor() {
        if (equippedArmor != null) {
            stats = stats.remove(equippedArmor.getAttributes());
            inventory.addItem(equippedArmor);
            equippedArmor = null;
            GameServer.databaseManager.saveCharacter(this);
        }
    }

    public boolean isInventoryFull() {
        return inventory.isStashFull();
    }

    public void addItemToInventory(Item item) {
        inventory.addItem(item);
    }

    public void removeInventoryItem(StashedItem stashedItem) {
        inventory.removeItem(stashedItem);
    }

    public double calcAttackRating() {
        double damage = equippedWeapon.getItemTemplate().getWeaponBaseDamage();
        boolean melee = equippedWeapon.getItemTemplate().getEquipSlot() == GameConstants.EQUIPMENT_MELEE;
        if (melee) {
            // Base stats are all lvl 10
            damage = damage * (1 + stats.strength/100);
        } else {
            // This calculates the damage for each bullet/shell
            // So a shotgun might fire 2 shells at once, each having this damage
            damage = damage * (1 + stats.dexterity/100);
        }
        // Vary the true damage a little (+upto10% - 5%)
        //damage += (damage*(Math.random()*0.1) - damage*0.05);
        return damage;
    }

    public void setInventoryOpen(boolean open) { inventoryOpen = open; }
    public boolean isInventoryOpen() { return inventoryOpen; }
}
