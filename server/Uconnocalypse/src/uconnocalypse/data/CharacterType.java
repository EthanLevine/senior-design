package uconnocalypse.data;

public final class CharacterType {
    private CharacterType() {}
    
    public static int ENGINEER = 1;
    public static int CHEMIST = 2;
    public static int BIOLOGIST = 3;
    public static int PHILOSOPHER = 4;
    
    public static boolean isValid(int charType) {
        return (charType >= 1) && (charType <= 4);
    }
}
