package uconnocalypse.data;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="stashed_items")
public class StashedItem implements Serializable {
    
    @Column(name="item_id") 
    @Id
    protected Long itemId;
    
    @OneToOne
    @PrimaryKeyJoinColumn
    protected Item item;
    
    @Column(name="slot_x", nullable=false)
    protected Integer slotX;
    
    @Column(name="slot_y", nullable=false)
    protected Integer slotY;
    
    @ManyToOne
    @JoinColumn(name="stash_id", nullable=false)
    protected Stash stash;

    public Long getId() { return itemId; }
    public Item getItem() { return item; }
    public Integer getSlotX() { return slotX; }
    public Integer getSlotY() { return slotY; }
    public Stash getStash() { return stash; }
}
