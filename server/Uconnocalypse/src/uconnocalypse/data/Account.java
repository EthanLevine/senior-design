package uconnocalypse.data;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Set;
import javax.persistence.*;

@Entity
@Table(name="accounts")
public class Account implements Serializable {
    
    @Column(name="id")
    @Id
    @GeneratedValue
    private Long id;
    
    @Column(name="username", unique=true, nullable=false)
    protected String username;
    
    @Column(name="password_hash", nullable=false)
    protected String passwordHash;
    
    @Column(name="email")
    protected String email;
    
    @Column(name="creation_time", nullable=false)
    @Temporal(TemporalType.TIMESTAMP)
    protected Calendar creationTime;
    
    @Column(name="last_access_time", nullable=false)
    @Temporal(TemporalType.TIMESTAMP)
    protected Calendar lastAccessTime;
    
    @OneToMany(mappedBy="account")
    private Set<PlayerCharacter> characters;
    
    @OneToMany(mappedBy="account")
    private Set<AchievementRecord> achievementRecords;
    
    @ManyToMany
    @JoinTable(name="account_progress",
            joinColumns = { @JoinColumn(name="account_id") },
            inverseJoinColumns = { @JoinColumn(name="task_id") })
    private Set<Task> completedTasks;

    @ManyToOne
    @JoinColumn(name="account_stash_id")
    protected Stash accountStash;

    @Column(name="gold")
    protected Integer gold;
    
    public Long getId() { return id; }
    public String getUsername() { return username; }
    public String getPasswordHash() { return passwordHash; }
    public String getEmail() { return email; }
    public Calendar getCreationTime() { return creationTime; }
    public Calendar getLastAccessTime() { return lastAccessTime; }
    public Set<PlayerCharacter> getCharacters() { return characters; }
    public Set<AchievementRecord> getAchievementRecords() { return achievementRecords; }
    public Set<Task> getCompletedTasks() { return completedTasks; }
    public Stash getAccountStash() { return accountStash; }
}
