package uconnocalypse.data;

import java.io.Serializable;
import javax.persistence.*;
import uconnocalypse.server.GameServer;

@Entity
@Table(name="items")
public class Item implements Serializable {
    
    @Column(name="id")
    @Id
    @GeneratedValue
    private Long id;
    
    @Column(name="name", nullable=false)
    protected String name;
    
    @Column(name="item_quality", nullable=false)
    protected Integer itemQuality;
    
    @ManyToOne
    @JoinColumn(name="item_template_id", nullable=false)
    protected ItemTemplate itemTemplate;
    
    @ManyToOne
    @JoinColumn(name="attribute_set_id", nullable=false)
    protected AttributeSet attributes;
    
    public Long getId() { return id; }
    public String getName() { return name; }
    public Integer getItemQuality() { return itemQuality; }
    public ItemTemplate getItemTemplate() { return itemTemplate; }
    public AttributeSet getAttributes() { return attributes; }
    
    public static final int FISTS_WEAPON_ID = 1;
    public static final int PISTOL_WEAPON_ID = 2;
    public static final int SWORD_WEAPON_ID = 3;
    public static final int SHOTGUN_WEAPON_ID = 4;
    public static final int ARMOR_ITEM_ID = 5;
}
