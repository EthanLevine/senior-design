package uconnocalypse.data;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="objectives")
public class Objective implements Serializable {
    
    @Column(name="id")
    @Id
    @GeneratedValue
    private Long id;
    
    @ManyToOne
    @JoinColumn(name="task_id", nullable=false)
    protected Task task;
    
    // Example:  "Kill zombies"
    @Column(name="display_text", nullable=false)
    protected String displayText;
    
    @Column(name="count", nullable=false)
    protected Integer count;
    
    @Column(name="objective_type", nullable=false)
    protected Integer objectiveType;
    
    // This will either be an entity species ID or an item template ID.
    @Column(name="target_id", nullable=false)
    protected Long targetId;
    
    public Long getId() { return id; }
    public Task getTask() { return task; }
    public String getDisplayText() { return displayText; }
    public Integer getCount() { return count; }
    public Integer getType() { return objectiveType; }
    public Long getTargetId() { return targetId; }
    
    public static final int TYPE_KILL_ENTITY = 1;
    public static final int TYPE_GATHER_ITEM = 2;
}
