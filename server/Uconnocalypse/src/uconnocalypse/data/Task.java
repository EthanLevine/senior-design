package uconnocalypse.data;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.*;
import uconnocalypse.engine.Point;

@Entity
@Table(name="tasks")
public class Task implements Serializable {
    
    @Column(name="id")
    @Id
    @GeneratedValue
    private Long id;
    
    @Column(name="name", nullable=false)
    protected String name;
    
    @Column(name="difficulty", nullable=false)
    protected Integer difficulty;
    
    @Column(name="task_order", nullable=false)
    protected Integer taskOrder;
    
    @OneToMany(mappedBy="task")
    protected Set<Objective> objectives;
    
    @ManyToOne
    @JoinColumn(name="initial_map_region_id", nullable=false)
    protected MapRegionTemplate initialMapRegion;
    
    @Column(name="initial_placement_x", nullable=false)
    protected Double initialPlacementX;
    
    @Column(name="initial_placement_y", nullable=false)
    protected Double initialPlacementY;
    
    public Long getId() { return id; }
    public String getName() { return name; }
    public Integer getDifficulty() { return difficulty; }
    public Integer getTaskOrder() { return taskOrder; }
    public MapRegionTemplate getInitialMapRegion() { return initialMapRegion; }
    public Point getInitialPlacement() {
        return new Point(initialPlacementX, initialPlacementY);
    }
    public Set<Objective> getObjectives() { return objectives; }
}
