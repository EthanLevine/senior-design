package uconnocalypse.data;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="achievement_records")
public class AchievementRecord implements Serializable {
    
    @Column(name="id")
    @Id
    @GeneratedValue
    private Long id;
    
    @ManyToOne
    @JoinColumn(name="account_id", nullable=false)
    protected Account account;
    
    @ManyToOne
    @JoinColumn(name="achievement_id", nullable=false)
    protected Achievement achievement;
    
    public Long getId() { return id; }
    public Account getAccount() { return account; }
    public Achievement getAchievement() { return achievement; }
}
