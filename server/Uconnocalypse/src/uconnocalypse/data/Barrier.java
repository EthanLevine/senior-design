package uconnocalypse.data;

import java.io.Serializable;
import javax.persistence.*;
import uconnocalypse.engine.PhysicalGeometry;

@Entity
@Table(name="barriers")
public class Barrier implements Serializable {
    
    @Column(name="id")
    @Id
    @GeneratedValue
    private Long id;
    
    @Column(name="geometry_string")
    protected String geometryString;
    
    @ManyToOne
    @JoinColumn(name="map_region_template_id")
    protected MapRegionTemplate mapRegionTemplate;
    
    public Long getId() { return id; }
    public MapRegionTemplate getMapRegionTemplate() { return mapRegionTemplate; }
    
    @Transient
    private PhysicalGeometry physicalGeometry = null;
    public PhysicalGeometry getGeometry() {
        if (physicalGeometry == null) {
            physicalGeometry = new PhysicalGeometry(geometryString);
        }
        return physicalGeometry;
    }
    
    @Transient
    private EntitySpecies virtualSpecies = null;
    public EntitySpecies getVirtualSpecies() {
        if (virtualSpecies == null) {
            virtualSpecies = new EntitySpecies();
            virtualSpecies.isStatic = true;
            virtualSpecies.name = "Barrier";
            virtualSpecies.physicalType = PhysicalType.SOLID;
            virtualSpecies.physicalGeometryString = geometryString;
            virtualSpecies.baseAttackRating = 0;
            virtualSpecies.baseDefenseRating = 0;
            virtualSpecies.maxHealth = 0;
            virtualSpecies.maxEnergy = 0;
        }
        return virtualSpecies;
    }
}
