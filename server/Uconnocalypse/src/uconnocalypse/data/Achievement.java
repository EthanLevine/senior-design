package uconnocalypse.data;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="achievements")
public class Achievement implements Serializable {
    
    @Column(name="id")
    @Id
    @GeneratedValue
    private Long id;
    
    @Column(name="name", nullable=false)
    protected String name;
    
    public Long getId() { return id; }
    public String getName() { return name; }
}
