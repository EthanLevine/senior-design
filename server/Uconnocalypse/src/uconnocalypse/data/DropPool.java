package uconnocalypse.data;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.*;

@Entity
@Table(name="drop_pools")
public class DropPool implements Serializable {
    
    @Column(name="id")
    @Id
    @GeneratedValue
    private Long id;
    
    @OneToMany(mappedBy="dropPool")
    private Set<DroppedItem> droppedItems;
    
    public Long getId() { return id; }
    public Set<DroppedItem> getDroppedItems() { return droppedItems; }
}
