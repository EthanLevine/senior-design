package uconnocalypse.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import uconnocalypse.comm.GameConnection;
import uconnocalypse.data.*;

public final class GameServer {
    
    public static DatabaseManager databaseManager;
    private final ConcurrentHashMap<Long, GameConnection> activeAccounts;
    private final ConcurrentHashMap<Long, GameInstance> gameInstances;
    
    public GameServer(DatabaseManager databaseManager) {
        GameServer.databaseManager = databaseManager;
        activeAccounts = new ConcurrentHashMap<>();
        gameInstances = new ConcurrentHashMap<>();
    }
    
    public void addConnection(GameConnection connection) {
        activeAccounts.put(connection.getAccount().getId(), connection);
    }

    public void removeConnection(GameConnection connection) {
        GameInstance instance = connection.getInstance();
        if (instance != null) {
            instance.removeConnection(connection);
            if (instance.getPlayerCount() == 0)
                gameInstances.remove(instance.getId());
        }
        activeAccounts.remove(connection.getAccount().getId());
    }
    
    public List<GameInstance> getInstanceList() {
        List<GameInstance> instances = new ArrayList<>();
        for (GameInstance instance : gameInstances.values()) {
            instances.add(instance);
        }
        return Collections.unmodifiableList(instances);
    }
    
    public GameInstance createInstance(String name, String password, Task task) {
        GameInstance instance = new GameInstance(databaseManager, name, password, task);
        gameInstances.put(instance.getId(), instance);
        return instance;
    }

    public GameInstance getInstance(Long instanceId) {
        return gameInstances.get(instanceId);
    }
}
