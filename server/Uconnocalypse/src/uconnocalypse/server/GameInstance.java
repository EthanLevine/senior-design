package uconnocalypse.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import uconnocalypse.comm.GameConnection;
import uconnocalypse.data.*;
import uconnocalypse.engine.MapEngine;

public final class GameInstance {
    private static long nextInstanceId = 0;
    private final long instanceId;
    
    private final Semaphore instanceLock = new Semaphore(1, true);
    private final ArrayList<GameConnection> connectionList = new ArrayList<>();
    private final HashMap<Long, ConnectionData> connections = new HashMap<>();
    private final HashMap<Long, MapEngine> mapRegions = new HashMap<>();
    private Task currentTask;
    private ConcurrentHashMap<Long, Integer> objectiveProgress;
    
    private final DatabaseManager databaseManager;
    
    private int playerCount = 0;
    
    private String name;
    private String password;
    
    public GameInstance(DatabaseManager databaseManager, String name, String password, Task task) {
        instanceId = nextInstanceId++;
        this.name = name;
        this.password = password;
        this.databaseManager = databaseManager;
        switchTask(task);
    }
    
    private void switchTask(Task task) {
        currentTask = task;
        objectiveProgress = new ConcurrentHashMap<>();
        for (Objective objective : task.getObjectives()) {
            objectiveProgress.put(objective.getId(), 0);
        }
        for (MapEngine mapRegion : mapRegions.values()) {
            mapRegion.watchObjectives(task.getObjectives(), objectiveProgress);
        }
    }
    
    public long getId() { return instanceId; }
    public String getName() { return name; }
    public String getPassword() { return password; }
    public Task getCurrentTask() { return currentTask; }
    public Integer getObjectiveProgress(Long id) { return objectiveProgress.get(id); }
    public void setObjectiveProgress(Long id, Integer value) {
        objectiveProgress.put(id, value);
        Objective objective = null;
        for (Objective o : currentTask.getObjectives()) {
            if (o.getId().equals(id)) {
                objective = o;
                break;
            }
        }
        serverMessage(GameConstants.TASK_MESSAGE, objective.getDisplayText().trim() + "(" + value
                + "/" + objective.getCount() + ")");
        taskSet();
        checkTaskCompletion();
    }
    public int getPlayerCount() { return playerCount; }

    private void checkTaskCompletion() {
        for (Objective objective : currentTask.getObjectives()) {
            if (objectiveProgress.get(objective.getId()) < objective.getCount()) {
                return;
            }
        }
        taskComplete();
        switchTask(currentTask); // needs to get the next task
    }
    
    private MapEngine getMapRegion(long mapRegionId) {
        MapEngine mapRegion = mapRegions.get(mapRegionId);
        if (mapRegion == null) {
            MapRegionTemplate regionTemplate = databaseManager.getMapRegionTemplate(mapRegionId);
            mapRegion = new MapEngine(regionTemplate, this);
            mapRegion.start();
            mapRegions.put(mapRegionId, mapRegion);
        }
        return mapRegion;
    }
    
    public MapEngine getMapEngine(GameConnection connection) {
        return connections.get(connection.getAccount().getId()).mapRegion;
    }
    
    public void addConnection(GameConnection connection) {
        instanceLock.acquireUninterruptibly();
        try {
            connectionList.add(connection);
            ConnectionData data = new ConnectionData(connection, getMapRegion(currentTask.getInitialMapRegion().getId()));
            connections.put(connection.getAccount().getId(), data);
            playerCount++;
            data.mapRegion.addConnection(connection, currentTask.getInitialPlacement());
            connection.refreshMapEngine();
        } finally {
            instanceLock.release();
        }
    }
    
    public void removeConnection(GameConnection connection) {
        instanceLock.acquireUninterruptibly();
        try {
            connectionList.remove(connection);
            connections.get(connection.getAccount().getId()).mapRegion.removeConnection(connection);
            connections.remove(connection.getAccount().getId());
            playerCount--;
            if (playerCount == 0) {
                // close this instance.
                for (MapEngine engine : mapRegions.values()) {
                    engine.stop();
                }
            }
        } finally {
            instanceLock.release();
        }
    }

    public void sendChat(String message) {
        for(GameConnection connection : connectionList) {
            connection.getSender().sendMessage("player.chat", message);//capitalizeString(message));
        }
    }

    public void serverMessage(int type, String message) {
        for(GameConnection connection : connectionList) {
            connection.getSender().sendMessage("server.message", type, message);
        }
    }

    public void taskSet() {
        for(GameConnection connection : connectionList) {
            List data = new ArrayList();
            data.add(currentTask.getName().trim());
            for (Objective objective : currentTask.getObjectives()) {
                data.add(objective.getDisplayText().trim() + "(" + objectiveProgress.get(objective.getId())
                        + "/" + objective.getCount() + ")");
            }
            connection.getSender().sendMessage("task.set", data);
        }
    }

    public void taskComplete() {
        for(GameConnection connection : connectionList) {
            connection.getSender().sendMessage("task.set", currentTask.getName().trim());
        }
    }

    private String capitalizeString(String string) {
        char[] chars = string.toLowerCase().toCharArray();
        boolean found = false;
        for (int i = 0; i < chars.length; i++) {
            if (!found && Character.isLetter(chars[i])) {
                chars[i] = Character.toUpperCase(chars[i]);
                found = true;
            } else if (chars[i]=='.' || chars[i]=='\'' || chars[i]=='\"' || chars[i]==':') {
                found = false;
            }
        }
        return String.valueOf(chars);
    }
    
    private final class ConnectionData {
        private final GameConnection connection;
        private MapEngine mapRegion;
        
        public ConnectionData(GameConnection connection, MapEngine mapRegion) {
            this.connection = connection;
            this.mapRegion = mapRegion;
        }
    }
}
