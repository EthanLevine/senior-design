package uconnocalypse.engine;

import uconnocalypse.data.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * GroundItem.java
 */
public class GroundItem {
    private final int id;
    private boolean isActive;
    private boolean universal;
    private Item item; //rw
    private double xPos, yPos;
    private final List<GroundItemWatcher> watchers;

    public int getId() {
        return id;
    }

    public boolean isActive() { return isActive; }

    public Item getItem() { return item; }
    public double getXPos() { return xPos; }
    public double getYPos() { return yPos; }
    public boolean isUniversal() { return universal; }
    public Point getPos() { return new Point(xPos, yPos); }

    protected void setItem(Item item) { this.item = item; }
    protected void setXPos(double xPos) { this.xPos = xPos; }
    protected void setYPos(double yPos) { this.yPos = yPos; }
    protected void setUniversal(boolean universal) { this.universal = universal; }

    protected GroundItem(int id) {
        this.id = id;
        watchers = new ArrayList<>();
        isActive = false;
    }

    protected void deactivate() {
        isActive = false;
    }

    protected void activate() {
        isActive = true;
    }

    public void addWatcher(GroundItemWatcher watcher) {
        watchers.add(watcher);
    }

    public void removeWatcher(GroundItemWatcher watcher) {
        watchers.remove(watcher);
    }

    protected void removeAllWatchers() {
        watchers.clear();
    }

    protected void notifyRemoved() {
        for (GroundItemWatcher watcher : watchers) {
            watcher.removed(this);
        }
    }

    protected void notifyAdded() {
        for (GroundItemWatcher watcher : watchers) {
            watcher.added(this);
        }
    }
}
