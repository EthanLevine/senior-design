package uconnocalypse.engine;

public final class IntervalTreeNode<V> {
    private final double begin;
    private final double end;
    private double max;
    private int height;
    private final V value;
    private IntervalTreeNode<V> left;
    private IntervalTreeNode<V> right;
    private IntervalTreeNode<V> parent;
    private boolean staleData;

    public IntervalTreeNode(double begin, double end, V value) {
        this.begin = begin;
        this.end = end;
        this.max = end;
        this.height = 0;
        this.value = value;
        this.staleData = false;
    }
    
    public double getBegin() {
        return begin;
    }
    
    public double getEnd() {
        return end;
    }
    
    public double getMax() {
        if (staleData) {
            recalcData();
        }
        return max;
    }
    
    public int getHeight() {
        if (staleData) {
            recalcData();
        }
        return height;
    }
    
    public V getValue() {
        return value;
    }
    
    public IntervalTreeNode<V> getLeft() {
        return left;
    }
    
    public IntervalTreeNode<V> getRight() {
        return right;
    }
    
    public IntervalTreeNode<V> getParent() {
        return parent;
    }
    
    public void setLeft(IntervalTreeNode<V> left) {
        this.left = left;
        makeDataStale();
    }
    
    public void setRight(IntervalTreeNode<V> right) {
        this.right = right;
        makeDataStale();
    }
    
    public void setParent(IntervalTreeNode<V> parent) {
        this.parent = parent;
    }

    public int getLeftHeight() {
        if (left == null)
            return -1;
        else
            return left.getHeight();
    }

    public int getRightHeight() {
        if (right == null)
            return -1;
        else
            return right.getHeight();
    }
    
    protected void makeDataStale() {
        staleData = true;
        if (parent != null)
            parent.makeDataStale();
    }

    private void recalcData() {
        int lh = getLeftHeight();
        int rh = getRightHeight();
        height = 1 + (lh > rh ? lh : rh);
        
        double maxL = Double.NEGATIVE_INFINITY;
        double maxR = Double.NEGATIVE_INFINITY;
        if (left != null)
            maxL = left.getMax();
        if (right != null)
            maxR = right.getMax();
        max = (maxL > maxR ? maxL : maxR);
        max = (max > end ? max : end);
        
        staleData = false;
    }

    public void rotateRight() {
        //       GP
        //     P <- THIS
        //   L   C
        //  A B
        IntervalTreeNode P = this;
        IntervalTreeNode L = P.getLeft();
        IntervalTreeNode C = P.getRight();
        IntervalTreeNode A = L.getLeft();
        IntervalTreeNode B = L.getRight();
        IntervalTreeNode GP = P.getParent();
        
        L.setLeft(A);
        L.setRight(P);
        L.setParent(GP);
        P.setLeft(B);
        P.setRight(C);
        P.setParent(L);
        if (A != null) {
            A.setParent(L);
        }
        if (B != null) {
            B.setParent(P);
        }
        if (C != null) {
            C.setParent(P);
        }
        if (GP != null) {
            if (GP.getLeft() == P)
                GP.setLeft(L);
            else
                GP.setRight(L);
        }
        P.makeDataStale();
        L.makeDataStale();
    }

    public void rotateLeft() {
        //       GP
        //     P <- THIS
        //   A   R
        //      B C
        IntervalTreeNode P = this;
        IntervalTreeNode R = P.right;
        IntervalTreeNode A = P.left;
        IntervalTreeNode B = R.left;
        IntervalTreeNode C = R.right;
        IntervalTreeNode GP = P.parent;
        R.left = P;
        R.right = C;
        R.parent = GP;
        P.left = A;
        P.right = B;
        P.parent = R;
        if (A != null) {
            A.parent = P;
        }
        if (B != null) {
            B.parent = P;
        }
        if (C != null) {
            C.parent = R;
        }
        if (GP != null) {
            if (GP.left == P)
                GP.left = R;
            else
                GP.right = R;
        }
        P.makeDataStale();
        R.makeDataStale();
    }
    
    public void balance() {
        IntervalTreeNode<V> P = this;
        int balanceFactor = P.getLeftHeight() - P.getRightHeight();
        if (balanceFactor <= -2) {
            IntervalTreeNode<V> R = P.getRight();
            balanceFactor = R.getLeftHeight() - R.getRightHeight();
            if (balanceFactor == 1)
                R.rotateRight();
            P.rotateLeft();
        } else if (balanceFactor >= 2) {
            IntervalTreeNode<V> L = P.getLeft();
            balanceFactor = L.getLeftHeight() - L.getRightHeight();
            if (balanceFactor == -1)
                L.rotateLeft();
            P.rotateRight();
        }
    }
}
