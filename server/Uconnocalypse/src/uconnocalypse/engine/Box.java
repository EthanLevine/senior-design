package uconnocalypse.engine;

public interface Box {
    public double getXMin();
    public double getXMax();
    public double getYMin();
    public double getYMax();
}
