package uconnocalypse.engine;

public class SyncManager {
    
    private long lastActionTime = 0; // ns, in server time
    private long clientBaseOffset = 0; // ns, client time - server time
    private long clientLatency = 0; // ns, EWMA (alpha=0.25)
    private long pingSendTime = 0; // ns, in server time
    private boolean isInitialized = false;
    
    private final static long MS_TO_NS = 1000000;
    
    public boolean isInitialized() {
        return this.isInitialized;
    }
    
    public void ping() {
        pingSendTime = System.nanoTime();
    }
    
    // clientTime in ms
    public void pong(long clientTime) {
        lastActionTime = System.nanoTime();
        clientLatency = (lastActionTime - pingSendTime) / 2;
        long matchingServerTime = pingSendTime + clientLatency;
        clientBaseOffset = clientTime*MS_TO_NS - matchingServerTime;
        
        isInitialized = true;
    }
    
    // returns estimated latency in NANOSECONDS
    public long getLatency() {
        if (!isInitialized) throw new RuntimeException("Attempt to estimate latency before initialization.");
        return clientLatency;
    }
    
    // returns the server time associated with this client time (applies offset)
    public long registerAction(long clientTime) {
        if (!isInitialized) throw new RuntimeException("Attempt to register actions before initialization.");
        long currentServerTime = System.nanoTime();
        long matchingServerTime = currentServerTime + clientBaseOffset;
        // if the server time is LESS than the last action time, throw this away
        if ((matchingServerTime - lastActionTime) < 0) {
            return -1;
        }
        long currentLatency = clientTime*MS_TO_NS - matchingServerTime;
        clientLatency = (long) (clientLatency * 0.75 + currentLatency * 0.25);
        lastActionTime = matchingServerTime;
        return matchingServerTime;
    }
}
