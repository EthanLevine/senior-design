package uconnocalypse.engine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public final class IntervalTree<V> implements Iterable<V> {
    private IntervalTreeNode<V> root;
    
    public IntervalTree() {
    }
    
    public void insert(double begin, double end, V value) {
        IntervalTreeNode<V> newNode = new IntervalTreeNode<>(begin, end, value);
        if (root == null) {
            root = newNode;
        } else {
            insert(newNode, root);
            // adjust the root back to where it was
            while (root.getParent() != null) {
                root = root.getParent();
            }
        }
    }
    
    private void insert(IntervalTreeNode<V> newNode, IntervalTreeNode<V> node) {
        if (node.getBegin() < newNode.getBegin()) {
            if (node.getRight() != null) {
                insert(newNode, node.getRight());
            } else {
                node.setRight(newNode);
                newNode.setParent(node);
            }
        } else {
            if (node.getLeft() != null) {
                insert(newNode, node.getLeft());
            } else {
                node.setLeft(newNode);
                newNode.setParent(node);
            }
        }
        node.balance();
    }
    
    // NOTE: This does NOT do anything if the interval cannot be found.
    public void remove(double begin, double end, V value) {
        remove(begin, end, value, root);
    }
    
    private void remove(double begin, double end, V value, IntervalTreeNode<V> node) {
        if (node == null)
            return;
        if (node.getBegin() == begin) {
            if (node.getEnd() == end &&
                node.getValue() == value) {
                // remove node
            } else {
                remove(begin, end, value, node.getLeft());
                remove(begin, end, value, node.getRight());
            }
        } else if (node.getBegin() > begin) {
            remove(begin, end, value, node.getLeft());
        } else {
            remove(begin, end, value, node.getRight());
        }
    }
    
    public void clear() {
        root = null;
    }
    
    public List<V> intersect(double point) {
        return intersect(point, point);
    }
    
    public List<V> intersect(double begin, double end) {
        List<V> matches = new ArrayList<>();
        intersect(begin, end, root, matches);
        return matches;
    }
    
    private void intersect(double begin, double end, IntervalTreeNode<V> node, List<V> results) {
        // Don't check null nodes.
        if (node == null)
            return;
        
        // Don't check if the interval begins after all intervals under this node.
        if (node.getMax() < begin)
            return;
        
        // Search left children first.
        intersect(begin, end, node.getLeft(), results);
        
        // Check this node.
        if (node.getBegin() < end && node.getEnd() > begin)
            results.add(node.getValue());
        
        // Only check right children if 'end' is greater than this node's begin
        if (node.getBegin() < end)
            intersect(begin, end, node.getRight(), results);
    }
    
    @Override
    public Iterator<V> iterator() {
        return new IntervalTreeIterator<>(root);
    }
    
    public String getPrintout() {
        return getPrintout(root, 0);
    }
    
    private String getPrintout(IntervalTreeNode<V> node, int indent) {
        if (node == null)
            return "";
        char[] spaces = new char[indent];
        Arrays.fill(spaces, ' ');
        String line = new String(spaces) + "[" + node.getBegin() + ", " + node.getEnd() + "]: " + node.getValue().toString() + "\n";
        return line + getPrintout(node.getLeft(), indent + 2) + getPrintout(node.getRight(), indent + 2);
    }
    
    private static final class IntervalTreeIterator<V> implements Iterator<V> {
        private IntervalTreeNode<V> node;
        
        public IntervalTreeIterator(IntervalTreeNode<V> node) {
            while (node != null) {
                this.node = node;
                node = node.getLeft();
            }
        }
        
        private IntervalTreeNode<V> nextNode() {
            if (node == null)
                return null;
            
            if (node.getRight() != null) {
                IntervalTreeNode<V> sinkNode = node.getRight();
                while (sinkNode.getLeft() != null) {
                    sinkNode = sinkNode.getLeft();
                }
                return sinkNode;
            }
            
            IntervalTreeNode<V> parent = node.getParent();
            IntervalTreeNode<V> child = node;
            while (parent != null) {
                if (parent.getLeft() == child)
                    return parent;
                
                child = parent;
                parent = parent.getParent();
            }
            return null;
        }
        
        @Override
        public boolean hasNext() {
            return node != null;
        }
        
        @Override
        public V next() {
            V oldValue = node.getValue();
            node = nextNode();
            return oldValue;
        }
        
        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
