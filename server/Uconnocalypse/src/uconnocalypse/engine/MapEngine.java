package uconnocalypse.engine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Semaphore;
import java.util.Random;
import uconnocalypse.comm.GameConnection;
import uconnocalypse.comm.StandardErrors;
import uconnocalypse.data.*;
import uconnocalypse.server.GameInstance;
import uconnocalypse.server.GameServer;


public class MapEngine {
    private final GameInstance instance;

    private final EntityStore entityStore;
    private final GroundItemStore universalGroundItemStore;
    private final HashMap<Long, GroundItemStore> groundItemStores;
    
    private final CollisionEngine collisionEngine;
    private final ConcurrentLinkedQueue<Command> inputCommands;
    private final Thread loopThread;
    private volatile boolean stopSignal;
    private final Semaphore stopSemaphore;
    private final long tickTime;
    private final HashMap<Long, ConnectionData> activeConnections;
    private final double AISpeed;
    
    private final List<EntityWatcher> objectiveEntityWatchers = new ArrayList<>();
    private final List<GroundItemWatcher> objectiveItemWatchers = new ArrayList<>();
    
    private final List<AIBase> aiList = new ArrayList<>();
    private final Random generator = new Random();

    public MapEngine(MapRegionTemplate regionTemplate, GameInstance instance) {
        this.instance = instance;
        entityStore = new EntityStore();
        universalGroundItemStore = new GroundItemStore();
        groundItemStores = new HashMap<>();
        tickTime = 1000 * 1000 * 10; // 10 ms (100 tick)
        collisionEngine = new CollisionEngine(entityStore, tickTime);
        inputCommands = new ConcurrentLinkedQueue<>();
        stopSignal = false;
        stopSemaphore = new Semaphore(1);
        activeConnections = new HashMap<>();
        AISpeed=100;

        // Load barriers
        // Note: In future, barriers may become deprecated for fixed spawns and
        //       their own EntitySpecies objects.
        for (Barrier barrier : regionTemplate.getBarriers()) {
            entityStore.newEntity(barrier.getVirtualSpecies(), 0, 0, 0, 0, 0);
        }
        // Load fixed spawns
        for (FixedSpawn spawn : regionTemplate.getFixedSpawns()) {
            entityStore.newEntity(spawn.getSpecies(), spawn.getX(), spawn.getY(), 0, 0, spawn.getAngle());
        }

        // Igor, this line places a universal item on the map region (one anyone can see)
        inputCommands.add(new PlaceUniversalItemCommand(GameServer.databaseManager.instantiateItem(1), 500, 890));
        
        // @TODO AI - Add AIBase objects here, like this:
        // In this example, 1L and 2L are ENTITY SPECIES IDS.  Replace this as desired.
//        aiList.add(new AIBase(Arrays.asList(1L, 2L), entityStore) {
//            @Override
//            protected void update(Entity self) {
//                // Update the AI's velocity
//            }
//            @Override
//            protected void onCollision(Entity self, Entity other) {
//                // Optionally do something when a collision occurs.
//            }
//        });
//        aiList.add(new AIBase(Arrays.asList(2L, 3L), entityStore) {
//            @Override
//            protected void update(Entity self) {
//                // Update the AI's velocity
//                int randomIndex = generator.nextInt(20);
//                if(randomIndex <= 7)
//                {
//                    switch(randomIndex)
//                    {
//                        case 0:
//                            self.setXVel(-1*AISpeed);
//                            self.setYVel(-1*AISpeed);
//                        break;
//                        case 1:
//                            self.setXVel(-1*AISpeed);
//                            self.setYVel(0);
//                        break;
//                        case 2:
//                            self.setXVel(-1*AISpeed);
//                            self.setYVel(1*AISpeed);
//                        break;
//                        case 3:
//                            self.setXVel(1*AISpeed);
//                            self.setYVel(-1*AISpeed);
//                        break;
//                        case 4:
//                            self.setXVel(1*AISpeed);
//                            self.setYVel(0);
//                        break;
//                        case 5:
//                            self.setXVel(1*AISpeed);
//                            self.setYVel(1*AISpeed);
//                        break;
//                        case 6:
//                            self.setXVel(0);
//                            self.setYVel(-1*AISpeed);
//                        break;
//                        case 7:
//                            self.setXVel(0);
//                            self.setYVel(1*AISpeed);
//                        break;
//                    }
//                    entityStore.notifyUpdated(self);
//                }
//            }
//            @Override
//            protected void onCollision(Entity self, Entity other) {
//            }
//        });
        
        loopThread = new Thread(new Runnable() {
            @Override
            public void run() {
                tickLoop();
                stopSemaphore.release();
            }
        });
    }
    
    public void start() {
        stopSignal = false;
        stopSemaphore.acquireUninterruptibly();
        loopThread.start();
    }
    
    public void stop() {
        stopSignal = true;
        stopSemaphore.acquireUninterruptibly();
        stopSemaphore.release();
    }
    
    @SuppressWarnings("SleepWhileInLoop")
    private void tickLoop() {
        long targetTime = System.nanoTime();
        long prevTargetTime = targetTime;
        long tickCount = 0;
        while (!stopSignal) {
            // pull in any queued actions.
            Command inputCommand;
            while ((inputCommand = inputCommands.poll()) != null) {
                inputCommand.execute();
            }
            
            // Execute AI as necessary (every 10 ticks)
            if (tickCount % 10 == 0) {
                for (AIBase ai : aiList) {
                    ai.updateAll();
                }
            }

            // Update positions of all entities.
            for (Entity entity : entityStore) {
                if (!entity.getSpecies().isStatic()) {
                    entity.setXPos(entity.getXPos() + entity.getXVel() * 0.000000001 * (targetTime - prevTargetTime));
                    entity.setYPos(entity.getYPos() + entity.getYVel() * 0.000000001 * (targetTime - prevTargetTime));
                }
            }

            // Check players attacking
            long currentTime = System.nanoTime();
            for (ConnectionData connectionData : activeConnections.values()) {
                double attackRate = connectionData.weapon.getAttackRate();
                if (connectionData.isAttacking &&
                        (currentTime - connectionData.lastAttackTime) > (1E9 * attackRate)) {
                    connectionData.lastAttackTime = currentTime;
                    connectionData.weapon.use();
                }
            }

            // Update dynamic, monster, and player boxtrees
            collisionEngine.checkCollisions();

            // pull in any more queued actions.
            while ((inputCommand = inputCommands.poll()) != null) {
                inputCommand.execute();
            }

            long sleepTime = tickTime - (System.nanoTime() - targetTime);
            prevTargetTime = targetTime;
            if (sleepTime > 0) {
                targetTime += tickTime;
                try {
                    Thread.sleep(sleepTime / 1000000, (int)(sleepTime % 1000000));
                } catch (InterruptedException e) {
                }
            } else {
                targetTime = System.nanoTime();
            }
            
            tickCount = (tickCount + 1) % Long.MAX_VALUE; // do not become negative
        }
    }
    
    public void addConnection(final GameConnection connection, final Point spawnPoint) {
        Command addConnectionCommand = new Command() {
            @Override
            public void executeImpl() {
                EntityWatcher entityWatcher = new EntityWatcher() {
                    @Override
                    protected void added(Entity entity) {
                        if (entity.getSpecies().getId() == null) {
                            // this is a virtual species (like a barrier)
                            // do not inform client of this entity
                            return;
                        }
                        connection.getSender().sendMessage("engine.add-entity",
                                entity.getId(),
                                entity.getSpecies().getId(),
                                entity.getXPos(),
                                entity.getYPos(),
                                entity.getAngle(),
                                entity.getXVel(),
                                entity.getYVel());
                    }
                    @Override
                    protected void updated(Entity entity) {
                        connection.getSender().sendMessage("engine.update-entity",
                                entity.getId(),
                                entity.getXPos(),
                                entity.getYPos(),
                                entity.getAngle(),
                                entity.getXVel(),
                                entity.getYVel());
                    }
                    @Override
                    protected void removed(Entity entity) {
                        connection.getSender().sendMessage("engine.remove-entity",
                                entity.getId());
                    }
                };
                GroundItemWatcher itemWatcher = new GroundItemWatcher() {
                    @Override
                    protected void added(GroundItem groundItem) {
                        connection.getSender().sendMessage("item.place",
                                groundItem.getId(),
                                groundItem.getItem().getItemTemplate().getId(),
                                groundItem.getItem().getName(),
                                groundItem.getItem().getItemQuality(),
                                groundItem.getXPos(),
                                groundItem.getYPos());
                    }
                    @Override
                    protected void removed(GroundItem groundItem) {
                        connection.getSender().sendMessage("item.remove", groundItem.getId());
                    }
                };
                GroundItemWatcher universalItemWatcher = new GroundItemWatcher() {
                    // Universal Item Const is used to distinguish whether the item the client is talking about
                    // is in the universal store or their own store.
                    // Constant should be a number larger than the amount of items in a region at a time.
                    @Override
                    protected void added(GroundItem groundItem) {
                        connection.getSender().sendMessage("item.place",
                                (groundItem.getId()+GameConstants.UNIVERSAL_ITEM_CONST),
                                groundItem.getItem().getItemTemplate().getId(),
                                groundItem.getItem().getName(),
                                groundItem.getItem().getItemQuality(),
                                groundItem.getXPos(),
                                groundItem.getYPos());
                    }
                    @Override
                    protected void removed(GroundItem groundItem) {
                        connection.getSender().sendMessage("item.remove", (groundItem.getId()+GameConstants.UNIVERSAL_ITEM_CONST));
                    }
                };
                // User has their own ground item store for items that are theirs
                GroundItemStore store = new GroundItemStore();
                store.addWatcher(itemWatcher);
                groundItemStores.put(connection.getAccount().getId(), store);
                // Send existing items
                for (GroundItem groundItem : universalGroundItemStore) {
                    universalItemWatcher.added(groundItem);
                }
                universalGroundItemStore.addWatcher(universalItemWatcher);
                // send existing entities
                for (Entity entity : entityStore) {
                    entityWatcher.added(entity);
                }
                // add watchers
                entityStore.addWatcher(entityWatcher);
                // create player entity
                Entity playerEntity = entityStore.newEntity(EntitySpecies.PLAYER_SPECIES,
                        spawnPoint.x, spawnPoint.y, 0, 0, 0);
                ConnectionData connectionData = new ConnectionData(connection,
                        entityStore.requestToken(), playerEntity, entityWatcher,
                        itemWatcher, universalItemWatcher);
                activeConnections.put(connection.getAccount().getId(), connectionData);
                connection.getSender().sendMessage("player.set-entity-id", playerEntity.getId());
            }
        };
        inputCommands.add(addConnectionCommand);
        addConnectionCommand.waitOnReturn();
    }
    
    public void removeConnection(final GameConnection connection) {
        Command removeConnectionCommand = new Command() {
            @Override
            public void executeImpl() {
                GroundItemStore personalStore = groundItemStores.remove(connection.getAccount().getId());
                ConnectionData connectionData = activeConnections.remove(
                        connection.getAccount().getId());
                connectionData.token.destroy();
                entityStore.delete(connectionData.playerEntity.getId());
                entityStore.removeWatcher(connectionData.entityWatcher);
                personalStore.removeWatcher(connectionData.itemWatcher);
                universalGroundItemStore.removeWatcher(connectionData.universalItemWatcher);
            }
        };
        inputCommands.add(removeConnectionCommand);
        removeConnectionCommand.waitOnReturn();
    }
    
    public void confirmEntityDelete(final GameConnection connection) {
        Command confirmDeleteCommand = new Command() {
            @Override
            public void executeImpl() {
                activeConnections.get(connection.getAccount().getId()).token.confirmDelete();
            }
        };
        inputCommands.add(confirmDeleteCommand);
    }
    
    public void setPlayerMotion(GameConnection connection, String motionDirection,
            long serverChangeTime) {
        final double playerSpeed = 100;
        double playerVelX = 0;
        double playerVelY = 0;
        switch (motionDirection) {
            case "":
                break;
            case "N":
                playerVelY = -1;
                break;
            case "NE":
                playerVelX = 0.707;
                playerVelY = -0.707;
                break;
            case "E":
                playerVelX = 1;
                break;
            case "SE":
                playerVelX = 0.707;
                playerVelY = 0.707;
                break;
            case "S":
                playerVelY = 1;
                break;
            case "SW":
                playerVelX = -0.707;
                playerVelY = 0.707;
                break;
            case "W":
                playerVelX = -1;
                break;
            case "NW":
                playerVelX = -0.707;
                playerVelY = -0.707;
                break;
        }
        playerVelX *= playerSpeed;
        playerVelY *= playerSpeed;
        ConnectionData data = activeConnections.get(connection.getAccount().getId());
        if (data.playerEntity.getXVel() == data.lastMovementXVel && data.playerEntity.getYVel() == data.lastMovementYVel) {
            // ONLY apply this fix if the engine has not modified the player's velocity since the last movement command.
            // Otherwise, DO NOT correct the player's position - the server has already set it.
            data.lastMovementXPos += data.lastMovementXVel * (serverChangeTime - data.lastMovementTime) * 1E-9;
            data.lastMovementYPos += data.lastMovementYVel * (serverChangeTime - data.lastMovementTime) * 1E-9;
        } else {
            data.lastMovementXPos = data.playerEntity.getXPos();
            data.lastMovementYPos = data.playerEntity.getYPos();
        }
        data.lastMovementTime = serverChangeTime;
        data.lastMovementXVel = playerVelX;
        data.lastMovementYVel = playerVelY;
        inputCommands.add(new ModifyEntityCommand(activeConnections.get(connection.getAccount().getId()).playerEntity.getId(),
                false, data.lastMovementXPos, data.lastMovementYPos, playerVelX, playerVelY, null));
    }

    public void setPlayerAngle(GameConnection connection, Double playerAngle) {
        inputCommands.add(new ModifyEntityCommand(activeConnections.get(connection.getAccount().getId()).playerEntity.getId(),
                false, null, null, null, null, playerAngle));
    }

    public void pickupItem(GameConnection connection, int groundItemId) {
        inputCommands.add(new PickupItemCommand(connection, groundItemId));
    }

    public void dropInventoryItem(GameConnection connection, StashedItem stashedItem) {
        inputCommands.add(new DropInventoryItemCommand(connection, stashedItem));
    }

    public void equipItem(GameConnection connection, StashedItem stashedItem) {
        inputCommands.add(new EquipItemCommand(connection, stashedItem));
    }

    public void unequipItem(GameConnection connection, Item item) {
        inputCommands.add(new UnequipItemCommand(connection, item));
    }
    
    public void startAttacking(final GameConnection connection) {
        inputCommands.add(new Command() {
            @Override
            public void executeImpl() {
                activeConnections.get(connection.getAccount().getId()).isAttacking = true;
            };
        });
    }
    
    public void stopAttacking(final GameConnection connection) {
        inputCommands.add(new Command() {
            @Override
            public void executeImpl() {
                activeConnections.get(connection.getAccount().getId()).isAttacking = false;
            }
        });
    }
    
    public void TEMP_equipWeapon(final GameConnection connection, final int itemId) {
        inputCommands.add(new Command() {
            @Override
            public void executeImpl() {
                ConnectionData connectionData = activeConnections.get(connection.getAccount().getId());
                connectionData.weapon = Weapon.createWeapon(GameServer.databaseManager.getItem(itemId),
                        connectionData.playerEntity, entityStore, collisionEngine);
            }
        });
    }

    public void watchObjectives(Set<Objective> objectives,
            final ConcurrentHashMap<Long, Integer> objectiveProgress) {
        objectiveEntityWatchers.clear();
        objectiveItemWatchers.clear();
        for (Objective objective : objectives) {
            if (objective.getType() == Objective.TYPE_KILL_ENTITY) {
                final long entitySpeciesId = objective.getTargetId();
                final long objectiveId = objective.getId();
                objectiveEntityWatchers.add(new EntityWatcher() {
                    @Override
                    public void removed(Entity e) {
                        if (e.getSpecies().getId() == entitySpeciesId &&
                                e.getHealth() == 0) {
                            boolean replaced = false;
                            while (!replaced) {
                                int progress = objectiveProgress.get(objectiveId);
                                replaced = objectiveProgress.replace(objectiveId,
                                        progress, progress + 1);
                            }
                        }
                    }
                });
            } else if (objective.getType() == Objective.TYPE_GATHER_ITEM) {
                final long itemTemplateId = objective.getTargetId();
                final long objectiveId = objective.getId();
                objectiveItemWatchers.add(new GroundItemWatcher() {
                    @Override
                    public void removed(GroundItem i) {
                        if (i.getItem().getItemTemplate().getId() == itemTemplateId) {
                            boolean replaced = false;
                            while (!replaced) {
                                int progress = objectiveProgress.get(objectiveId);
                                replaced = objectiveProgress.replace(objectiveId,
                                        progress, progress + 1);
                            }
                        }
                    }
                });
            }
        }
    }

    /**
     * This is a dirty method for easy checking and updating of objectives
     * @param type This is the ObjectiveType (kill or collect)
     * @param id This is the id of the item or entity species
     * @param increment This is the value we are going to change the objective by if necessary
     * @return True if this is part of our current objective
     */
    protected boolean updateObjective(int type, long id, int increment) {
        for (Objective objective : instance.getCurrentTask().getObjectives()) {
            if (objective.getType() == type && objective.getTargetId() == id) {
                if (instance.getObjectiveProgress(objective.getId()) < objective.getCount()) {
                    instance.setObjectiveProgress(objective.getId(),
                            instance.getObjectiveProgress(objective.getId())+increment);
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }
        
    private abstract class Command<T> {
        private final Semaphore sem = new Semaphore(0);
        private T returnedObject = null;
        
        protected abstract void executeImpl();
        
        protected final void setReturnedObject(T returnedObject) {
            this.returnedObject = returnedObject;
        }
        
        public final void execute() {
            executeImpl();
            sem.release();
        }
        
        public final T waitOnReturn()  {
            sem.acquireUninterruptibly();
            sem.release();
            return returnedObject;
        }
    }

    private final class PickupItemCommand extends Command {
        private final GameConnection connection;
        private final long accountId;
        private int groundItemId;

        public PickupItemCommand(GameConnection connection, int groundItemId) {
            this.connection = connection;
            this.accountId = connection.getAccount().getId();
            this.groundItemId = groundItemId;
        }

        @Override
        protected void executeImpl() {
            ConnectionData connectionData = activeConnections.get(accountId);
            GroundItem groundItem;
            boolean universal = false;
            if(groundItemId >= GameConstants.UNIVERSAL_ITEM_CONST) {
                groundItemId -= GameConstants.UNIVERSAL_ITEM_CONST;
                groundItem = universalGroundItemStore.get(groundItemId);
                universal = true;
            } else {
                groundItem = groundItemStores.get(accountId).get(groundItemId);
            }
            if(groundItem == null || !groundItem.isActive()) {
                connection.getSender().sendError(StandardErrors.ITEM_DOESNT_EXIST, "The item you are attempting to pickup" +
                        "is no longer there.");
                return;
            }
            // TODO: Make this into a radius
            if(connectionData.playerEntity.getXPos() - groundItem.getXPos() > GameConstants.MAX_PICKUP_DIST ||
                    connectionData.playerEntity.getYPos() - groundItem.getYPos() > GameConstants.MAX_PICKUP_DIST) {
                connection.getSender().sendError(StandardErrors.ITEM_PICKUP_TOO_FAR, "The item you are trying to pick up" +
                        "is too far away.");
                return;
            }
            if(connection.getCharacter().isInventoryFull()) {
                connection.getSender().sendError(StandardErrors.INVENTORY_FULL, "No room in inventory to pick up items.");
                return;
            }
            if(universal) {
                universalGroundItemStore.delete(groundItemId);
            } else {
                groundItemStores.get(accountId).delete(groundItemId);
            }

            if (GameConstants.isTaskItem(groundItem.getItem().getItemTemplate().getId())) {
                if (updateObjective(Objective.TYPE_GATHER_ITEM, groundItem.getItem().getItemTemplate().getId(), 1)) {
                    connection.getSender().sendConfirm("inventory.pickup");
                    return;
                }
            }
            // Add to the player's inventory
            connection.getCharacter().addItemToInventory(groundItem.getItem());
            connection.getSender().sendConfirm("inventory.pickup");
        }
    }

    private final class DropInventoryItemCommand extends Command {
        private final GameConnection connection;
        private final long accountId;
        private StashedItem stashedItem;

        public DropInventoryItemCommand(GameConnection connection, StashedItem stashedItem) {
            this.connection = connection;
            this.accountId = connection.getAccount().getId();
            this.stashedItem = stashedItem;
        }

        @Override
        protected void executeImpl() {
            if(!connection.getCharacter().getInventory().getStashedItems().contains(stashedItem)) {
                connection.getSender().sendError(StandardErrors.ITEM_DOESNT_EXIST, "Item doesn't exist in inventroy.");
                return;
            }
            // Remove the stashedItem from the character's inventory
            connection.getCharacter().removeInventoryItem(stashedItem);
            ConnectionData connectionData = activeConnections.get(accountId);
            universalGroundItemStore.newGroundItem(stashedItem.getItem(), connectionData.playerEntity.getXPos(), connectionData.playerEntity.getYPos());
            connection.getSender().sendConfirm("inventory.drop");
        }
    }

    private final class EquipItemCommand extends Command {
        private final GameConnection connection;
        private final StashedItem stashedItem;
        public EquipItemCommand(GameConnection connection, StashedItem stashedItem) {
            this.connection = connection;
            this.stashedItem = stashedItem;
        }

        @Override
        protected void executeImpl() {
            // TODO: Check to see that the player has the levels to use the item
            if(!connection.getCharacter().getInventory().getStashedItems().contains(stashedItem)) {
                connection.getSender().sendError(StandardErrors.ITEM_DOESNT_EXIST, "Item doesn't exist in inventroy.");
                return;
            }
            connection.getCharacter().equipInventoryItem(stashedItem);
            connection.getSender().sendConfirm("inventory.equip");
        }
    }

    private final class UnequipItemCommand extends Command {
        private final GameConnection connection;
        private final Item item;
        public UnequipItemCommand(GameConnection connection, Item item) {
            this.connection = connection;
            this.item = item;
        }

        @Override
        protected void executeImpl() {
            if(connection.getCharacter().isInventoryFull()) {
                connection.getSender().sendError(StandardErrors.INVENTORY_FULL, "No room in your inventory to unequip.");
                return;
            }
            
            if (item.getItemTemplate().getEquipSlot() > 10) {
                connection.getCharacter().unequipWeapon();
            } else {
                connection.getCharacter().unequipArmor();
            }
            connection.getSender().sendConfirm("inventory.unequip");
        }
    }

    private final class PlaceItemCommand extends Command {
        private final long accountId;
        private final Item item;
        private final double xPos;
        private final double yPos;

        public PlaceItemCommand(long accountId, Item item, double xPos, double yPos) {
            this.accountId = accountId;
            this.item = item;
            this.xPos = xPos;
            this.yPos = yPos;
        }

        @Override
        protected void executeImpl() {
            groundItemStores.get(accountId).newGroundItem(item, xPos, yPos);
        }
    }

    private final class RemoveItemCommand extends Command {
        private final long accountId;
        private final int groundItemId;
        public RemoveItemCommand(long accountId, int groundItemId) {
            this.accountId = accountId;
            this.groundItemId = groundItemId;
        }

        @Override
        protected void executeImpl() {
            groundItemStores.get(accountId).delete(groundItemId);
        }
    }

    private final class PlaceUniversalItemCommand extends Command {
        private final Item item;
        private final double xPos;
        private final double yPos;

        public PlaceUniversalItemCommand(Item item, double xPos, double yPos) {
            this.item = item;
            this.xPos = xPos;
            this.yPos = yPos;
        }

        @Override
        protected void executeImpl() {
            universalGroundItemStore.newGroundItem(item, xPos, yPos, true);
        }
    }

    private final class RemoveUniversalItemCommand extends Command {
        private final int groundItemId;
        public RemoveUniversalItemCommand(int groundItemId) {
            this.groundItemId = groundItemId;
        }

        @Override
        protected void executeImpl() {
            universalGroundItemStore.delete(groundItemId);
        }
    }
    
    private final class AddEntityCommand extends Command<Entity> {
        private final EntitySpecies entitySpecies;
        private final double xPos;
        private final double yPos;
        private final double xVel;
        private final double yVel;
        private final double angle;
        
        public AddEntityCommand(EntitySpecies entitySpecies, double xPos, double yPos,
                double xVel, double yVel, double angle) {
            this.entitySpecies = entitySpecies;
            this.xPos = xPos;
            this.yPos = yPos;
            this.xVel = xVel;
            this.yVel = yVel;
            this.angle = angle;
        }
        
        @Override
        protected void executeImpl() {
            setReturnedObject(entityStore.newEntity(entitySpecies, xPos, yPos, xVel, yVel, angle));
        }
    }
    
    private final class ModifyEntityCommand extends Command {
        private final int entityId;
        private final boolean isRelative;
        // Note:  'null' values mean to ignore.
        private final Double xPos;
        private final Double yPos;
        private final Double xVel;
        private final Double yVel;
        private final Double angle;
        public ModifyEntityCommand(int entityId, boolean isRelative,
                Double xPos, Double yPos, Double xVel, Double yVel, Double angle) {
            this.entityId = entityId;
            this.isRelative = isRelative;
            this.xPos = xPos;
            this.yPos = yPos;
            this.xVel = xVel;
            this.yVel = yVel;
            this.angle = angle;
        }
        
        @Override
        protected void executeImpl() {
            Entity entity = entityStore.get(entityId);
            if (isRelative) {
                if (xPos != null) entity.setXPos(entity.getXPos() + xPos);
                if (yPos != null) entity.setYPos(entity.getYPos() + yPos);
                if (xVel != null) entity.setXVel(entity.getXVel() + xVel);
                if (yVel != null) entity.setYVel(entity.getYVel() + yVel);
                if (angle != null) entity.setAngle(entity.getAngle() + angle);
            } else {
                if (xPos != null) entity.setXPos(xPos);
                if (yPos != null) entity.setYPos(yPos);
                if (xVel != null) entity.setXVel(xVel);
                if (yVel != null) entity.setYVel(yVel);
                if (angle != null) entity.setAngle(angle);
            }
            entityStore.notifyUpdated(entity);
        }
    }
    
    private final class RemoveEntityCommand extends Command {
        private final int entityId;
        public RemoveEntityCommand(int entityId) {
            this.entityId = entityId;
        }
        
        @Override
        protected void executeImpl() {
            entityStore.delete(entityId);
        }
    }
    
    private final class ConnectionData {
        private final GameConnection connection;
        private final EntityStore.Token token;
        private final Entity playerEntity;
        private final EntityWatcher entityWatcher;
        private final GroundItemWatcher itemWatcher;
        private final GroundItemWatcher universalItemWatcher;
        
        private boolean isAttacking = false;
        private long lastAttackTime = System.nanoTime();
        private Weapon weapon;
        
        private long lastMovementTime;
        private double lastMovementXPos;
        private double lastMovementYPos;
        private double lastMovementXVel = 0;
        private double lastMovementYVel = 0;
        
        public ConnectionData(GameConnection connection, EntityStore.Token token,
                Entity playerEntity, EntityWatcher entityWatcher,
                GroundItemWatcher itemWatcher,
                GroundItemWatcher universalItemWatcher) {
            this.connection = connection;
            this.token = token;
            this.playerEntity = playerEntity;
            this.entityWatcher = entityWatcher;
            this.itemWatcher = itemWatcher;
            this.universalItemWatcher = universalItemWatcher;
            Item equippedWeapon = connection.getCharacter().getEquippedWeapon();
            if (equippedWeapon == null) {
                equippedWeapon = GameServer.databaseManager.getItem(Item.PISTOL_WEAPON_ID);
            }
            this.weapon = Weapon.createWeapon(equippedWeapon, playerEntity,
                    entityStore, collisionEngine);
            this.lastMovementTime = System.nanoTime();
            this.lastMovementXPos = playerEntity.getXPos();
            this.lastMovementYPos = playerEntity.getYPos();
        }
    }
}
