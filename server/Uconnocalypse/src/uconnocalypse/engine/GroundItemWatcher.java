package uconnocalypse.engine;

/**
 * i didn't want to do it this way but since we are already doing it with Entities it seems like the easiest way.
 */
public class GroundItemWatcher {
    protected void added(GroundItem e) {}

    protected void removed(GroundItem e) {}
}
