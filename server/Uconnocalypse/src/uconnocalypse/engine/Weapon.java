package uconnocalypse.engine;

import java.util.ArrayList;
import java.util.List;
import uconnocalypse.data.EntitySpecies;
import uconnocalypse.data.GameConstants;
import uconnocalypse.data.Item;
import uconnocalypse.data.PhysicalType;

public abstract class Weapon {
    protected final Item item;
    protected final Entity owner;
    protected final double attackRate;
    protected final EntityStore entityStore;
    protected final CollisionEngine collisionEngine;
    
    protected Weapon(Item item, Entity owner, double attackRate,
            EntityStore entityStore, CollisionEngine collisionEngine) {
        this.item = item;
        this.owner = owner;
        this.attackRate = attackRate;
        this.entityStore = entityStore;
        this.collisionEngine = collisionEngine;
    }
    
    public static Weapon createWeapon(Item item, Entity owner,
            EntityStore entityStore, CollisionEngine collisionEngine) {
        boolean isMelee = item.getItemTemplate().getEquipSlot() == GameConstants.EQUIPMENT_MELEE;
        if (isMelee) {
            return new Melee(item, owner,
                    item.getItemTemplate().getWeaponAttackRate(),
                    entityStore, collisionEngine,
                    item.getItemTemplate().getWeaponMeleeRange(),
                    item.getItemTemplate().getWeaponSpread());
        } else {
            return new Gun(item, owner,
                    item.getItemTemplate().getWeaponAttackRate(),
                    entityStore, collisionEngine,
                    item.getItemTemplate().getWeaponProjectileSpecies(),
                    item.getItemTemplate().getWeaponProjectileSpeed(),
                    item.getItemTemplate().getWeaponProjectileCount(),
                    item.getItemTemplate().getWeaponSpread());
        }
    }
    
    public abstract void use();
    
    public final double getAttackRate() {
        return attackRate;
    }
    
    protected void dealDamage(double attackRating, Entity victim) {
        double defense = victim.getDefenseRating();
        // vary attack rating from -5% to +10%
        double attack = attackRating * (Math.random() * 0.15 + 0.95);
        // formula:  mitigation = 20 ^ (- defense / 70)
        int damage = (int)Math.round(attack * Math.pow(20, -defense/70));
        // now, deal the damage
        if (victim.getHealth() <= damage) {
            victim.setHealth(0);
            entityStore.delete(victim.getId());
        } else {
            victim.setHealth(victim.getHealth() - damage);
        }
    }
    
    private static class Gun extends Weapon {
        protected final EntitySpecies projectileSpecies;
        protected final double projectileSpeed;
        protected final EntityWatcher projectileWatcher;
        protected final int projectileCount;
        protected final double projectileSpread;
        
        public Gun(Item item, Entity owner, double attackRate,
                EntityStore entityStore, CollisionEngine collisionEngine,
                EntitySpecies projectileSpecies, double projectileSpeed,
                int projectileCount, double projectileSpread) {
            super(item, owner, attackRate, entityStore, collisionEngine);
            this.projectileSpecies = projectileSpecies;
            this.projectileSpeed = projectileSpeed;
            this.projectileCount = projectileCount;
            this.projectileSpread = projectileSpread;
            this.projectileWatcher = new EntityWatcher() {
                @Override
                public void collision(Entity collider, Entity collidee) {
                    if (Gun.this.owner.canAttack(collidee)) {
                        dealDamage(collider.getAttackRating(), collidee);
                    }
                    if (PhysicalType.isSolid(collidee.getSpecies().getPhysicalType()) &&
                            !PhysicalType.isPlayer(collidee.getSpecies().getPhysicalType())) {
                        Gun.this.entityStore.delete(collider.getId());
                    }
                }
            };
        }
        
        @Override
        public void use() {
            // Spawn projectiles
            Point ownerPosition = owner.getPos();
            for (int i = 0; i < projectileCount; i++) {
                double projectileAngle = owner.getAngle() + projectileSpread * (Math.random() - 0.5);
                // note:  "10" is the distance from the player that the bullet should spawn.
                Point spawnPoint = ownerPosition.add(new Point(0,-10).rotate(projectileAngle));
                Point velocityVector = new Point(0, -projectileSpeed).rotate(projectileAngle);
                Entity projectile = entityStore.newEntity(projectileSpecies,
                        spawnPoint.x, spawnPoint.y,
                        velocityVector.x, velocityVector.y,
                        projectileAngle);
                projectile.setAttackRating(owner.getAttackRating());
                projectile.addWatcher(projectileWatcher);
            }
        }
    }
    
    private static class Melee extends Weapon {
        private final PhysicalGeometry relativeHitArea;
        
        public Melee(Item item, Entity owner, double attackRate,
                EntityStore entityStore, CollisionEngine collisionEngine,
                double hitRange, double hitSpread) {
            super(item, owner, attackRate, entityStore, collisionEngine);
            List<Point> hitAreaPoints = new ArrayList<>();
            hitAreaPoints.add(new Point(0, 0));
            Point rangePoint = new Point(hitRange, 0);
            final int fidelity = 2;
            for (int i = -fidelity; i <= fidelity; i++) {
                hitAreaPoints.add(rangePoint.rotate(i * hitSpread / (2 * fidelity)));
            }
            relativeHitArea = new PhysicalGeometry(hitAreaPoints);
        }
        
        @Override
        public void use() {
            // Get a transformed hit area
            PhysicalGeometry hitArea = new PhysicalGeometry(relativeHitArea, owner.getPos(), owner.getAngle());
            for (Entity hit : collisionEngine.getCollidingEntities(hitArea)) {
                if (owner.canAttack(hit)) {
                    dealDamage(owner.getAttackRating(), hit);
                }
            }
        }
    }
}
