package uconnocalypse.engine;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import uconnocalypse.data.EntitySpecies;

public final class EntityStore implements Iterable<Entity> {    
    private final ArrayList<Entity> entityList;
    
    private final LinkedList<Integer> entityFreeList;
    
    private final ArrayList<Token> tokenList;
    
    private final List<EntityWatcher> watchers;
    
    public EntityStore() {
        entityList = new ArrayList<>();
        entityFreeList = new LinkedList<>();
        tokenList = new ArrayList<>();
        watchers = new ArrayList<>();
    }
    
    /////////////////////////////
    // Entity Creation Methods //
    /////////////////////////////
    
    // Creates an entity.
    public Entity newEntity(EntitySpecies species, double x_pos, double y_pos, double x_vel, double y_vel, double angle) {
        Entity entity;
        if (entityFreeList.size() > 0) {
            entity = entityList.get(entityFreeList.removeLast());
        } else {
            entity = new Entity(entityList.size());
            entityList.add(entity);
        }
        entity.setSpecies(species);
        entity.setXPos(x_pos);
        entity.setYPos(y_pos);
        entity.setXVel(x_vel);
        entity.setYVel(y_vel);
        entity.setAngle(angle);
        entity.setEnergy(species.getMaxEnergy());
        entity.setHealth(species.getMaxHealth());
        entity.setAttackRating(species.getBaseAttackRating());
        entity.setDefenseRating(species.getBaseDefenseRating());
        entity.removeAllWatchers();
        
        entity.activate();
        notifyAdded(entity);
        return entity;
    }
    
    ///////////////////////////
    // Entity Access Methods //
    ///////////////////////////
    public Entity get(int entity_id) {
        return entityList.get(entity_id);
    }
    
    /////////////////////////////
    // Entity Deletion Methods //
    /////////////////////////////
    public void delete(int entity_id) {
        // note:  This does NOT immediate delete the entity
        //        The deletion MUST be confirmed by all tokens
        Entity entity = entityList.get(entity_id);
        entity.deactivate();
        notifyRemoved(entity);
        for (Token token : tokenList) {
            token.pendingList.addFirst(entity_id);
        }
    }
    
    //////////////////////////////////////
    // Watcher and Notification Methods //
    //////////////////////////////////////
    public void addWatcher(EntityWatcher watcher) {
        watchers.add(watcher);
    }
    
    public void removeWatcher(EntityWatcher watcher) {
        watchers.remove(watcher);
    }
    
    protected void removeAllWatchers() {
        watchers.clear();
    }
    
    private void notifyRemoved(Entity entity) {
        for (EntityWatcher watcher : watchers) {
            watcher.removed(entity);
        }
        entity.notifyRemoved();
    }
    
    protected void notifyUpdated(Entity entity) {
        for (EntityWatcher watcher : watchers) {
            watcher.updated(entity);
        }
        entity.notifyUpdated();
    }
    
    private void notifyAdded(Entity entity) {
        for (EntityWatcher watcher : watchers) {
            watcher.added(entity);
        }
        entity.notifyAdded();
    }
    
    protected void notifyCollided(Entity a, Entity b) {
        for (EntityWatcher watcher : watchers) {
            watcher.collision(a, b);
        }
        a.notifyCollided(b);
    }
    
    //////////////////////////////
    // Token Management Methods //
    //////////////////////////////
    public Token requestToken() {
        Token token = new Token();
        synchronized(tokenList) {
            tokenList.add(token);
        }
        return token;
    }
    
    private void destroyToken(Token token) {
        while (token.pendingList.size() > 0) {
            confirmDelete(token);
        }
        synchronized(tokenList) {
            tokenList.remove(token);
        }
    }
    
    private void confirmDelete(Token token) {
        // Are we the last token with this item (are the the token with the most
        // items)?
        synchronized(tokenList) {
            int token_size = token.pendingList.size();
            int entity_id = token.pendingList.removeLast();
            for (Token otherToken : tokenList) {
                if (otherToken != token) {
                    if (otherToken.pendingList.size() >= token_size) {
                        return;
                    }
                }
            }
            // We made it through and haven't returned, so it's up to us to
            // ACTUALLY free this entity.
            entityFreeList.addFirst(entity_id);
        }
    }
    
    public final class Token {
        private final LinkedList<Integer> pendingList;
        
        private Token() {
            pendingList = new LinkedList<>();
        }
        
        public void confirmDelete() {
            EntityStore.this.confirmDelete(this);
        }
        
        public void destroy() {
            EntityStore.this.destroyToken(this);
        }
    }
    
    @Override
    public Iterator<Entity> iterator() {
        return new EntityIterator();
    }

    private final class EntityIterator implements Iterator<Entity> {
        private int entityId = 0;

        @Override
        public boolean hasNext() {
            Entity entity;
            do {
                if (entityId == entityList.size()) {
                    return false;
                }
                entity = entityList.get(entityId);
            } while (!entity.isActive() && (++entityId > 0));
            return true;
        }

        @Override
        public Entity next() {
            // make sure entityId is active
            hasNext();
            Entity entity = entityList.get(entityId);
            entityId++;
            return entity;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
