package uconnocalypse.engine;

import uconnocalypse.data.Item;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * GroundItemStore.java
 */
public class GroundItemStore implements Iterable<GroundItem> {
    private final ArrayList<GroundItem> groundItemList;
    private final LinkedList<Integer> groundItemFreeList;
    private final List<GroundItemWatcher> watchers;

    public GroundItemStore() {
        groundItemList = new ArrayList<>();
        groundItemFreeList = new LinkedList<>();
        watchers = new ArrayList<>();
    }

    public GroundItem newGroundItem(Item item, double x_pos, double y_pos) {
        return newGroundItem(item, x_pos, y_pos, false);
    }

    // Creates an entity.
    public GroundItem newGroundItem(Item item, double x_pos, double y_pos, boolean universal) {
        GroundItem groundItem;
        if (groundItemFreeList.size() > 0) {
            groundItem = groundItemList.get(groundItemFreeList.removeLast());
        } else {
            groundItem = new GroundItem(groundItemList.size());
            groundItemList.add(groundItem);
        }
        groundItem.setItem(item);
        groundItem.setXPos(x_pos);
        groundItem.setYPos(y_pos);
        groundItem.setUniversal(universal);

        groundItem.activate();
        notifyAdded(groundItem);
        return groundItem;
    }

    public GroundItem get(int groundItem_id) {
        return groundItemList.get(groundItem_id);
    }

    public Item delete(int groundItem_id) {
        GroundItem groundItem = groundItemList.get(groundItem_id);
        groundItem.deactivate();
        notifyRemoved(groundItem);
        groundItemFreeList.addFirst(groundItem_id);
        return groundItem.getItem();
    }

    public void addWatcher(GroundItemWatcher watcher) {
        watchers.add(watcher);
    }

    public void removeWatcher(GroundItemWatcher watcher) {
        watchers.remove(watcher);
    }

    protected void removeAllWatchers() {
        watchers.clear();
    }

    private void notifyRemoved(GroundItem groundItem) {
        for (GroundItemWatcher watcher : watchers) {
            watcher.removed(groundItem);
        }
        groundItem.notifyRemoved();
    }

    private void notifyAdded(GroundItem groundItem) {
        for (GroundItemWatcher watcher : watchers) {
            watcher.added(groundItem);
        }
        groundItem.notifyAdded();
    }

    @Override
    public Iterator<GroundItem> iterator() {
        return new GroundItemIterator();
    }

    private final class GroundItemIterator implements Iterator<GroundItem> {
        private int groundItemId = 0;

        @Override
        public boolean hasNext() {
            GroundItem groundItem;
            do {
                if (groundItemId == groundItemList.size()) {
                    return false;
                }
                groundItem = groundItemList.get(groundItemId);
            } while (!groundItem.isActive() && (++groundItemId > 0));
            return true;
        }

        @Override
        public GroundItem next() {
            hasNext();
            GroundItem groundItem = groundItemList.get(groundItemId);
            groundItemId++;
            return groundItem;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
