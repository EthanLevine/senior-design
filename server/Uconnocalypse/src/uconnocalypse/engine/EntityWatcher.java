package uconnocalypse.engine;

public class EntityWatcher {
    
    protected void added(Entity e) {}
    
    protected void updated(Entity e) {}
    
    protected void removed(Entity e) {}
    
    protected void collision(Entity collider, Entity collidee) {}
}
