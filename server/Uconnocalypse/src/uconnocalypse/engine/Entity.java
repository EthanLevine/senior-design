package uconnocalypse.engine;

import java.util.ArrayList;
import java.util.List;
import uconnocalypse.data.EntitySpecies;
import uconnocalypse.data.PhysicalType;

public final class Entity implements Box {
    // The ID of this entity, immutable.
    private final int id;
    private boolean isActive;
    // Fields common to all entities
    private double xPos; //rw
    private double yPos; //rw
    private double xVel; //rw
    private double yVel; //rw
    private double angle; //rw
    
    private int curHealth;
    private int curEnergy;
    private double attackRating;
    private double defenseRating;
    
    private EntitySpecies species; //rw
    private final List<EntityWatcher> watchers;
    
    private PhysicalGeometry absoluteGeometry;
    private boolean absoluteGeometryStale = true;

    /******************
     * GETTER METHODS *
     ******************/
    public int getId() {
        return id;
    }
    
    public boolean isActive() { return isActive; }
    
    public EntitySpecies getSpecies() { return species; }
    public double getXPos() { return xPos; }
    public double getYPos() { return yPos; }
    public Point getPos() { return new Point(xPos, yPos); }
    public double getXVel() { return xVel; }
    public double getYVel() { return yVel; }
    public double getAngle() { return angle; }
    public int getHealth() { return curHealth; }
    public int getEnergy() { return curEnergy; }
    public double getAttackRating() { return attackRating; }
    public double getDefenseRating() { return defenseRating; }
    
    protected void setSpecies(EntitySpecies species) {
        this.species = species;
        absoluteGeometryStale = true;
    }
    protected void setXPos(double xPos) {
        this.xPos = xPos;
        absoluteGeometryStale = true;
    }
    protected void setYPos(double yPos) {
        this.yPos = yPos;
        absoluteGeometryStale = true;
    }
    protected void setXVel(double xVel) { this.xVel = xVel; }
    protected void setYVel(double yVel) { this.yVel = yVel; }
    protected void setAngle(double angle) {
        this.angle = angle;
        absoluteGeometryStale = true;
    }
    
    protected void setHealth(int health) { curHealth = health; }
    protected void setEnergy(int energy) { curEnergy = energy; }
    protected void setAttackRating(double attackRating) { this.attackRating = attackRating; }
    protected void setDefenseRating(double defenseRating) { this.defenseRating = defenseRating; }
    
    public void addWatcher(EntityWatcher watcher) {
        watchers.add(watcher);
    }
    
    public void removeWatcher(EntityWatcher watcher) {
        watchers.remove(watcher);
    }
    
    protected void removeAllWatchers() {
        watchers.clear();
    }
    
    private void recalcAbsoluteGeometry() {
        absoluteGeometry = new PhysicalGeometry(species.getGeometry(), new Point(xPos, yPos), angle);
        absoluteGeometryStale = false;
    }
    
    public PhysicalGeometry getAbsoluteGeometry() {
        if (absoluteGeometryStale) recalcAbsoluteGeometry();
        return absoluteGeometry;
    }
    
    @Override
    public double getXMin() {
        if (absoluteGeometryStale) recalcAbsoluteGeometry();
        return absoluteGeometry.getXMin();
    }
    
    @Override
    public double getXMax() {
        if (absoluteGeometryStale) recalcAbsoluteGeometry();
        return absoluteGeometry.getXMax();
    }
    
    @Override
    public double getYMin() {
        if (absoluteGeometryStale) recalcAbsoluteGeometry();
        return absoluteGeometry.getYMin();
    }
    
    @Override
    public double getYMax() {
        if (absoluteGeometryStale) recalcAbsoluteGeometry();
        return absoluteGeometry.getYMax();
    }
    
    /*********************************************
     * PROTECTED METHODS (for this package ONLY) *
     *********************************************/
    protected Entity(int id) {
        this.id = id;
        watchers = new ArrayList<>();
        isActive = false;
    }
    
    protected void deactivate() {
        isActive = false;
    }
    
    protected void activate() {
        isActive = true;
    }
    
    protected void notifyUpdated() {
        for (EntityWatcher watcher : watchers) {
            watcher.updated(this);
        }
    }
    
    protected void notifyRemoved() {
        for (EntityWatcher watcher : watchers) {
            watcher.removed(this);
        }
    }
    
    protected void notifyAdded() {
        for (EntityWatcher watcher : watchers) {
            watcher.added(this);
        }
    }
    
    protected void notifyCollided(Entity other) {
        for (EntityWatcher watcher : watchers) {
            watcher.collision(this, other);
        }
    }
    
    public boolean canAttack(Entity other) {
        // the other entity must have max health
        if (other.getSpecies().getMaxHealth() == 0) {
            return false;
        }
        // a player cannot attack a player
        if (PhysicalType.isPlayer(other.getSpecies().getPhysicalType()) &&
                PhysicalType.isPlayer(species.getPhysicalType())) {
            return false;
        }
        // a monster cannot attack a monster
        if (PhysicalType.isMonster(other.getSpecies().getPhysicalType()) &&
                PhysicalType.isMonster(species.getPhysicalType())) {
            return false;
        }
        // otherwise, return true
        return true;
    }
}
