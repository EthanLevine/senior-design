package uconnocalypse.engine;

import java.util.ArrayList;
import java.util.List;

public abstract class AIBase {
    private final List<Long> targetEntitySpecies;
    private final List<Entity> targetEntities = new ArrayList<>();
    private final EntityWatcher aiWatcher = new EntityWatcher() {
        @Override
        public void collision(Entity a, Entity b) {
            onCollision(a, b);
        }
    };
    private final EntityWatcher managedListWatcher = new EntityWatcher() {
        @Override
        public void removed(Entity e) {
            if (targetEntitySpecies.contains(e.getSpecies().getId())) {
                e.removeWatcher(aiWatcher);
                targetEntities.remove(e);
            }
        }
        
        @Override
        public void added(Entity e) {
            if (targetEntitySpecies.contains(e.getSpecies().getId())) {
                targetEntities.add(e);
                e.addWatcher(aiWatcher);
            }
        }
    };
    
    protected AIBase(List<Long> targetEntitySpecies, EntityStore entityStore) {
        this.targetEntitySpecies = targetEntitySpecies;
        entityStore.addWatcher(managedListWatcher);
    }
    
    protected void update(Entity self) {}
    
    protected void onCollision(Entity self, Entity other) {}
    
    public final void updateAll() {
        for (Entity e : targetEntities) {
            update(e);
        }
    }
}
