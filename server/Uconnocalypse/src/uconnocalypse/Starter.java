package uconnocalypse;

import java.util.List;
import java.util.Properties;
import java.util.TimeZone;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import net.minidev.json.JSONStyle;
import net.minidev.json.JSONValue;
import org.hibernate.Hibernate;
import uconnocalypse.comm.CommPortal;
import uconnocalypse.data.*;
import uconnocalypse.server.GameServer;
import uconnocalypse.webbit.WebbitServer;

/**
 * Responsible for setting up components of the server and launching them.
 */
public class Starter {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        JSONValue.COMPRESSION = JSONStyle.NO_COMPRESS;
        
        Properties props = new Properties();
        
        // Change these properties to specify your server's connection info
        props.setProperty("javax.persistence.jdbc.url", "jdbc:postgresql://teamf.engr.uconn.edu:5432/uconnocalypse");
        props.setProperty("javax.persistence.jdbc.driver", "org.postgresql.Driver");
        props.setProperty("javax.persistence.jdbc.user", "postgres");
        props.setProperty("javax.persistence.jdbc.password", "teamf");
        props.setProperty("hibernate.cache.provider_class", "org.hibernate.cache.NoCacheProvider");
        
        //sorry I changed the settings to suit my server needs, but probably will blow you out since I
        //committed other files.  We can either make all our environments match

        
        
        // Uncomment the next line to automatically populate the database.
        props.setProperty("hibernate.hbm2ddl.auto", "update");
        
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("UconnocalypsePU", props);
        DatabaseManager manager = new DatabaseManager(factory);
        GameServer gameServer = new GameServer(manager);
        CommPortal commPortal = new CommPortal(gameServer, manager);
        WebbitServer ws = new WebbitServer(8080, commPortal);
        
        // Uncomment the next line to run the server.
        ws.run();
    }
}
