package uconnocalypse.comm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import net.minidev.json.*;
import net.minidev.json.parser.*;
import uconnocalypse.data.*;
import uconnocalypse.server.GameInstance;
import uconnocalypse.server.GameServer;

/**
 * Handles all client-to-server communication.  Whenever a new action is defined
 * that a client can call, it should be added to this class.
 * 
 * To handle the communication properly, instances of this class need references
 * to an Authenticator object and a GameServer object, which are passed into the
 * constructor.
 * 
 * Messages in this class are JSON objects.  The "action" entry must always be
 * present, and is used to determine what to do with the packet.
 * 
 * TODO: Create better validation methods for incoming JSON objects.
 */
public class CommPortal {
    private final static int STATE_NO_ACCOUNT = 1;
    private final static int STATE_NO_CHAR = 2;
    private final static int STATE_NO_INSTANCE = 3;
    private final static int STATE_FULL = 4;
    
    private final GameServer gameServer;
    private final DatabaseManager databaseManager;
    
    public CommPortal(GameServer gameServer, DatabaseManager databaseManager) {
        this.gameServer = gameServer;
        this.databaseManager = databaseManager;
    }

    public void HandleMessage(String message, GameConnection connection) {
        try {
            JSONParser parser = new JSONParser(JSONParser.MODE_PERMISSIVE);
            List messageObjs = (JSONArray)parser.parse(message);
            String action = (String)messageObjs.get(0);
            long receivedTime = System.currentTimeMillis();
            connection.acquireLock();
            /*
             * Message handlers go here.  Actions that require the connection to
             * be in a certain state should check that for themselves.  Actions
             * should be listed in alphabetical order for ease of development.
             * Actions do NOT need to have a trailing return.
             */
            switch (action) {
                case "auth.authenticate":
                    authAuthenticate(connection, messageObjs);
                    break;
                case "auth.create-account":
                    authCreateAccount(connection, messageObjs);
                    break;
                case "char.create":
                    charCreate(connection, messageObjs);
                    break;
                case "char.list":
                    charList(connection, messageObjs);
                    break;
                case "char.list-tasks":
                    charListTasks(connection, messageObjs);
                    break;
                case "char.select":
                    charSelect(connection, messageObjs);
                    break;
                case "char.deselect":
                    charDeselect(connection, messageObjs);
                    break;
                case "char.delete":
                    charDelete(connection, messageObjs);
                    break;
                case "instance.list":
                    instanceList(connection, messageObjs);
                    break;
                case "instance.create":
                    instanceCreate(connection, messageObjs);
                    break;
                case "instance.join":
                    instanceJoin(connection, messageObjs);
                    break;
                case "logout":
                    logout(connection);
                    break;
                case "player.set-motion":
                    playerSetMotion(connection, messageObjs);
                    break;
                case "player.set-angle":
                    playerSetAngle(connection, messageObjs);
                    break;
                case "player.say":
                    playerSay(connection, messageObjs);
                    break;
                case "player.start-attack":
                    playerStartAttack(connection, messageObjs);
                    break;
                case "player.stop-attack":
                    playerStopAttack(connection, messageObjs);
                    break;
                case "network.time-request":
                    networkTimeRequest(connection, messageObjs, receivedTime);
                    break;
                case "inventory.pickup":
                    inventoryPickup(connection, messageObjs);
                    break;
                case "inventory.drop":
                    inventoryDrop(connection, messageObjs);
                    break;
                case "inventory.open":
                    inventoryOpen(connection, messageObjs);
                    break;
                case "inventory.close":
                    inventoryClose(connection, messageObjs);
                    break;
                case "inventory.equip":
                    inventoryEquip(connection, messageObjs);
                    break;
                case "inventory.unequip":
                    inventoryUnequip(connection, messageObjs);
                    break;
                case "weapon":
                    TEMP_weapon(connection, messageObjs);
                    break;
                case "time.pong":
                    timePong(connection, messageObjs);
                    break;
                /*case "stash.open":
                    break;
                case "stash.add":
                    break;
                case "stash.remove":
                    break;
                case "stash.drop":
                    break;
                case "stash.close":
                    break;
                 // Shops eventually implemented as well*/
                /*case "item.template-info":
                    itemTemplateInfo(connection, messageObjs);*/
                case "engine.remove-entity-confirm":
                    engineRemoveEntityConfirm(connection, messageObjs);
                    break;
                default:
                    connection.getSender().sendError(StandardErrors.INVALID_ACTION, "Unknown action.");
                    break;
            }
        } catch (ClassCastException e) {
            connection.getSender().sendError(StandardErrors.JSON_BAD_TYPE, "JSON message had incompatible type.");
        } catch (ParseException e) {
            connection.getSender().sendError(StandardErrors.JSON_BAD_FORMAT, "JSON message was malformed.");
        } catch (IndexOutOfBoundsException e) {
            connection.getSender().sendError(StandardErrors.JSON_TOO_FEW_ARGS, "JSON message did not have enough arguments.");
        } finally {
            connection.releaseLock();
        }
    }

    /**
     * Handles the message "auth.authenticate" by checking the username and password
     * against the database.
     * @param connection The connection that the message came from
     * @param messageObjs The message objects contain 1:username and 2:password
     */
    private void authAuthenticate(GameConnection connection, List messageObjs) {
        if (!ensureState(connection, STATE_NO_ACCOUNT, STATE_NO_ACCOUNT)) return;
        String username = (String)messageObjs.get(1);
        String password = (String)messageObjs.get(2);
        // try to authAuthenticate
        Account account = databaseManager.authenticateUser(username, password);
        if (account == null) {
            connection.getSender().sendError(StandardErrors.AUTH_FAILED_AUTHENTICATE, "Wrong username or password.");
        } else {
            connection.assignServer(gameServer);
            connection.assignAccount(account);
            gameServer.addConnection(connection);
            connection.getSender().sendConfirm((String)messageObjs.get(0));
        }
    }

    /**
     * Handles the message "auth.create-account" by checking the username is available
     * and if it is then creates the account using the given username and password.
     * @param connection The connection that the message came from
     * @param messageObjs The message objects contain 1:username and 2:password
     */
    private void authCreateAccount(GameConnection connection, List messageObjs) {
        if (!ensureState(connection, STATE_NO_ACCOUNT, STATE_NO_ACCOUNT)) return;
        String username = (String)messageObjs.get(1);
        String password = (String)messageObjs.get(2);
        if (username.length() < 4 || username.length() > 20) {
            connection.getSender().sendError(StandardErrors.AUTH_USERNAME_INVALID, "Username is invalid.");
            return;
        }
        // Check to see if the username is taken
        if (databaseManager.isAccountNameTaken(username)) {
            connection.getSender().sendError(StandardErrors.AUTH_DUPLICATE_USERNAME, "Username already taken.");
            return;
        }
        Account account = databaseManager.createNewUser(username, password);
        if (account == null) {
            connection.getSender().sendError(StandardErrors.UNKNOWN_FAILURE, "Account creation failed.");
        } else {
            connection.assignServer(gameServer);
            connection.assignAccount(account);
            gameServer.addConnection(connection);
            connection.getSender().sendConfirm((String)messageObjs.get(0));
        }
    }

    /**
     * Handles the message "char.create" by creating a character using
     * the given character name and type.
     * @param connection The connection that the message came from
     * @param messageObjs The message objects contain 1:character name and 2:character type
     */
    private void charCreate(GameConnection connection, List messageObjs) {
        if (!ensureState(connection, STATE_NO_CHAR, STATE_NO_CHAR)) return;
        String characterName = (String)messageObjs.get(1);
        if(characterName.length() < 4 || characterName.length() > 20) {
            connection.getSender().sendError(StandardErrors.CHAR_NAME_INVALID, "Requested character name is not valid.");
            return;
        }
        int characterType = ((Number)messageObjs.get(2)).intValue();
        if (!CharacterType.isValid(characterType)) {
            connection.getSender().sendError(StandardErrors.CHAR_INVALID_TYPE, "Requested character type is not valid.");
            return;
        }
        if (databaseManager.isCharacterNameTaken(characterName)) {
            connection.getSender().sendError(StandardErrors.CHAR_DUPLICATE_NAME, "Character name already taken.");
            return;
        }
        PlayerCharacter character = databaseManager.createCharacter(connection.getAccount().getId(), characterName, characterType);
        if (character == null) {
            connection.getSender().sendError(StandardErrors.UNKNOWN_FAILURE, "Character creation failed.");
        } else {
            connection.assignCharacter(character);
            connection.getSender().sendConfirm((String)messageObjs.get(0));
        }
    }

    /**
     * Handles the message "char.list" by sending a list of the characters (names)
     * associated with the active connection's account.
     * @param connection The connection that the message came from
     */
    private void charList(GameConnection connection, List messageObjs) {
        if (!ensureState(connection, STATE_NO_CHAR, STATE_FULL)) return;
        // note: no additional data required.
        // For now, just send a list of character names.
        List confirmData = new ArrayList();
        for (PlayerCharacter character : databaseManager.getCharacterList(connection.getAccount().getId())) {
            confirmData.add(Arrays.asList(character.getId(), character.getName(), character.getCharType(), character.getLevel()));
        }
        connection.getSender().sendConfirm((String)messageObjs.get(0), confirmData);
    }

    /**
     * Handles the message "char.select" by assigning the player with their character
     * they have selected according to the characterId.
     * @param connection The connection that the message came from
     * @param messageObjs The message objects contain characterId
     */
    private void charSelect(GameConnection connection, List messageObjs) {
        if (!ensureState(connection, STATE_NO_CHAR, STATE_NO_CHAR)) return;
        long characterId = ((Number)messageObjs.get(1)).longValue();
        // try to select it
        PlayerCharacter character = databaseManager.selectCharacter(connection.getAccount().getId(), characterId);
        if (character == null) {
            connection.getSender().sendError(StandardErrors.CHAR_ID_NOT_FOUND, "Character ID not found on this account.");
        } else {
            connection.assignCharacter(character);
            connection.getSender().sendConfirm((String)messageObjs.get(0));
        }
    }

    /**
     * Handles the message "char.deselect" by deselecting the accounts currently
     * active character.
     * @param connection The connection that the message came from
     */
    private void charDeselect(GameConnection connection, List messageObjs) {
        if (!ensureState(connection, STATE_NO_INSTANCE, STATE_NO_INSTANCE)) return;
        connection.assignCharacter(null);
        connection.getSender().sendConfirm((String)messageObjs.get(0));
    }

    private void charDelete(GameConnection connection, List messageObjs) {
        if (!ensureState(connection, STATE_NO_CHAR, STATE_NO_CHAR)) return;
        long characterId = ((Number)messageObjs.get(1)).longValue();
        boolean owner = databaseManager.accountOwnsCharacter(connection.getAccount().getId(), characterId);
        if (owner) {
            databaseManager.deleteCharacter(characterId);
            connection.getSender().sendConfirm((String)messageObjs.get(0));
        } else {
            connection.getSender().sendError(StandardErrors.CHAR_ID_NOT_FOUND, "This account doesn't own a character with the given id.");
        }
    }
    
    private void charListTasks(GameConnection connection, List messageObjs) {
        if (!ensureState(connection, STATE_NO_INSTANCE, STATE_FULL)) return;
        Set<Long> availableTaskIds = databaseManager.getAvailableTaskIds(connection.getAccount().getId());
        List taskList = new ArrayList();
        for (Task task : databaseManager.getAllTasks()) {
            boolean available = availableTaskIds.contains(task.getId());
            taskList.add(Arrays.asList(task.getId(), task.getName(), available));
        }
        connection.getSender().sendConfirm((String)messageObjs.get(0), taskList);
    }
    
    private void engineRemoveEntityConfirm(GameConnection connection, List messageObjs) {
        if (!ensureState(connection, STATE_FULL, STATE_FULL)) return;
        connection.getMapEngine().confirmEntityDelete(connection);
        connection.getSender().sendConfirm((String)messageObjs.get(0));
    }

    /**
     * Handles the message "instance.list" by returning to the connection with
     * a list of all the active instances.
     * @param connection The connection that the message came from
     */
    private void instanceList(GameConnection connection, List messageObjs) {
        if (!ensureState(connection, STATE_NO_INSTANCE, STATE_FULL)) return;
        // note: no additional data required.
        List confirmData = new ArrayList();
        for (GameInstance instance : gameServer.getInstanceList()) {
            confirmData.add(Arrays.asList(instance.getId(),
                    instance.getName(),
                    instance.getPassword().equals(""),
                    instance.getPlayerCount()));
        }
        connection.getSender().sendConfirm((String)messageObjs.get(0), confirmData);
    }

    /**
     * Handles the message "instance.create" by creating a new instance of the game
     * using the given name and optional password.
     * @param connection The connection that the message came from
     * @param messageObjs The message objects contain 1:instance name and 2:instance password
     */
    private void instanceCreate(GameConnection connection, List messageObjs) {
        if (!ensureState(connection, STATE_NO_INSTANCE, STATE_NO_INSTANCE)) return;
        String instanceName = (String)messageObjs.get(1);
        String instancePassword = (String)messageObjs.get(2);
        if(instanceName.length() < 4 || instanceName.length() > 30) {
            connection.getSender().sendError(StandardErrors.INSTANCE_NAME_INVALID, "Instance name is invalid.");
            return;
        }
        long taskId = ((Number)messageObjs.get(3)).longValue();
        if (!databaseManager.getAvailableTaskIds(connection.getAccount().getId()).contains(taskId)) {
            connection.getSender().sendError(StandardErrors.INSTANCE_TASK_NOT_AVAILABLE, "The requested task is not available on this account.");
        }
        GameInstance instance = gameServer.createInstance(instanceName, instancePassword, databaseManager.getTask(taskId));
        connection.assignInstance(instance);
        instance.addConnection(connection);
        connection.getSender().sendConfirm((String)messageObjs.get(0));
    }

    /**
     * Handles the message "instance.join" by assigning this connection to the requested
     * instance (by Id).
     * @param connection The connection that the message came from
     * @param messageObjs The message objects contain 1:instance Id and 2:instance password
     */
    private void instanceJoin(GameConnection connection, List messageObjs) {
        if (!ensureState(connection, STATE_NO_INSTANCE, STATE_NO_INSTANCE)) return;
        long instanceId = ((Number)messageObjs.get(1)).longValue();
        GameInstance instance = gameServer.getInstance(instanceId);
        if (instance == null) {
            connection.getSender().sendError(StandardErrors.INSTANCE_ID_NOT_FOUND, "Instance ID not found on the server.");
            return;
        }
        //If the instance isn't public we require a password
        String instancePassword = "";
        if (!instance.getPassword().equals("")) instancePassword = (String) messageObjs.get(2);
        if (!instance.getPassword().equals(instancePassword)) {
            connection.getSender().sendError(StandardErrors.INSTANCE_WRONG_PASSWORD, "Password for instance was incorrect.");
        } else if (instance.getPlayerCount() >= 4) {
            connection.getSender().sendError(StandardErrors.INSTANCE_FULL, "This instance is full.");
        } else {
            connection.assignInstance(instance);
            instance.addConnection(connection);
            connection.getSender().sendConfirm((String)messageObjs.get(0));
        }
    }

    /**
     * Handles the message "logout" by closing the socket with the client.
     * @param connection The connection that the message came from
     */
    private void logout(GameConnection connection) {
        // Remove the connection from the GameInstance and close the socket there.
        // This will be separated eventually as a player might want to leave an
        // instance and join another without logging out.
        connection.getInstance().removeConnection(connection);
        //connection.getSender().closeSocket();
    }
    
    private void playerSetMotion(GameConnection connection, List messageObjs) {
        if (!ensureState(connection, STATE_FULL, STATE_FULL)) return;
        String motionDirection = (String)messageObjs.get(1);
        long clientTime = ((Number)messageObjs.get(2)).longValue();
        long serverTime = connection.getSyncManager().registerAction(clientTime);
        connection.getMapEngine().setPlayerMotion(connection, motionDirection, serverTime);
        connection.getSender().sendConfirm((String)messageObjs.get(0));
    }

    private void playerSetAngle(GameConnection connection, List messageObjs) {
        if (!ensureState(connection, STATE_FULL, STATE_FULL)) return;
        connection.getMapEngine().setPlayerAngle(connection, ((Number)messageObjs.get(1)).doubleValue());
        connection.getSender().sendConfirm((String)messageObjs.get(0));
    }

    private void playerSay(GameConnection connection, List messageObjs) {
        if (!ensureState(connection, STATE_FULL, STATE_FULL)) return;
        String message = (String)messageObjs.get(1);
        connection.getInstance().sendChat(connection.getCharacter().getName() + ": " + message.trim());
        connection.getSender().sendConfirm((String)messageObjs.get(0));
    }

    private void playerStartAttack(GameConnection connection, List messageObjs) {
        connection.getMapEngine().startAttacking(connection);
        connection.getSender().sendConfirm((String)messageObjs.get(0));
    }

    private void playerStopAttack(GameConnection connection, List messageObjs) {
        connection.getMapEngine().stopAttacking(connection);
        connection.getSender().sendConfirm((String)messageObjs.get(0));
    }
    
    private void TEMP_weapon(GameConnection connection, List messageObjs) {
        int weaponNumber = ((Number)messageObjs.get(1)).intValue();
        switch (weaponNumber) {
            case 1:
                // pistol
                connection.getMapEngine().TEMP_equipWeapon(connection, Item.PISTOL_WEAPON_ID);
                break;
            case 2:
                // shotgun
                connection.getMapEngine().TEMP_equipWeapon(connection, Item.SHOTGUN_WEAPON_ID);
                break;
            case 3:
                // sword
                connection.getMapEngine().TEMP_equipWeapon(connection, Item.SWORD_WEAPON_ID);
                break;
        }
        connection.getSender().sendConfirm((String)messageObjs.get(0));
    }

    private void networkTimeRequest(GameConnection connection, List messageObjs, long receivedTime) {
        List confirmData = new ArrayList();
        confirmData.add(receivedTime);
        confirmData.add(System.currentTimeMillis());
        connection.getSender().sendConfirm((String)messageObjs.get(0), confirmData);
    }
    
    private void timePong(GameConnection connection, List messageObjs) {
        connection.getSyncManager().pong(((Number)messageObjs.get(1)).longValue());
        connection.getSender().sendConfirm((String)messageObjs.get(0));
    }

    private void inventoryPickup(GameConnection connection, List messageObjs) {
        int groundItemId = ((Number)messageObjs.get(1)).intValue();
        connection.getMapEngine().pickupItem(connection, groundItemId);
    }

    private void inventoryDrop(GameConnection connection, List messageObjs) {
        long stashedItemId = ((Number)messageObjs.get(1)).longValue();
        if(!connection.getCharacter().isInventoryOpen()) {
            connection.getSender().sendError(StandardErrors.INVENTORY_STATE_INVALID, "Cannot drop inventory items while inventory is closed.");
        }
        connection.getMapEngine().dropInventoryItem(connection, databaseManager.getStashedItem(stashedItemId));
    }

    private void inventoryOpen(GameConnection connection, List messageObjs) {
        connection.getCharacter().setInventoryOpen(true);
        List confirmData = new ArrayList();
        for (StashedItem stashedItem : connection.getCharacter().getInventory().getStashedItems()) {
            confirmData.add(Arrays.asList(stashedItem.getId(),
                    stashedItem.getItem().getName(),
                    stashedItem.getItem().getItemTemplate().getId(),
                    stashedItem.getItem().getItemQuality(),
                    stashedItem.getSlotX(),
                    stashedItem.getSlotY()));
        }
        Item weapon = connection.getCharacter().getEquippedWeapon();
        if (weapon != null) {
            confirmData.add(Arrays.asList(weapon.getId(),
                    weapon.getName(),
                    weapon.getItemTemplate().getId(),
                    weapon.getItemQuality(),
                    GameConstants.INVENTORY_WIDTH+1, 1));
        }
        Item armor = connection.getCharacter().getEquippedArmor();
        if (armor != null) {
            confirmData.add(Arrays.asList(armor.getId(),
                    armor.getName(),
                    armor.getItemTemplate().getId(),
                    armor.getItemQuality(),
                    GameConstants.INVENTORY_WIDTH+1, 2));
        }
        connection.getSender().sendConfirm((String)messageObjs.get(0), confirmData);
    }

    private void inventoryClose(GameConnection connection, List messageObjs) {
        connection.getCharacter().setInventoryOpen(false);
        connection.getSender().sendConfirm((String)messageObjs.get(0));
    }

    private void inventoryEquip(GameConnection connection, List messageObjs) {
        long stashedItemId = ((Number)messageObjs.get(1)).longValue();
        connection.getMapEngine().equipItem(connection, databaseManager.getStashedItem(stashedItemId));
    }

    private void inventoryUnequip(GameConnection connection, List messageObjs) {
        long itemId = ((Number)messageObjs.get(1)).longValue();
        connection.getMapEngine().unequipItem(connection, databaseManager.getItem(itemId));
    }

    /*private void itemTemplateInfo(GameConnection connection, List messageObjs) {
        long itemTemplateId = ((Number)messageObjs.get(1)).longValue();
        ItemTemplate itemTemplate = databaseManager.getItemTemplate(itemTemplateId);
        if(itemTemplate == null) {
            connection.getSender().sendError(StandardErrors.ITEM_DOESNT_EXIST, "Item template requested doesn't exist.");
            return;
        }
        connection.getSender().sendConfirm((String)messageObjs.get(0), itemTemplate.getUrl());
    }*/

    /**
     * General-purpose function to ensure a connection has a valid state for a
     * given action.  States are STATE_NO_ACCOUNT, STATE_NO_CHAR,
     * STATE_NO_INSTANCE, and STATE_FULL.  States have an ordering (the same
     * order as above), so that STATE_NO_ACCOUNT is the "least" state and
     * STATE_FULL is the "most" state.  This function returns true if the
     * connection's state is "between" minState and maxState, inclusive.
     * Otherwise, this function sends an error and returns false.
     * 
     * The result of this function should be used for control flow to exit an
     * action handler early if the state is invalid.
     * 
     * @param connection the connection whose state to check
     * @param minState the minimum allowed state
     * @param maxState the maximum allowed state
     * @return true if connection's state is in-between minState and maxState,
     * false otherwise.
     */
    private static boolean ensureState(GameConnection connection, int minState, int maxState) {
        switch (minState) {
            case STATE_NO_ACCOUNT:
                break;
            case STATE_NO_CHAR:
                if (connection.getAccount() == null) {
                    connection.getSender().sendError(StandardErrors.INVALID_ACTION, "Action requires an authenticated connection, but the connection is not authenticated.");
                    return false;
                }
                break;
            case STATE_NO_INSTANCE:
                if (connection.getCharacter() == null) {
                    connection.getSender().sendError(StandardErrors.INVALID_ACTION, "Action requires a selected character, but the connection has none.");
                    return false;
                }
                break;
            case STATE_FULL:
                if (connection.getInstance() == null) {
                    connection.getSender().sendError(StandardErrors.INVALID_ACTION, "Action requires a game instance, but the connection is not bound to one.");
                    return false;
                }
                break;
            default:
                // TODO: Better exception here.
                throw new RuntimeException("Invalid value for minState.");
        }
        
        switch (maxState) {
            case STATE_NO_ACCOUNT:
                if (connection.getAccount() != null) {
                    connection.getSender().sendError(StandardErrors.INVALID_ACTION, "Action requires an unauthenticated connection, but the connection is authenticated.");
                    return false;
                }
                break;
            case STATE_NO_CHAR:
                if (connection.getCharacter() != null) {
                    connection.getSender().sendError(StandardErrors.INVALID_ACTION, "Action requires no selected character, but the connection has one.");
                    return false;
                }
                break;
            case STATE_NO_INSTANCE:
                if (connection.getInstance() != null) {
                    connection.getSender().sendError(StandardErrors.INVALID_ACTION, "Action requires no game instance, but the connection is bound to one.");
                    return false;
                }
                break;
            case STATE_FULL:
                break;
            default:
                // TODO: Better exception here
                throw new RuntimeException("Invalid value for maxState.");
        }
        
        return true;
    }
}
