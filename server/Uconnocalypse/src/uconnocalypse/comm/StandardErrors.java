package uconnocalypse.comm;

public class StandardErrors {
    private StandardErrors() {}
    
    public static final int UNKNOWN_FAILURE             =    1;
    public static final int INVALID_ACTION              =    2;
    public static final int WEBSOCKET_ERROR             =    3; // note: not used in server.
    
    public static final int JSON_BAD_FORMAT             =   11;
    public static final int JSON_BAD_TYPE               =   12;
    public static final int JSON_TOO_FEW_ARGS           =   13;
    
    public static final int AUTH_FAILED_AUTHENTICATE    =  101;
    public static final int AUTH_DUPLICATE_USERNAME     =  102;
    public static final int AUTH_USERNAME_INVALID       =  103;
    
    public static final int CHAR_DUPLICATE_NAME         =  111;
    public static final int CHAR_INVALID_TYPE           =  112;
    public static final int CHAR_ID_NOT_FOUND           =  113;
    public static final int CHAR_NAME_INVALID           =  114;
    
    public static final int INSTANCE_ID_NOT_FOUND       =  121;
    public static final int INSTANCE_WRONG_PASSWORD     =  122;
    public static final int INSTANCE_FULL               =  123;
    public static final int INSTANCE_TASK_NOT_AVAILABLE =  124;
    public static final int INSTANCE_NAME_INVALID       =  125;

    public static final int ITEM_PICKUP_TOO_FAR         =  140;
    public static final int ITEM_DOESNT_EXIST           =  141;
    public static final int INVENTORY_FULL              =  142;
    public static final int INVENTORY_STATE_INVALID     =  143;
}
