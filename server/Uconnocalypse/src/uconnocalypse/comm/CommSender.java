package uconnocalypse.comm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import net.minidev.json.JSONArray;
import org.webbitserver.WebSocketConnection;

/**
 * Handles all server-to-client communication.  Whenever a new action is defined
 * that the server sends to the client, it should be implemented in this class.
 * 
 * A new instance of this class is created with every client connection (or to
 * be specific, every NettyServerHandler).
 * 
 * Instances of this class should generally be found in GameConnection objects.
 * 
 * Messages are formatted as JSON objects, for easy consumption in the
 * JavaScript client.
 */
public class CommSender {
    private final WebSocketConnection connection;
    private boolean connectionOpen = true;
    
    public CommSender(WebSocketConnection connection) {
        this.connection = connection;
    }
    
    //////////////////////////////////////////////////////////
    // Public methods to reply messages back to the client. //
    //////////////////////////////////////////////////////////
    public void sendError(int errorCode, String errorMessage) {
        if (connectionOpen)
            connection.send(JSONArray.toJSONString(Arrays.asList("error", errorCode, errorMessage)));
    }

    public void sendError(int errorCode, String errorMessage, Object... extraData) {
        List<Object> objects = new ArrayList<>(Arrays.asList(extraData));
        objects.add(0, "error");
        objects.add(1, errorCode);
        objects.add(2, errorMessage);
        if (connectionOpen)
            connection.send(JSONArray.toJSONString(objects));
    }

    public void sendConfirm(String confirmedAction) {
        if (connectionOpen)
            connection.send(JSONArray.toJSONString(Arrays.asList("confirm", confirmedAction)));
    }

    public void sendConfirm(String confirmedAction, Object... extraData) {
        List<Object> objects = new ArrayList<>(Arrays.asList(extraData));
        objects.add(0, "confirm");
        objects.add(1, confirmedAction);
        if (connectionOpen)
            connection.send(JSONArray.toJSONString(objects));
    }
    
    ////////////////////////////////////////////////////
    // Public methods to send messages to the client. //
    ////////////////////////////////////////////////////
    public void sendMessage(Object... data) {
        if (connectionOpen)
            connection.send(JSONArray.toJSONString(Arrays.asList(data)));
    }
    
    protected void closeSocket() {
        connectionOpen = false;
        connection.close();
    }
}
