package uconnocalypse.comm;

import org.webbitserver.WebSocketConnection;
import uconnocalypse.data.Account;
import uconnocalypse.data.PlayerCharacter;
import uconnocalypse.engine.MapEngine;
import uconnocalypse.engine.SyncManager;
import uconnocalypse.server.GameInstance;
import uconnocalypse.server.GameServer;

import java.util.concurrent.Semaphore;

/**
 * Represents an authenticated connection with a client.  Instances of this
 * class contain a CommSender object which is used to send messages back to the
 * client.  This class also includes a server-unique ID which is used to
 * identify the remote client, as well as information about the authenticated
 * user.
 */
public class GameConnection {
    private final CommSender sender;
    private final Semaphore connectionLock = new Semaphore(1, true);
    private final SyncManager syncManager = new SyncManager();
    private GameServer server;
    private Account account;
    private PlayerCharacter selectedChar;
    private GameInstance instance;
    private MapEngine mapEngine;
    
    public GameConnection(WebSocketConnection connection) {
        this.sender = new CommSender(connection);
    }
    
    protected void assignServer(GameServer server) {
        this.server = server;
    }
    
    protected void assignAccount(Account account) {
        this.account = account;
    }
    
    protected void assignCharacter(PlayerCharacter character) {
        this.selectedChar = character;
    }
    
    protected void assignInstance(GameInstance instance) {
        this.instance = instance;
    }
    
    public void refreshMapEngine() {
        mapEngine = instance.getMapEngine(this);
    }
    
    public Account getAccount() {
        return account;
    }
    
    public PlayerCharacter getCharacter() {
        return selectedChar;
    }
    
    public GameInstance getInstance() {
        return instance;
    }
    
    public MapEngine getMapEngine() {
        return mapEngine;
    }
    
    public CommSender getSender() {
        return sender;
    }
    
    public SyncManager getSyncManager() {
        return syncManager;
    }
    
    public void acquireLock() {
        connectionLock.acquireUninterruptibly();
    }
    
    public void releaseLock() {
        connectionLock.release();
    }

    public void deactivate() {
        acquireLock();
        sender.closeSocket();
        if (server != null)
            server.removeConnection(this);
    }
}
