package uconnocalypse.webbit;

import java.util.concurrent.ConcurrentHashMap;
import org.webbitserver.BaseWebSocketHandler;
import org.webbitserver.WebSocketConnection;
import uconnocalypse.comm.CommPortal;
import uconnocalypse.comm.GameConnection;

public final class WebbitHandler extends BaseWebSocketHandler {
    private final ConcurrentHashMap<WebSocketConnection, GameConnection> connectionMap;
    private final CommPortal commPortal;
    
    public WebbitHandler(CommPortal commPortal) {
        this.commPortal = commPortal;
        connectionMap = new ConcurrentHashMap<>();
    }
    
    @Override
    public void onOpen(WebSocketConnection connection) {
        GameConnection gameConnection = new GameConnection(connection);
        connectionMap.put(connection, gameConnection);
        gameConnection.getSender().sendMessage("time.ping");
        gameConnection.getSyncManager().ping();
    }
    
    @Override
    public void onClose(WebSocketConnection connection) {
        connectionMap.get(connection).deactivate();
        connectionMap.remove(connection);
    }
    
    @Override
    public void onMessage(WebSocketConnection connection, String message) {
        commPortal.HandleMessage(message, connectionMap.get(connection));
    }
}
