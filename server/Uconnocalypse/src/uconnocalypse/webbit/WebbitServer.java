package uconnocalypse.webbit;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import org.webbitserver.WebServer;
import org.webbitserver.WebServers;
import uconnocalypse.comm.CommPortal;

public final class WebbitServer {
    private static final String WEBSOCKET_PATH = "/websocket";
    
    private final WebServer webServer;
    
    public WebbitServer(int port, CommPortal commPortal) {
        webServer = WebServers.createWebServer(Executors.newCachedThreadPool(), port)
                .add(WEBSOCKET_PATH, new WebbitHandler(commPortal));
    }
    
    public void run() {
        try {
            webServer.start().get();
        } catch (InterruptedException e) {
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }
}
