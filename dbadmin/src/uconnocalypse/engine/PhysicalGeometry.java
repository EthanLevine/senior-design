package uconnocalypse.engine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class PhysicalGeometry {
    
    private final List<Point> points;
    private final Point center;
    private final double radius;
    
    public PhysicalGeometry(String geometry) {
        /*
         * Acceptable forms of the geometry string:
         * <x>,<y>;<radius>
         * <x1>,<y1>;<x2>,<y2>;...
         */
        String[] doubleStrs = geometry.split("[,;]");
        if (doubleStrs.length == 3) {
            center = new Point(Double.parseDouble(doubleStrs[0]),
                    Double.parseDouble(doubleStrs[1]));
            radius = Double.parseDouble(doubleStrs[2]);
            points = null;
        } else if (doubleStrs.length % 2 == 0) {
            List<Point> pointList = new ArrayList<>(doubleStrs.length / 2);
            for (int i = 0; i < doubleStrs.length; i += 2) {
                pointList.add(new Point(Double.parseDouble(doubleStrs[i]),
                        Double.parseDouble(doubleStrs[i+1])));
            }
            points = Collections.unmodifiableList(pointList);
            center = null;
            radius = 0;
        } else {
            // TODO: Better exception here.
            throw new RuntimeException("Bad geometry string.");
        }
    }
    
    public List<Point> getPoints() {
        return points;
    }
    
    public Point getCenter() {
        return center;
    }
    
    public double getRadius() { 
        return radius;
    }
    
    public boolean isCircular() { 
        return (radius > 0) && (center != null);
    }
    
    public boolean isPolygonal() {
        return (points != null);
    }
}
