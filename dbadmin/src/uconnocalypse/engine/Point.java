package uconnocalypse.engine;

public final class Point {
    public final double x;
    public final double y;
    
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }
    
    public Point add(Point other) {
        return new Point(x + other.x, y + other.y);
    }
    
    public Point subtract(Point other) {
        return new Point(x - other.x, y - other.y);
    }
    
    public Point scale(double scale) {
        return new Point(x*scale, y*scale);
    }
    
    public Point rotate(double angle) {
        // note: angle in clockwise degrees
        double rad = Math.toRadians(angle);
        return new Point(x * Math.cos(rad) + y * Math.sin(rad),
                -x * Math.sin(rad) + y * Math.cos(rad));
    }
}
