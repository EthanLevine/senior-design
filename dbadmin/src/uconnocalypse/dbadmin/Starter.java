package uconnocalypse.dbadmin;

import uconnocalypse.data.DatabaseManager;
import uconnocalypse.dbadmin.gui.DBFrame;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Properties;
import java.util.TimeZone;

/**
 * @author Kevin
 */
public class Starter {
    public static DatabaseManager manager;
    public static void main(String[] args) {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));

        Properties props = new Properties();

        // Change these properties to specify the server's connection info
        props.setProperty("javax.persistence.jdbc.url", "jdbc:postgresql://localhost:5432/uconnocalypse");
        props.setProperty("javax.persistence.jdbc.driver", "org.postgresql.Driver");
        props.setProperty("javax.persistence.jdbc.user", "dbadmin");
        props.setProperty("javax.persistence.jdbc.password", "password");
        props.setProperty("hibernate.cache.provider_class", "org.hibernate.cache.NoCacheProvider");
        // Uncomment the next line to automatically populate the database.
        //props.setProperty("hibernate.hbm2ddl.auto", "update");

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("UconnocalypsePU", props);
        manager = new DatabaseManager(factory);
        DBFrame app = new DBFrame();
    }
}
