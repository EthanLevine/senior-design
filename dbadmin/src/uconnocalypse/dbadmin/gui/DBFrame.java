package uconnocalypse.dbadmin.gui;

import uconnocalypse.dbadmin.gui.itemtab.ItemTemplateTab;
import uconnocalypse.dbadmin.gui.maptab.MapTab;
import uconnocalypse.dbadmin.gui.spawntab.SpawnTab;

import javax.swing.*;
import java.awt.*;

/**
 *
 * @author Kevin
 */
public class DBFrame extends JFrame {

    private MapTab mapTab;
    private SpawnTab monsterSpawnTab;
    private ItemTemplateTab itemTemplateTab;

    public DBFrame() {
        super("UConnocalypse DB Admin");
        init();
    }

    private void init() {
        initComponents();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(1000, 700));
        //setExtendedState(MAXIMIZED_BOTH);
        setResizable(true);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }
    private void initComponents(){
        DBMenuBar menuBar = new DBMenuBar(this);
        setJMenuBar(menuBar);

        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        mapTab = new MapTab();
        tabbedPane.addTab("Map Regions", mapTab);
        monsterSpawnTab = new SpawnTab();
        tabbedPane.addTab("Monster Spawns", monsterSpawnTab);
        itemTemplateTab = new ItemTemplateTab();
        tabbedPane.addTab("Item Templates", itemTemplateTab);

        getContentPane().add(tabbedPane);
    }
}
