package uconnocalypse.dbadmin.gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

/**
 * @author Kevin
 */
public class DBMenuBar extends JMenuBar {
    private final DBFrame frame;

    //File Menu
    private JMenu file;
    private JMenuItem importMap;
    private JMenuItem exit;

    public DBMenuBar(DBFrame frame) {
        super();
        this.frame = frame;

        initFileMenu();
    }

    private void initFileMenu() {
        file = new JMenu("File");
        file.setMnemonic(KeyEvent.VK_F);

        exit = new JMenuItem("Exit");
        exit.setMnemonic(KeyEvent.VK_X);
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        //exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.ALT_MASK));
        file.add(exit);

        add(file);
    }
}
