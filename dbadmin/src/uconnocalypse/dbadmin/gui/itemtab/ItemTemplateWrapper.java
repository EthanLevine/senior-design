package uconnocalypse.dbadmin.gui.itemtab;

import uconnocalypse.data.*;
import uconnocalypse.dbadmin.Starter;

/**
 * @author Kevin
 */
public class ItemTemplateWrapper {
    private ItemTemplate itemTemplate;

    public ItemTemplateWrapper() {
        this.itemTemplate = Starter.manager.createNewItemTemplate();
        id = itemTemplate.getId();
        name = itemTemplate.getName();
        itemLevel = itemTemplate.getItemLevel();
        equipSlot = itemTemplate.getEquipSlot();
        reqLevel = itemTemplate.getReqLevel();
        baseItemQuality = itemTemplate.getBaseItemQuality();
        /*strength = itemTemplate.getAttributeSet().getStrength();
        dexterity = itemTemplate.getAttributeSet().getDexterity();
        fortitude = itemTemplate.getAttributeSet().getFortitude();*/
        maximumMods = itemTemplate.getMaximumMods();
        minimumMods = itemTemplate.getMinimumMods();
    }

    public ItemTemplateWrapper(ItemTemplate itemTemplate) {
        this.itemTemplate = itemTemplate;
        id = itemTemplate.getId();
        name = itemTemplate.getName();
        itemLevel = itemTemplate.getItemLevel();
        equipSlot = itemTemplate.getEquipSlot();
        reqLevel = itemTemplate.getReqLevel();
        baseItemQuality = itemTemplate.getBaseItemQuality();
        strength = itemTemplate.getAttributeSet().getStrength();
        dexterity = itemTemplate.getAttributeSet().getDexterity();
        fortitude = itemTemplate.getAttributeSet().getFortitude();
        maximumMods = itemTemplate.getMaximumMods();
        minimumMods = itemTemplate.getMinimumMods();
        attackRate = itemTemplate.getWeaponAttackRate();
        baseDamage = itemTemplate.getWeaponBaseDamage();
        weaponSize = itemTemplate.getWeaponMeleeRange();
        armor = itemTemplate.getArmor();
    }

    private Long id;
    protected String name;
    protected Integer itemLevel;
    protected Integer equipSlot;
    protected Integer reqLevel;
    protected Integer baseItemQuality;
    protected Integer strength;
    protected Integer dexterity;
    protected Integer fortitude;
    protected Integer maximumMods;
    protected Integer minimumMods;
    protected Double attackRate;
    protected Integer baseDamage;
    protected Double weaponSize;
    protected Integer armor;

    public Object[] getRowData() {
        return new Object[]{ id, name, getSlotString(), itemLevel, reqLevel, baseItemQuality,
                strength, dexterity, fortitude, minimumMods, maximumMods,
                attackRate, baseDamage, weaponSize, armor };
    }

    private String getSlotString() {
        String type = "";
        if(equipSlot.equals(GameConstants.NON_EQUIP)) {
            type = "None";
        } else if(equipSlot.equals(GameConstants.EQUIPMENT_SLOT_BODY)) {
            type = "Armor";
        } else if(equipSlot.equals(GameConstants.EQUIPMENT_MELEE)) {
            type = "Melee";
        } else if(equipSlot.equals(GameConstants.EQUIPMENT_PISTOL)) {
            type = "Pistol";
        } else if(equipSlot.equals(GameConstants.EQUIPMENT_SHOTGUN)) {
            type = "Shotgun";
        }
        return type;
    }

    public Long getId() { return id; }

    public void updateDBEntry() {
        Starter.manager.updateItemTemplate(itemTemplate, name, equipSlot, itemLevel, reqLevel, baseItemQuality,
                strength,dexterity, fortitude, minimumMods, maximumMods, attackRate, baseDamage, weaponSize, armor);
    }

    public void deleteEntry() {
        Starter.manager.deleteItemTemplate(itemTemplate);
    }
}
