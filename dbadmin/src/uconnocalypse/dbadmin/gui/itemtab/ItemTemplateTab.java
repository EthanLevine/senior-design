package uconnocalypse.dbadmin.gui.itemtab;

import uconnocalypse.data.GameConstants;
import uconnocalypse.data.ItemTemplate;
import uconnocalypse.dbadmin.Starter;
import uconnocalypse.dbadmin.gui.spawntab.SpeciesWrapper;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

/**
 * @author Kevin
 */
public class ItemTemplateTab extends JPanel {
    private JTable table;
    private SpeciesTableModel model;

    private ArrayList<ItemTemplateWrapper> itemTemplates = new ArrayList<>();

    public ItemTemplateTab() {
        super();
        init();
    }

    private void init() {
        setLayout(new BorderLayout());
        model = new SpeciesTableModel();
        table = new JTable(model);
        add(new JScrollPane(table), BorderLayout.CENTER);

        JButton deleteButton = new JButton("Delete Selected Entries");
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.removeSelectedRows(table.getSelectedRows());
            }
        });
        JButton addButton = new JButton("Add Entry");
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.addRow();
            }
        });

        TableColumn equipSlotCol = table.getColumnModel().getColumn(2);
        JComboBox<String> types = new JComboBox<>();
        types.addItem("None");
        types.addItem("Armor");
        types.addItem("Melee");
        types.addItem("Pistol");
        types.addItem("Shotgun");
        equipSlotCol.setCellEditor(new DefaultCellEditor(types));

        JPanel south = new JPanel();
        south.add(deleteButton);
        south.add(addButton);
        add(south, BorderLayout.SOUTH);
    }

    private int getSlotInt(String equipSlot) {
        int type = 0;
        switch (equipSlot) {
            case "None":
                type = GameConstants.NON_EQUIP;
                break;
            case "Armor":
                type = GameConstants.EQUIPMENT_SLOT_BODY;
                break;
            case "Melee":
                type = GameConstants.EQUIPMENT_MELEE;
                break;
            case "Pistol":
                type = GameConstants.EQUIPMENT_PISTOL;
                break;
            case "Shotgun":
                type = GameConstants.EQUIPMENT_SHOTGUN;
                break;
        }
        return type;
    }

    /**
     * SpeciesTableModel is a customised table model to allow for the specific features we want it to have
     */
    class SpeciesTableModel extends AbstractTableModel {
        private String[] columnNames = { "ID", "Name", "Equip Slot", "Item Lvl", "Req Lvl",
                "Base Quality", "Str", "Dext", "Fort", "Min Mods", "Max Mods", "Atk Rate",
                "Base Dmg", "Size", "Armor" };
        private Vector<Vector> dataVector;

        public SpeciesTableModel() {
            super();
            dataVector = new Vector<>();
            // Pulls in the initial EntitySpecies data and adds it to the table
            java.util.List<ItemTemplate> existingItems = Starter.manager.getItemTemplates();
            for(ItemTemplate itemTemplate : existingItems) {
                ItemTemplateWrapper itemTemplateWrapper = new ItemTemplateWrapper(itemTemplate);
                itemTemplates.add(itemTemplateWrapper);
                dataVector.insertElementAt(convertToVector(itemTemplateWrapper.getRowData()), getRowCount());
                justifyRows(getRowCount()-1, getRowCount());
            }
        }

        public int getColumnCount() {
            return columnNames.length;
        }

        public int getRowCount() {
            return dataVector.size();
        }

        public String getColumnName(int col) {
            Object id = null;
            if (col < columnNames.length && (col >= 0)) {
                id = columnNames[col];
            }
            return (id == null) ? super.getColumnName(col)
                    : id.toString();
        }

        public Object getValueAt(int row, int col) {
            Vector rowVector = dataVector.elementAt(row);
            return rowVector.elementAt(col);
        }

        /**
         * Gets the class of a column so that instead of a string saying True/False we have a check box
         * and instead of a string for type we have a drop down.
         * @param c - Int refering to the column we are getting the class of
         * @return The class of the column
         */
        public Class getColumnClass(int c) {
            if (getValueAt(0, c) == null) return Integer.class;
            return getValueAt(0, c).getClass();
        }

        /**
         * Checks if a cell is editable.
         * @param row - The row of the cell we are checking
         * @param col - The column of the cell we are checking
         * @return This is always true unless it is the ID column.
         */
        public boolean isCellEditable(int row, int col) {
            return col >= 1; // All cells except the id are editable
        }

        /**
         * Updates a cell with a new value and then sets off a TableCellUpdated event.
         * @param value - The new value to place into the cell
         * @param row - The row of the cell being modified
         * @param col - The column of the cell being modified
         */
        public void setValueAt(Object value, int row, int col) {
            Vector<Object> rowVector = dataVector.elementAt(row);
            switch(col) {
                case 1:
                    itemTemplates.get(row).name = (String)value;
                    itemTemplates.get(row).updateDBEntry();
                    break;
                case 2:
                    itemTemplates.get(row).equipSlot = getSlotInt((String) value);
                    itemTemplates.get(row).updateDBEntry();
                    break;
                case 3:
                    itemTemplates.get(row).itemLevel = (Integer)value;
                    itemTemplates.get(row).updateDBEntry();
                    break;
                case 4:
                    itemTemplates.get(row).reqLevel = (Integer)value;
                    itemTemplates.get(row).updateDBEntry();
                    break;
                case 5:
                    itemTemplates.get(row).baseItemQuality = (Integer)value;
                    itemTemplates.get(row).updateDBEntry();
                    break;
                case 6:
                    itemTemplates.get(row).strength = (Integer)value;
                    itemTemplates.get(row).updateDBEntry();
                    break;
                case 7:
                    itemTemplates.get(row).dexterity = (Integer)value;
                    itemTemplates.get(row).updateDBEntry();
                    break;
                case 8:
                    itemTemplates.get(row).fortitude = (Integer)value;
                    itemTemplates.get(row).updateDBEntry();
                    break;
                case 9:
                    itemTemplates.get(row).minimumMods = (Integer)value;
                    itemTemplates.get(row).updateDBEntry();
                    break;
                case 10:
                    itemTemplates.get(row).maximumMods = (Integer)value;
                    itemTemplates.get(row).updateDBEntry();
                    break;
                case 11:
                    itemTemplates.get(row).attackRate = (Double)value;
                    itemTemplates.get(row).updateDBEntry();
                    break;
                case 12:
                    itemTemplates.get(row).baseDamage = (Integer)value;
                    itemTemplates.get(row).updateDBEntry();
                    break;
                case 13:
                    itemTemplates.get(row).weaponSize = (Double)value;
                    itemTemplates.get(row).updateDBEntry();
                    break;
                case 14:
                    itemTemplates.get(row).armor = (Integer)value;
                    itemTemplates.get(row).updateDBEntry();
                    break;
            }
            rowVector.setElementAt(value, col);
            fireTableCellUpdated(row, col);
        }

        private void justifyRows(int from, int to) {
            dataVector.setSize(getRowCount());
            for (int i = from; i < to; i++) {
                if (dataVector.elementAt(i) == null) {
                    dataVector.setElementAt(new Vector(), i);
                }
                dataVector.elementAt(i).setSize(getColumnCount());
            }
        }

        /**
         * Converts an array of objects into a vector of the objects.
         * @param anArray - An array to be turned into a vector
         * @return A vector of the given objects
         */
        protected Vector<Object> convertToVector(Object[] anArray) {
            if (anArray == null) {
                return null;
            }
            Vector<Object> v = new Vector<>(anArray.length);
            for (Object o : anArray) {
                v.addElement(o);
            }
            return v;
        }

        /**
         * Turns a 2d array of objects into a Vector of Vectors containing the Objects.
         * @param anArray - A 2d array to be turned into a vector
         * @return A vector of vectors of the objects in the array given
         */
        protected Vector<Vector> convertToVector(Object[][] anArray) {
            if (anArray == null) {
                return null;
            }
            Vector<Vector> v = new Vector<>(anArray.length);
            for (Object[] o : anArray) {
                v.addElement(convertToVector(o));
            }
            return v;
        }

        /**
         * Add a new row to our table
         */
        public void addRow() {
            addRow(getRowCount());
        }

        /**
         * Add a new row to our table in a given row
         * @param row - The row where we want to insert a new row
         */
        public void addRow(int row) {
            itemTemplates.add(new ItemTemplateWrapper());
            dataVector.insertElementAt(convertToVector(itemTemplates.get(row).getRowData()), row);
            justifyRows(row, row+1);
            fireTableRowsInserted(row, row);
        }

        /**
         * Removes selected rows based on a given array of the locations of the rows.
         * @param rows - Array of ints that refer to the rows to be removed
         */
        public void removeSelectedRows(int[] rows) {
            for(int i = rows.length-1; i >= 0; i--) {
                dataVector.remove(rows[i]);
                itemTemplates.get(rows[i]).deleteEntry();
                itemTemplates.remove(rows[i]);
                fireTableRowsDeleted(rows[i], rows[i]);
            }
        }
    }
}
