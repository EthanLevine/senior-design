package uconnocalypse.dbadmin.gui.maptab;

import uconnocalypse.data.Barrier;
import uconnocalypse.data.EntitySpecies;
import uconnocalypse.data.FixedSpawn;
import uconnocalypse.data.MapRegionTemplate;
import uconnocalypse.dbadmin.Starter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Kevin
 */
public class MapPanel extends JPanel implements AdjustmentListener, MouseListener, MouseMotionListener {

    private int currentStartX = 0;  // where mouse first pressed
    private int currentStartY = 0;
    private int currentEndX = 0;  // where dragged to or released
    private int currentEndY = 0;

    private Image mapImage;
    private int imgX = 0, imgY = 0;

    private MapTab mapTab;
    private MapRegionTemplate mapRegion; // This mapRegion

    private static final int SIZE = 1000; // size of the map window
    private static JScrollBar horizontal, vertical;

    private BarrierWrapper currentWall;
    private FixedSpawnWrapper currentSpawn;
    private Object currentSelection = null;
    private int prevDragX, prevDragY;
    private ArrayList<BarrierWrapper> barriers = new ArrayList<>();
    private ArrayList<FixedSpawnWrapper> spawnPoints = new ArrayList<>();

    public MapPanel(MapTab mapTab, Image mapImage, MapRegionTemplate mapRegion) {
        super();
        this.mapTab = mapTab;
        this.mapImage = mapImage;
        this.mapRegion = mapRegion;
        //java.util.List<FixedSpawn> spawnList = Starter.manager.getFixedSpawns();
        Set<FixedSpawn> spawnList = mapRegion.getFixedSpawns();
        //java.util.List<Barrier> barrierList = Starter.manager.getBarriers();
        Set<Barrier> barrierList = mapRegion.getBarriers();
        for(FixedSpawn spawn : spawnList)
            spawnPoints.add(new FixedSpawnWrapper(spawn));
        for(Barrier barrier : barrierList)
            barriers.add(new BarrierWrapper(barrier));

        setLayout(new BorderLayout());

        int imageHeight = mapImage.getHeight(this);
        int imageWidth = mapImage.getWidth(this);
        vertical = new JScrollBar(JScrollBar.VERTICAL);
        horizontal = new JScrollBar(JScrollBar.HORIZONTAL);
        vertical.addAdjustmentListener(this);
        horizontal.addAdjustmentListener(this);
        add(vertical, BorderLayout.EAST);
        add(horizontal, BorderLayout.SOUTH);

        setBackground(Color.WHITE);
        setPreferredSize(new Dimension(SIZE, SIZE));
        addMouseListener(this);
        addMouseMotionListener(this);

        vertical.setValues(0, getSize().height, 0, imageHeight);
        horizontal.setValues(0, getSize().width, 0, imageWidth);
        repaint();
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(getBackground());
        g.fillRect(0,0,getSize().width,getSize().height);
        g.drawImage(mapImage, -imgX, -imgY, this);

        if(mapTab.showWalls()) {
            for (BarrierWrapper wall : barriers) {
                wall.draw(g,imgX,imgY);
            }
        }
        if(mapTab.showSpawns()) {
            for (FixedSpawnWrapper spawn : spawnPoints) {
                spawn.draw(g,imgX,imgY);
            }
        }
        mapTab.updateData();
    }

    public void mousePressed(MouseEvent e) {
        if(mapTab.getTool() == 1) { //Drawing a new Wall
            System.out.println("Drawing...");
            currentStartX = e.getX(); // save x coordinate of the click
            currentStartY = e.getY(); // save y
            currentEndX = currentStartX;   // set end to same pixel
            currentEndY = currentStartY;
            currentWall = new BarrierWrapper(mapRegion, currentStartX+imgX, currentStartY+imgY, currentEndX-currentStartX, currentEndY-currentStartY);
            barriers.add(currentWall);
        } else if(mapTab.getTool() == 0) { //Using the hand tool on an existing wall
            int x = e.getX()+imgX;  // x-coordinate of point where mouse was clicked
            int y = e.getY()+imgY;  // y-coordinate of point
            for (BarrierWrapper wall : barriers) {
                if (wall.containsPoint(x,y)) {
                    if(currentSelection != null) {
                        if(currentSelection instanceof BarrierWrapper) ((BarrierWrapper) currentSelection).setSelected(false);
                        else if(currentSelection instanceof FixedSpawnWrapper) ((FixedSpawnWrapper) currentSelection).setSelected(false);
                    }
                    wall.setSelected(true);
                    currentWall = wall;
                    currentSelection = wall;
                    prevDragX = x;
                    prevDragY = y;
                    return;
                }
            }
            for(FixedSpawnWrapper spawn : spawnPoints) {
                if(spawn.containsPoint(x,y)) {
                    if(currentSelection != null) {
                        if(currentSelection instanceof BarrierWrapper) ((BarrierWrapper) currentSelection).setSelected(false);
                        else if(currentSelection instanceof FixedSpawnWrapper) ((FixedSpawnWrapper) currentSelection).setSelected(false);
                    }
                    spawn.setSelected(true);
                    currentSpawn = spawn;
                    currentSelection = spawn;
                    prevDragX = x;
                    prevDragY = y;
                    return;
                }
            }
        } else if(mapTab.getTool() == 3) { //Using the delete tool
            int x = e.getX()+imgX;  // x-coordinate of point where mouse was clicked
            int y = e.getY()+imgY;  // y-coordinate of point
            for (BarrierWrapper wall : barriers) {
                if (wall.containsPoint(x,y)) {
                    if(currentSelection != null) {
                        if(currentSelection instanceof BarrierWrapper) ((BarrierWrapper) currentSelection).setSelected(false);
                        else if(currentSelection instanceof FixedSpawnWrapper) ((FixedSpawnWrapper) currentSelection).setSelected(false);
                    }
                    delete(wall);
                    return;
                }
            }
            for(FixedSpawnWrapper spawn : spawnPoints) {
                if(spawn.containsPoint(x,y)) {
                    if(currentSelection != null) {
                        if(currentSelection instanceof BarrierWrapper) ((BarrierWrapper) currentSelection).setSelected(false);
                        else if(currentSelection instanceof FixedSpawnWrapper) ((FixedSpawnWrapper) currentSelection).setSelected(false);
                    }
                    delete(spawn);
                    return;
                }
            }
        }
    }

    public void mouseDragged(MouseEvent e) {
        if(mapTab.getTool() == 1) {
            currentEndX = e.getX();   // save new x and y coordinates
            currentEndY = e.getY();

            if(currentEndX - currentStartX > 0 && currentEndY - currentStartY > 0)
                currentWall.updateDim(currentStartX+imgX, currentStartY+imgY, currentEndX-currentStartX, currentEndY-currentStartY);
            else if(currentEndX - currentStartX < 0 && currentEndY - currentStartY > 0)
                currentWall.updateDim(currentEndX+imgX, currentStartY+imgY, currentStartX-currentEndX, currentEndY-currentStartY);
            else if(currentEndX - currentStartX > 0 && currentEndY - currentStartY < 0)
                currentWall.updateDim(currentStartX+imgX, currentEndY+imgY, currentEndX-currentStartX, currentStartY-currentEndY);
            else if(currentEndX - currentStartX < 0 && currentEndY - currentStartY < 0)
                currentWall.updateDim(currentEndX+imgX, currentEndY+imgY, currentStartX-currentEndX, currentStartY-currentEndY);
            repaint();            // show new shape
        } else if(mapTab.getTool() == 0) {
            // User has moved the mouse.  Move the dragged shape by the same amount.
            int x = e.getX()+imgX;
            int y = e.getY()+imgY;
            if (currentWall != null) {
                currentWall.moveBy(x - prevDragX, y - prevDragY);
                prevDragX = x;
                prevDragY = y;
                repaint();      // redraw canvas to show shape in new position
            }
            if (currentSpawn != null) {
                currentSpawn.moveBy(x - prevDragX, y - prevDragY);
                prevDragX = x;
                prevDragY = y;
                repaint();      // redraw canvas to show shape in new position
            }
        }
    }

    public void mouseReleased(MouseEvent e) {
        if(mapTab.getTool() == 1) {
            System.out.println("Finished.");
            // This will save the shape that has been dragged by
            // drawing it onto the bufferedImage where all shapes
            // are written.
            currentEndX = e.getX(); // save ending coordinates
            currentEndY = e.getY();

            if(currentEndX - currentStartX > 0 && currentEndY - currentStartY > 0)
                currentWall.updateDim(currentStartX+imgX, currentStartY+imgY, currentEndX-currentStartX, currentEndY-currentStartY);
            else if(currentEndX - currentStartX < 0 && currentEndY - currentStartY > 0)
                currentWall.updateDim(currentEndX+imgX, currentStartY+imgY, currentStartX-currentEndX, currentEndY-currentStartY);
            else if(currentEndX - currentStartX > 0 && currentEndY - currentStartY < 0)
                currentWall.updateDim(currentStartX+imgX, currentEndY+imgY, currentEndX-currentStartX, currentStartY-currentEndY);
            else if(currentEndX - currentStartX < 0 && currentEndY - currentStartY < 0)
                currentWall.updateDim(currentEndX+imgX, currentEndY+imgY, currentStartX-currentEndX, currentStartY-currentEndY);

            updateData();
            repaint();
        } else if(mapTab.getTool() == 0) {
            // User has released the mouse.  Move the dragged shape, then set
            // shapeBeingDragged to null to indicate that dragging is over.
            // If the shape lies completely outside the canvas, remove it
            // from the list of shapes (since there is no way to ever move
            // it back onscreen).
            int x = e.getX()+imgX;
            int y = e.getY()+imgY;
            if (currentWall != null) {
                currentWall.moveBy(x - prevDragX, y - prevDragY);
                currentWall = null;
                repaint();
            }
            if (currentSpawn != null) {
                currentSpawn.moveBy(x - prevDragX, y - prevDragY);
                currentSpawn = null;
                repaint();
            }
        }
    }

    public ArrayList<BarrierWrapper> getBarriers() {
        return barriers;
    }
    public ArrayList<FixedSpawnWrapper> getSpawnPoints() {
        return spawnPoints;
    }

    public void setCurrentWall(BarrierWrapper barrier) { currentWall = barrier; }
    public void setCurrentSpawn(FixedSpawnWrapper spawn) { currentSpawn = spawn; }
    public void setCurrentSelection(Object o) { currentSelection = o; }
    public Object getSelected() { return currentSelection; }

    public void updateData() {
        mapTab.updateData();
    }

    /*public void updateMapRegionData() {
        Set<FixedSpawn> spawnSet = new HashSet<>();
        for(FixedSpawnWrapper fixedSpawn : spawnPoints) {
            spawnSet.add(fixedSpawn.getFixedSpawn());
        }
        Set<Barrier> barrierSet = new HashSet<>();
        for(BarrierWrapper barrier : barriers) {
            barrierSet.add(barrier.getBarrier());
        }
        Starter.manager.updateMapRegionTemplate(mapRegion, mapRegion.getName(), spawnSet, barrierSet);
    }*/
    public void mouseMoved(MouseEvent e) {}
    public void mouseEntered(MouseEvent e) {}
    public void mouseExited(MouseEvent e) {}

    public void mouseClicked(MouseEvent e) {
        //TODO: Barriers are polygons?, on click add a new point.
        if(e.getButton() == MouseEvent.BUTTON2) {
            if(mapTab.getTool() == 1 && !barriers.isEmpty()) {
                barriers.remove(0);
                updateData();
            }
            if(mapTab.getTool() == 2 && !spawnPoints.isEmpty()) {
                spawnPoints.remove(0);
                updateData();
            }
        }
        if(e.getButton() == MouseEvent.BUTTON1 && mapTab.getTool() == 2) {
            // Options for placeable Entities
            java.util.List<EntitySpecies> speciesOptions = Starter.manager.getEntitySpecies();
            // Choose from the options
            if(speciesOptions.size() > 0) {
                ArrayList<String> speciesNames = new ArrayList<>();
                for(EntitySpecies species : speciesOptions) {
                    speciesNames.add(species.getName());
                }
                String selection = (String)JOptionPane.showInputDialog(
                        this,
                        "Choose an entity species to place:\n",
                        "Fixed Spawn Placement",
                        JOptionPane.PLAIN_MESSAGE,
                        null,
                        speciesNames.toArray(),
                        "");
                if(selection != null && selection.length() > 0) {
                    System.out.println("Placed a spawn.");
                    EntitySpecies species = null;
                    for(EntitySpecies sp : speciesOptions) {
                        if(sp.getName().equals(selection)) { species = sp; }
                    }
                    spawnPoints.add(new FixedSpawnWrapper(Starter.manager.createNewFixedSpawn(mapRegion,
                            species, e.getX()+imgX, e.getY()+imgY, 0.0d)));
                    repaint();
                }
            }
        }
    }

    @Override
    public void adjustmentValueChanged(AdjustmentEvent e) {
        if (e.getSource() instanceof JScrollBar) {
            if (e.getSource() == horizontal) {
                imgX = e.getValue();
            } else if (e.getSource() == vertical) {
                imgY = e.getValue();
            }
            repaint();
        }
    }

    public void delete(Object o) {
        if(o instanceof BarrierWrapper) {
            Starter.manager.deleteBarrier(((BarrierWrapper) o).getBarrier());
            barriers.remove(o);
            currentSelection = null;
            repaint();
        } else if(o instanceof FixedSpawnWrapper) {
            Starter.manager.deleteFixedSpawn(((FixedSpawnWrapper) o).getFixedSpawn());
            spawnPoints.remove(o);
            currentSelection = null;
            repaint();
        }
    }
}
