package uconnocalypse.dbadmin.gui.maptab;

import uconnocalypse.data.Barrier;
import uconnocalypse.data.MapRegionTemplate;
import uconnocalypse.dbadmin.Starter;
import uconnocalypse.engine.Point;

import java.awt.*;
import java.util.List;

/**
 * @author Kevin
 */
public class BarrierWrapper {
    private Barrier barrier;
    private int x, y, width, height;
    private boolean selected = false;

    public BarrierWrapper(Barrier barrier) {
        this.barrier = barrier;
        List<Point> geometry = barrier.getGeometry().getPoints();
        Point topLeft = geometry.get(0);
        Point botRight = geometry.get(0);
        for(Point p : geometry) {
            if(p.x <= topLeft.x && p.y <= topLeft.y) {
                topLeft = p;
            } else if(p.x >= botRight.x && p.y >= topLeft.y) {
                botRight = p;
            }
        }
        x = (int)topLeft.x;
        y = (int)topLeft.y;
        width = (int)botRight.x-x;
        height = (int)botRight.y-y;
    }

    public BarrierWrapper(MapRegionTemplate mapRegion, int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        barrier = Starter.manager.createNewBarrier(mapRegion, generateGeometryString());
    }

    private String generateGeometryString() {
        String geometry = "";
        geometry += x+","+y+";";
        geometry += +(x+width)+","+y+";";
        geometry += +x+","+(y+height)+";";
        geometry += +(x+width)+","+(y+height)+";";
        return geometry;
    }

    public void updateDim(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        Starter.manager.updateBarrier(barrier, generateGeometryString());
    }

    public void moveBy(int dx, int dy) {
        x += dx;
        y += dy;
    }

    public void draw(Graphics g, int imgX, int imgY) {
        Color paintColor;
        if(selected)
            paintColor = new Color(0, 0, 1, 0.5f);
        else
            paintColor = new Color(0, 0, 0, 0.5f);
        g.setColor(paintColor);
        g.fillRect(x-imgX,y-imgY,width,height);
        g.setColor(Color.ORANGE);
        g.drawRect(x-imgX,y-imgY,width,height);
    }

    public String getString() {
        return "X: " + x + ", Y: " + y + ", Width: " + width + ", Height: " + height;
    }

    public boolean containsPoint(int x, int y) {
        return x >= this.x && x < this.x + width && y >= this.y && y < this.y + height;
    }
    public void setSelected(boolean selected) { this.selected = selected; }
    public boolean getSelected() { return selected; }
    public Rectangle getRectangle() {
        return new Rectangle(x, y, width, height);
    }

    public Barrier getBarrier() { return barrier; }
}
