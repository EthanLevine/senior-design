package uconnocalypse.dbadmin.gui.maptab;

import javax.swing.filechooser.FileFilter;
import java.io.File;

/**
 * @author Kevin
 */
public class MapImageFileFilter extends FileFilter {
    public boolean accept(File file) {
        if(file.isDirectory()) return true;
        int extIndex = file.getAbsolutePath().lastIndexOf(".");
        if(extIndex < 0) return false;
        String fileExt = file.getAbsolutePath().substring(extIndex);
        if(fileExt.equals(".png")) return true;
        return false;
    }
    public String getDescription() { return "Map Image Files"; }
}
