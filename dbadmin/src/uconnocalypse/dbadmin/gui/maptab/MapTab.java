package uconnocalypse.dbadmin.gui.maptab;

import uconnocalypse.data.MapRegionTemplate;
import uconnocalypse.dbadmin.Starter;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

/**
 * Map tab will be in charge of it's Barrier objects, spawn points + their pools
 *
 * When loading in a polygon wall object from existing MapRegionTemplate
 *  - Take the smallest x,y values and the largest x,y values and create the rectangle based on then
 *  - When committing it back it will use the polygon coords of the rectangle
 *
 * @author Kevin
 */
public class MapTab extends JPanel implements ActionListener, MouseListener {
    public MapTab() {
        super(new BorderLayout());
        init();
    }

    private JToggleButton handTool;
    private JToggleButton drawWalls;
    private JToggleButton placeFixedSpawns;
    private JToggleButton deleteTool;
    private JCheckBox showWalls;
    private JCheckBox showFixedSpawns;
    private int tool = 0; // 0 - hand, 1 - walls, 2 - spawns

    private MapPanel mapPanel;

    private MapDataListPane listPanel;

    private JButton editImg;
    private JButton importImg;

    private void init() {
        //Hand tool, for moving/selecting walls (JToggleButton button)
        JPanel northPanel = new JPanel();
        handTool = new JToggleButton("Hand Tool");
        handTool.addActionListener(this);
        handTool.setSelected(true);
        northPanel.add(handTool);
        //Draw walls (JToggleButton button)
        drawWalls = new JToggleButton("Draw Walls");
        drawWalls.addActionListener(this);
        northPanel.add(drawWalls);
        //Place spawn points (JToggleButton button)
        placeFixedSpawns = new JToggleButton("Place Fixed Spawns");
        placeFixedSpawns.addActionListener(this);
        northPanel.add(placeFixedSpawns);
        //Place spawn points (JToggleButton button)
        deleteTool = new JToggleButton("Delete Tool");
        deleteTool.addActionListener(this);
        northPanel.add(deleteTool);

        showWalls = new JCheckBox("Show Walls");
        showWalls.addActionListener(this);
        showWalls.setSelected(true);
        northPanel.add(showWalls);
        showFixedSpawns = new JCheckBox("Show Fixed Spawns");
        showFixedSpawns.addActionListener(this);
        showFixedSpawns.setSelected(true);
        northPanel.add(showFixedSpawns);
        add(northPanel, BorderLayout.NORTH);

        //Big image viewer pane (that can be drawn on)
        //mapPanel = new MapPanel(this);
        //add(mapPanel, BorderLayout.CENTER);

        //Side bar with rectangles that have been drawn
        //listPanel = new MapDataListPane(this);
        //add(listPanel, BorderLayout.EAST);

        JPanel southPanel = new JPanel();
        //Edit image button, edit an existing MapRegion
        editImg = new JButton("Edit Map Region");
        editImg.addActionListener(this);
        southPanel.add(editImg);
        //Import map button on bottom
        //Check to make sure no unsaved work/confirm continue button
        importImg = new JButton("New Map Region");
        importImg.addActionListener(this);
        southPanel.add(importImg);
        add(southPanel, BorderLayout.SOUTH);
    }

    private Image browseForMapImage() {
        JFileChooser fc = new JFileChooser("C:\\Users\\Kevin\\Projects\\CSE Senior Design\\senior-design\\Prototypes\\login ptt");
        fc.setFileFilter(new MapImageFileFilter());
        int res = fc.showOpenDialog(null);
        // We have an image!
        try {
            if(res == JFileChooser.APPROVE_OPTION) {
                //Toolkit toolkit = Toolkit.getDefaultToolkit();
                //mapImage = toolkit.createImage(fc.getName());
                Image mapImage = ImageIO.read(fc.getSelectedFile());
                MediaTracker tracker = new MediaTracker(this);
                tracker.addImage(mapImage, 0);
                try { tracker.waitForAll(); } catch(Exception e) { }
                return mapImage;
            } else { /*They decided not to go with this option */ }
        } catch (Exception IOException) {
        }
        return null;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().equals(handTool)) {
            tool = 0;
            handTool.setSelected(true);
            placeFixedSpawns.setSelected(false);
            drawWalls.setSelected(false);
            deleteTool.setSelected(false);
        } else if(e.getSource().equals(drawWalls)) {
            tool = 1;
            handTool.setSelected(false);
            placeFixedSpawns.setSelected(false);
            drawWalls.setSelected(true);
            deleteTool.setSelected(false);
        } else if(e.getSource().equals(placeFixedSpawns)) {
            tool = 2;
            handTool.setSelected(false);
            drawWalls.setSelected(false);
            placeFixedSpawns.setSelected(true);
            deleteTool.setSelected(false);
        } else if(e.getSource().equals(deleteTool)) {
            tool = 3;
            handTool.setSelected(false);
            drawWalls.setSelected(false);
            placeFixedSpawns.setSelected(false);
            deleteTool.setSelected(true);
        } else if(e.getSource().equals(importImg)) {
            Image img = browseForMapImage();
            if(img != null) {
                String name = JOptionPane.showInputDialog("Please enter a name/descriptor for the new map,\n" +
                        "this will be used to distinguish the map later on.");
                //Big image viewer pane (that can be drawn on)
                if(name != null && name.length() > 0) {
                    mapPanel = new MapPanel(this, img, Starter.manager.createNewMapRegionTemplate(name,
                            img.getWidth(this), img.getHeight(this)));
                    add(mapPanel, BorderLayout.CENTER);
                    //Side bar with objects that have been drawn onto this image
                    listPanel = new MapDataListPane(this);
                    add(listPanel, BorderLayout.EAST);
                }
            }
        } else if(e.getSource().equals(editImg)) {
            // Get the requested MapRegion
            // This gets a list of all the map region templates
            java.util.List<MapRegionTemplate> mapOptions = Starter.manager.getMapRegionTemplates();
            // Choose from the map options
            if(mapOptions.size() > 0) {
                ArrayList<String> mapNames = new ArrayList<>();
                for(MapRegionTemplate map : mapOptions) {
                    mapNames.add(map.getName());
                }
                String selection = (String)JOptionPane.showInputDialog(
                        this,
                        "Choose a map region:\n",
                        "Edit Existing Map Region",
                        JOptionPane.PLAIN_MESSAGE,
                        null,
                        mapNames.toArray(),
                        "");
                // If they chose an option, have them select the map image
                if(selection != null && selection.length() > 0) {
                    MapRegionTemplate mapReg = null;
                    for(MapRegionTemplate map : mapOptions) {
                        if(map.getName().equals(selection)) { mapReg = map; }
                    }
                    // Before we just got a list of the templates, now we want to get a complete object
                    // This method will initialize all the child sets of this template
                    mapReg = Starter.manager.getMapRegionTemplate(mapReg.getId());
                    Image img = browseForMapImage();
                    // If image isn't null, load it up!
                    if(img != null) {
                        //Big image viewer pane (that can be drawn on)
                        mapPanel = new MapPanel(this, img, mapReg);
                        add(mapPanel, BorderLayout.CENTER);
                        //Side bar with objects that have been drawn onto this image
                        listPanel = new MapDataListPane(this);
                        add(listPanel, BorderLayout.EAST);
                    }
                }
            }
            // Then ask for the associated image
        } else if(e.getSource().equals(showWalls) || e.getSource().equals(showFixedSpawns)) {
            mapPanel.repaint();
        }
    }

    public void updateData() {
        listPanel.updateData();
    }

    @Override
    public void mouseClicked(MouseEvent e) { }
    @Override
    public void mousePressed(MouseEvent e) { }
    @Override
    public void mouseReleased(MouseEvent e) { }
    @Override
    public void mouseEntered(MouseEvent e) { }
    @Override
    public void mouseExited(MouseEvent e) { }
    public int getTool() { return tool; }
    public boolean showWalls() { return showWalls.isSelected(); }
    public boolean showSpawns() { return showFixedSpawns.isSelected(); }
    public MapPanel getMapPanel() { return mapPanel; }
}
