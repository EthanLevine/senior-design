package uconnocalypse.dbadmin.gui.maptab;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

/**
 *
 * @author Kevin
 */
public class MapDataListPane extends JTabbedPane {
    private MapTab mapTab;

    private JScrollPane wallTab;
    private JList<String> wallList;
    private JScrollPane spawnTab;
    private JList<String> spawnList;

    private ArrayList<BarrierWrapper> walls;
    private int wallIdx;
    private ArrayList<FixedSpawnWrapper> spawns;
    private int spawnIdx;

    public MapDataListPane(MapTab mapTab) {
        super();
        this.mapTab = mapTab;

        wallTab = new JScrollPane();
        wallList = new JList<>();
        MouseListener wallMouse = new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int index = wallList.locationToIndex(e.getPoint());
                walls.get(wallIdx).setSelected(false);
                walls.get(index).setSelected(true);
                MapDataListPane.this.mapTab.getMapPanel().setCurrentSelection(walls.get(index));
                wallIdx = index;
                MapDataListPane.this.mapTab.repaint();
            }
        };
        wallList.addMouseListener(wallMouse);
        wallTab.getViewport().setView(wallList);
        addTab("Walls", wallTab);

        spawnTab = new JScrollPane();
        spawnList = new JList<>();
        MouseListener spawnMouse = new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int index = spawnList.locationToIndex(e.getPoint());
                spawns.get(spawnIdx).setSelected(false);
                spawns.get(index).setSelected(true);
                MapDataListPane.this.mapTab.getMapPanel().setCurrentSelection(spawns.get(index));
                spawnIdx = index;
                MapDataListPane.this.mapTab.repaint();
            }
        };
        spawnList.addMouseListener(spawnMouse);
        spawnTab.getViewport().setView(spawnList);
        addTab("Spawn Points", spawnTab);
    }

    public void updateData() {
        walls = mapTab.getMapPanel().getBarriers();
        spawns = mapTab.getMapPanel().getSpawnPoints();
        DefaultListModel<String> wallModel = new DefaultListModel<>();
        for(BarrierWrapper wall : walls) {
            wallModel.addElement(wall.getString());
        }
        wallList.setModel(wallModel);

        DefaultListModel<String> spawnModel = new DefaultListModel<>();
        for(FixedSpawnWrapper spawn : spawns) {
            spawnModel.addElement(spawn.getString());
        }
        spawnList.setModel(spawnModel);
        repaint();
    }
}
