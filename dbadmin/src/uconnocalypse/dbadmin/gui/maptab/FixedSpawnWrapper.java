package uconnocalypse.dbadmin.gui.maptab;

import uconnocalypse.data.EntitySpecies;
import uconnocalypse.data.FixedSpawn;

import java.awt.*;

/**
 * @author Kevin
 */
public class FixedSpawnWrapper {
    private FixedSpawn fixedSpawn;
    private EntitySpecies species;
    private int x, y;
    private double angle;
    private boolean selected = false;

    public FixedSpawnWrapper(FixedSpawn fixedSpawn) {
        this.fixedSpawn = fixedSpawn;
        x = fixedSpawn.getX();
        y = fixedSpawn.getY();
        species = fixedSpawn.getSpecies();
    }

    public void updateLoc(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void moveBy(int dx, int dy) {
        x += dx;
        y += dy;
    }

    public void draw(Graphics g, int imgX, int imgY) {
        Color paintColor;
        if(selected)
            paintColor = new Color(0, 0, 1, 0.75f);
        else
            paintColor = new Color(0, 1, 0, 0.75f);
        g.setColor(paintColor);
        g.fillOval(x - imgX - 10, y - imgY - 10, 20, 20);
        g.setColor(Color.ORANGE);
        g.drawOval(x - imgX - 10, y - imgY - 10, 20, 20);
    }

    public String getString() {
        return "X: " + x + ", Y: " + y;
    }

    public void setSelected(boolean selected) { this.selected = selected; }
    public boolean getSelected() { return selected; }
    public int[] getLocation() {
        return new int[]{x, y};
    }
    public boolean containsPoint(int x, int y) {
        return this.x - 10 <= x && this.x + 10 >= x && this.y - 10 <= y && this.y + 10 >= y;
    }
    public FixedSpawn getFixedSpawn() { return fixedSpawn; }
}
