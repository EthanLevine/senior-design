package uconnocalypse.dbadmin.gui.spawntab;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;

/**
 * Just came to the realization I have no idea how this should work... need to check my notes
 *
 * @author Kevin
 */
public class GroupTab extends JPanel {
    private JTable table;
    private GroupTableModel model;

    private ArrayList<SpeciesWrapper> species = new ArrayList<>();

    private JButton addButton;

    public GroupTab() {
        super();
        init();
    }

    private void init() {
        setLayout(new BorderLayout());
        model = new GroupTableModel();
        table = new JTable(model);
        add(new JScrollPane(table), BorderLayout.CENTER);

        addButton = new JButton("Add Entry");
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.addRow();
            }
        });

        JPanel south = new JPanel();
        south.add(addButton);
        add(south, BorderLayout.SOUTH);
    }

    class GroupTableModel extends AbstractTableModel {
        private String[] columnNames = {"ID", "Set<Species>", "Set<Multiplicities>"};
        private Object[][] data = { {0, "NAME", 0.75f} };
        private Object[] newRowData = new Object[]{0, "NAME", 0.75f};
        private Vector dataVector;

        public GroupTableModel() {
            super();
            dataVector = convertToVector(data);
        }

        public int getColumnCount() {
            return columnNames.length;
        }

        public int getRowCount() {
            return dataVector.size();
        }

        public String getColumnName(int col) {
            Object id = null;
            if (col < columnNames.length && (col >= 0)) {
                id = columnNames[col];
            }
            return (id == null) ? super.getColumnName(col)
                    : id.toString();
        }

        public Object getValueAt(int row, int col) {
            Vector rowVector = (Vector)dataVector.elementAt(row);
            return rowVector.elementAt(col);
        }

        /*
         * JTable uses this method to determine the default renderer/
         * editor for each cell.  If we didn't implement this method,
         * then the last column would contain text ("true"/"false"),
         * rather than a check box.
         */
        public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }

        public boolean isCellEditable(int row, int col) {
            return col >= 1; // All cells except the id are editable
        }

        public void setValueAt(Object value, int row, int col) {
            Vector rowVector = (Vector)dataVector.elementAt(row);
            rowVector.setElementAt(value, col);
            fireTableCellUpdated(row, col);
        }

        private void justifyRows(int from, int to) {
            dataVector.setSize(getRowCount());
            for (int i = from; i < to; i++) {
                if (dataVector.elementAt(i) == null) {
                    dataVector.setElementAt(new Vector(), i);
                }
                ((Vector)dataVector.elementAt(i)).setSize(getColumnCount());
            }
        }
        protected Vector convertToVector(Object[] anArray) {
            if (anArray == null) {
                return null;
            }
            Vector<Object> v = new Vector<>(anArray.length);
            for (Object o : anArray) {
                v.addElement(o);
            }
            return v;
        }
        protected Vector convertToVector(Object[][] anArray) {
            if (anArray == null) {
                return null;
            }
            Vector<Vector> v = new Vector<Vector>(anArray.length);
            for (Object[] o : anArray) {
                v.addElement(convertToVector(o));
            }
            return v;
        }
        public void addRow() {
            dataVector.insertElementAt(convertToVector(newRowData), data.length);
            justifyRows(data.length, data.length+1);
            fireTableRowsInserted(data.length, data.length);
        }
    }
}