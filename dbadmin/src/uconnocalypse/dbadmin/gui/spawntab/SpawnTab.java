package uconnocalypse.dbadmin.gui.spawntab;

import uconnocalypse.data.DatabaseManager;
import uconnocalypse.dbadmin.gui.DBFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * This tab will be in charge of Monster Entities in the db, Spawn Groups, and Spawn Pools
 *
 * @author Kevin
 */
public class SpawnTab extends JPanel {
    private EntityTab entityTab;
    private GroupTab groupTab;
    private PoolTab poolTab;

    public SpawnTab() {
        super();
        init();
    }

    private void init() {
        setLayout(new BorderLayout());
        JTabbedPane spawnTabs = new JTabbedPane(JTabbedPane.TOP);

        /* Pool and Group tabs not implemented
        poolTab = new PoolTab();
        spawnTabs.add("Spawn Pools", poolTab);
        groupTab = new GroupTab();
        spawnTabs.add("Spawn Groups", groupTab);*/
        entityTab = new EntityTab();
        spawnTabs.add("Entity Species", entityTab);

        add(spawnTabs, BorderLayout.CENTER);
    }
}
