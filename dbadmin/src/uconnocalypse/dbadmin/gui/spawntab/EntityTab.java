package uconnocalypse.dbadmin.gui.spawntab;

import uconnocalypse.data.EntitySpecies;
import uconnocalypse.data.PhysicalType;
import uconnocalypse.dbadmin.Starter;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;
import java.util.List;

/**
 * @author Kevin
 */
public class EntityTab extends JPanel {
    private JTable table;
    private SpeciesTableModel model;

    private ArrayList<SpeciesWrapper> species = new ArrayList<>();

    public EntityTab() {
        super();
        init();
    }

    private void init() {
        setLayout(new BorderLayout());
        model = new SpeciesTableModel();
        table = new JTable(model);
        add(new JScrollPane(table), BorderLayout.CENTER);

        JButton deleteButton = new JButton("Delete Selected Entries");
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.removeSelectedRows(table.getSelectedRows());
            }
        });
        JButton addButton = new JButton("Add Entry");
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.addRow();
            }
        });

        TableColumn physicalTypeCol = table.getColumnModel().getColumn(2);
        JComboBox<String> types = new JComboBox<>();
        types.addItem("NON_PHYSICAL");
        types.addItem("SOLID");
        types.addItem("HOLLOW");
        types.addItem("MINUSCULE");
        physicalTypeCol.setCellEditor(new DefaultCellEditor(types));

        JPanel south = new JPanel();
        south.add(deleteButton);
        south.add(addButton);
        add(south, BorderLayout.SOUTH);
    }

    private int getTypeInt(String physicalType) {
        int type = 0;
        switch (physicalType) {
            case "NON_PHYSICAL":
                type = PhysicalType.NON_PHYSICAL;
                break;
            case "SOLID":
                type = PhysicalType.SOLID;
                break;
            case "HOLLOW":
                type = PhysicalType.HOLLOW;
                break;
            case "MINUSCULE":
                type = PhysicalType.MINUSCULE;
                break;
        }
        return type;
    }

    /**
     * SpeciesTableModel is a customised table model to allow for the specific features we want it to have
     */
    class SpeciesTableModel extends AbstractTableModel {
        private String[] columnNames = {"ID", "Name", "Physical Type", "Is Static", "Geometry String",
                "Max Health", "Max Energy", "Max Power"};
        private Vector<Vector> dataVector;

        public SpeciesTableModel() {
            super();
            dataVector = new Vector<>();
            // Pulls in the initial EntitySpecies data and adds it to the table
            List<EntitySpecies> existingSpecies = Starter.manager.getEntitySpecies();
            for(EntitySpecies entitySpecies : existingSpecies) {
                SpeciesWrapper speciesWrapper = new SpeciesWrapper(entitySpecies);
                species.add(speciesWrapper);
                dataVector.insertElementAt(convertToVector(speciesWrapper.getRowData()), getRowCount());
                justifyRows(getRowCount()-1, getRowCount());
            }
        }

        public int getColumnCount() {
            return columnNames.length;
        }

        public int getRowCount() {
            return dataVector.size();
        }

        public String getColumnName(int col) {
            Object id = null;
            if (col < columnNames.length && (col >= 0)) {
                id = columnNames[col];
            }
            return (id == null) ? super.getColumnName(col)
                    : id.toString();
        }

        public Object getValueAt(int row, int col) {
            Vector rowVector = dataVector.elementAt(row);
            return rowVector.elementAt(col);
        }

        /**
         * Gets the class of a column so that instead of a string saying True/False we have a check box
         * and instead of a string for type we have a drop down.
         * @param c - Int refering to the column we are getting the class of
         * @return The class of the column
         */
        public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }

        /**
         * Checks if a cell is editable.
         * @param row - The row of the cell we are checking
         * @param col - The column of the cell we are checking
         * @return This is always true unless it is the ID column.
         */
        public boolean isCellEditable(int row, int col) {
            return col >= 1; // All cells except the id are editable
        }

        /**
         * Updates a cell with a new value and then sets off a TableCellUpdated event.
         * @param value - The new value to place into the cell
         * @param row - The row of the cell being modified
         * @param col - The column of the cell being modified
         */
        public void setValueAt(Object value, int row, int col) {
            Vector<Object> rowVector = dataVector.elementAt(row);
            switch(col) {
                case 1:
                    species.get(row).setName((String) value);
                    break;
                case 2:
                    species.get(row).setPhysicalType(getTypeInt((String) value));
                    break;
                case 3:
                    species.get(row).setStatic((Boolean) value);
                    break;
                case 4:
                    species.get(row).setPhysicalGeometryString((String) value);
                    break;
                case 5:
                    species.get(row).setMaxHealth((Integer) value);
                    break;
                case 6:
                    species.get(row).setMaxEnergy((Integer) value);
                    break;
                case 7:
                    species.get(row).setMaxPower((Integer) value);
                    break;
            }
            rowVector.setElementAt(value, col);
            fireTableCellUpdated(row, col);
        }

        private void justifyRows(int from, int to) {
            dataVector.setSize(getRowCount());
            for (int i = from; i < to; i++) {
                if (dataVector.elementAt(i) == null) {
                    dataVector.setElementAt(new Vector(), i);
                }
                dataVector.elementAt(i).setSize(getColumnCount());
            }
        }

        /**
         * Converts an array of objects into a vector of the objects.
         * @param anArray - An array to be turned into a vector
         * @return A vector of the given objects
         */
        protected Vector<Object> convertToVector(Object[] anArray) {
            if (anArray == null) {
                return null;
            }
            Vector<Object> v = new Vector<>(anArray.length);
            for (Object o : anArray) {
                v.addElement(o);
            }
            return v;
        }

        /**
         * Turns a 2d array of objects into a Vector of Vectors containing the Objects.
         * @param anArray - A 2d array to be turned into a vector
         * @return A vector of vectors of the objects in the array given
         */
        protected Vector<Vector> convertToVector(Object[][] anArray) {
            if (anArray == null) {
                return null;
            }
            Vector<Vector> v = new Vector<>(anArray.length);
            for (Object[] o : anArray) {
                v.addElement(convertToVector(o));
            }
            return v;
        }

        /**
         * Add a new row to our table
         */
        public void addRow() {
            addRow(getRowCount());
        }

        /**
         * Add a new row to our table in a given row
         * @param row - The row where we want to insert a new row
         */
        public void addRow(int row) {
            species.add(new SpeciesWrapper());
            dataVector.insertElementAt(convertToVector(species.get(row).getRowData()), row);
            justifyRows(row, row+1);
            fireTableRowsInserted(row, row);
        }

        /**
         * Removes selected rows based on a given array of the locations of the rows.
         * @param rows - Array of ints that refer to the rows to be removed
         */
        public void removeSelectedRows(int[] rows) {
            for(int i = rows.length-1; i >= 0; i--) {
                dataVector.remove(rows[i]);
                species.get(rows[i]).deleteEntry();
                species.remove(rows[i]);
                fireTableRowsDeleted(rows[i], rows[i]);
            }
        }
    }
}
