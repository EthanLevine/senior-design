package uconnocalypse.dbadmin.gui.spawntab;

import uconnocalypse.data.EntitySpecies;
import uconnocalypse.data.PhysicalType;
import uconnocalypse.dbadmin.Starter;

/**
 * @author Kevin
 */
public class SpeciesWrapper {
    private EntitySpecies species;

    public SpeciesWrapper() {
        name = "NAME_DESCRIPTOR";
        physicalType = PhysicalType.MINUSCULE;
        isStatic = false;
        physicalGeometryString = "";
        species = Starter.manager.createNewEntitySpecies(name, physicalType, isStatic, physicalGeometryString);
        id = species.getId();
    }

    public SpeciesWrapper(EntitySpecies species) {
        this.species = species;
        id = species.getId();
        name = species.getName();
        physicalType = species.getPhysicalType();
        isStatic = species.isStatic();
        physicalGeometryString = "";
        maxHealth = species.getMaxHealth();
        maxEnergy = species.getMaxEnergy();
        //maxPower = species.getMaxPower();
    }

    private Long id;
    private String name;
    private Integer physicalType;
    private Boolean isStatic;
    private String physicalGeometryString;
    private Integer maxHealth = 0;
    private Integer maxEnergy = 0;
    private Integer maxPower = 0;

    public Object[] getRowData() {
        return new Object[]{ id, name, getTypeString(), isStatic, physicalGeometryString, maxHealth, maxEnergy, maxPower };
    }

    private String getTypeString() {
        String type = "";
        if(physicalType.equals(PhysicalType.NON_PHYSICAL)) {
            type = "NON_PHYSICAL";
        } else if(physicalType.equals(PhysicalType.SOLID)) {
            type = "SOLID";
        } else if(physicalType.equals(PhysicalType.HOLLOW)) {
            type = "HOLLOW";
        } else if(physicalType.equals(PhysicalType.MINUSCULE)) {
            type = "MINUSCULE";
        }
        return type;
    }

    public void setName(String name) {
        this.name = name;
        updateDBEntry();
    }
    public void setPhysicalType(Integer physicalType) {
        this.physicalType = physicalType;
        updateDBEntry();
    }
    public void setStatic(Boolean isStatic) {
        this.isStatic = isStatic;
        updateDBEntry();
    }
    public void setPhysicalGeometryString(String physicalGeometryString) {
        this.physicalGeometryString = physicalGeometryString;
        updateDBEntry();
    }
    public void setMaxHealth(Integer maxHealth) {
        this.maxHealth = maxHealth;
        updateDBEntry();
    }
    public void setMaxEnergy(Integer maxEnergy) {
        this.maxEnergy = maxEnergy;
        updateDBEntry();
    }
    public void setMaxPower(Integer maxPower) {
        this.maxPower = maxPower;
        updateDBEntry();
    }

    public Long getId() { return id; }

    public void updateDBEntry() {
        Starter.manager.updateEntitySpecies(species, name, physicalType, isStatic, physicalGeometryString, maxHealth, maxEnergy, maxPower);
    }

    public void deleteEntry() {
        Starter.manager.deleteEntitySpecies(species);
    }
}
