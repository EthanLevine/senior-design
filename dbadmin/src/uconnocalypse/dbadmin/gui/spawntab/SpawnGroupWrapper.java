package uconnocalypse.dbadmin.gui.spawntab;

import uconnocalypse.data.SpawnGroup;

/**
 * @author Kevin
 */
public class SpawnGroupWrapper {
    private SpawnGroup group;

    private Long id;

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public SpawnGroup getGroup() {
        return group;
    }

    public void setGroup(SpawnGroup group) {
        this.group = group;
    }
}
