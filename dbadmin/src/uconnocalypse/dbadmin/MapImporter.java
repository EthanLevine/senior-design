package uconnocalypse.dbadmin;

import uconnocalypse.data.MapRegionTemplate;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Kevin
 */
public class MapImporter {
    private String input =
            "Wall1[1] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 80, y: 0, z:0, w:16, h: 848});\n"
            + "Wall1[2] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 80, y: 1024, z:0, w: 16, h: 1152});\n"
            + "Wall1[3] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 0, y: 1008, z:0, w: 192, h: 16});\n"
            + "Wall1[4] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 96, y: 0, z:0, w: 1408, h: 16});\n"
            + "Wall1[5] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1504, y: 0, z:0, w:16, h: 848});\n"
            + "Wall1[6] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1504, y: 1024, z:0, w: 16, h: 1152});\n"
            + "Wall1[7] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 96, y: 2164, z:1, w: 1408, h: 12});\n"
            + "Wall1[8] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1392, y: 1008, z:0, w: 208, h: 16});\n"
            + "Wall1[9] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1392, y: 832, z:0, w: 144, h: 16});\n"
            + "Wall1[10] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 448, y: 980, z:0, w: 688, h: 8});\n"
            + "Wall1[11] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 436, y: 1108, z:0, w: 700, h: 8});\n"
            + "Wall1[12] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 96, y: 1104, z:0, w: 284, h: 16});\n"
            + "Wall1[13] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 800, y: 1120, z:0, w: 16, h: 528});\n"
            + "Wall1[14] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 448, y: 1392, z:0, w: 672, h: 16});\n"
            + "Wall1[15] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 176, y: 976, z:0, w: 16, h: 32});\n"
            + "Wall1[16] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 0, y: 976, z:0, w: 16, h: 32});\n"
            + "Wall1[17] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 336, y: 1120, z:0, w: 16, h: 268});\n"
            + "Wall1[18] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 96, y: 1456, z:0, w: 240, h: 16});\n"
            + "Wall1[19] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 96, y: 1808, z:0, w: 240, h: 16});\n"
            + "Wall1[20] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 448, y: 980, z:0, w: 16, h: 128});\n"
            + "Wall1[21] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 448, y: 1184, z:0, w: 16, h: 236});\n"
//computer lab walls
            + "Wall1[22] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 96, y: 448, z:0, w: 524, h: 16});\n"
            + "Wall1[23] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 996, y: 448, z:0, w: 524, h: 28});\n"
            + "Wall1[24] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 672, y: 128, z:0, w: 272, h: 16});\n"
            + "Wall1[25] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 688, y: 528, z:0, w: 240, h: 16});\n"
            + "Wall1[26] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 688, y: 272, z:0, w: 240, h: 16});\n"
//computer lab door walls
            + "Wall1[27] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 676, y: 452, z:0, w: 8, h: 92});\n"
            + "Wall1[28] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 932, y: 452, z:0, w: 8, h: 92});\n"
            + "Wall1[29] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 608, y: 452, z:0, w: 12, h: 92});\n"
            + "Wall1[30] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 996, y: 452, z:0, w: 8, h: 92});\n"

//systems optimization lab walls
            + "Wall1[31] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 96, y: 608, z:0, w: 224, h: 16});\n"
            + "Wall1[32] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 304, y: 560, z:0, w: 16, h: 48});\n"

            + "Wall1[33] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 448, y: 1584, z:0, w: 480, h: 16});\n"
            + "Wall1[34] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 464, y: 1824, z:0, w: 16, h: 358});\n"
            + "Wall1[35] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 976, y: 1824, z:0, w: 16, h: 358});\n"
            + "Wall1[36] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 0, y: 832, z:0, w: 16, h: 64});\n"
            + "Wall1[37] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 0, y: 832, z:0, w: 32, h: 16});\n"
            + "Wall1[38] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 96, y: 832, z:0, w: 80, h: 16});\n"
            + "Wall1[39] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 176, y: 832, z:0, w: 16, h: 64});\n"
            + "Wall1[40] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 304, y: 464, z:0, w: 16, h: 64});\n"

//more walls
            + "Wall1[41] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 432, y: 784, z:0, w: 16, h: 48});\n"
            + "Wall1[42] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 432, y: 640, z:0, w: 16, h: 96});\n"
            + "Wall1[43] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1008, y: 768, z:0, w: 16, h: 48});\n"
            + "Wall1[44] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 448, y: 1488, z:0, w: 356, h: 16});\n"
            + "Wall1[45] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 448, y: 1584, z:0, w: 16, h: 64});\n"
            + "Wall1[46] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 480, y: 1584, z:0, w: 16, h: 64});\n"
            + "Wall1[47] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 560, y: 1584, z:0, w: 16, h: 64});\n"
            + "Wall1[48] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 450, y: 1184, z:0, w: 270, h: 16});\n"
            + "Wall1[49] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 880, y: 1184, z:0, w: 240, h: 16});\n"
            + "Wall1[50] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 768, y: 1296, z:0, w:94 , h: 16});\n"
            + "Wall1[51] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 450, y: 1394, z:0, w: 672, h: 16});\n"
            + "Wall1[52] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 450, y: 1488, z:0, w: 360, h: 16});\n"
            + "Wall1[53] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 450, y: 1584, z:0, w: 500, h: 16});\n"
            + "Wall1[54] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 812, y: 1520, z:0, w: 310, h: 16});\n"

            + "Wall1[55] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 800, y: 1118, z:0, w: 16, h: 534});\n"
            + "Wall1[56] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 450, y: 1186, z:0, w: 16, h: 240});\n"
            + "Wall1[57] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1104, y: 1186, z:0, w: 16, h: 220});\n"
            + "Wall1[58] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 514, y: 1326, z:0, w: 16, h: 72});\n"
            + "Wall1[59] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 578, y: 1296, z:0, w: 16, h: 110});\n"
            + "Wall1[60] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 642, y: 1296, z:0, w: 16, h: 110});\n"
            + "Wall1[61] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 706, y: 1296, z:0, w: 16, h: 110});\n"
            + "Wall1[62] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 914, y: 1296, z:0, w: 16, h: 110});\n"
            + "Wall1[63] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 978, y: 1296, z:0, w: 16, h: 110});\n"
            + "Wall1[64] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1042, y: 1296, z:0, w: 16, h: 110});\n"
            + "Wall1[65] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1104, y: 1460, z:0, w: 16, h: 240});\n"
            + "Wall1[66] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 944, y: 1536, z:0, w: 16, h: 110});\n"
            + "Wall1[67] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1056, y: 1632, z:0, w: 64, h: 16});\n"
            + "Wall1[68] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 946, y: 1632, z:0, w: 62, h: 16});\n"
            + "Wall1[69] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 754, y: 1596, z:0, w: 16, h: 52});\n"
            + "Wall1[70] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 674, y: 1596, z:0, w: 16, h: 52});\n"
            + "Wall1[71] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 560, y: 1596, z:0, w: 16, h: 52});\n"
            + "Wall1[72] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 448, y: 1596, z:0, w: 46, h: 52});\n"
            + "Wall1[73] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 568, y: 1632, z:0, w: 110, h: 16});\n"
            + "Wall1[74] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 760, y: 1632, z:0, w: 44, h: 16});\n"
            + "Wall1[75] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 432, y: 816, z:0, w: 590, h: 16});\n"
            + "Wall1[76] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 432, y: 816, z:0, w: 590, h: 16});\n"
            + "Wall1[77] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 592, y: 656, z:0, w: 416, h: 160});\n"
            + "Wall1[78] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 448, y: 640, z:0, w: 672, h: 16});\n"
            + "Wall1[79] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1120, y: 640, z:0, w: 16, h: 176});\n"
            + "Wall1[80] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1088, y: 816, z:0, w: 48, h: 16});\n"
            + "Wall1[81] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1024, y: 768, z:0, w: 64, h: 16});\n"
            + "Wall1[82] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 800, y: 288, z:0, w: 16, h: 240});\n"
            + "Wall1[83] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 928, y: 256, z:0, w: 16, h: 140});\n"
            + "Wall1[84] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 672, y: 256, z:0, w: 16, h: 140});\n"
            + "Wall1[85] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 928, y: 144, z:0, w: 16, h: 64});\n"
            + "Wall1[86] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 672, y: 144, z:0, w: 16, h: 64});\n"
            + "Wall1[87] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1008, y: 528, z:0, w: 240, h: 16});\n"
            + "Wall1[88] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1248, y: 528, z:0, w: 16, h:48});\n"
            + "Wall1[89] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1248, y: 624, z:0, w: 16, h:48});\n"
            + "Wall1[90] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1248, y: 674, z:0, w: 256, h:16});\n"
            + "Wall1[91] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1088, y: 656, z:0, w: 32, h: 160});\n"
            + "Wall1[92] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1008, y: 640, z:0, w: 80, h: 128});\n"
            + "Wall1[93] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 90, y: 1440, z:0, w: 16, h: 48});\n"
            + "Wall1[94] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 90, y: 1440, z:0, w: 16, h: 48});\n"
            + "Wall1[95] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 336, y: 1536, z:0, w: 16, h: 288});\n"
            + "Wall1[96] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 336, y: 1440, z:0, w: 16, h: 48});\n"
            + "Wall1[97] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 448, y: 1504, z:0, w: 16, h: 16});\n"
            + "Wall1[98] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 96, y: 1904, z:0, w: 64, h: 16});\n"
            + "Wall1[99] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 160, y: 1888, z:0, w: 16, h: 16});\n"
            + "Wall1[100] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 224, y: 1888, z:0, w: 16, h: 160});\n"
            + "Wall1[101] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 224, y: 2048, z:0, w: 241, h: 16});\n"
            + "Wall1[102] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 480, y: 1824, z:0, w: 64, h: 16});\n"
            + "Wall1[103] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 896, y: 1824, z:0, w: 80, h: 16});\n"
            + "Wall1[104] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 592, y: 1824, z:0, w: 256, h: 16});\n"
            + "Wall1[105] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 992, y: 1824, z:0, w: 160, h: 16});\n"
            + "Wall1[106] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1104, y: 1776, z:0, w: 16, h: 64});\n"
            + "Wall1[107] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1264, y: 1712, z:0, w: 144, h: 16});\n"
            + "Wall1[108] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1408, y: 1712, z:0, w: 16, h: 48});\n"
            + "Wall1[109] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1408, y: 1744, z:0, w: 112, h: 16});\n"
            + "Wall1[110] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1392, y: 848, z:0, w: 16, h: 48});\n"
            + "Wall1[111] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1392, y: 976, z:0, w: 16, h: 32});\n"
            + "Wall1[112] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1120, y: 1040, z:0, w: 16, h: 64});\n"
            + "Wall1[113] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1056, y: 1040, z:0, w: 64, h: 16});\n"
            + "Wall1[114] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 464, y: 992, z:0, w: 592, h: 128});\n"
            + "Wall1[115] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1056, y: 1056, z:0, w: 64, h: 64});\n"
            + "Wall1[116] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1264, y: 1824, z:0, w: 240, h: 16});\n"
            + "Wall1[117] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1264, y: 1712, z:0, w: 16, h: 48});\n"
//computers
            + "table1[1] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 96, y: 416, z:0, w: 496, h: 32});\n"
            + "table1[2] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 96, y: 32, z:0, w: 496, h: 32});\n"
            + "table1[3] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 96, y: 160, z:0, w: 496, h: 32});\n"
            + "table1[4] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 96, y: 288, z:0, w: 496, h: 32});\n"
            + "table1[5] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 688, y: 160, z:0, w: 240, h: 32});\n"

            + "table1[6] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1024, y: 416, z:0, w: 496, h: 32});\n"
            + "table1[7] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1024, y: 32, z:0, w: 496, h: 32});\n"
            + "table1[8] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1024, y: 160, z:0, w: 496, h: 32});\n"
            + "table1[9] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1024, y: 288, z:0, w: 496, h: 32});\n"
//tables
            + "table1[10] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 148, y: 1120, z:0, w: 22, h: 160});\n"
            + "table1[11] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 222, y: 1120, z:0, w: 22, h: 160});\n"
            + "table1[12] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 148, y: 1344, z:0, w: 22, h: 288});\n"
            + "table1[13] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 222, y: 1344, z:0, w: 22, h: 288});\n"
            + "table1[14] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1024, y: 416, z:0, w: 496, h: 32});\n"
//back entrance tables
            + "table1[15] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 181, y: 715, z:0, w: 40, h: 40});\n"
            + "table1[16] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 277, y: 715, z:0, w: 40, h: 40});\n"
//systems op computers
            + "table1[17] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 100, y: 560, z:0, w: 200, h: 40});\n"
            + "table1[18] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 100, y: 480, z:0, w: 200, h: 40});\n"
            + "table1[19] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 736, y: 352, z:0, w: 64, h: 112});\n"
            + "table1[20] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 480, y: 1936, z:0, w: 208, h: 16});\n"
            + "table1[21] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 480, y: 2016, z:0, w: 208, h: 16});\n"
            + "table1[22] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 480, y: 1936, z:0, w: 208, h: 16});\n"
            + "table1[23] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 480, y: 2096, z:0, w: 208, h: 16});\n"
            + "table1[24] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 768, y: 1936, z:0, w: 208, h: 16});\n"
            + "table1[25] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 768, y: 2016, z:0, w: 208, h: 16});\n"
            + "table1[26] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 768, y: 1936, z:0, w: 208, h: 16});\n"
            + "table1[27] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 768, y: 2096, z:0, w: 208, h: 16});\n"
            + "table1[28] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1088, y: 2000, z:0, w: 96, h: 48});\n"
            + "table1[29] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 1296, y: 2000, z:0, w: 96, h: 48});\n"
            + "table1[30] = Crafty.e(\"2D, Color, DOM, solid\" )   .attr({x: 864, y: 1440, z:0, w: 96, h: 32});\n";

    public MapImporter() {
        // RegEx: .*\{x:\s*(\d+).*?(\d+).*?\d+.*?(\d+).*?(\d+).*
        Pattern regex = Pattern.compile(".*\\{x:\\s*(\\d+).*?(\\d+).*?\\d+.*?(\\d+).*?(\\d+).*");
        String[] lines = input.split("\n");
        MapRegionTemplate mapRegion = Starter.manager.getMapRegionTemplates().get(0);
        for(String line : lines) {
            Matcher matcher = regex.matcher(line);
            if(matcher.find()) {
                String geometry = generateGeometryString(Integer.parseInt(matcher.group(1)),
                        Integer.parseInt(matcher.group(2)),
                        Integer.parseInt(matcher.group(3)),
                        Integer.parseInt(matcher.group(4)));
                Starter.manager.createNewBarrier(mapRegion, geometry);
            }
        }
        System.out.println("Map Import is complete.");
    }

    private String generateGeometryString(int x, int y, int width, int height) {
        String geometry = "";
        geometry += x+","+y+";";
        geometry += +(x+width)+","+y+";";
        geometry += +x+","+(y+height)+";";
        geometry += +(x+width)+","+(y+height)+";";
        return geometry;
    }
}
