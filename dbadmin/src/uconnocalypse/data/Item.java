package uconnocalypse.data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="items")
public class Item implements Serializable {
    
    @Column(name="id")
    @Id
    @GeneratedValue
    private Long id;
    
    @Column(name="name", nullable=false)
    protected String name;
    
    @Column(name="item_quality", nullable=false)
    protected Integer itemQuality;
    
    @ManyToOne
    @JoinColumn(name="item_template_id", nullable=false)
    protected ItemTemplate itemTemplate;
    
    @ManyToOne
    @JoinColumn(name="attribute_set_id", nullable=false)
    protected AttributeSet attributes;
    
    public Long getId() { return id; }
    public String getName() { return name; }
    public Integer getItemQuality() { return itemQuality; }
    public ItemTemplate getItemTemplate() { return itemTemplate; }
    public AttributeSet getAttributes() { return attributes; }
}
