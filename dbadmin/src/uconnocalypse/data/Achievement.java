package uconnocalypse.data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="achievements")
public class Achievement implements Serializable {
    
    @Column(name="id")
    @Id
    @GeneratedValue
    private Long id;
    
    @Column(name="name", nullable=false)
    protected String name;
    
    public Long getId() { return id; }
    public String getName() { return name; }
}
