package uconnocalypse.data;

import org.hibernate.Hibernate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class DatabaseManager {
    private final EntityManagerFactory emf;
    
    public DatabaseManager(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public List<EntitySpecies> getEntitySpecies() {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            return em.createQuery("FROM EntitySpecies es", EntitySpecies.class).getResultList();
        } finally {
            if (em != null) em.close();
        }
    }

    public List<ItemTemplate> getItemTemplates() {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            return em.createQuery("FROM ItemTemplate it", ItemTemplate.class).getResultList();
        } finally {
            if (em != null) em.close();
        }
    }

    /**
     * This method will obtain all the MapRegionTemplates in the database. This is a lazy fetch
     * and the returned objects will not be initialized. If you want a complete MapRegionTemplate you must
     * use the map regions ID and the method #getMapRegionTemplate for the initialized object.
     * @return - List of MapRegionTemplates that are available in the database.
     */
    public List<MapRegionTemplate> getMapRegionTemplates() {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            return em.createQuery("FROM MapRegionTemplate mrt", MapRegionTemplate.class).getResultList();
        } finally {
            if (em != null) em.close();
        }
    }

    /**
     * This method will get and initialize a MapRegion, including fetching and populating the references for it's sets.
     * @param id - The id of the MapRegionTemplate that we are finding.
     * @return - An initialized MapRegionTemplate object.
     */
    public MapRegionTemplate getMapRegionTemplate(Long id) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            MapRegionTemplate mapRegion = em.find(MapRegionTemplate.class, id);
            Hibernate.initialize(mapRegion.getBarriers());
            Hibernate.initialize(mapRegion.getFixedSpawns());
            return mapRegion;
        } finally {
            if (em != null) em.close();
        }
    }

    public List<FixedSpawn> getFixedSpawns(MapRegionTemplate mapRegion) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            return em.createQuery("FROM FixedSpawn fs", FixedSpawn.class).getResultList();
        } finally {
            if (em != null) em.close();
        }
    }

    public List<Barrier> getBarriers(MapRegionTemplate mapRegion) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            return em.createQuery("FROM Barrier b", Barrier.class).getResultList();
        } finally {
            if (em != null) em.close();
        }
    }

    protected AttributeSet mergeAttributeSet(AttributeSet newSet) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            // Search for the exact attribute set
            List<AttributeSet> discoveredSets =
                    em.createQuery("FROM AttributeSet a WHERE a.strength = ?1 AND a.dexterity = ?2 AND a.fortitude = ?3", AttributeSet.class)
                            .setParameter(1, newSet.strength)
                            .setParameter(2, newSet.dexterity)
                            .setParameter(3, newSet.fortitude)
                            .setMaxResults(1).getResultList();
            if (discoveredSets.isEmpty()) {
                EntityTransaction et = em.getTransaction();
                et.begin();
                em.persist(newSet);
                et.commit();
                return newSet;
            } else {
                return discoveredSets.get(0);
            }
        } finally {
            if (em != null) em.close();
        }
    }

    public ItemTemplate createNewItemTemplate() {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            EntityTransaction et = em.getTransaction();
            ItemTemplate itemTemplate = new ItemTemplate();
            itemTemplate.name = "NAME_DESCRIPTOR";
            itemTemplate.itemLevel = 1;
            itemTemplate.equipSlot = 0;
            itemTemplate.reqLevel = 1;
            itemTemplate.baseItemQuality = 0;
            AttributeSet attributeSet = new AttributeSet();
            attributeSet.strength = 0;
            attributeSet.dexterity = 0;
            attributeSet.fortitude = 0;
            itemTemplate.attributeSet = mergeAttributeSet(attributeSet);
            itemTemplate.maximumMods = 0;
            itemTemplate.minimumMods = 0;
            et.begin();
            em.persist(itemTemplate);
            et.commit();
            return itemTemplate;
        } finally {
            if (em != null) em.close();
        }
    }

    public void updateItemTemplate(ItemTemplate itemTemplate, String name, Integer equipSlot, Integer itemLevel,
                                   Integer reqLevel, Integer baseItemQuality, Integer strength, Integer dexterity,
                                   Integer fortitude, Integer minimumMods, Integer maximumMods, Double attackRate,
                                   Integer baseDamage, Double weaponSize, Integer armor) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            EntityTransaction et = em.getTransaction();
            itemTemplate.name = name;
            itemTemplate.itemLevel = itemLevel;
            itemTemplate.equipSlot = equipSlot;
            itemTemplate.reqLevel = reqLevel;
            itemTemplate.baseItemQuality = baseItemQuality;
            AttributeSet attributeSet = new AttributeSet();
            attributeSet.strength = strength;
            attributeSet.dexterity = dexterity;
            attributeSet.fortitude = fortitude;
            itemTemplate.attributeSet = mergeAttributeSet(attributeSet);
            itemTemplate.maximumMods = maximumMods;
            itemTemplate.minimumMods = minimumMods;
            itemTemplate.weaponAttackRate = attackRate;
            itemTemplate.weaponBaseDamage = baseDamage;
            itemTemplate.weaponMeleeRange = weaponSize;
            itemTemplate.armor = armor;
            et.begin();
            em.merge(itemTemplate);
            et.commit();
        } finally {
            if (em != null) em.close();
        }
    }

    public void deleteItemTemplate(ItemTemplate itemTemplate) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            EntityTransaction et = em.getTransaction();
            et.begin();
            itemTemplate = em.find(ItemTemplate.class, itemTemplate.getId());
            em.remove(itemTemplate);
            et.commit();
        } finally {
            if (em != null) em.close();
        }
    }

    public EntitySpecies createNewEntitySpecies(String name, Integer type, Boolean isStatic, String geometry) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            EntityTransaction et = em.getTransaction();
            EntitySpecies species = new EntitySpecies();
            species.name = name;
            species.physicalType = type;
            species.isStatic = isStatic;
            species.physicalGeometryString = geometry;
            species.maxHealth = 0;
            species.maxEnergy = 0;
            //species.maxPower = 0;
            et.begin();
            em.persist(species);
            et.commit();
            return species;
        } finally {
            if (em != null) em.close();
        }
    }

    public void updateEntitySpecies(EntitySpecies species, String name, Integer type, Boolean isStatic, String geometry, Integer maxHealth, Integer maxEnergy, Integer maxPower) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            EntityTransaction et = em.getTransaction();
            species.name = name;
            species.physicalType = type;
            species.isStatic = isStatic;
            species.physicalGeometryString = geometry;
            species.maxHealth = maxHealth;
            species.maxEnergy = maxEnergy;
            //species.maxPower = maxPower;
            et.begin();
            em.merge(species);
            et.commit();
        } finally {
            if (em != null) em.close();
        }
    }

    public void deleteEntitySpecies(EntitySpecies species) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            EntityTransaction et = em.getTransaction();
            et.begin();
            species = em.find(EntitySpecies.class, species.getId());
            em.remove(species);
            et.commit();
        } finally {
            if (em != null) em.close();
        }
    }

    public MapRegionTemplate createNewMapRegionTemplate(String name, Integer width, Integer height) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            EntityTransaction et = em.getTransaction();
            MapRegionTemplate mapRegion = new MapRegionTemplate();
            mapRegion.name = name;
            mapRegion.width = width;
            mapRegion.height = height;
            mapRegion.fixedSpawns = new HashSet<>();
            mapRegion.barriers = new HashSet<>();
            et.begin();
            em.persist(mapRegion);
            et.commit();
            return mapRegion;
        } finally {
            if (em != null) em.close();
        }
    }

    public void updateMapRegionTemplate(MapRegionTemplate mapRegion, String name, Set<FixedSpawn> spawns, Set<Barrier> barriers) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            EntityTransaction et = em.getTransaction();
            mapRegion.name = name;
            mapRegion.fixedSpawns = spawns;
            mapRegion.barriers = barriers;
            et.begin();
            em.merge(mapRegion);
            et.commit();
        } finally {
            if (em != null) em.close();
        }
    }

    public Barrier createNewBarrier(MapRegionTemplate mapRegion, String geometry) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            EntityTransaction et = em.getTransaction();
            Barrier barrier = new Barrier();
            barrier.geometryString = geometry;
            barrier.mapRegionTemplate = mapRegion;
            et.begin();
            em.persist(barrier);
            et.commit();
            return barrier;
        } finally {
            if (em != null) em.close();
        }
    }

    public void updateBarrier(Barrier barrier, String geometry) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            EntityTransaction et = em.getTransaction();
            barrier.geometryString = geometry;
            et.begin();
            em.merge(barrier);
            et.commit();
        } finally {
            if (em != null) em.close();
        }
    }

    public void deleteBarrier(Barrier barrier) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            EntityTransaction et = em.getTransaction();
            et.begin();
            barrier = em.find(Barrier.class, barrier.getId());
            em.remove(barrier);
            et.commit();
        } finally {
            if (em != null) em.close();
        }
    }

    public FixedSpawn createNewFixedSpawn(MapRegionTemplate mapRegion, EntitySpecies species, Integer x, Integer y, Double angle) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            EntityTransaction et = em.getTransaction();
            FixedSpawn spawn = new FixedSpawn();
            spawn.mapRegionTemplate = mapRegion;
            spawn.species = species;
            spawn.x = x;
            spawn.y = y;
            spawn.angle = angle;
            et.begin();
            em.persist(spawn);
            et.commit();
            return spawn;
        } finally {
            if (em != null) em.close();
        }
    }

    public void updateFixedSpawn(FixedSpawn spawn, EntitySpecies species, Integer x, Integer y) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            EntityTransaction et = em.getTransaction();
            spawn.species = species;
            spawn.x = x;
            spawn.y = y;
            et.begin();
            em.merge(spawn);
            et.commit();
        } finally {
            if (em != null) em.close();
        }
    }

    public void deleteFixedSpawn(FixedSpawn spawn) {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            EntityTransaction et = em.getTransaction();
            et.begin();
            spawn = em.find(FixedSpawn.class, spawn.getId());
            em.remove(spawn);
            et.commit();
        } finally {
            if (em != null) em.close();
        }
    }
}
