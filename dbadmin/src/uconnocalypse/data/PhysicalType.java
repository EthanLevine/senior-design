package uconnocalypse.data;

public final class PhysicalType {
    private PhysicalType() {}
    
    public static final int NON_PHYSICAL = 0;
    public static final int SOLID = 1;
    public static final int HOLLOW = 2;
    public static final int MINUSCULE = 4;
    public static final int PLAYER = 8;
    public static final int MONSTER = 16;
    
    public static boolean isNonPhysical(int type) {
        return type == NON_PHYSICAL;
    }
    
    public static boolean isSolid(int type) {
        return (type & SOLID) > 0;
    }
    
    public static boolean isHollow(int type) {
        return (type & HOLLOW) > 0;
    }
    
    public static boolean isMinuscule(int type) {
        return (type & MINUSCULE) > 0;
    }
    
    public static boolean isPlayer(int type) {
        return (type & PLAYER) > 0;
    }
    
    public static boolean isMonster(int type) {
        return (type & MONSTER) > 0;
    }
}
