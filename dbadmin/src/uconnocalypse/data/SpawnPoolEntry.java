package uconnocalypse.data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="spawn_pool_entries")
public class SpawnPoolEntry implements Serializable {
    
    @Column(name="id")
    @Id
    @GeneratedValue
    private Long id;
    
    @ManyToOne
    @JoinColumn(name="spawn_pool_id", nullable=false)
    private SpawnPool spawnPool;
    
    @ManyToOne
    @JoinColumn(name="spawn_group_id", nullable=false)
    private SpawnGroup spawnGroup;
    
    @Column(name="spawn_rate", nullable=false)
    private Double spawnRate;
    
    @Column(name="spawn_minimum", nullable=false)
    private Integer spawnMinimum;
    
    @Column(name="spawn_maximum", nullable=false)
    private Integer spawnMaximum;
    
    public Long getId() { return id; }
    public SpawnPool getSpawnPool() { return spawnPool; }
    public SpawnGroup getSpawnGroup() { return spawnGroup; }
    public Double getSpawnRate() { return spawnRate; }
    public Integer getSpawnMinimum() { return spawnMinimum; }
    public Integer getSpawnMaximum() { return spawnMaximum; }
}
