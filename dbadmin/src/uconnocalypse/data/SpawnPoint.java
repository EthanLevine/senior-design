package uconnocalypse.data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="spawn_points")
public class SpawnPoint implements Serializable {
    
    @Column(name="id")
    @Id
    @GeneratedValue
    private Long id;
    
    @Column(name="x", nullable=false)
    private Double x;
    
    @Column(name="y", nullable=false)
    private Double y;
    
    @ManyToOne
    @JoinColumn(name="spawn_pool_id", nullable=false)
    private SpawnPool spawnPool;
    
    @ManyToOne
    @JoinColumn(name="map_region_template_id", nullable=false)
    private MapRegionTemplate mapRegionTemplate;
    
    public Long getId() { return id; }
    public Double getX() { return x; }
    public Double getY() { return y; }
    public SpawnPool getSpawnPool() { return spawnPool; }
    public MapRegionTemplate getMapRegionTemplate() { return mapRegionTemplate; }
}
