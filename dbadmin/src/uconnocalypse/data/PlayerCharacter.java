package uconnocalypse.data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;

@Entity
@Table(name="characters")
public class PlayerCharacter implements Serializable {
    
    @Column(name="id")
    @Id
    @GeneratedValue
    private Long id;
    
    @ManyToOne
    @JoinColumn(name="account_id")
    protected Account account;
    
    @Column(name="name", unique=true, nullable=false)
    protected String name;
    
    @Column(name="character_type", nullable=false)
    protected Integer charType;
    
    @Column(name="creation_time", nullable=false)
    @Temporal(TemporalType.TIMESTAMP)
    protected Calendar creationTime;
    
    @Column(name="exp_level", nullable=false)
    protected Integer level;
    
    @Column(name="exp_total", nullable=false)
    protected Long expTotal;

    @ManyToOne
    @JoinColumn(name="stats_id")
    protected AttributeSet stats;

    @ManyToOne
    @JoinColumn(name="inventory_id")
    protected Stash inventory;
    
    @ManyToOne
    @JoinColumn(name="equipped_weapon_id", nullable=true)
    protected Item equippedWeapon;
    
    @ManyToOne
    @JoinColumn(name="equipped_armor_id", nullable=true)
    protected Item equippedArmor;
    
    public Long getId() { return id; }
    public Account getAccount() { return account; }
    public String getName() { return name; }
    public Integer getCharType() { return charType; }
    public Calendar getCreationTime() { return creationTime; }
    public Integer getLevel() { return level; }
    public Long getExpTotal() { return expTotal; }
    public Stash getInventory() { return inventory; }
    public Item getEquippedWeapon() { return equippedWeapon; }
    public Item getEquippedArmor() { return equippedArmor; }

    // Variables not tracked by the DB
    @Transient
    private boolean inventoryOpen = false;

    @Transient
    private boolean attacking = false;

    @Transient
    private long lastAttack = 0;

    /**
     * Checking that we are qualified to use this item should be done beforehand.
     * @param stashedItem - The item in our inventory that we want to equip
     */

}
