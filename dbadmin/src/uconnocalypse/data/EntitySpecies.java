package uconnocalypse.data;

import uconnocalypse.engine.PhysicalGeometry;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="entity_species")
public class EntitySpecies implements Serializable {
    
    @Column(name="id")
    @Id
    @GeneratedValue
    private Long id;
    
    @Column(name="name", nullable=false)
    protected String name;
    
    @Column(name="physical_type", nullable=false)
    protected Integer physicalType;
    
    @Column(name="is_static", nullable=false)
    protected Boolean isStatic;
    
    @Column(name="physical_geometry", nullable=true)
    protected String physicalGeometryString;

    @Column(name="max_health", nullable=false)
    protected Integer maxHealth;

    @Column(name="max_energy", nullable=false)
    protected Integer maxEnergy;
    
    public Long getId() { return id; }
    public String getName() { return name; }
    public Integer getPhysicalType() { return physicalType; }
    public Boolean isStatic() { return isStatic; }
    public Integer getMaxHealth() { return maxHealth; }
    public Integer getMaxEnergy() { return maxEnergy; }
    
    @Transient
    private PhysicalGeometry physicalGeometry = null;
    public PhysicalGeometry getGeometry() {
        if (physicalGeometry == null) {
            physicalGeometry = new PhysicalGeometry(physicalGeometryString);
        }
        return physicalGeometry;
    }
    
    public static final EntitySpecies PLAYER_SPECIES;
    static {
        PLAYER_SPECIES = new EntitySpecies();
        PLAYER_SPECIES.id = -2L;
        PLAYER_SPECIES.isStatic = false;
        PLAYER_SPECIES.maxEnergy = 100;
        PLAYER_SPECIES.maxHealth = 100;
        PLAYER_SPECIES.name = "PLAYER";
        PLAYER_SPECIES.physicalType = PhysicalType.SOLID + PhysicalType.PLAYER;
        PLAYER_SPECIES.physicalGeometryString = "0,0;10";
    }
}
