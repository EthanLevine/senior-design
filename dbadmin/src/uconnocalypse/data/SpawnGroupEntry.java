package uconnocalypse.data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="spawn_group_entries")
public class SpawnGroupEntry implements Serializable {
    
    @Column(name="id")
    @Id
    @GeneratedValue
    private Long id;
    
    @ManyToOne
    @JoinColumn(name="spawn_group_id", nullable=false)
    private SpawnGroup spawnGroup;
    
    @ManyToOne
    @JoinColumn(name="entity_species_id", nullable=false)
    private EntitySpecies entitySpecies;
    
    @Column(name="multiplicity", nullable=false)
    private Integer multiplicity;
    
    public Long getId() { return id; }
    public SpawnGroup getSpawnGroup() { return spawnGroup; }
    public EntitySpecies getEntitySpecies() { return entitySpecies; }
    public Integer getMultiplicity() { return multiplicity; }
}
