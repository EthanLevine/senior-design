package uconnocalypse.data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="dropped_items")
public class DroppedItem implements Serializable {
    
    @Column(name="item_template_id") 
    @Id
    private Long itemTemplateId;
    
    @OneToOne
    @PrimaryKeyJoinColumn
    protected ItemTemplate itemTemplate;
    
    @Column(name="drop_rate", nullable=false)
    protected Double dropRate;
    
    @ManyToOne
    @JoinColumn(name="drop_pool_id", nullable=false)
    protected DropPool dropPool;
    
    public ItemTemplate getItemTemplate() { return itemTemplate; }
    public Double getDropRate() { return dropRate; }
    public DropPool getDropPool() { return dropPool; }
}
