package uconnocalypse.data;

public class ItemQuality {
    private ItemQuality() {}
    
    public final static int TRASH = 0;
    public final static int COMMON = 1;
    public final static int FINE = 2;
    public final static int RARE = 3;
    public final static int EXOTIC = 4;
    public final static int LEGENDARY = 5;
}
