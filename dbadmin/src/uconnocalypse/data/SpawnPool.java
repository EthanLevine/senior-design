package uconnocalypse.data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name="spawn_pools")
public class SpawnPool implements Serializable {
    
    @Column(name="id")
    @Id
    @GeneratedValue
    private Long id;
    
    @OneToMany(mappedBy="spawnPool")
    private Set<SpawnPoolEntry> entries;
    
    public Long getId() { return id; }
    public Set<SpawnPoolEntry> getEntries() { return entries; }
}
