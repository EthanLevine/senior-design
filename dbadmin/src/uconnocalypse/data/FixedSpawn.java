package uconnocalypse.data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="fixed_spawns")
public class FixedSpawn implements Serializable {
    
    @Column(name="id")
    @Id
    @GeneratedValue
    private Long id;
    
    @ManyToOne
    @JoinColumn(name="entity_species_id", nullable=false)
    protected EntitySpecies species;
    
    @ManyToOne
    @JoinColumn(name="map_region_template_id", nullable=false)
    protected MapRegionTemplate mapRegionTemplate;
    
    @Column(name="x", nullable=false)
    protected Integer x;
    
    @Column(name="y", nullable=false)
    protected Integer y;
    
    @Column(name="angle", nullable=false)
    protected Double angle;
    
    public Long getId() { return id; }
    public EntitySpecies getSpecies() { return species; }
    public MapRegionTemplate getMapRegionTemplate() { return mapRegionTemplate; }
    public Integer getX() { return x; }
    public Integer getY() { return y; }
    public Double getAngle() { return angle; }
}
