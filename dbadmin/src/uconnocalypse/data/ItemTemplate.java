package uconnocalypse.data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="item_templates")
public class ItemTemplate implements Serializable {
    
    @Column(name="id")
    @Id
    @GeneratedValue
    private Long id;
    
    @Column(name="name", nullable=false)
    protected String name;
    
    @Column(name="item_level", nullable=false)
    protected Integer itemLevel;
    
    @Column(name="equip_slot", nullable=false)
    protected Integer equipSlot;
    
    @Column(name="req_level", nullable=false)
    protected Integer reqLevel;
    
    @Column(name="base_item_quality", nullable=false)
    protected Integer baseItemQuality;
    
    @ManyToOne
    @JoinColumn(name="attribute_set_id", nullable=false)
    protected AttributeSet attributeSet;
    
    @Column(name="max_mods", nullable=false)
    protected Integer maximumMods;
    
    @Column(name="min_mods", nullable=false)
    protected Integer minimumMods;
    
    @Column(name="weapon_attack_rate", nullable=true)
    protected Double weaponAttackRate;
    
    @Column(name="weapon_base_damage", nullable=true)
    protected Integer weaponBaseDamage;
    
    @Column(name="weapon_spread", nullable=true)
    protected Double weaponSpread;
    
    @Column(name="weapon_projectile_count", nullable=true)
    protected Integer weaponProjectileCount;
    
    @ManyToOne
    @JoinColumn(name="weapon_projectile_species", nullable=true)
    protected EntitySpecies weaponProjectileSpecies;
    
    @Column(name="weapon_projectile_speed", nullable=true)
    protected Double weaponProjectileSpeed;
    
    @Column(name="weapon_melee_range", nullable=true)
    protected Double weaponMeleeRange;
    
    @Column(name="armor", nullable=true)
    protected Integer armor;
    
    public Long getId() { return id; }
    public String getName() { return name; }
    public Integer getItemLevel() { return itemLevel; }
    public Integer getEquipSlot() { return equipSlot; }
    public Integer getReqLevel() { return reqLevel; }
    public Integer getBaseItemQuality() { return baseItemQuality; }
    public AttributeSet getAttributeSet() { return attributeSet; }
    public Integer getMaximumMods() { return maximumMods; }
    public Integer getMinimumMods() { return minimumMods; }
    
    public Double getWeaponAttackRate() { return weaponAttackRate; }
    public Integer getWeaponBaseDamage() { return weaponBaseDamage; }
    public Double getWeaponSpread() { return weaponSpread; }
    public EntitySpecies getWeaponProjectileSpecies() { return weaponProjectileSpecies; }
    public Integer getWeaponProjectileCount() { return weaponProjectileCount; }
    public Double getWeaponProjectileSpeed() { return weaponProjectileSpeed; }
    public Double getWeaponMeleeRange() { return weaponMeleeRange; }
    
    public Integer getArmor() { return armor; }
}
