package uconnocalypse.data;

import uconnocalypse.engine.Point;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name="stashes")
public class Stash implements Serializable {
    
    @Column(name="id")
    @Id
    @GeneratedValue
    private Long id;
    
    @Column(name="height", nullable=false)
    protected Integer height;
    
    @Column(name="width", nullable=false)
    protected Integer width;
    
    @OneToMany(mappedBy="stash")
    protected Set<StashedItem> stashedItems;
    
    public Long getId() { return id; }
    public Integer getHeight() { return height; }
    public Integer getWidth() { return width; }
    public Set<StashedItem> getStashedItems() { return stashedItems; }

    public Point getNextFreeSlot() {
        if(isStashFull()) return null;
        int[][] invMap = new int[width][height];
        for(StashedItem stashedItem : stashedItems) {
            invMap[stashedItem.getSlotX()][stashedItem.getSlotY()] = 1;
        }
        for(int i = 0; i < invMap.length; i++) {
            for(int j = 0; j < invMap[i].length; j++) {
                if (invMap[i][j] != 1) return new Point(i, j);
            }
        }
        return null;
    }

    public boolean isStashFull() {
        int maxSpace = width * height;
        return stashedItems.size() >= maxSpace;
    }

    /*protected void addItem(Item item) {
        Point p = getNextFreeSlot();
        StashedItem stashedItem = GameServer.databaseManager.createStashedItem(item);
        stashedItem.slotX = (int)p.x;
        stashedItem.slotY = (int)p.y;
        stashedItem.stash = this;
        stashedItems.add(stashedItem);
        GameServer.databaseManager.saveStash(this);
    }

    protected void removeItem(StashedItem stashedItem) {
        stashedItems.remove(stashedItem);
        GameServer.databaseManager.deleteStashedItem(stashedItem);
        GameServer.databaseManager.saveStash(this);
    }*/
}
