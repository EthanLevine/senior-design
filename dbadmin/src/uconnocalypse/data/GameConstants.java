package uconnocalypse.data;

/**
 * GameConstants.java
 */
public class GameConstants {
    public static final int ACCOUNT_STASH_HEIGHT = 6;
    public static final int ACCOUNT_STASH_WIDTH = 10;

    public static final int INVENTORY_HEIGHT = 8;
    public static final int INVENTORY_WIDTH = 4;

    public static final int NON_EQUIP = 0;
    public static final int EQUIPMENT_SLOTS = 6;
    public static final int EQUIPMENT_SLOT_HELM = 1;
    public static final int EQUIPMENT_SLOT_BODY = 2;
    public static final int EQUIPMENT_SLOT_LEGS = 3;
    public static final int EQUIPMENT_SLOT_BOOTS = 4;
    public static final int EQUIPMENT_SLOT_GLOVES = 5;
    public static final int EQUIPMENT_SLOT_WEAPON = 6;

    public static final int EQUIPMENT_MELEE = 11;
    public static final int EQUIPMENT_PISTOL = 12; // pistol, sniper, minigun are all based off of pistol
    public static final int EQUIPMENT_SHOTGUN = 13; // shotgun could have spray?

    public static final int UNIVERSAL_ITEM_CONST = 10000;

    public static final int MAX_PICKUP_DIST = 100;

    public static final int ATTACK_TIME = 1000; // attack time in ms
}
