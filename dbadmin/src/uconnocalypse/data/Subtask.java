package uconnocalypse.data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="subtasks")
public class Subtask implements Serializable {
    
    @Column(name="id")
    @Id
    @GeneratedValue
    private Long id;
    
    @Column(name="name", nullable=false)
    protected String name;
    
    @ManyToOne
    @JoinColumn(name="task_id", nullable=false)
    private Task task;
}
