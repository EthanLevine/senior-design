package uconnocalypse.data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name="spawn_groups")
public class SpawnGroup implements Serializable {
    
    @Column(name="id")
    @Id
    @GeneratedValue
    private Long id;
    
    @OneToMany(mappedBy="spawnGroup")
    private Set<SpawnGroupEntry> entries;
    
    public Long getId() { return id; }
    public Set<SpawnGroupEntry> getEntries() { return entries; }
}
