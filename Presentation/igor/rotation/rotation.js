/*
Kerboard Commands

Move: WASD
Collision: C
Rotation: R
Image Sprite: I
PlayerBox: P
PlayerBoxOutline: O



*/


Crafty.init(800	, 500);
Crafty.background("gray");
var imageon=true;
var rotationon = false;
var collisionon = false;
var playerboxon = false;

var playerbox;

//wall corner

var wall1= Crafty.e("2D, DOM, Color, solid")
.attr({x:50,y:250,w:50,h:200})
.color("blue");

var wall2= Crafty.e("2D, DOM, Color, solid")
.attr({x:100,y:400,w:200,h:50})
.color("blue");



//player
var player = Crafty.e("2D, DOM, Color, Image, Multiway, Collision")
.attr({x:150,y:50,w:150,h:150,z:2})
.color("yellow")
.image("sprite.png", "no-repeat")
.origin("center")
.multiway(9, {W: -90, S: 90, D: 0, A: 180})
.bind('Moved', function(from) 
    {
	    if(collisionon){
		   if(playerboxon){
			   if(playerbox.hit('solid'))
			   {
				   this.attr({x: from.x, y:from.y});

				}
			}
			else {
				if(this.hit('solid'))
			   {
				   this.attr({x: from.x, y:from.y});

				}
			
			}
			
		}
	})
;



 //mouse based rotation
Crafty.addEvent(this, "mousemove", function(e) {
		
		player._centerx=player._x+(player._w/2);
		player._centery=player._y+(player._h/2);
		
		
		var pos = Crafty.DOM.translate(e.clientX, e.clientY);
        var theta = Math.atan2(pos.y - ( player._centery), pos.x - (player._centerx));
        if (theta < 0){
           theta += 2 * Math.PI;
        }
        player.angle2=(theta * 180 / Math.PI)
        var angle = (theta * 180 / Math.PI) + 90
        if (angle>360)
        {
                angle=angle-360;
        }
        if(rotationon){
			player.rotation=angle;
		}

});

Crafty.addEvent(this, "keydown", function(e){
		
		//turns sprite image on/off
        if(e.key === Crafty.keys['I']) {
			player.toggleComponent("Image");
		}
		
		//toggle rotation
		else if(e.key === Crafty.keys['R']) {
			if(!rotationon)
				rotationon=true;
			else
				rotationon=false;
		}
		
		//toggle collision
		else if(e.key === Crafty.keys['C']) {
			if(!collisionon)
				collisionon=true;
			else
				collisionon=false;
		}
		
		else if(e.key ===Crafty.keys['P'])  {
			if(!playerboxon){
				playerboxon=true;
				playerbox= Crafty.e("2D, DOM, Color, Collision")
				.attr({x:player._x,y:player._y,w:150,h:150,z:1})
				.color("red")
				.origin("center");
				player.attach(playerbox);
				
			}
			
			else{
				playerboxon=false;
				playerbox.destroy();
			}
		
		}
		else if(e.key === Crafty.keys['O']) {
			playerbox.toggleComponent("Color");
		}
		
	});
